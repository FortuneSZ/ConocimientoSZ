-- 1. Crea un procediment que mostre la suma dels primers n nombres enters, sent n un paràmetre d'entrada.

CREATE OR REPLACE PROCEDURE nNums(n IN NUMBER) 
IS 
numero NUMBER(5) := 0;
BEGIN
    FOR i IN 1 .. n LOOP
        numero := numero + i;
    END LOOP;
    dbms_output.put_line(numero);
END nNums;

DECLARE
numeros NUMBER(3) := 5;
BEGIN
nNums(numeros);
END;


-- 2. Crea una funció que retorne el factorial d'un nombre enter que es passa com a paràmetre.

CREATE OR REPLACE FUNCTION factorial(numero IN NUMBER) RETURN NUMBER
IS
facto NUMBER(5) := numero;
i NUMBER(5) := numero-1;
BEGIN
    WHILE i >= 1 LOOP
        facto := facto * i;
        i := i - 1;
    END LOOP;
    RETURN facto;
END factorial;

DECLARE
numero NUMBER(5) := 5;
facti NUMBER(5) := 0;
BEGIN
facti := factorial(numero);
dbms_output.put_line(facti);
END;

-- 3. Crea una funció que accepte un text i diga si és palíndrom sense utilitzar la funció REVERSE.
-- Per a provar la funció, creeu un bloc anònim que mitjançant condicionals servei un missatge per 
-- consola de si és vertader o fals.

CREATE OR REPLACE FUNCTION palindromo (texto IN VARCHAR2) RETURN BOOLEAN
IS
tam NUMBER(3);
BEGIN
    SELECT LENGTH(texto) INTO tam FROM DUAL;
    LOOP EXIT WHEN tam=0;
        EXIT WHEN not(substr(texto,tam,1) = substr(texto,((length(texto)+1)-tam),1));
        tam:=tam-1;
    END LOOP;
    IF tam=0 
    THEN
        RETURN TRUE;
    ELSE 
        RETURN FALSE;
    END IF;
END palindromo;

DECLARE
pal BOOLEAN;
palabra VARCHAR2(50) := 'aia';
BEGIN
pal := palindromo(palabra);
if pal = TRUE
THEN
    dbms_output.put_line('Es palíndromo');
ELSE
    dbms_output.put_line('No es palíndromo');
END IF;
END;

-- 4. Crea una funció que determine si un número és nombre primer. Utilitzeu un bloc anònim per a provar
-- com en el exercisi anterior.

CREATE OR REPLACE FUNCTION primer(numero IN NUMBER) RETURN BOOLEAN
IS
BEGIN
IF numero = 1
THEN
    RETURN TRUE;
ELSE
    RETURN FALSE;
END IF;
END primer;

DECLARE
verifi BOOLEAN;
BEGIN
    verifi := primer(5);   
    IF verifi = TRUE
    THEN
        dbms_output.put_line('Si es el primero');
    ELSE
        dbms_output.put_line('No es el primero');
    END IF;
END;


