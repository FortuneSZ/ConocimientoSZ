
-- 1. Crea un procediment per a mostrar l'any actual i el mes actual en una sola cadena 
-- d'eixida amb el format aaaa-mm, sent l'any estrictament en 4 xifres i el mes en 2. 
-- Recordeu que es pot invocar a les funcions predefinides de oracle que siguen necessàries 
-- usant la taula dual.

CREATE OR REPLACE PROCEDURE FechaFech(año IN NUMBER,mes IN NUMBER) IS
BEGIN
    
    if mes < 10
    THEN
    dbms_output.put_line(año|| '-' ||'0'|| mes);
    ELSE
    dbms_output.put_line(mes|| '--' ||año);
    END IF;
END FechaFech;

DECLARE
año NUMBER(4);
mes NUMBER(2);
BEGIN
SELECT EXTRACT(YEAR FROM sysdate) INTO año FROM dual;
SELECT EXTRACT(MONTH FROM sysdate) INTO mes FROM dual;
FechaFech(año,mes);
END;


-- 2. Crea un procediment que incremente la variable que se li passa per paràmetre cada 
-- vegada que s'execute. En aquest cas la variable haurà de ser d'entrada-eixida ja que 
-- necessitem el seu valor per a incrementar-lo. Per a provar-ho, crea un bloc anònim 
-- que invoque a aqueix procediment passant-li una variable per paràmetres.

CREATE OR REPLACE PROCEDURE aumento (numero in out NUMBER)
is 
BEGIN
numero := numero + 1;
dbms_output.put_line(numero);
END;

DECLARE
numero NUMBER := 0;
BEGIN
aumento(numero);
aumento(numero);
END;


-- 3. Crea una funció que accepte un text com a paràmetre d'entrada i que retorne les 
-- tres primeres lletres del text en majúscules.

CREATE OR REPLACE FUNCTION iniciales (palabra IN VARCHAR2) RETURN VARCHAR2
IS
BEGIN
return SUBSTR(palabra, 0, 3);
END iniciales;

DECLARE
ini VARCHAR2(3);
palabra VARCHAR2(50) := 'pollo';
BEGIN
ini := iniciales(palabra);
dbms_output.put_line(ini);
END;

-- 4. Crea un procediment que mostre dues cadenes passades com paràmetres concatenades i en majúscules.

CREATE OR REPLACE PROCEDURE mayus(cadena1 IN VARCHAR2,cadena2 IN VARCHAR2)
IS
BEGIN
dbms_output.put_line(UPPER(cadena1)||'-'||UPPER(cadena2));
END mayus;

DECLARE
cadena1 VARCHAR2(50) := 'Castillo';
cadena2 VARCHAR2(50) := 'Rojo';
BEGIN
mayus(cadena1,cadena2);
END;



-- 5. Crea una funció que accepte un text com a paràmetre d'entrada i que retorne la longitud d'aquest.
-- Exemple: “hola món” → 8

CREATE OR REPLACE FUNCTION tamanyo(cadena IN VARCHAR2) RETURN NUMBER
IS
tam NUMBER(3);
BEGIN
SELECT LENGTH(cadena) INTO tam FROM DUAL; 
RETURN tam;
END tamanyo;

DECLARE
cadena VARCHAR2(30) := 'Papiroflexia';
tam NUMBER(3);
BEGIN
tam := tamanyo(cadena);
dbms_output.put_line('"'||cadena||'"'||' → ' || tam);
END;

-- 6. Crea una funció que retorne el valor de la hipotenusa d'un triangle a partir dels valors dels seus costats o catets.
-- Utilitzar el tipus FLOAT en els valors.

CREATE OR REPLACE FUNCTION hipotenusa(cateto1 IN FLOAT,cateto2 IN FLOAT) RETURN FLOAT
IS
hipo FLOAT;
BEGIN
hipo := POWER(cateto1,2) + POWER(cateto2,2);
hipo := SQRT(hipo);
return hipo;
END hipotenusa;

DECLARE
cateto1 FLOAT := 2;
cateto2 FLOAT := 2;
hipo FLOAT;
BEGIN
hipo := hipotenusa(cateto1,cateto2);
dbms_output.put_line(hipo);
END;

-- 7. Crea una funció que accepte un text com a paràmetre d'entrada i que retorne les tres primeres 
-- lletres del text en majúscules i la resta en minúscules.

CREATE OR REPLACE FUNCTION mayusMinus (palabra IN VARCHAR2) RETURN VARCHAR2
IS
ini VARCHAR2(3);
rest VARCHAR2(30);
palabraCom VARCHAR2(50);
BEGIN
ini := SUBSTR(palabra, 0, 3);
ini := UPPER(ini);
rest := SUBSTR(palabra,4);
palabraCom := CONCAT(ini,rest);
return palabraCom;
END mayusMinus;

DECLARE
palabraMayusMinus VARCHAR2(50);
palabra VARCHAR2(50) := 'pollo';
BEGIN
palabraMayusMinus := mayusMinus(palabra);
dbms_output.put_line(palabraMayusMinus);
END;
