drop table COMPANYIA cascade constraints;
drop table JOC cascade constraints;
drop table JUGADOR cascade constraints;
drop table QUOTA_FACT cascade constraints;
drop table RPG cascade constraints;
drop table LLUITA cascade constraints;
drop table METROIDVANIA cascade constraints;
drop table ESPORTS cascade constraints;
drop table RECORD_JOC cascade constraints;
drop table OSTENTA cascade constraints;
drop table PASSA cascade constraints;
drop table ASSOLIMENT cascade constraints;
drop table OBTÉ cascade constraints;
drop table PASSA_RPG cascade constraints;
drop table PASSA_METROIDVANIA cascade constraints;
drop table TORNEIG cascade constraints;
drop table PARTICIPA cascade constraints;

CREATE TABLE JUGADOR
(
    dni VARCHAR2(9) PRIMARY KEY,
    nom VARCHAR2(200) NOT NULL,
    cognoms VARCHAR2(200) NOT NULL,
    dat_nai DATE,
    mail VARCHAR2(200) UNIQUE NOT NULL,
    tlf NUMBER(9),
    
    CONSTRAINT mail_check CHECK(mail LIKE '%@%.%')
);

CREATE TABLE QUOTA_FACT
(
    codQ VARCHAR2(6) PRIMARY KEY,
    id_fact VARCHAR2(200) NOT NULL UNIQUE,
    mes NUMBER(2) NOT NULL,
    import_q NUMBER(6,2) DEFAULT 30,
    data_q DATE,
    jugador VARCHAR2(9) NOT NULL,
    
    CONSTRAINT quota_fk FOREIGN KEY(jugador) REFERENCES JUGADOR(dni)
);

CREATE TABLE COMPANYIA
(
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(200) NOT NULL,
    país VARCHAR2(200) NOT NULL
);

CREATE TABLE JOC
(
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(200) NOT NULL,
    comp VARCHAR2(6) NOT NULL,
    
    CONSTRAINT joc_fk FOREIGN KEY(comp) REFERENCES COMPANYIA(cod)
);

CREATE TABLE RPG
(
    cod VARCHAR2(6) PRIMARY KEY,
    
    CONSTRAINT rpg_fk FOREIGN KEY(cod) REFERENCES JOC(cod)
);

CREATE TABLE METROIDVANIA
(
    cod VARCHAR2(6) PRIMARY KEY,
    
    CONSTRAINT metroidvania_fk FOREIGN KEY(cod) REFERENCES JOC(cod)
);

CREATE TABLE LLUITA
(
    cod VARCHAR2(6) PRIMARY KEY,
    
    CONSTRAINT lluita_fk FOREIGN KEY(cod) REFERENCES JOC(cod)
);

CREATE TABLE ESPORTS
(
    cod VARCHAR2(6) PRIMARY KEY,
    
    CONSTRAINT esports_fk FOREIGN KEY(cod) REFERENCES JOC(cod)
);

CREATE TABLE RECORD_JOC
(
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(200) NOT NULL
);

CREATE TABLE OSTENTA
(
    joc VARCHAR2(6),
    record_joc VARCHAR2(6),
    jugador VARCHAR2(9) NOT NULL,
    
    CONSTRAINT ostenta_pk PRIMARY KEY(joc,record_joc),
    CONSTRAINT ostenta_joc_fk FOREIGN KEY(joc) REFERENCES JOC(cod),
    CONSTRAINT ostenta_record_fk FOREIGN KEY(record_joc) REFERENCES RECORD_JOC(cod),
    CONSTRAINT ostenta_jugador_fk FOREIGN KEY(jugador) REFERENCES JUGADOR(dni)
);

CREATE TABLE PASSA
(
    jugador VARCHAR2(9),
    joc VARCHAR2(6),
    hores NUMBER(5),
    
    CONSTRAINT passa_check CHECK(hores > 0),
    CONSTRAINT passa_pk PRIMARY KEY(jugador,joc),
    CONSTRAINT passa_jugador_fk FOREIGN KEY(jugador) REFERENCES JUGADOR(dni),
    CONSTRAINT passa_joc_fk FOREIGN KEY(joc) REFERENCES JOC(cod)
);

CREATE TABLE ASSOLIMENT
(
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(200) NOT NULL,
    descripcio VARCHAR2(1000),
    tipus VARCHAR2(200) NOT NULL,
    joc VARCHAR2(6),
    
    CONSTRAINT assoliment_fk FOREIGN KEY(joc) REFERENCES JOC(cod)
);

CREATE TABLE OBTÉ
(
    jugador VARCHAR2(9),
    assoliment VARCHAR2(6),
    
    CONSTRAINT obte_pk PRIMARY KEY(jugador,assoliment),
    CONSTRAINT obte_jugador_fk FOREIGN KEY(jugador) REFERENCES JUGADOR(dni),
    CONSTRAINT obte_assoliment_fk FOREIGN KEY(assoliment) REFERENCES ASSOLIMENT(cod)
);

CREATE TABLE PASSA_RPG
(
    jugador VARCHAR2(9),
    rpg VARCHAR2(6),
    lvl NUMBER(3),
    
    CONSTRAINT passa_rpg_pk PRIMARY KEY(jugador,rpg),
    CONSTRAINT passa_rpg_jugador_fk FOREIGN KEY(jugador) REFERENCES JUGADOR(dni),
    CONSTRAINT passa_rpg_fk FOREIGN KEY(rpg) REFERENCES RPG(cod)
);

CREATE TABLE PASSA_METROIDVANIA
(
    jugador VARCHAR2(9),
    metroidvania VARCHAR2(6),
    porcent NUMBER(3),
    
    CONSTRAINT passa_metroidvania_pk PRIMARY KEY(jugador,metroidvania),
    CONSTRAINT passamv_jugador FOREIGN KEY(jugador) REFERENCES JUGADOR(dni),
    CONSTRAINT passamv_metroidvania FOREIGN KEY(metroidvania) REFERENCES METROIDVANIA(cod)
);

CREATE TABLE TORNEIG
(
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(200) NOT NULL,
    data_t DATE,
    joc_lluita VARCHAR2(6),
    joc_esports VARCHAR2(6),
    guanyador VARCHAR2(9),
    
    CONSTRAINT torneig_lluita_fk FOREIGN KEY(joc_lluita) REFERENCES LLUITA(cod),
    CONSTRAINT torneig_esports_fk FOREIGN KEY(joc_esports) REFERENCES ESPORTS(cod),
    CONSTRAINT torneig_guanyador_fk FOREIGN KEY(guanyador) REFERENCES JUGADOR(dni)
);

CREATE TABLE PARTICIPA
(
    jugador VARCHAR2(9),
    torneig VARCHAR2(6),
    
    CONSTRAINT participa_pk PRIMARY KEY(jugador,torneig),
    CONSTRAINT jugador_fk FOREIGN KEY(jugador) REFERENCES JUGADOR(dni),
    CONSTRAINT torneig_fk FOREIGN KEY(torneig) REFERENCES TORNEIG(cod)
);
