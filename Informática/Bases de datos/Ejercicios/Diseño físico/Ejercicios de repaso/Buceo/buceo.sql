--Script DDL --

drop table CURSO cascade constraints;
drop table INSTRUCTOR cascade constraints;
drop table CLIENTE cascade constraints;
drop table IMPARTIR cascade constraints;
drop table LICENCIA cascade constraints;
drop table ARTICULO cascade constraints;
drop table TRAJE cascade constraints;
drop table ALETAS cascade constraints;
drop table BOTELLA cascade constraints;
drop table ALMACEN cascade constraints;
drop table COMPRA cascade constraints;
drop table CONTIENE cascade constraints;
drop table ALMACENAR cascade constraints;
drop table REVISOR cascade constraints;
drop table REVISAR cascade constraints;

CREATE TABLE curso
(
    cod VARCHAR2(20) PRIMARY KEY,
    nombre VARCHAR2(50) NOT NULL,
    fecha DATE
);

CREATE TABLE instructor
(
    dni VARCHAR2(9) PRIMARY KEY,
    licencia VARCHAR2(50) NOT NULL,
    nombre VARCHAR2(50) NOT NULL,
    apellidos VARCHAR2(70) NOT NULL,
    tlf NUMBER(9),
    coordinador VARCHAR2(50),
    
    CONSTRAINT instructor_fk FOREIGN KEY(dni) REFERENCES instructor(dni)
);

CREATE TABLE cliente
(
    dni VARCHAR2(9) PRIMARY KEY,
    nombre VARCHAR2(50) NOT NULL,
    apellidos VARCHAR2(70) NOT NULL,
    direccion VARCHAR2(100),
    email VARCHAR2(50),
    tlf NUMBER(9)
);

CREATE TABLE impartir
(
    cliente VARCHAR2(9),
    curso VARCHAR2(20),
    instructor VARCHAR2(9) NOT NULL,
    
    CONSTRAINT impartir_pk PRIMARY KEY(cliente,curso),
    CONSTRAINT impartir_clientefk FOREIGN KEY(cliente) REFERENCES cliente(dni),
    CONSTRAINT impartir_cursofk FOREIGN KEY(curso) REFERENCES curso(cod),
    CONSTRAINT impartir_instructorfk FOREIGN KEY(instructor) REFERENCES instructor(dni)
);

CREATE TABLE licencia
(
    cod NUMBER(8)GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nombre VARCHAR2(50) NOT NULL,
    fecha DATE,
    cliente VARCHAR2(9) NOT NULL,
    curso VARCHAR2(20) NOT NULL,
    
    CONSTRAINT licencia_clientefk FOREIGN KEY(cliente) REFERENCES cliente(dni),
    CONSTRAINT licencia_cursofk FOREIGN KEY(curso) REFERENCES curso(cod)
);

CREATE TABLE compra
(
    cod NUMBER(8)GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    fecha DATE,
    metodo VARCHAR2(30),
    Cliente VARCHAR2(9) NOT NULL,
    
    CONSTRAINT compra_Clientefk FOREIGN KEY(Cliente) REFERENCES cliente(dni)
);

CREATE TABLE articulo
(
    cod NUMBER(8) PRIMARY KEY,
    nombre VARCHAR2(50) NOT NULL,
    color VARCHAR2(20),
    precio NUMBER(4) NOT NULL
);

CREATE TABLE traje
(
    cod NUMBER(8) PRIMARY KEY,
    talla VARCHAR2(5),
    
    CONSTRAINT traje_fk FOREIGN KEY(cod) REFERENCES articulo(cod)
);

CREATE TABLE aletas
(
    cod NUMBER(8) PRIMARY KEY,
    pie NUMBER(4),
    
    CONSTRAINT aletas_fk FOREIGN KEY(cod) REFERENCES articulo(cod)
);

CREATE TABLE botella
(
    cod NUMBER(8) PRIMARY KEY,
    capacidad NUMBER(4),
    
    CONSTRAINT botella_fk FOREIGN KEY(cod) REFERENCES articulo(cod)

);

CREATE TABLE almacen
(
    cod NUMBER(8)GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nombre VARCHAR2(50) NOT NULL
);

CREATE TABLE contiene
(
    compra NUMBER(8),
    articulo NUMBER(8),
    cantidad NUMBER(4) DEFAULT 0,
    
    CONSTRAINT contiene_pk PRIMARY KEY(compra,articulo),
    CONSTRAINT contiene_comprafk FOREIGN KEY(compra) REFERENCES compra(cod),
    CONSTRAINT contiene_articulofk FOREIGN KEY(articulo) REFERENCES articulo(cod),
    CONSTRAINT contiene_ck CHECK (cantidad >= 0)
);

CREATE TABLE almacenar
(
    almacen NUMBER(8),
    articulo NUMBER(8),
    cantidad NUMBER(4) DEFAULT 0,
    
    CONSTRAINT almacenar_pk PRIMARY KEY(almacen,articulo),
    CONSTRAINT almacenar_almacenfk FOREIGN KEY(almacen) REFERENCES almacen(cod),
    CONSTRAINT almacenar_articulofk FOREIGN KEY(articulo) REFERENCES articulo(cod),
    CONSTRAINT almacenar_ck CHECK (cantidad >= 0)
);

CREATE TABLE revisor
(
    dni VARCHAR2(9) PRIMARY KEY,
    licencia VARCHAR2(50) NOT NULL,
    nombre VARCHAR2(50) NOT NULL,
    apellidos VARCHAR2(70) NOT NULL,
    tlf NUMBER(9)
);

CREATE TABLE revisar
(
    botella NUMBER(8),
    revisor VARCHAR2(9),
    año DATE,
    descrip VARCHAR2(50),
    
    CONSTRAINT revisar_pk PRIMARY KEY(botella,revisor,año),
    CONSTRAINT revisar_botellafk FOREIGN KEY(botella) REFERENCES botella(cod),
    CONSTRAINT revisar_revisorfk FOREIGN KEY(revisor) REFERENCES revisor(dni)
);

--Script DML --

INSERT INTO curso(cod,nombre,fecha) VALUES
(
    'CIF1',
    'Ciclo inferior formativo',
    TO_DATE('24/09/21','DD-MM-YY')
);

INSERT INTO curso(cod,nombre,fecha) VALUES
(
    'CIF2',
    'Ciclo inferior formativo',
    TO_DATE('15/02/18','DD-MM-YY')
);

INSERT INTO instructor (dni,licencia,nombre,apellidos,tlf)
VALUES
(
    '45263789A',
    'ANUIJHGF',
    'Manuel',
    'García Rodriguez',
    624782125
);

INSERT INTO instructor VALUES
(
    '27416985K',
    'QSMNZPOL',
    'Paco',
    'Ortega Martinez',
    687423698,
    '45263789A'
);

INSERT INTO cliente VALUES
(
    '57923654R',
    'Sara',
    'Gordillo Martin',
    'calle arquitecto 13,5 puerta 2',
    'Saragordillo@gmail.com',
    698541247
);

INSERT INTO cliente VALUES
(
    '24157865N',
    'Andrea',
    'Amorós Sánchez',
    'calle Perú 1,3 puerta 6',
    'Andreamoros@gmail.com',
    625478632
);

INSERT INTO impartir VALUES
(
    '57923654R',
    'CIF1',
    '45263789A'
);

INSERT INTO impartir VALUES
(
    '24157865N',
    'CIF2',
    '27416985K'
);

INSERT INTO licencia (nombre,fecha,cliente,curso) VALUES
(
    'Licencia básica',
    TO_DATE('15/02/22','DD-MM-YY'),
    '24157865N',
    'CIF2' 
);

INSERT INTO licencia (nombre,fecha,cliente,curso) VALUES
(
    'Licencia básica-',
    TO_DATE('24/11/20','DD-MM-YY'),
    '57923654R',
    'CIF1' 
);

INSERT INTO licencia (nombre,fecha,cliente,curso) VALUES
(
    'Licencia básica-',
    TO_DATE('24/11/20','DD-MM-YY'),
    '57923654R',
    'CIF1' 
);

INSERT INTO compra (fecha,metodo,cliente) VALUES
(
    TO_DATE('03/04/19','DD-MM-YY'),
    'Tarjeta',
    '24157865N'
);

INSERT INTO compra (fecha,metodo,cliente) VALUES
(
    TO_DATE('07/07/18','DD-MM-YY'),
    'Metálico',
    '57923654R'
);

INSERT INTO articulo VALUES
(
    12345678,
    'aletas básicas',
    'azul',
    25
);

INSERT INTO articulo VALUES
(
    48263178,
    'aletas avanzadas',
    'verde',
    23
);

INSERT INTO articulo VALUES
(
    12211221,
    'botella de oxigeno 10L',
    'gris',
    30
);

INSERT INTO articulo VALUES
(
    13311331,
    'botella de oxigeno 15L',
    'negro',
    35
);

INSERT INTO articulo VALUES
(
    45665412,
    'Traje neopreno simple',
    'negro',
    40
);

INSERT INTO articulo VALUES
(
    36985214,
    'Traje neopreno complejo',
    'gris',
    50
);

INSERT INTO aletas VALUES
(
    12345678,
    25
);

INSERT INTO aletas VALUES
(
    48263178,
    23
);

INSERT INTO botella VALUES
(
    12211221,
    10
);

INSERT INTO botella VALUES
(
    13311331,
    15
);

INSERT INTO traje VALUES
(
    45665412,
    'M'
);

INSERT INTO traje VALUES
(
    36985214,
    'L'
);

INSERT INTO almacen(nombre) VALUES
(
    'Almacén de emergencia'
);

INSERT INTO almacen(nombre) VALUES
(
    'Almacén principal'
);

INSERT INTO contiene VALUES
(
    1,
    12345678,
    2
);

INSERT INTO contiene VALUES
(
    1,
    12211221,
    1
);

INSERT INTO contiene VALUES
(
    1,
    45665412,
    1
);

INSERT INTO contiene VALUES
(
    2,
    48263178,
    2
);

INSERT INTO contiene VALUES
(
    2,
    13311331,
    2
);

INSERT INTO contiene VALUES
(
    2,
    36985214,
    2
);

INSERT INTO almacenar VALUES
(
    1,
    12345678,
    10
);

INSERT INTO almacenar VALUES
(
    1,
    12211221,
    15
);

INSERT INTO almacenar VALUES
(
    1,
    45665412,
    20
);

INSERT INTO almacenar VALUES
(
    2,
    48263178,
    10
);

INSERT INTO almacenar VALUES
(
    2,
    13311331,
    15
);

INSERT INTO almacenar VALUES
(
    2,
    36985214,
    20
);

INSERT INTO revisor VALUES
(
    '45263789A',
    'ANUIJHGF',
    'Manuel',
    'García Rodriguez',
    624782125
);

INSERT INTO revisor VALUES
(
    '27416985K',
    'QSMNZPOL',
    'Paco',
    'Ortega Martinez',
    687423698
);

INSERT INTO revisar VALUES
(
    12211221,
    '45263789A',
    TO_DATE('25/12/21','DD-MM-YY'),
    'Revisión anual de navidad'
);

INSERT INTO revisar VALUES
(
    13311331,
    '27416985K',
    TO_DATE('12/01/22','DD-MM-YY'),
    'Revisión simple,no se detectaron problemas'
);

