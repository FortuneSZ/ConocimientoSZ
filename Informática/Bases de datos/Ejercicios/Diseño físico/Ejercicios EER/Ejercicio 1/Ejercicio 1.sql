drop table CAMIONERO cascade constraints;
drop table CAMION cascade constraints;
drop table PAQUETE cascade constraints;
drop table CONDUCIR cascade constraints;
drop table PROVINCIA cascade constraints;

CREATE TABLE Provincia
(
    codigo NUMBER(10) PRIMARY KEY,
    nombre VARCHAR2(50)
    
);

CREATE TABLE Camionero
(
    DNI VARCHAR2(9) PRIMARY KEY,
    nombre VARCHAR2(50),
    direccion VARCHAR2(100),
    poblacion VARCHAR2(50),
    tlf NUMBER(9),
    salario NUMBER(4)
);

CREATE TABLE Camion
(
    matricula VARCHAR2(7) PRIMARY KEY,
    modelo VARCHAR2(30),
    tipo VARCHAR2(20),
    potencia NUMBER(5)
);

CREATE TABLE Paquete
(
    codigo NUMBER(10) PRIMARY KEY,
    descripcion VARCHAR2(100),
    destinatario VARCHAR2(100),
    destino VARCHAR2(50),
    DNI_cam VARCHAR2(9),
    cod_prov NUMBER(10),
    
    CONSTRAINT CAMIONERO_FK FOREIGN KEY(DNI_cam) REFERENCES Camionero(DNI),
    CONSTRAINT PROVINCIA_FK FOREIGN KEY(cod_prov) REFERENCES Provincia(codigo)
);

CREATE TABLE Conducir
(
    DNI_cam VARCHAR2(9),
    matricula VARCHAR2(7),
    fecha_inicio DATE,
    fecha_fin DATE,
    
    CONSTRAINT CONDUCIR_PK PRIMARY KEY(DNI_cam,matricula,fecha_inicio),
    CONSTRAINT CAMIONERO2_FK FOREIGN KEY(DNI_cam) REFERENCES Camionero(DNI),
    CONSTRAINT CAMION2_FK FOREIGN KEY(matricula) REFERENCES Camion(matricula)
);

INSERT INTO Provincia VALUES
(
    494949,
    'Alicante'
);

INSERT INTO Provincia VALUES
(
    454545,
    'Madrid'
);

INSERT INTO camion VALUES
(
    '1254ASD',
    'Cactus',
    'hibrido',
    300
);

INSERT INTO camion VALUES
(
    '7854QLC',
    'DELFIN',
    'DIESEL',
    150
);

INSERT INTO camionero VALUES
(
    '48773966M',
    'Fran',
    'Castellet',
    'Alicante',
    654145599,
    2000
);

INSERT INTO camionero VALUES
(
    '45781296A',
    'Sara',
    'Arquitecto',
    'Bormujos',
    145789623,
    2000
);

INSERT INTO paquete VALUES
(
    1231231235,
    'Paquete de prueba 1',
    'Jose Antonio',
    'Murcia',
    '48773966M',
    494949
);

INSERT INTO paquete VALUES
(
    7845698745,
    'Paquete de prueba 2',
    'Francisco Jose',
    'Madrid',
    '45781296A',
    454545
);

INSERT INTO conducir VALUES
(
    '48773966M',
    '1254ASD',
    TO_DATE('04/09/1999','DD-MM-YYYY'),
    TO_DATE('15/09/1999','DD-MM-YYYY')
);

INSERT INTO conducir VALUES
(
    '45781296A',
    '7854QLC',
    TO_DATE('14/02/2035','DD-MM-YYYY'),
    TO_DATE('25/05/2036','DD-MM-YYYY')
);
