INSERT INTO Provincia VALUES
(
    494949,
    'Alicante'
);

INSERT INTO Provincia VALUES
(
    454545,
    'Madrid'
);

INSERT INTO camion VALUES
(
    '1254ASD',
    'Cactus',
    'hibrido',
    300
);

INSERT INTO camion VALUES
(
    '7854QLC',
    'DELFIN',
    'DIESEL',
    150
);

INSERT INTO camionero VALUES
(
    '48773966M',
    'Fran',
    'Castellet',
    'Alicante',
    654145599,
    2000
);

INSERT INTO camionero VALUES
(
    '45781296A',
    'Sara',
    'Arquitecto',
    'Bormujos',
    145789623,
    2000
);

INSERT INTO paquete VALUES
(
    1231231235,
    'Paquete de prueba 1',
    'Jose Antonio',
    'Murcia',
    '48773966M',
    494949
);

INSERT INTO paquete VALUES
(
    7845698745,
    'Paquete de prueba 2',
    'Francisco Jose',
    'Madrid',
    '45781296A',
    454545
);

INSERT INTO conducir VALUES
(
    '48773966M',
    '1254ASD',
    TO_DATE('04/09/1999','DD-MM-YYYY'),
    TO_DATE('15/09/1999','DD-MM-YYYY')
);

INSERT INTO conducir VALUES
(
    '45781296A',
    '7854QLC',
    TO_DATE('14/02/2035','DD-MM-YYYY'),
    TO_DATE('25/05/2036','DD-MM-YYYY')
);