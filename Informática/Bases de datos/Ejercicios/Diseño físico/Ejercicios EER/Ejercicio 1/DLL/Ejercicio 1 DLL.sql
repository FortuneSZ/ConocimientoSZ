drop table CAMIONERO cascade constraints;
drop table CAMION cascade constraints;
drop table PAQUETE cascade constraints;
drop table CONDUCIR cascade constraints;
drop table PROVINCIA cascade constraints;

CREATE TABLE Provincia
(
    codigo NUMBER(10) PRIMARY KEY,
    nombre VARCHAR2(50)
    
);

CREATE TABLE Camionero
(
    DNI VARCHAR2(9) PRIMARY KEY,
    nombre VARCHAR2(50),
    direccion VARCHAR2(100),
    poblacion VARCHAR2(50),
    tlf NUMBER(9),
    salario NUMBER(4)
);

CREATE TABLE Camion
(
    matricula VARCHAR2(7) PRIMARY KEY,
    modelo VARCHAR2(30),
    tipo VARCHAR2(20),
    potencia NUMBER(5)
);

CREATE TABLE Paquete
(
    codigo NUMBER(10) PRIMARY KEY,
    descripcion VARCHAR2(100),
    destinatario VARCHAR2(100),
    destino VARCHAR2(50),
    DNI_cam VARCHAR2(9),
    cod_prov NUMBER(10),
    
    CONSTRAINT CAMIONERO_FK FOREIGN KEY(DNI_cam) REFERENCES Camionero(DNI),
    CONSTRAINT PROVINCIA_FK FOREIGN KEY(cod_prov) REFERENCES Provincia(codigo)
);

CREATE TABLE Conducir
(
    DNI_cam VARCHAR2(9),
    matricula VARCHAR2(7),
    fecha_inicio DATE,
    fecha_fin DATE,
    
    CONSTRAINT CONDUCIR_PK PRIMARY KEY(DNI_cam,matricula,fecha_inicio),
    CONSTRAINT CAMIONERO2_FK FOREIGN KEY(DNI_cam) REFERENCES Camionero(DNI),
    CONSTRAINT CAMION2_FK FOREIGN KEY(matricula) REFERENCES Camion(matricula)
);


