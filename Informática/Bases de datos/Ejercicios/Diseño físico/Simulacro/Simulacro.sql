-- Para las siguientes tablas y los datos de ejemplo que se incluyen a continuación:

@CREATE TABLE plataformes(
    codi CHAR(4), 
    nom VARCHAR2(30),
    CONSTRAINT pk_plataformes PRIMARY KEY (codi)
);

CREATE TABLE jocs(
    codi CHAR(5), 
    nom VARCHAR2(50),
    descripcio VARCHAR2(1000),
    anyLlancament NUMBER(4),
    espaiOcupatMb NUMBER(9,3), 
    codiPlataforma CHAR(4), 
    CONSTRAINT pk_jocs PRIMARY KEY (codi),
    CONSTRAINT fk_plataformes 
        FOREIGN KEY (codiPlataforma) REFERENCES plataformes(codi)
); 

INSERT INTO plataformes VALUES('cpc', 'Amstrad CPC');
INSERT INTO plataformes VALUES('pcw', 'Amstrad PCW');
INSERT INTO plataformes VALUES('msx', 'MSX');
INSERT INTO plataformes VALUES('spec', 'Sinclair ZX Spectrum');
INSERT INTO plataformes VALUES('psx', 'Playstation');
INSERT INTO plataformes VALUES('ps2', 'Playstation 2');
INSERT INTO plataformes VALUES('ps3', 'Playstation 3');
INSERT INTO plataformes VALUES('ps4', 'Playstation 4');
INSERT INTO plataformes VALUES('ps5', 'Playstation 5');
INSERT INTO plataformes VALUES('wii', 'Nintendo WII');
INSERT INTO plataformes VALUES('stea', 'PC + Steam');
INSERT INTO plataformes VALUES('epic', 'PC + Epic');

INSERT INTO jocs VALUES('efre', 'Electro Freddy', NULL,1982,  0.2, 'cpc');
INSERT INTO jocs VALUES('mmic', 'Manic Miner', 'Plataformas sin scroll', 1983, 0.2, 'cpc');
INSERT INTO jocs VALUES('mmiz', 'Manic Miner', 'Plataformas sin scroll', 1983, 0.2, 'spec');
INSERT INTO jocs VALUES('aa', 'Ant Attack',  NULL,1983, 0.1, 'spec');
INSERT INTO jocs VALUES('ikaw', 'Ikari Warriors', 'Disparos, vista cenital', 1986, 0.2, 'msx');
INSERT INTO jocs VALUES('wsr', 'Wii Sports Resort', NULL,2009,  NULL, 'wii');
INSERT INTO jocs VALUES('gt5', 'Gran Turismo 5', NULL, 2010,  NULL, 'ps3');
INSERT INTO jocs VALUES('last1', 'The last of US',  NULL,2013, NULL, 'ps3');
INSERT INTO jocs VALUES('fortn', 'Fortnite', 'FPS + Battle Royale',2017, NULL,  'epic');
INSERT INTO jocs VALUES('aliso', 'Alien: Isolation', NULL, 2017, 35000, 'epic');
INSERT INTO jocs VALUES('cont', 'Control', 'Aventura', 2019, NULL, 'epic');
INSERT INTO jocs VALUES('batao', 'Batman: A.O.', NULL, 2013, 18250, 'stea');


-- 01 - Muestra el nombre de los juegos que ocupan menos de 1 MB.

SELECT nom FROM jocs WHERE espaiOcupatMb < 1;


-- 02 - Datos de las plataformas que comienzan por "PC".

SELECT * FROM plataformes WHERE lower(nom) LIKE 'pc%';


-- 03 - Nombre de los juegos cuya plataforma se llama "Nintendo WII" y que no 
-- son del año 2010.

SELECT jocs.nom 
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
AND plataformes.nom ='Nintendo WII'
AND anyLlancament <> 2010;


-- 04 - Nombre de los juegos para los que no sabemos el espacio ocupado, 
-- ordenados alfabéticamente.

SELECT nom FROM jocs WHERE espaiOcupatMb IS NULL ORDER BY nom;


-- 05 - Nombres de las plataformas que contienen la palabra "STATION" (quizá 
-- con mayúsculas distintas).

SELECT nom FROM plataformes WHERE UPPER(nom) LIKE '%STATION%';


-- 06 - Nombres de los juegos que son de las plataformas "Amstrad CPC" o "MSX", 
-- usando IN.

SELECT jocs.nom 
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
AND plataformes.nom IN ('Amstrad CPC', 'MSX');


-- 07 - Nombres de los juegos que son de las plataformas "Amstrad CPC" o "MSX", 
-- sin usar IN.

SELECT jocs.nom 
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
AND (plataformes.nom = 'Amstrad CPC' OR plataformes.nom = 'MSX');


-- 08 - Nombre de cada juego y nombre de la plataforma a la que pertenece, 
-- ordenado por juego y (si coincide el nombre del juego) por plataforma.

SELECT jocs.nom, plataformes.nom
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
ORDER BY jocs.nom, plataformes.nom;


-- 09 - Nombre de cada plataforma y cantidad de juegos que tenemos para esa 
-- plataforma, incluso si de alguna plataforma no tenemos ningún juego.

SELECT plataformes.nom, COUNT(jocs.nom)
FROM plataformes LEFT JOIN jocs
ON plataformes.codi = jocs.codiPlataforma
GROUP BY plataformes.nom;


-- 10 - Nombre de cada plataforma y cantidad de juegos que tenemos para esa 
-- plataforma, para las plataformas en las que tenemos más de un juego.

SELECT plataformes.nom, count(jocs.nom)
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
GROUP BY plataformes.nom
HAVING COUNT(jocs.nom) > 1;


-- 11 - Nombre de las plataformas para las que no tenemos ningún juego, 
-- ordenadas alfabéticamente.

SELECT plataformes.nom
FROM plataformes LEFT JOIN jocs
ON plataformes.codi = jocs.codiPlataforma
GROUP BY plataformes.nom
HAVING COUNT(jocs.nom) = 0
ORDER BY plataformes.nom;

SELECT plataformes.nom
FROM plataformes 
WHERE codi NOT IN
(SELECT codiPlataforma FROM jocs);


-- 12 - Nombre y descripción de los juegos que son para la misma plataforma que 
-- el juego más grande que tenemos en nuestra base de datos.

-- Previo: juego más grande (V1)

SELECT nom, codiPlataforma, espaiOcupatMb
FROM jocs WHERE espaiOcupatMb = 
(SELECT MAX(espaiOcupatMb) FROM jocs);

SELECT codiPlataforma
    FROM jocs WHERE espaiOcupatMb = 
    (
        SELECT MAX(espaiOcupatMb) FROM jocs
    );

-- Completo:

SELECT nom, descripcio
FROM jocs WHERE codiPlataforma = 
(
    SELECT codiPlataforma
    FROM jocs WHERE espaiOcupatMb = 
    (
        SELECT MAX(espaiOcupatMb) FROM jocs
    )
);


-- 13 - Nombre de los juegos que tenemos para más de una plataforma.

-- Previo: juegos y cantidad de plataformas veces que aparecen

SELECT nom, COUNT(*)
FROM jocs
GROUP BY nom;

-- Completo

SELECT nom
FROM jocs
GROUP BY nom
HAVING COUNT(*) > 1;


-- 14 - Espacio ocupado por el juego que menos ocupa en cada plataforma.



SELECT plataformes.nom, MIN(espaiOcupatMb)
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
GROUP BY plataformes.nom;


-- 15 - Nombre del juego (o juegos) que menos ocupa(n) en cada una de las plataformas.

SELECT nom FROM jocs j
WHERE espaiOcupatMb = 
(
    SELECT MIN(espaiOcupatMb) FROM jocs j2
    WHERE j.codiPlataforma = j2.codiPlataforma
);


-- 16 - Nombre de todos los juegos que tenemos, sin duplicados, ordenados 
-- alfabéticamente.

SELECT DISTINCT nom FROM jocs ORDER BY nom;


-- 17 - Nombre de cada plataforma y año medio de los juegos que tenemos para 
-- esa plataforma.

SELECT plataformes.nom, AVG(anyLlancament)
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
GROUP BY plataformes.nom
ORDER BY AVG(anyLlancament);


-- 18 - Nombre de la plataforma más antigua (aquella cuyos juegos son, como media, más antiguos).

-- Oracle

SELECT plataformes.nom, AVG(anyLlancament)
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
GROUP BY plataformes.nom
ORDER BY AVG(anyLlancament)
FETCH FIRST 1 ROWS ONLY;


-- 19 - Nombre de los juegos y espacio que ocupan, redondeado sin decimales, 
-- para aquellos que ocupan menos de 50 MB.

select * from jocs;

SELECT nom, ROUND(espaiOcupatMb,1)
FROM jocs 
WHERE espaiOcupatMb < 50;


-- 20 - Nombre de las plataformas cuyo código coincida con las 3 primeras 
-- letras de su nombre.
SELECT codi, nom
FROM plataformes;


SELECT nom
FROM plataformes 
WHERE UPPER(TRIM(codi)) = UPPER(SUBSTR(nom,1,3));


-- 21 - Añade un campo "Categoría" a la tabla de juegos, que podrá contener 
-- hasta 20 letras.

ALTER TABLE jocs ADD categoria VARCHAR2(20);


-- 22 - Añade a la tabla de juegos una restricción de que el año de lanzamiento 
-- deba ser superior a 1979.

ALTER TABLE jocs
ADD CONSTRAINT ck_jocs_any CHECK (anyLlancament > 1979);


-- 23 - Crea una tabla Valoraciones, con un código de valoración (número, de 5 
-- cifras), que actuará como clave primaria, un código de juego (clave ajena a la 
-- tabla de juegos), un texto (hasta 100 letras) y una valoración numérica (hasta 
-- 2 cifras antes de la coma decimal, y una cifra después de la coma). Usa 
-- sintaxis de Oracle.

CREATE TABLE valoracions(
    codi NUMBER(5), 
    codiJoc CHAR(5),
    detalls VARCHAR2(100),
    valorac NUMBER(3,1), 
    CONSTRAINT pk_valoracions PRIMARY KEY (codi),
    CONSTRAINT fk_valoracions_jocs 
        FOREIGN KEY (codiJoc) REFERENCES jocs(codi)
);


-- 24 - Añade dos datos de ejemplo a la tabla de Valoraciones, indicando 
-- valores para todos los datos a excepción del texto, y haciéndolo de 2 formas 
-- distintas (una vez empleando los nombres de los campos y otra vez sin 
-- detallarlos).

INSERT INTO valoracions VALUES(1, 'efre', NULL, 5);

INSERT INTO valoracions (codi, codiJoc, valorac) VALUES(2, 'mmic', 6);


-- 25 - Crea una tabla Usuarios, con un código de exactamente 5 letras para 
-- cada usuario, un nombre y una fecha de alta. El nombre debe ser único y no 
-- nulo. Usa sintaxis de Oracle.

CREATE TABLE usuaris(
    codi CHAR(5), 
    nom VARCHAR2(40) NOT NULL,
    dataAlta DATE, 
    CONSTRAINT pk_usuaris PRIMARY KEY (codi),
    CONSTRAINT uk_usuaris_nom UNIQUE (nom)
);


-- 26 - Introduce 3 usuarios de ejemplo: en uno no indicarás los nombres de los 
-- campos, en otro indicarás todos los nombres de los campos e introducirás todos 
-- los datos, en otro indicarás los nombres de algunos campos e introducirás datos 
-- sólo en algunos campos.

-- SQLite

INSERT INTO usuaris VALUES('a', 'Alfredo', '2023-02-15');
INSERT INTO usuaris (codi, nom, dataAlta) VALUES('b', 'Bernardo', '2023-02-16');
INSERT INTO usuaris (codi,nom) VALUES('c', 'Carla');


-- 27 - Modifica el juego cuyo nombre es 'Batman: A.O.' para que pase a ser 
-- 'Batman: Arkham Origins'.

UPDATE jocs
SET nom = 'Batman: Arkham Origins'
WHERE nom = 'Batman: A.O.';


-- 28 - Modifica los datos de juegos: si alguno tiene espacio ocupado 0, 
-- cámbialo por un valor nulo.

UPDATE jocs
SET espaiOcupatMb = NULL
WHERE espaiOcupatMb = 0;


-- 29 - Borra los juegos que sean (a la vez) anteriores a 1982 y para 
-- plataformas cuyo nombre comience por "Play".

DELETE FROM jocs
WHERE anyLlancament < 1982
AND codiPlataforma IN
(SELECT codi FROM plataformes WHERE nom LIKE 'Play%');


-- 30 - Muestra los nombres de las plataformas cuyas 4 primeras letras no 
-- coinciden con ninguna otra plataforma.

SELECT nom FROM plataformes p1 WHERE SUBSTR(nom, 1, 4) NOT IN
(
    SELECT SUBSTR(nom, 1, 4) 
    FROM plataformes p2
    WHERE p2.nom <> p1.nom
);


-- 31 - Nombres de todos los usuarios y nombres de todas las plataformas.

SELECT nom FROM usuaris
UNION
SELECT nom FROM plataformes;


-- 32 - Nombres de los usuarios que coincidan con el nombre de alguna plataforma.

SELECT nom FROM usuaris WHERE nom IN
(SELECT nom FROM plataformes);


-- 33 - Nombre del juego, nombre de la plataforma y espacio ocupado, para los 
-- juegos que ocupen menos que cualquier juego de la plataforma "Amstrad CPC".

-- SQLite
SELECT jocs.nom, plataformes.nom, espaiOcupatMb
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
AND espaiOcupatMb <
(
    SELECT MIN(espaiOcupatMb)
    FROM jocs, plataformes 
    WHERE jocs.codiPlataforma = plataformes.codi
    AND plataformes.nom = 'Amstrad CPC' 
);

-- Oracle
SELECT jocs.nom, plataformes.nom, espaiOcupatMb
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
AND espaiOcupatMb < ALL
(
    SELECT espaiOcupatMb
    FROM jocs, plataformes 
    WHERE jocs.codiPlataforma = plataformes.codi
    AND plataformes.nom = 'Amstrad CPC' 
);


-- 34 - Nombres de las plataformas para las que el conjunto de todos los juegos 
-- que tenemos ocupa menos de 10 MB.

SELECT plataformes.nom
FROM jocs, plataformes 
WHERE jocs.codiPlataforma = plataformes.codi
GROUP BY plataformes.nom
HAVING SUM(espaiOcupatMb) < 10;


-- 35 - Crea una tabla "JuegosPS3", con parte de los datos (código, nombre y 
-- año) de los juegos que tenemos para PS3.

CREATE TABLE jocsPS3 AS
(SELECT codi, nom, anyLlancament FROM jocs WHERE codiPlataforma = 'ps3');


-- 36 - Borra la tabla de Valoraciones, que hemos decidido 
-- que no vamos a utilizar.

DROP TABLE valoracions;


-- 37 - Añade a la tabla de Juegos un nuevo campo, valoracion, con hasta 2 
-- cifras antes de la coma decimal, y una cifra después de la coma, y que sólo 
-- debe aceptar valores entre 0 y 10.

ALTER TABLE jocs
ADD valoracio NUMBER(3,1);

-- Lo siguiente, sólo en Oracle

ALTER TABLE jocs
ADD CONSTRAINT ck_jocs_valoracio CHECK(valoracio BETWEEN 0 AND 10);


-- 38 - Asegúrate de que todos los juegos tengan valoración nula y, 
-- posteriormente, de que "The Last of Us" para la plataforma "ps3" tiene una 
-- valoración de 9.5.

UPDATE jocs SET valoracio = NULL;

UPDATE jocs SET valoracio = 9.5
WHERE LOWER(nom) = 'the last of us' AND codiPlataforma = 'ps3';


-- 39 - Muestra nombre y espacio ocupado por los juegos que ocupan más que la 
-- media de los juegos de su misma plataforma.

SELECT nom, espaiOcupatMb 
FROM jocs j
WHERE espaiOcupatMb >
(
    SELECT AVG(espaiOcupatMb) FROM jocs j2
    WHERE j.codiPlataforma = j2.codiPlataforma
);


-- 40 - Muestra el nombre de cada juego, su descripción y el espacio ocupado, 
-- para los juegos que ocupen más del doble de la media de su plataforma.

SELECT nom, descripcio, espaiOcupatMb 
FROM jocs j
WHERE espaiOcupatMb >
(
    SELECT AVG(espaiOcupatMb)*2 FROM jocs j2
    WHERE j.codiPlataforma = j2.codiPlataforma
);
