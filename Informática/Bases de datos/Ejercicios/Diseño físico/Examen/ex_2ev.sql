CREATE TABLE editorial (
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(100) NOT NULL,
    ciudad VARCHAR2(100),
    anyo NUMBER(4)
);

CREATE TABLE dibujante (
    dni VARCHAR2(9) PRIMARY KEY,
    nom VARCHAR2(100) NOT NULL,
    fec_nac DATE,
    ciudad_nac VARCHAR2(100)
);

CREATE TABLE manga (
    cod VARCHAR2(6) PRIMARY KEY,
    nom VARCHAR2(100) NOT NULL,
    anyo NUMBER(4),
    caps NUMBER(4),
    editorial VARCHAR2(6),
    dibujante VARCHAR2(9),

    CONSTRAINT fk_mnga_editorial FOREIGN KEY (editorial) REFERENCES editorial(cod),
    CONSTRAINT fk_mnga_dibujante FOREIGN KEY (dibujante) REFERENCES dibujante(dni)
);

INSERT INTO editorial VALUES ('SHUEIS','Shūeisha Inc','Tokyo',1949);
INSERT INTO editorial VALUES ('SHOGAK','Shōgakukan Inc','Tokyo',1922);
INSERT INTO editorial VALUES ('KODANS','Kōdansha','Tokyo',1911);
INSERT INTO editorial VALUES ('KADOKA','Kadokawa Shoten','Tokyo',1945);
INSERT INTO editorial VALUES ('HAKUSE','Hakusensha','Kyoto',1973);
INSERT INTO editorial VALUES ('FUTABA','Futabasha','Osaka',1948);
INSERT INTO editorial VALUES ('SQUENI','Square Enix','Tokyo',1975);

INSERT INTO dibujante VALUES ('11111111A', 'Akira Toriyama', TO_DATE('05/04/1955','DD/MM/YYYY'),'Kiyosu');
INSERT INTO dibujante VALUES ('22222222B', 'Hiro Mashima', TO_DATE('03/05/1977','DD/MM/YYYY'),'Nagano');
INSERT INTO dibujante VALUES ('33333333C', 'Masashi Kishimoto', TO_DATE('08/11/1974','DD/MM/YYYY'),'Nagi');
INSERT INTO dibujante VALUES ('44444444D', 'Eiichirō Oda', TO_DATE('01/01/1975','DD/MM/YYYY'),'Kumamoto');
INSERT INTO dibujante VALUES ('55555555E', 'Tite Kubo', TO_DATE('26/06/1977','DD/MM/YYYY'),'Hiroshima');
INSERT INTO dibujante VALUES ('66666666F', 'Hiromu Arakawa', TO_DATE('08/05/1973','DD/MM/YYYY'),'Hokkaidō');
INSERT INTO dibujante VALUES ('77777777G', 'Hayao Miyazaki', TO_DATE('05/01/1941','DD/MM/YYYY'),'Tokyo');
INSERT INTO dibujante VALUES ('88888888H', 'Katsuhiro Ōtomo', TO_DATE('14/04/1954','DD/MM/YYYY'),'Miyagi');
INSERT INTO dibujante VALUES ('99999999I', 'Takeshi Kitano', TO_DATE('12/11/1956','DD/MM/YYYY'),'Kyoto');

INSERT INTO manga VALUES ('DRSLUM','Dr. Slump',1979,18,'SHUEIS','11111111A');
INSERT INTO manga VALUES ('DRAGON','Dragon Ball',1984,42,'SHUEIS','11111111A');
INSERT INTO manga VALUES ('FAIRYT','Fairy Tail',2006,63,'KODANS','22222222B');
INSERT INTO manga VALUES ('EDENSZ','Edens Zero',2018,25,'KODANS','22222222B');
INSERT INTO manga VALUES ('NARUTO','Naruto',1999,72,'SHUEIS','33333333C');
INSERT INTO manga VALUES ('BORUTO','Boruto: Naruto Next Generations',2016,19,'SHUEIS','33333333C');
INSERT INTO manga VALUES ('ONEPIE','One Piece',1997,105,'SHUEIS','44444444D');
INSERT INTO manga VALUES ('BLEACH','Bleach',2001,74,'SHUEIS','55555555E');
INSERT INTO manga VALUES ('FMALCH','Full Metal Alchemist',2001,27,'SQUENI','66666666F');
INSERT INTO manga VALUES ('CHIHIR','El viaje de Chihiro',2001,1,null,'77777777G');
INSERT INTO manga VALUES ('HOWLCA','El castillo ambulante',2004,1,null,'77777777G');
INSERT INTO manga VALUES ('MONONO','La princesa Mononoke',1997,1,null,'77777777G');
INSERT INTO manga VALUES ('TOTORO','Mi vecino Totoro',1988,1,null,'77777777G');
INSERT INTO manga VALUES ('AKIRAM','Akira',1982,6,'KODANS','88888888H');
INSERT INTO manga VALUES ('MEMORI','Memories',1995,1,null,'88888888H');
INSERT INTO manga VALUES ('COWBOY','Cowboy Bebop',1998,26,null,null);
INSERT INTO manga VALUES ('JUJUTS','Jujutsu Kaisen',2018,22,'SHUEIS',null);
INSERT INTO manga VALUES ('KIMETS','Kimetsu no Yaiba',2016,23,'SHUEIS',null);
INSERT INTO manga VALUES ('SWORDA','Sword Art Online',2009,27,'KADOKA',null);



-- 01 - Muestra el nombre de los mangas que hayan sido publicados entre 2000 y 2010.

SELECT nom 
    FROM manga 
        WHERE anyo BETWEEN 2000 AND 2010;


-- 02 - Todos los datos de las editoriales que comienzan por "SH".

SELECT * 
    FROM editorial 
        WHERE lower(nom) LIKE 'sh%';


-- 03 - Nombre de los dibujantes que trabajan para la editorial "Shūeisha Inc" desde 
-- antes del año 2000.

SELECT DISTINCT d.nom 
	FROM dibujante d, manga m, editorial e 
		WHERE d.dni = m.dibujante 
    		AND m.editorial = e.cod
			AND e.nom ='Shūeisha Inc'
			AND m.anyo < 2000;


-- 04 - Nombre de los mangas que no tienen editorial, pero si dibujante, 
-- ordenados alfabéticamente.

SELECT nom 
    FROM manga WHERE editorial IS NULL 
    	AND dibujante IS NOT NULL 
    		ORDER BY nom;

-- 05 - Nombres de las editoriales en las cuales la segunda letra sea una "a".

SELECT nom 
    FROM editorial 
        WHERE lower(nom) LIKE '_a%';


-- 06 - Nombre de los mangas que son de las editoriales "Kōdansha" o "Kadokawa Shoten", 
-- usando IN.

SELECT m.nom 
	FROM manga m, editorial e 
		WHERE m.editorial = e.cod
			AND e.nom IN ('Kōdansha', 'Kadokawa Shoten');


-- 07 - Nombre de cada editorial y cantidad de mangas que hay para esa 
-- editorial en la base de datos, para las editoriales en las que tenemos más de un manga.

SELECT e.nom, count(m.nom)
    FROM editorial e, manga m 
        WHERE e.cod = m.editorial
            GROUP BY e.nom
            HAVING COUNT(m.nom) > 1;


-- 08 - Nombre de los mangas que son de las editoriales "Kōdansha" o "Kadokawa Shoten", 
-- sin usar IN.

SELECT m.nom 
	FROM manga m, editorial e 
		WHERE m.editorial = e.cod
			AND (e.nom = 'Kōdansha' OR e.nom = 'Kadokawa Shoten');


-- 09 - Nombre de la editorial, el dibujante y el manga, para cada manga con editorial y dibujante,
-- ordenado por el nombre de la editorial. Si coinciden, se ordenará en segunda instancia por nombre
-- del dibujante, y si ocurre lo mismo, se ordenara por el nombre del manga.

SELECT e.nom, d.nom, m.nom
	FROM editorial e, dibujante d, manga m
		WHERE e.cod = m.editorial
			AND m.dibujante = d.dni
    			ORDER BY e.nom, d.nom, m.nom;


-- 10 - Nombre de cada editorial y cantidad de mangas que hay para esa 
-- editorial en la base de datos, incluso si de alguna editorial no tenemos ningún manga.

SELECT e.nom, COUNT(m.nom)
	FROM editorial e LEFT JOIN manga m
		ON e.cod = m.editorial
			GROUP BY e.nom;



-- 11 - Nombre de las editoriales para las que no tenemos ningún manga, 
-- ordenadas alfabéticamente en orden descendente (sin utilizar subconsultas).

SELECT e.nom
    FROM editorial e LEFT JOIN manga m
        ON e.cod = m.editorial
            GROUP BY e.nom
                HAVING COUNT(m.nom) = 0
                    ORDER BY e.nom DESC;



-- 12 - Nombres de las editoriales y el total de capitulos que acumulan.


SELECT e.nom, sum(m.caps)
    FROM editorial e, manga m
        WHERE e.cod = m.editorial
            GROUP BY e.nom;


-- 13 - Nombre de la editorial y el número de capítulos  del manga que más capítulos tiene en cada editorial.


SELECT e.nom, MAX(m.caps)
    FROM editorial e, manga m 
        WHERE e.cod = m.editorial
            GROUP BY e.nom;


-- 14 - Nombre del manga (o mangas) que menos capitulos tiene(n) en cada una de las editoriales.

SELECT m.nom 
    FROM manga m
        WHERE m.caps = 
        (
            SELECT MIN(m2.caps) FROM manga m2
                WHERE m.editorial = m2.editorial
        );


-- 15 - Nombre de todas las editoriales en las que tenemos mangas registrados, sin duplicados, ordenados 
-- alfabéticamente.

SELECT DISTINCT e.nom 
    FROM editorial e, manga m
        WHERE e.cod = m.editorial
            ORDER BY e.nom;


-- 16 - Nombre de cada dibujante y número medio de capítulos que tenemos para 
-- sus mangas.

SELECT d.nom, AVG(m.caps)
    FROM dibujante d, manga m 
        WHERE d.dni = m.dibujante
            GROUP BY d.nom;


-- 17 - Nombre de los 3 dibujantes con la media de capítulos más alta.

SELECT d.nom
    FROM dibujante d, manga m 
        WHERE d.dni = m.dibujante
            GROUP BY d.nom
                ORDER BY AVG(m.caps) DESC
                    FETCH FIRST 3 ROWS ONLY;


-- 18 - Nombre y capitulos de los mangas que son de la misma editorial que 
-- el manga con más capítulos que tenemos en nuestra base de datos, ordenado por numero
-- de capítulos en orden descendiente.


SELECT nom, caps
    FROM manga WHERE editorial = 
    (
        SELECT editorial
            FROM manga WHERE caps = 
            (
                SELECT MAX(caps) FROM manga
            )
    )
    ORDER BY caps DESC;



-- 19 - Nombre de cada dibujante y número medio de capítulos que tenemos para 
-- sus mangas, pero redondeando la media y ordenando de forma ascendente por el número de capítulos.

SELECT d.nom, ROUND(AVG(m.caps))
    FROM dibujante d, manga m 
        WHERE d.dni = m.dibujante
            GROUP BY d.nom
                ORDER BY AVG(m.caps);


-- 20 Saca los nombres de las editoriales que no tienen mangas registrados en la base de datos, sin utilizar ningún tipo de JOIN 
-- (ni siquiera el INNER JOIN clásico con el WHERE).

SELECT nom
	FROM editorial
		WHERE cod NOT IN 
			(SELECT DISTINCT editorial
    			FROM manga
    				WHERE editorial IS NOT NULL);


-- 21 - Añade un campo "categoria" a la tabla de manga, que podrá contener 
-- hasta 100 letras.

ALTER TABLE manga ADD categoria VARCHAR2(100);


-- 22 - Añade a la tabla de manga una restricción de que el año de lanzamiento 
-- deba ser igual o superior a 1814 (año donde se "creó" el manga).

ALTER TABLE manga
    ADD CONSTRAINT ck_manga_anyo CHECK (anyo >= 1814);


-- 23 - Crea una tabla "criticas", con un campo "cod" (6 carácteres), 
-- que actuará como clave primaria, un campo "nota"(hasta 2 cifras antes de la coma decimal, 
-- y una cifra después de la coma), un campo "critica" (500 carácteres), y un campo "manga" 
-- que será la clave ajena a la tabla manga. El campo "nota" debe contener solo notas entre 0 y 10.

CREATE TABLE criticas(
    cod VARCHAR2(6) PRIMARY KEY, 
    nota NUMBER(3,1),
    critica VARCHAR2(500),
    manga VARCHAR2(6) , 
    CONSTRAINT ck_nota CHECK (nota BETWEEN 0 AND 10),
    CONSTRAINT fk_criticas_manga FOREIGN KEY (manga) REFERENCES manga(cod)
);


-- 24 - Añade dos datos de ejemplo a la tabla de criticas, indicando 
-- valores para todos los datos a excepción del campo critica, y haciéndolo de 2 formas 
-- distintas (forma simplificada y forma compleja).

INSERT INTO criticas VALUES('C00001',  9.9 , null , 'DRSLUM');
INSERT INTO criticas (cod, nota, manga) VALUES('C00001', 6.3, 'DRSLUM');

-- 25 - Crea una tabla usuarios, con un campo cod (6 carácteres) cada usuario, 
-- un campo "nom" (100 carácteres) y un campo "fec_alta" de tipo fecha.
-- El nombre debe ser único y no nulo.

CREATE TABLE usuarios(
    cod VARCHAR(6) PRIMARY KEY, 
    nom VARCHAR2(100) NOT NULL,
    fec_alta DATE,
    CONSTRAINT uk_usuarios_nom UNIQUE (nom)
);

-- 26 - Introduce 3 usuarios de ejemplo: en uno no indicarás los nombres de los 
-- campos, en otro indicarás todos los nombres de los campos e introducirás todos 
-- los datos, en otro indicarás los nombres de algunos campos e introducirás datos 
-- sólo en algunos campos.

INSERT INTO usuaris VALUES('USU001', 'Alfredo', TO_DATE('2023-02-15', 'YYYY-MM-DD'));
INSERT INTO usuaris (cod, nom, fec_alta) VALUES ('USU002', 'Bernardo', TO_DATE('2023-02-16', 'YYYY-MM-DD'));
INSERT INTO usuaris (cod, nom) VALUES ('USU003', 'Carla');


-- 27 - Modifica el manga cuyo nombre es 'Dragon Ball' para que pase a ser 
-- 'Dragon Ball Z'.

UPDATE manga
    SET nom = 'Dragon Ball Z'
        WHERE nom = 'Dragon Ball';


-- 28 - Modifica los datos de los mangas: si alguno tiene 1 capítulo, 
-- cámbialo por un valor nulo.

UPDATE manga
    SET caps = NULL
        WHERE caps = 1; 


-- 29 - Borra los mangas que sean (a la vez) anteriores a 2000 y para 
-- la editorial "Shūeisha Inc", sin usar JOINS.

DELETE FROM manga
    WHERE anyo < 2000
        AND editorial IN
            (SELECT cod FROM editorial WHERE nom = 'Shūeisha Inc');


-- 30 - Muestra los nombres de las editoriales cuyas 2 primeras letras
-- coinciden con alguna otra editorial.

SELECT e1.nom FROM editorial e1 WHERE SUBSTR(nom, 1, 2) IN
(
    SELECT SUBSTR(e2.nom, 1, 2) 
    	FROM editorial e2
    		WHERE e2.nom <> e1.nom
);


-- 31 - Nombres de todos los dibujantes y nombres de todas las editoriales, en la misma columna.

SELECT nom FROM dibujante
UNION
SELECT nom FROM editorial;


-- 32 - Nombre y año de los mangas que sean mas antiguos que algún manga de la editorial "Square Enix".

SELECT nom, anyo
    FROM manga 
        WHERE anyo < ANY
                (
                    SELECT  m2.anyo
                        FROM manga m2, editorial e2
                            WHERE m2.editorial = e2.cod
                                AND e2.nom = 'Square Enix' 
                );


-- 33 - Nombre del manga, nombre de la editorial y número de capitulos, para los 
-- mangas que tengan más capítulos que cualquier manga de la editorial "Kōdansha".


SELECT m.nom, e.nom, m.caps
    FROM manga m, editorial e 
        WHERE m.editorial = e.cod
            AND m.caps > ALL
                (
                    SELECT  m2.caps
                        FROM manga m2, editorial e2
                            WHERE m2.editorial = e2.cod
                                AND e2.nom = 'Kōdansha' 
                );


-- 34 - Nombres de las editoriales para las que el conjunto de todos los mangas 
-- que tenemos tienen en total menos de 30 capítulos.

SELECT e.nom
    FROM manga m, editorial e 
        WHERE m.editorial = e.cod
            GROUP BY e.nom
            HAVING SUM(m.caps) < 30;


-- 35 - Crea una tabla "MangasShueisha", con parte de los datos (código, nombre y 
-- año) de los mangas de "Shūeisha Inc".

CREATE TABLE MangasShueisha AS
    (SELECT m.cod, m.nom, m.anyo 
        FROM manga m, editorial e 
            WHERE m.editorial = e.cod 
                AND e.nom = 'Shūeisha Inc');


-- 36 - Borra la tabla de criticas, teniendo en cuenta que puede tener datos relacionados con otras tablas.

DROP TABLE criticas CASCADE CONSTRAINTS;


-- 37 - Añade a la tabla de mangas un nuevo campo, critica, con hasta 2 
-- cifras antes de la coma decimal, y una cifra después de la coma, y que sólo 
-- debe aceptar valores entre 0 y 10.

ALTER TABLE manga
    ADD critica NUMBER(3,1);

ALTER TABLE manga
    ADD CONSTRAINT ck_manga_critica CHECK(critica BETWEEN 0 AND 10);


-- 38 - Haz de que todos los mangas tengan crítica nula y, posteriormente añade con 
-- una sola sentencia y sin usar el código de la editorial directamente (es decir cruzando datos), que todos los 
-- mangas de la editorial "Shūeisha Inc" tengan un 9.9.

UPDATE manga SET critica = NULL;

UPDATE manga SET critica = 9.9
    WHERE editorial = 
        (SELECT cod FROM editorial WHERE nom = 'Shūeisha Inc');


-- 39 - Muestra nombre y número de capitulos por los mangas que tienen más capitulos que la 
-- media de capitulos del resto de editoriales.

SELECT m.nom, m.caps 
    FROM manga m
        WHERE  
			m.editorial IS NOT NULL
			AND m.caps > ALL
        (   
            SELECT AVG(m2.caps)
                FROM manga m2
                    WHERE 
						m2.editorial IS NOT NULL
						AND m.editorial <> m2.editorial
    					GROUP BY m2.editorial
        );


-- 40 - Muestra el nombre de cada manga y el numero de capitulos, 
-- para los mangas que tengan menos de la mitad de la media de su plataforma.

SELECT nom,  caps 
    FROM manga m
        WHERE caps <
        (
            SELECT AVG(caps)/2 
                FROM manga m2
                    WHERE m.editorial = m2.editorial
        );
