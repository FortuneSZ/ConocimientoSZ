"use strict";

import { CategoriesService } from "./classes/categories-service.js";
import { ProductsService } from "./classes/products-service.js";

const categoriesService = new CategoriesService();
const productsService = new ProductsService();

async function getCategories()
{
    const categorias = await categoriesService.getAll();
    categorias.forEach(element => {addcategory(element)});
}

getCategories();

const NuevoProducto = document.getElementById("newProduct");
const Categoria = document.querySelector('#category');

function addcategory(category)
{
    const CategoriaOpcion = document.createElement("option");
    Categoria.append(CategoriaOpcion);

    CategoriaOpcion.value = category.id;
    CategoriaOpcion.innerText = category.name;
}

const imagen = document.getElementById("imgPreview");

NuevoProducto.image.addEventListener('change', event => 
{
    let file = event.target.files[0];
    let reader = new FileReader();
    if (file) reader.readAsDataURL(file);
    reader.addEventListener('load', e => 
    {
        // Borra la clase d-none del elemento “imgPreview”
        imagen.src = reader.result;
        imagen.classList.remove("d-none");
    });
})

const titulo = NuevoProducto.title;
const descripcion = NuevoProducto.description;
const precio = NuevoProducto.price;
const categoria = NuevoProducto.category;
const image = NuevoProducto.image;
const imageSrc = imagen.src;

NuevoProducto.addEventListener("submit", async (e) => 
{
    e.preventDefault();


const Valid = ValidarFormulario(titulo,descripcion,precio,categoria,image);

if (Valid)
{
    const producto = 
    {
        title: titulo.value,
        description: descripcion.value,
        price: +precio.value,
        category: +categoria.value,
        mainPhoto: imageSrc
    };

    try
    {
        await productsService.post(producto);

        vaciarCampos(NuevoProducto,image,titulo,descripcion,precio,categoria,image);

        location.assign("index.html");

    }
    catch(e)
    {
        alert("Ha habido un error inesperado al insertar el producto")
    }
}});

function ValidarFormulario(titulo,descripcion,precio,categoria,foto)
{
    let Valido = false;
    if (titulo.value == "")
        {
            if (titulo.classList.contains("is-valid") == true)
            {
                titulo.classList.remove("is-valid");
            }
            if (titulo.classList.contains("is-invalid") == false)
            {
                titulo.classList.add("is-invalid");
            }
        }
        else
        {
            if (titulo.classList.contains("is-invalid") == true)
            {
                titulo.classList.remove("is-invalid");
            }
            if (titulo.classList.contains("is-valid") == false)
            {
                titulo.classList.add("is-valid");
            }
        }

        //Comprobación descripción
        if (descripcion.value == "")
        {
            if (descripcion.classList.contains("is-valid") == true)
            {
                descripcion.classList.remove("is-valid");
            }
            if (descripcion.classList.contains("is-invalid") == false)
            {
                descripcion.classList.add("is-invalid");
            }
        }
        else
        {
            if (descripcion.classList.contains("is-invalid") == true)
            {
                descripcion.classList.remove("is-invalid");
            }
            if (descripcion.classList.contains("is-valid") == false)
            {
                descripcion.classList.add("is-valid");
            }
        }

        //Comprobación precio
        if (precio.value > 0)
        {
            if (precio.classList.contains("is-invalid") == true)
            {
                precio.classList.remove("is-invalid");
            }
            if (precio.classList.contains("is-valid") == false)
            {
                precio.classList.add("is-valid");
            }
        }
        else
        {
            if (precio.classList.contains("is-valid") == true)
            {
                precio.classList.remove("is-valid");
            }
            if (precio.classList.contains("is-invalid") == false)
            {
                precio.classList.add("is-invalid");
            }
        }

        //Comprobación categoría
        if (categoria.value == 0)
        {
            if (categoria.classList.contains("is-valid") == true)
            {
                categoria.classList.remove("is-valid");
            }
            if (categoria.classList.contains("is-invalid") == false)
            {
                categoria.classList.add("is-invalid");
            }
        }
        else
        {
            if (categoria.classList.contains("is-invalid") == true)
            {
                categoria.classList.remove("is-invalid");
            }
            if (categoria.classList.contains("is-valid") == false)
            {
                categoria.classList.add("is-valid");
            }
        }
        
        //Comprobación foto
        if (foto.getAttribute("src") == "")
        {
            if (foto.classList.contains("is-valid") == true)
            {
                foto.classList.remove("is-valid");
            }
            if (foto.classList.contains("is-invalid") == false)
            {
                foto.classList.add("is-invalid");
            }
        }
        else
        {
            if (foto.classList.contains("is-invalid") == true)
            {
                foto.classList.remove("is-invalid");
            }
            if (foto.classList.contains("is-valid") == false)
            {
                foto.classList.add("is-valid");
            }
        }

        if (titulo.classList.contains("is-valid") && descripcion.classList.contains("is-valid") &&
        precio.classList.contains("is-valid") && categoria.classList.contains("is-valid") 
        && foto.classList.contains("is-valid"))
        {
            Valido = true;
        }

        return Valido;
}

function vaciarCampos(NuevoProducto,image,titulo,descripcion,precio,categoria)
{
    NuevoProducto.reset();
    image.classList.add("d-none");
    image.src = "";

    titulo.classList.remove("is-valid");
    descripcion.classList.remove("is-valid");
    precio.classList.remove("is-valid");
    categoria.classList.remove("is-valid");
    image.classList.remove("is-valid");

}
