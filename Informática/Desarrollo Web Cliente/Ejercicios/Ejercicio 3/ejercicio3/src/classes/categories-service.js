"use strict";

import { Http } from './http.js';
import { SERVER } from '../constants.js';

export class CategoriesService {
    #http = new Http();

    async getAll() {
        const resp = await this.#http.get(`${SERVER}/categories`);
        return resp.categories;
    }
}