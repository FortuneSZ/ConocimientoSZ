"use strict";

import { Http } from './http.js';
import { SERVER } from '../constants.js';

export class ProductsService {
    // Se declara el atributo privado
    #http = new Http();

    async getAll() {
        const resp = await this.#http.get(`${SERVER}/products`);
        return resp.products;
    }

    async post(producto) {
        const resp = await this.#http.post(`${SERVER}/products`, producto);
        return resp.product;
    }

    async delete(id) {
        await this.#http.delete(`${SERVER}/products/${id}`);
    }
}