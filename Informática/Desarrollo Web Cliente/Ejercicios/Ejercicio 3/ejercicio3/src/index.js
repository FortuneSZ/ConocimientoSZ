"use strict";

import { ProductsService } from "./classes/products-service.js";

const productsService = new ProductsService();

// array global para almacenar los productos
const productsArray = [];

async function getProducts()
{
    const products = await productsService.getAll();
    products.forEach((p) => productsArray.push(p));
    showProducts(productsArray);
}

getProducts();

const priceFormatter = new Intl.NumberFormat("es-ES", {
    style: "currency",
    currency: "EUR",
  });

function showProducts(productsArray) {
  
    const productsContainer = document.querySelector("#productsContainer");

    // Elimina el contenido dentro de productsContainer
    while (productsContainer.firstChild) {
        productsContainer.removeChild(productsContainer.firstChild);
    }

    // Se recorren los productos y muestra en la página
    productsArray.forEach((product) => {
        const divProduct = document.createElement("div");
        divProduct.classList.add("col");
        productsContainer.append(divProduct);

        const divCard = document.createElement("div");
        divCard.classList.add("card");
        divCard.classList.add("h-100");
        divCard.classList.add("shadow");
        divProduct.append(divCard);
        
        const img = document.createElement("img");
        img.src = product.mainPhoto;
        img.classList.add("card-img-top");
        divCard.append(img);
        
        const divBody = document.createElement("div");
        divBody.classList.add("card-body");
        divCard.append(divBody);

        const btnDelete = document.createElement("button");
        btnDelete.classList.add("btn", "btn-danger", "btn-sm", "float-end");
        btnDelete.append("Delete");

        btnDelete.dataset.id = product.id;
        divBody.append(btnDelete);

        btnDelete.addEventListener("click", deleteProduct);

        const cardTitle = document.createElement("h5");
        cardTitle.classList.add("card-title");
        cardTitle.textContent = product.title;
        divBody.append(cardTitle);

        const cardText = document.createElement("p");
        cardText.classList.add("card-text");
        cardText.textContent = product.description;
        divBody.append(cardText);

        const divFooter = document.createElement("div");
        divFooter.classList.add("card-footer");
        divFooter.classList.add("bg-transparent");
        divFooter.classList.add("text-muted");
        divCard.append(divFooter);

        const divFooterRow = document.createElement("div");
        divFooterRow.classList.add("row");
        divFooter.append(divFooterRow);

        const divCategory = document.createElement("div");
        divCategory.classList.add("col");

        let categoryText;

        switch (product.category.id) {
        case "1":
            categoryText = "Electronics";
            break;
        case "2":
            categoryText = "Motor and vehicles";
            break;
        case "3":
            categoryText = "Sports and hobbies";
            break;
        case "4":
            categoryText = "Consoles and videogames";
            break;
        case "5":
            categoryText = "Books, movies and music";
            break;
        case "6":
            categoryText = "Fashion";
            break;
        case "7":
            categoryText = "Kids and babies";
            break;
        case "8":
            categoryText = "Real state";
            break;
        case "9":
            categoryText = "Home appliances";
            break;
        case "10":
            categoryText = "Other";
            break;
        }

        divCategory.textContent = categoryText;
        divFooterRow.append(divCategory);


        const divPrecio = document.createElement("div");
        divPrecio.classList.add("col");
        divPrecio.classList.add("text-end");
        divPrecio.textContent = priceFormatter.format(product.price);
        divFooterRow.append(divPrecio);
    });
}

async function deleteProduct() {
    try 
    {
        await productsService.delete(this.dataset.id);
        this.closest("divProduct").remove();
    } 
    catch (e) 
    {
        console.error(e);
    }
}

// Haría #search pero no consigo cargar los productos :(



