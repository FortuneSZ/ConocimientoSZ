"use strict";

let titulo = document.getElementById("title");
let clasesTitulo = titulo.classList;
let reg = /[a-z]{4}/gi;
let descripcion = document.getElementById("description");
let clasesDesc = descripcion.classList;
let precio = document.getElementById("price");
let clasesPrecio = precio.classList;
let categoria = document.getElementById("category");
let clasesCat = categoria.classList;
let foto = document.getElementById("imgPreview");
let clasesFot =foto.classList;
let imgPreview = document.getElementById("imgPreview");

const newProdForm = document.getElementById("newProduct");
// Aquí va tu código (sólo puedes modificar este archivo)
newProdForm.image.addEventListener('change', event => 
{
    let file = event.target.files[0];
    let reader = new FileReader();
    if (file) reader.readAsDataURL(file);
    reader.addEventListener('load', e => 
    {
        // Borra la clase d-none del elemento “imgPreview”
        imgPreview.src = reader.result;
        imgPreview.classList.remove("d-none");
    });
});

newProdForm.addEventListener("submit",async e =>
    {
        e.preventDefault();
        let opcion = document.getElementsByTagName("option")[categoria.value].innerText;
        //Comprobación título
        if (reg.test(titulo.value) == false)
        {
            if (clasesTitulo.contains("is-valid") == true)
            {
                clasesTitulo.remove("is-valid");
            }
            if (clasesTitulo.contains("is-invalid") == false)
            {
                clasesTitulo.add("is-invalid");
            }
        }
        else
        {
            if (clasesTitulo.contains("is-invalid") == true)
            {
                clasesTitulo.remove("is-invalid");
            }
            if (clasesTitulo.contains("is-valid") == false)
            {
                clasesTitulo.add("is-valid");
            }
        }

        //Comprobación descripción
        if (descripcion.value == "")
        {
            if (clasesDesc.contains("is-valid") == true)
            {
                clasesDesc.remove("is-valid");
            }
            if (clasesDesc.contains("is-invalid") == false)
            {
                clasesDesc.add("is-invalid");
            }
        }
        else
        {
            if (clasesDesc.contains("is-invalid") == true)
            {
                clasesDesc.remove("is-invalid");
            }
            if (clasesDesc.contains("is-valid") == false)
            {
                clasesDesc.add("is-valid");
            }
        }

        //Comprobación precio
        if (precio.value > 0)
        {
            if (clasesPrecio.contains("is-invalid") == true)
            {
                clasesPrecio.remove("is-invalid");
            }
            if (clasesPrecio.contains("is-valid") == false)
            {
                clasesPrecio.add("is-valid");
            }
        }
        else
        {
            if (clasesPrecio.contains("is-valid") == true)
            {
                clasesPrecio.remove("is-valid");
            }
            if (clasesPrecio.contains("is-invalid") == false)
            {
                clasesPrecio.add("is-invalid");
            }
        }

        //Comprobación categoría
        if (categoria.value == 0)
        {
            if (clasesCat.contains("is-valid") == true)
            {
                clasesCat.remove("is-valid");
            }
            if (clasesCat.contains("is-invalid") == false)
            {
                clasesCat.add("is-invalid");
            }
        }
        else
        {
            if (clasesCat.contains("is-invalid") == true)
            {
                clasesCat.remove("is-invalid");
            }
            if (clasesCat.contains("is-valid") == false)
            {
                clasesCat.add("is-valid");
            }
        }
        
        //Comprobación foto
        if (foto.getAttribute("src") == "")
        {
            if (clasesFot.contains("is-valid") == true)
            {
                clasesFot.remove("is-valid");
            }
            if (clasesFot.contains("is-invalid") == false)
            {
                clasesFot.add("is-invalid");
            }
        }
        else
        {
            if (clasesFot.contains("is-invalid") == true)
            {
                clasesFot.remove("is-invalid");
            }
            if (clasesFot.contains("is-valid") == false)
            {
                clasesFot.add("is-valid");
            }
        }

        if (clasesTitulo.contains("is-valid") && clasesDesc.contains("is-valid") &&
        clasesPrecio.contains("is-valid") && clasesCat.contains("is-valid") && clasesFot.contains("is-valid"))
        {
            let productos = document.getElementById("productsContainer");

            let producto = document.createElement("div");
            producto.classList.add("col");

            let div1 = document.createElement("div");
            div1.classList.add("card");
            div1.classList.add("h-100");
            div1.classList.add("shadow");

            let imagen = document.createElement("img");
            imagen.classList.add("card-img-top");
            imagen.setAttribute("src",foto.src);

            let div2 = document.createElement("div");
            div2.classList.add("card-body");

            let h5 = document.createElement("h5");
            h5.classList.add("card-title");
            h5.innerText = titulo.value;

            let p = document.createElement("p");
            p.classList.add("card-text");
            p.innerText = descripcion.value;

            let div3 = document.createElement("div");
            div3.classList.add("card-footer");
            div3.classList.add("bg-transparent");
            div3.classList.add("text-muted");

            let div4 = document.createElement("div");
            div4.classList.add("row");

            let div5 = document.createElement("div");
            div5.classList.add("col");
            div5.innerText = opcion;

            let div6 = document.createElement("div");
            div6.classList.add("col");
            div6.classList.add("text-end");
            div6.innerText = precio.value + " €";

            div4.appendChild(div5);
            div4.appendChild(div6);

            div3.appendChild(div4);

            div2.appendChild(h5);
            div2.appendChild(p);

            div1.appendChild(imagen);
            div1.appendChild(div2);
            div1.appendChild(div3);

            producto.appendChild(div1);

            productos.appendChild(producto);

            newProdForm.reset();
            imgPreview.classList.add("d-none");
            clasesTitulo.remove("is-valid");
            clasesCat.remove("is-valid");
            clasesDesc.remove("is-valid");
            clasesFot.remove("is-valid");
            clasesPrecio.remove("is-valid");
        }
    }
);

   