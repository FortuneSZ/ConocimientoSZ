/**
Parte 1
Crea una función que reciba 2 cadenas de texto. La segunda cadena debe contener solo una letra.
Debe devolver la cantidad de veces que esa letra (segundo parámetro) está incluida en la cadena (primer parámetro).
No debe diferenciar entre letras mayúsculas y minúsculas.
Comprueba que ambos parámetros sean cadenas de texto y que la segunda cadena tenga solo 1 carácter. Si hay un error, imprime un mensaje y devuelve -1.
Ejemplo: timesChar("Característica", "c") -> 3
*/
console.log("EJERCICIO 1 - Apartado 1");

function timesChar(palabra,letra)
{
    if (letra.length == 1)
    {
        let veces = 0;
        for (let i = 0; i < palabra.length; i++)
        {
            if (palabra.toLocaleLowerCase().charAt(i) == letra.toLocaleLowerCase())
            {
                veces++;
            }
        }
        console.log("El caracter " + letra + " se encuentra " + veces + " veces en la palabra " + palabra);
    }
    else
    {
        console.log("Error");
        return -1;
    }
    
}

let palabra = prompt("Escribe una palabra");
let letra = prompt("Escribe una letra");
timesChar(palabra,letra);

/**
Parte 2
Crea un array de cadenas de texto.
Filtra el array para incluir solo las cadenas de texto que tengan al menos 5 caracteres.
Transforma todas las cadenas de texto en el array filtrado a MAYÚSCULAS.
Imprime el array resultante, utilizando ";" como separador.
¡No utilices bucles tradicionales! (while, for, ...)
 */
console.log("EJERCICIO 1 - Apartado 2");
let a = new Array("pollo","murcia","perro","si","no","fran","alejandro");
let a2 = new Array();
a.forEach((palabra) => {if (palabra.length >= 5){a2.push(palabra)}});
a2 = a2.map(palabra => palabra.toLocaleUpperCase());
console.log(a2.join(";"));


/**
Parte 3
Crea una función que reciba 3 parámetros con valores predeterminados (producto -> "Producto genérico",
precio -> 100, impuesto -> 21). Transforma el nombre del producto a cadena de texto y los otros 2 parámetros a número.
Si el precio o el impuesto no se pueden convertir a número (NaN), muestra un error.
Finalmente, imprime el producto recibido y el precio final (incluyendo impuestos).
Llama a esta función varias veces, omitiendo parámetros o enviando valores que no sean numéricos.
 */
console.log("EJERCICIO 1 - Apartado 3");

function productos(producto = "producto genérico",precio = 100, impuesto = 21)
{
    producto = String(producto);
    precio = parseInt(precio);
    impuesto = parseInt(impuesto);

    if (isNaN(precio) || isNaN(impuesto))
    {
        console.log("Error, valores no numéricos");
    }
    else
    {
        let precioImpuesto = (precio / 100) * impuesto;
        let precioFinal = precio + precioImpuesto;
        console.log("producto: " + producto + ", Precio final: " + precioFinal);
    }
}
productos();
productos("",10,21);
productos("pollo","5",21);
productos("pescado","si",2);
productos("carne",23,"No");
productos(5);

/**
Parte 4
Crea un array con 4 valores y realiza lo siguiente (utiliza los métodos de array adecuados).
Agrega 2 elementos al principio.
Agrega 2 más al final.
Elimina las posiciones 3, 4 y 5.
Inserta 2 elementos antes del último elemento.
En cada cambio, muestra el array resultante con sus elementos separados por '=>' (no uses ningún bucle).
 */
console.log("EJERCICIO 1 - Apartado 4");

let num = new Array(1,2,3,4);
console.log(num.join("=>"))
num.unshift(0,7);
console.log(num.join("=>"))
num.push(5,6)
console.log(num.join("=>"))
num.splice(2,3);
console.log(num.join("=>"))
num.splice(num.length-1,0,9);
console.log(num.join("=>"))

/**
Parte 5
Crea un array con varias cadenas de texto. Utilizando el método reduce, devuelve una cadena de texto
que sea una concatenación de la primera letra de cada cadena en el array.
 */

console.log("EJERCICIO 1 - Apartado 5");

let palabras = new Array("Perro","Original","Llama","Lugar","Ojo");
console.log(palabras.reduce(((palabraFinal,palabra) => palabraFinal + palabra.charAt(0)),""));


/**
Parte 6
Crea un array con varias cadenas de texto. Utilizando el método reduce, devuelve suma de las longitudes de dichas cadenas.
 */
console.log("EJERCICIO 1 - Apartado 6");

let palabras2 = new Array("Kirby","Murcia","Sara","Fran","Amor");
console.log(palabras2.reduce(((SumaLongitudes,palabra) => SumaLongitudes + palabra.length),0));

/**
Parte 7
Crea una función que reciba un array y sume los tres primeros números del array.
Utiliza la destructuración de arrays (en los parámetros de la función) para obtener esos tres números.
Si alguno de esos números no está presente en el array, se asignará un valor predeterminado de 0.
Devuelve el resultado de sumar esos tres números.
 */
console.log("EJERCICIO 1 - Apartado 7");

function SumaNumeros(numeros)
{
    let [num1 = 0,num2 = 0,num3 = 0] = numeros;
    let Suma = num1 + num2 + num3;
    console.log(Suma);
}

let numeros = new Array(25,78,14,26,98);
SumaNumeros(numeros);

/**
Parte 8
Crea una función que pueda recibir una cantidad de números indefinida como parámetros. Utiliza el rest para agruparlos en
un array e imprime por separado los números pares y los números impares.
NO uses bucles (for, while, etc.).
Llama a esta función varias veces con diferente cantidad de valores.
 */
console.log("EJERCICIO 1 - Apartado 8");

function ParInpar(...numeros)
{
    let pares = [];
    let inpares = [];
    numeros.forEach((numero) => {if (numero % 2 == 0) { pares.push(numero)} else { inpares.push(numero)}});
    console.log("Hay " + pares.length + " números pares, y son " + pares.join(","));
    console.log("Hay " + inpares.length + " números pares, y son " + inpares.join(","));
    console.log("------------------------------------------------");
}

ParInpar(1,2,3,4,5,6,7,8,9,10);
ParInpar(2,56,13);
ParInpar(6,2,23,98,12);
/**
Parte 9
Crea un objeto Map. La clave será el nombre de un estudiante y el valor será un array con todas sus calificaciones en exámenes.
Itera a través del Map y muestra el nombre de cada estudiante, las calificaciones separadas por '-' y el promedio de calificaciones (con 2 decimales).
Ejemplo: Pedro (7.60 - 2.50 - 6.25 - 9.00). Promedio: 6.34
 */
console.log("EJERCICIO 1 - Apartado 9");
let estudiantes = new Map();
estudiantes.set("Fran",[10,7,8,9,8.5]);
estudiantes.set("Paco",[4,3,6,7,1]);
estudiantes.set("Pedro",[7,5,9,4,6]);

function Sumar(num)
{
    let suma = 0;
    for (let i = 0; i < num.length; i++)
    {
        suma = suma + num[i];
    }
    return suma;
};

for (let [estudiante, notas] of estudiantes)
{
    console.log(estudiante + " (" + notas.join("-") + "). " + "Promedio: " + Sumar(notas)/notas.length);
}


/**
Parte 10
Crea una función que reciba un array, elimine sus valores duplicados e imprima los valores únicos.
Crea un objeto Set a partir del array para eliminar los valores duplicados.
 */
console.log("EJERCICIO 1 - Apartado 10");

let numbers = new Array(1,2,3,4,5,6,7,8,9,7,5,3,1,7,3,8,9,3);
 let set = new Set();

 for(numeros of numbers)
 {
    set.add(numeros);
 }

 console.log("Valores únicos en el set")
 for (num of set)
 {
    console.log(num)
 }


