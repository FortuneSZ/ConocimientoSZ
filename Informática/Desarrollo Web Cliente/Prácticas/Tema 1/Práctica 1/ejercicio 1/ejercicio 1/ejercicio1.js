/**
 * Parte 1
 * Crea una función que tome una cadena como entrada y compruebe si es un palíndromo (es igual cuando se invierte).
 * Haz esto sin usar bucles (puedes usar Array.from para convertir una cadena en un array).
 * Comprueba que el tipo del parámetro es "string" y que la longitud es al menos 1 o muestra un error.
 * Ejemplo: esPalindromo("abeceba") -> true
 */

console.log("EJERCICIO 1 - PARTE 1");

let palabra = prompt("Escribe una palabra");
esPalindromo(palabra);

function esPalindromo(palabra)
{
    let tipo = typeof palabra;
    if (tipo === "string" && palabra.length >= 1)
    {
        let letras = Array.from(palabra);
        letras.reverse();
        palabra2 = letras.join("");

        if (palabra === palabra2)
        {
            console.log("Es palíndromo");
        }
        else
        {
            console.log("No es palíndromo");
        }
    }
    else
    {
        console.log("Error, formato incorrecto")
    }
}

/**
 * Parte 2
 * Desarrolla una función que comprima una cadena reemplazando caracteres repetitivos consecutivos con
 * el carácter y el número de repeticiones. Por ejemplo, "AAAABBBCC" se convertiría en "4A3B2C".
 * Ejemplo: stringCompression("GGGHHRRRRRRRUIIIOOOO") -> 3G2H7R1U3I4O
 */

console.log("EJERCICIO 1 - PARTE 2");

function stringCompression()
{
    let frase = document.getElementById('texto').value.trim().toLowerCase().split('');

    let vecesLetra = {};

    frase.forEach((letra) => {vecesLetra[letra] = (vecesLetra[letra] || 0) +1;});

    for(let letra in vecesLetra)
    {
        text = `${vecesLetra[letra]}${letra.toUpperCase()}`;
        document.getElementById('resultado').innerHTML += text;
    }
}

function contar() {
    // Capturas el valor del input, lo limpas de espacios al inicio y al final y luego haces un arreglo
    let texto = document.getElementById('texto').
    value.trim().toLowerCase().split('');
    
    // Creamos un objeto que será el que almacene las repeticiones
    const repeticiones = {};
    
    // Ciclamos el texto del input y verificamos si existe y sumamos 1, de no existir siempre valdrá 1
    texto.forEach( ( letra ) => {
      repeticiones[ letra ] = ( repeticiones[ letra ] || 0 ) + 1;
    });
    
    // Ahora ciclamos el objeto y lo agregamos al resultado
    for( let letra in repeticiones ) {
      const text = `${ letra } = ${ repeticiones[ letra ] }<br>`;
      document.getElementById('resultado').innerHTML += text;
    }
  
  
  }
let input = document.createElement("input");
input.setAttribute("type","text")
input.setAttribute("id","texto")
document.body.append(input);

let frase = document.getElementById('texto').value.trim().toLowerCase().split('');

let button = document.createElement("button");
button.setAttribute("type","button");
button.addEventListener("click",stringCompression)
document.body.append(button);
let but = document.getElementsByTagName("button");
but.innerHTML = "Validar";

let div = document.createElement("div");
div.setAttribute("id","resultado")
document.body.append(div);


/**
 * Parte 3
 * Crea una función que tome un array de números que contenga valores duplicados. Debería devolver el
 * primer número que se repite en el array, o -1 si no hay duplicados.
 * No uses bucles, y si no sabes cómo hacerlo sin bucles, solo puedes usar un bucle
 * (.forEach cuenta como un bucle).
 * Ejemplo: encuentraRepetido([1,4,7,3,8,7,4,5,5,1]) -> 7 (se repite antes que el 4)
 */

console.log("EJERCICIO 1 - PARTE 3");

function encuentraRepetido(numeros)
{
    let numerosRep = new Array(0);
    let numeros2 = numeros;
    numeros2.sort();
    for (let i = 0; i < numeros.length;i++)
    {
        if (i < numeros2.length && numeros2[i] == numeros2[i+1])
        {
            numerosRep.push(numeros2[i])
        }
    }

    if (numerosRep.length != 0)
    {
        for(let i = 0; i <numeros.length; i++)
        {
            for (let x = 0; x < numerosRep.length;x++)
            {
                if (numeros[i] == numerosRep[x])
                {
                    return numeros[i];
                }
            }
        }
    }
    else
    {
        return -1;
    }
}

let numeros = new Array(1,4,7,3,8,7,4,5,5,1);
let numeros2 = new Array(1,4,7,3,8,7,4,5,5);

console.log(encuentraRepetido(numeros));
console.log(encuentraRepetido(numeros2));

/**
 * Parte 4
 * Crea una función que tome un array de cadenas como primer parámetro y una cadena como segundo.
 * Debería devolver un nuevo array que contenga las palabras del primer array cuyas letras estén todas presentes
 * en la segunda cadena. Intenta no usar bucles a no ser que no sepas hacerlo de otra manera.
 * Ejemplo: fitraPalabras(["house", "car", "watch", "table"], "catboulerham") -> ['car', 'table']
 */

function fitraPalabras(palabras,word)
{
    let filtarray = new Array(0);
    let found = true;
    for(let palabra of palabras)
    {
        found = true;
        for(let letra of palabra)
        {
            if(word.indexOf(letra) == -1)
            {
                found = false;
                break;
            }
        }
        if (found != false)
        {
            filtarray.push(palabra);
        }
    }

    for(let palabra of filtarray)
    {
        console.log(palabra);
    }
}

console.log("EJERCICIO 1 - PARTE 4");
let palabras = new Array("house", "car", "watch", "table");
let word = "catboulerham";
fitraPalabras(palabras,word);

/**
 * Parte 5
 * Crea una función que tome un array de luces representadas por los caracteres '🔴' y '🟢'.
 * La función debe comprobar si las luces están alternando (por ejemplo, ['🔴', '🟢', '🔴', '🟢', '🔴']).
 * Devuelve el número mínimo de luces que necesitan ser cambiadas para que las luces alternen.
 * Ejemplo: ajustaLuces(['🔴', '🔴', '🟢', '🔴', '🟢'])  -> 1 (cambia la primera luz a verde)
 */

function ajustaLuces(luces)
{
    console.log("Luces")
    console.log(luces.join(""))
    for(let i = 0; i < luces.length; i++)
    {
        if (i == 0 && luces.length >= 3)
        {
            if (luces[i] == luces[i+1] && luces[i] != luces[i+2])
            {
                if (luces[i] == '🔴')
                {
                    console.log(`Cambia la ${i+1} luz a verde`);
                    luces[i] = '🟢';

                }
                else
                {
                    console.log(`Cambia la ${i+1} luz a rojo`);
                    luces[i] = '🔴';
                }
            }
        }
        else
        {
            if (luces[i] == luces[i-1])
            {
                if (luces[i] == '🔴')
                {
                    console.log(`Cambia la ${i+1} luz a verde`);
                    luces[i] = '🟢';
                }
                else
                {
                    console.log(`Cambia la ${i+1} luz a rojo`);
                    luces[i] = '🔴';
                }
                
            }
        }
    }
    console.log("Luces arregladas")
    console.log(luces.join(""))
}
console.log("EJERCICIO 1 - PARTE 5");
let luces = new Array('🔴', '🔴', '🟢', '🔴', '🟢');
let luces2 = new Array('🔴', '🟢', '🟢', '🔴', '🟢');
ajustaLuces(luces);
ajustaLuces(luces2);
/**
 * Parte 6
 * Crea una colección Map donde la clave es el nombre de un plato y el valor es un array de ingredientes.
 * Realiza el código para crear otro Map donde la clave sea el nombre del ingrediente y el valor sea el array de
 * platos donde aparece ese ingrediente.
 */

function ingredientesEnPlatos(ingrediente,platos)
{
    let platosIng = new Array(0);
    for(let [nomPlat,ingList] of platos)
    {
        for(let ing of ingList)
        {
            if (ingrediente == ing)
            {
                platosIng.push(nomPlat);
                break;
            }
        }
    }
    return platosIng;
}

console.log("EJERCICIO 1 - PARTE 6");
let platos = new Map();
let ingredientesMap = new Map();
let ingredientes = new Array("masa","queso","tomate","aceite","patata","huevo","jamón york")
platos.set("Pizza 4 quesos",["masa","queso","tomate"]);
platos.set("Tortilla de patatas",["patata","huevo","aceite"]);
platos.set("Pizza jamón york y queso",["masa","queso","jamón york"]);
platos.set("Patatas fritas con huevo",["patata","huevo","aceite"]);

for(let ingrediente of ingredientes)
{
    ingredientesMap.set(ingrediente,ingredientesEnPlatos(ingrediente,platos))
}

console.log(ingredientesMap);


/**
 * Parte 7
 * Crea una función que pueda recibir tantos números como quieras por parámetro. Utiliza rest para agruparlos en
 * un array e imprimir los que son pares y los que son impares por separado.
 * NO uses bucles (for, while, etc.)
 */

function REstfunct(...numeros)
{
    console.log(numeros);
    let pares = new Array(numeros.filter(num => num % 2 == 0));
    console.log(pares);

    let impares = new Array(numeros.filter(num => num % 2 != 0));
    console.log(impares);
}

console.log("EJERCICIO 1 - PARTE 7");
REstfunct(1,2,3,4,5,6,7,8,9,10);

/**
 * Parte 8
 * Crea una función que reciba un array y sume los primeros tres números del array.
 * Utiliza desestructuración de arrays en los parámetros para obtener esos tres números.
 * Si alguno de esos números no está presente en el array, se asignará un valor predeterminado de 0.
 * Devuelve el resultado de sumar esos tres números.
 */

function desestruct(desnum)
{
    let [num1 =0 ,num2 = 0,num3 = 0] = desnum;
    return num1 + num2 + num3;
}

console.log("EJERCICIO 1 - PARTE 8");
desnum = new Array(20,47,62,1,35,89);
console.log(desestruct(desnum));
/**
 * Crea una función que tome un número indeterminado de cadenas como argumentos,
 * las agrupa en un array y devuelve un nuevo array que contiene la longitud de cada cadena.
 * No uses bucles.
 * Ejemplo: stringLenghts("potato", "milk", "car", "table") -> [6, 4, 3, 5]
 */

function stringLenghts(...cadenas)
{
    cadenasLenght = new Array(0);
    cadenas.forEach(cadena => cadenasLenght.push(cadena.length));
    return cadenasLenght;
}

console.log("EJERCICIO 1 - PARTE 9");

console.log(stringLenghts("pollo","murcia","Alfonso","Sara","Fran"));
/**
 * Parte 10
 * Crea un array y, sin modificarlo, genera los siguientes arrays derivados (cada nuevo array deriva del anterior):
 * - Agrega 2 elementos al principio del array
 * - Elimina las posiciones 4 y 5
 * - Concatena los elementos de otro array al final Muestra el array resultante después de cada operación.
 * Ninguna operación realizada debe modificar el array sobre el que opera. Muestra el array original al final.
 */

console.log("EJERCICIO 1 - PARTE 10");
array = new Array(1,2,3,4,5,6,7,8,9,10);
console.log(array);
array2 = [-1,0,...array];
console.log(array2);
let [v1,v2,v3,,,v6,v7,v8,v9,v10] = array;
array3 = [v1,v2,v3,v6,v7,v8,v9,v10];
console.log(array3);
array4 = [...array,...array2];
console.log(array4);
console.log(array);