import { RestaurantService } from "./restaurant-service.class.js";

let restaurants = [];
const restaurantService = new RestaurantService();
const placesContainer = document.getElementById("placesContainer");
const searchInput = document.getElementById("search");
const restTemplate = document.getElementById("restaurantTemplate");

const WEEKDAYS = ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"];

async function getrestaurants() {
  restaurants = await restaurantService.getAll();
  showRestaurants(restaurants);
}

function showRestaurants(restaurants) {
  placesContainer.replaceChildren(
    ...restaurants.map((e) => restaurant2HTML(e))
  );
}

function restaurant2HTML(restaurant) {
  let col = restTemplate.content.cloneNode(true).firstElementChild;
  col.querySelector(".card-img-top").src = restaurant.image;
  col.querySelector(".card-title").textContent = restaurant.name;
  col.querySelector("p.card-text").textContent = restaurant.description;
  col.querySelector("div.card-text .text-muted").append(restaurant.daysOpen.map(d => WEEKDAYS[d]).join(", "));
  if(restaurant.daysOpen.includes(new Date().getDay().toString())) {
    col.querySelector(".badge.bg-danger").remove();
  } else {
    col.querySelector(".badge.bg-success").remove();
  }
  col.querySelector(".phone").append(restaurant.phone);
  col.querySelector(".card-footer small").textContent = restaurant.cuisine;
  col
    .querySelector("button.delete")
    .addEventListener("click", async (e) => {
      let del = confirm("¿Seguro que quieres borrar el restaurante?");
      if (del) {
        try {
          await restaurantService.delete(restaurant.id);
          restaurants = restaurants.filter((r) => r.id !== restaurant.id);
          col.remove();
        } catch (e) {
          alert("¡Error borrando restaurante!");
          console.error(e);
        }
      }
    });

  // let col = document.createElement("div");
  // col.classList.add("col");

  // let card = document.createElement("div");
  // card.classList.add("card", "h-100", "shadow");
  // col.append(card);

  // let img = document.createElement("img");
  // img.classList.add("card-img-top");
  // img.src = restaurant.image;
  // card.append(img);

  // let cardBody = document.createElement("div");
  // cardBody.classList.add("card-body");
  // card.append(cardBody);

  // let delButton = document.createElement("button");
  // delButton.classList.add("btn", "btn-danger", "delete");
  // const delIcon = document.createElement("i");
  // delIcon.classList.add("bi", "bi-trash");
  // delButton.append(delIcon);
  // cardBody.append(delButton);

  // let cardTitle = document.createElement("h4");
  // cardTitle.classList.add("card-title");
  // cardTitle.textContent = restaurant.name;
  // cardBody.append(cardTitle);

  // let cardText = document.createElement("p");
  // cardText.classList.add("card-text");
  // cardText.innerText = restaurant.description;
  // cardBody.append(cardText);

  // let cardDays = document.createElement("div");
  // cardDays.classList.add("card-text");
  // let cardSmallDays = document.createElement("small");
  // cardSmallDays.classList.add("text-muted");
  // let daysTitle = document.createElement("strong");
  // daysTitle.innerText = "Opens: ";
  // cardSmallDays.append(daysTitle);
  // cardSmallDays.append(document.createTextNode(restaurant.daysOpen.map(d => WEEKDAYS[d]).join(", ")));
  // cardDays.append(cardSmallDays);

  // let openBadge = document.createElement("span");
  // openBadge.classList.add("badge", "ms-2");

  // if(restaurant.daysOpen.includes(new Date().getDay().toString())) {
  //     openBadge.innerText = "Open";
  //     openBadge.classList.add("bg-success");
  // } else {
  //     openBadge.innerText = "Closed";
  //     openBadge.classList.add("bg-danger");
  // }
  // cardDays.append(openBadge);
  // cardBody.append(cardDays);

  // let cardPhone = document.createElement("div");
  // cardPhone.classList.add("card-text");
  // let cardSmallPhone = document.createElement("small");
  // cardSmallPhone.classList.add("text-muted");
  // let cardtitlePhone = document.createElement("strong");
  // cardtitlePhone.innerText = "Phone: ";
  // cardSmallPhone.append(cardtitlePhone);
  // cardSmallPhone.append(document.createTextNode(restaurant.phone));
  // cardPhone.append(cardSmallPhone);
  // cardBody.append(cardPhone);

  // let cardFooter = document.createElement("div");
  // cardFooter.classList.add("card-footer");
  // card.append(cardFooter);
  // let cardSmallCuisine = document.createElement("small");
  // cardSmallCuisine.classList.add("text-muted");
  // cardSmallCuisine.innerText = restaurant.cuisine;
  // cardFooter.append(cardSmallCuisine);

  // delButton.addEventListener("click", async e => {
  //     let del = confirm("¿Seguro que quieres borrar el restaurante?");
  //     if (del) {
  //         try {
  //             await restaurantService.delete(restaurant.id);
  //             restaurants = restaurants.filter(r => r.id !== restaurant.id);
  //             col.remove();
  //         } catch (e) {
  //             alert("¡Error borrando restaurante!");
  //             console.error(e);
  //         }
  //     }
  // });

  return col;
}

// Main
getrestaurants();

searchInput.addEventListener("keyup", (e) => {
  const filtered = restaurants.filter(
    (r) =>
      r.name
        .toLocaleLowerCase()
        .includes(searchInput.value.toLocaleLowerCase()) ||
      r.description
        .toLocaleLowerCase()
        .includes(searchInput.value.toLocaleLowerCase())
  );
  showRestaurants(filtered);
});
