"use strict";

function validarNombre(nombre,clasesNombre,nomReg)
{
    if (nomReg.test(nombre.value))
    {
        if (clasesNombre.contains("is-invalid") == true)
        {
            clasesNombre.remove("is-invalid");
        }
        if (clasesNombre.contains("is-valid") == false)
        {
            clasesNombre.add("is-valid");
        }
    }
    else
    {
        if (clasesNombre.contains("is-valid") == true)
        {
            clasesNombre.remove("is-valid");
        }
        if (clasesNombre.contains("is-invalid") == false)
        {
            clasesNombre.add("is-invalid");
        }
    }
}

function validarDesc(descripcion,clasesDesc,descRex)
{
    if (descRex.test(descripcion.value) == false)
    {
        if (clasesDesc.contains("is-valid") == true)
        {
            clasesDesc.remove("is-valid");
        }
        if (clasesDesc.contains("is-invalid") == false)
        {
            clasesDesc.add("is-invalid");
        }
    }
    else
    {
        if (clasesDesc.contains("is-invalid") == true)
        {
            clasesDesc.remove("is-invalid");
        }
        if (clasesDesc.contains("is-valid") == false)
        {
            clasesDesc.add("is-valid");
        }
    }
}

function validarCocina(cocina,clasesCocina,cociRex)
{
    if (cociRex.test(cocina.value) == false)
    {
        if (clasesCocina.contains("is-valid") == true)
        {
            clasesCocina.remove("is-valid");
        }
        if (clasesCocina.contains("is-invalid") == false)
        {
            clasesCocina.add("is-invalid");
        }
    }
    else
    {
        if (clasesCocina.contains("is-invalid") == true)
        {
            clasesCocina.remove("is-invalid");
        }
        if (clasesCocina.contains("is-valid") == false)
        {
            clasesCocina.add("is-valid");
        }
    }
}

function validarDias(lunes,martes,miercoles,jueves,viernes,sabado,domingo,dias)
{
    let errorDias = document.getElementById("daysError");
    if (lunes.checked || martes.checked || miercoles.checked || jueves.checked || viernes.checked || sabado.checked || domingo.checked)
    {
        for(let i = 0; i < dias.length; i++)
        {
            if (dias[i].classList.contains("is-invalid") == true)
            {
                dias[i].classList.remove("is-invalid");
            }
            if (dias[i].classList.contains("is-valid") == false)
            {
                dias[i].classList.add("is-valid");
            }
        }
        
        if (errorDias.classList.contains("d-none") == false)
        {
            errorDias.classList.add("d-none");
        }
    }
    else
    {
        for(let x = 0; x < dias.length; x++)
        {
            if (dias[x].classList.contains("is-valid") == true)
            {
                dias[x].classList.remove("is-valid");
            }
            if (dias[x].classList.contains("is-invalid") == false)
            {
                dias[x].classList.add("is-invalid");
            }
        }
        
        if (errorDias.classList.contains("d-none"))
        {
            errorDias.classList.remove("d-none");
        }
    }
}

function validarTelf(telefono,clasesTelf,telfRex)
{
    if (telfRex.test(telefono.value) == false)
    {
        if (clasesTelf.contains("is-valid") == true)
        {
            clasesTelf.remove("is-valid");
        }
        if (clasesTelf.contains("is-invalid") == false)
        {
            clasesTelf.add("is-invalid");
        }
    }
    else
    {
        if (clasesTelf.contains("is-invalid") == true)
        {
            clasesTelf.remove("is-invalid");
        }
        if (clasesTelf.contains("is-valid") == false)
        {
            clasesTelf.add("is-valid");
        }
    }
}

function reiniciarValidez(clasesNombre,clasesDesc,clasesCocina,clasesLunes,clasesMartes,clasesMiercoles,clasesJueves,
    clasesViernes,clasesSabado,clasesDomingo,clasesTelf,clasesImages)
    {
        clasesNombre.remove("is-valid");
        clasesDesc.remove("is-valid");
        clasesCocina.remove("is-valid");
        clasesLunes.remove("is-valid");
        clasesMartes.remove("is-valid");
        clasesMiercoles.remove("is-valid");
        clasesJueves.remove("is-valid");
        clasesViernes.remove("is-valid");
        clasesSabado.remove("is-valid");
        clasesDomingo.remove("is-valid");
        clasesTelf.remove("is-valid");
        clasesImages.remove("is-valid");
    }

//Nombre
let nombre = document.getElementById("name");
let clasesNombre = nombre.classList;
let nomReg = /^[a-zA-Z]{1}[a-zA-Z|\s]+$/;

//Descripción
let descripcion = document.getElementById("description");
let clasesDesc = descripcion.classList;
let descRex = /^[^\s]+$/

//Cocina
let cocina = document.getElementById("cuisine");
let clasesCocina = cocina.classList;
let cociRex = /^[^\s]+$/

//Días

let lunes = document.getElementById("checkMonday");
let clasesLunes = lunes.classList;

let martes = document.getElementById("checkTuesday");
let clasesMartes = martes.classList;

let miercoles = document.getElementById("checkWednesday");
let clasesMiercoles = miercoles.classList;

let jueves = document.getElementById("checkThursday");
let clasesJueves = jueves.classList;

let viernes = document.getElementById("checkFriday");
let clasesViernes = viernes.classList;

let sabado = document.getElementById("checkSaturday");
let clasesSabado = sabado.classList;

let domingo = document.getElementById("checkSunday");
let clasesDomingo = domingo.classList;

let dias = new Array(lunes,martes,miercoles,jueves,viernes,sabado,domingo);
let nomDias = new Array("dom","lun","mar","mie","jue","vie","sab");


//Teléfono

let telefono = document.getElementById("phone");
let clasesTelf = telefono.classList;
let telfRex = /^[0-9]{9}$/

//Imagen

let imagen = document.getElementById("imgPreview");
let clasesImages = imagen.classList;

//Preview de la foto
const newRestForm = document.getElementById("newRestaurant");
newRestForm.image.addEventListener('change', event => 
{
    let file = event.target.files[0];
    let reader = new FileReader();
    if (file) reader.readAsDataURL(file);
    reader.addEventListener('load', e => 
    {
        // Borra la clase d-none del elemento “imgPreview”
        imagen.src = reader.result;
        clasesImages.remove("d-none");
    });
});

newRestForm.addEventListener("submit",async e =>
{
    e.preventDefault();

    //Validación nombre
    validarNombre(nombre,clasesNombre,nomReg);

    //Validación descripción
    validarDesc(descripcion,clasesDesc,descRex);

    //Validación cocina
    validarCocina(cocina,clasesCocina,cociRex);

    //Validación días
    validarDias(lunes,martes,miercoles,jueves,viernes,sabado,domingo,dias);

    //Validación Teléfono
    validarTelf(telefono,clasesTelf,telfRex);

    if (clasesNombre.contains("is-valid") && clasesDesc.contains("is-valid") && clasesCocina.contains("is-valid")
    && clasesLunes.contains("is-valid") && clasesMartes.contains("is-valid") && clasesMiercoles.contains("is-valid")
    && clasesJueves.contains("is-valid") && clasesViernes.contains("is-valid") && clasesSabado.contains("is-valid")
    && clasesDomingo.contains("is-valid") && clasesTelf.contains("is-valid"))
    {

        let div1 = document.createElement("div");
        div1.classList.add("col");
        let div2 = document.createElement("div");
        div2.classList.add("card");
        div2.classList.add("h-100");
        div2.classList.add("shadow");
        let img = document.createElement("img");
        img.classList.add("card-img-top");
        img.setAttribute("src",imagen.src);
        div2.appendChild(img);
        let div3 = document.createElement("div");
        div3.classList.add("card-body");
        let h4 = document.createElement("h4");
        h4.classList.add("card-title");
        h4.innerText = nombre.value;
        div3.appendChild(h4);
        let p = document.createElement("p");
        p.classList.add("card-text");
        p.innerText = descripcion.value;
        div3.appendChild(p);
        let div4 = document.createElement("div");
        div4.classList.add("card-text");
        let small1 = document.createElement("small");
        small1.classList.add("text-muted");
        let strong = document.createElement("strong");
        strong.innerText = "Días: ";
        small1.appendChild(strong);
        let dias2Set = new Set();
        for(let y = 0; y < dias.length;y++)
        {
            if (dias[y].checked)
            {
                if (dias[y].id == "checkMonday")
                {
                    dias2Set.add("lun");
                }
                else if (dias[y].id == "checkTuesday")
                {
                    dias2Set.add("mar");
                }
                else if (dias[y].id == "checkWednesday")
                {
                    dias2Set.add("mie");
                }
                else if (dias[y].id == "checkThursday")
                {
                    dias2Set.add("jue");
                }
                else if (dias[y].id == "checkFriday")
                {
                    dias2Set.add("vie");
                }
                else if (dias[y].id == "checkSaturday")
                {
                    dias2Set.add("sab");
                }
                else if (dias[y].id == "checkSunday")
                {
                    dias2Set.add("dom");
                }
            }
        }
        let dias2 = Array.from(dias2Set);
        let diasJoin = dias2.join(",");
        let diasString = document.createTextNode(diasJoin);
        small1.appendChild(diasString);
        div4.appendChild(small1);

        let fecha = new Date();
        let day = nomDias[fecha.getDay()];

        let span1 = document.createElement("span"); 
        span1.classList.add("badge");
        span1.classList.add("ms-2");
        span1.classList.add("bg-success");
        span1.innerText = "Abierto";
        let span2 = document.createElement("span"); 
        span2.classList.add("badge");
        span2.classList.add("ms-2");
        span2.classList.add("bg-danger");
        span2.innerText = "Cerrado";

        if (diasJoin.indexOf(day) != -1)
        {
            div4.appendChild(span1);
        }
        else
        {
            div4.appendChild(span2);
        }
        div3.appendChild(div4);
        let div5 = document.createElement("div");
        div5.classList.add("card-text");
        let small2 = document.createElement("small");
        small2.classList.add("text-muted");
        let strong2 = document.createElement("strong");
        strong2.innerText = "Teléfono: ";
        small2.appendChild(strong2);
        let numTelf = document.createTextNode(telefono.value);
        small2.appendChild(numTelf);
        div5.appendChild(small2);
        div3.appendChild(div5);
        let div6 = document.createElement("div");
        div6.classList.add("card-footer");
        let small3 = document.createElement("small");
        small3.classList.add("text-muted");
        small3.innerText = cocina.value;
        div6.appendChild(small3);
        div3.appendChild(div6);
        div2.appendChild(div3);
        div1.appendChild(div2);
        let divContainer = document.getElementById("placesContainer");
        divContainer.appendChild(div1);
        document.getElementById("newRestaurant").reset();
        reiniciarValidez(clasesNombre,clasesDesc,clasesCocina,clasesLunes,clasesMartes,clasesMiercoles,clasesJueves,
            clasesViernes,clasesSabado,clasesDomingo,clasesTelf,clasesImages);
            clasesImages.add("d-none");

            
        /*let template = document.getElementsByTagName("template")[0];
        console.log(template);
        let divs = template.querySelectorAll("div");
        let image = template.querySelector("img");
        let h4 = template.querySelector("h4");
        let p = template.querySelector("p");
        let small = template.querySelectorAll("small");
        let strong = template.querySelectorAll("strong");
        let span = template.querySelectorAll("span");
        let dias2Set = new Set();

        for(let y = 0; y < dias.length;y++)
        {
            if (dias[y].checked)
            {
                if (dias[y].id == "checkMonday")
                {
                    dias2Set.add("lun");
                }
                else if (dias[y].id == "checkTuesday")
                {
                    dias2Set.add("mar");
                }
                else if (dias[y].id == "checkWednesday")
                {
                    dias2Set.add("mie");
                }
                else if (dias[y].id == "checkThursday")
                {
                    dias2Set.add("jue");
                }
                else if (dias[y].id == "checkFriday")
                {
                    dias2Set.add("vie");
                }
                else if (dias[y].id == "checkSaturday")
                {
                    dias2Set.add("sab");
                }
                else if (dias[y].id == "checkSunday")
                {
                    dias2Set.add("dom");
                }
            }
        }
        let dias2 = Array.from(dias2Set);
        console.log(dias2.length);
        let diasJoin = dias2.join(",");
        alert(diasJoin);

        let fecha = new Date();
        let day = nomDias[fecha.getDay()-1];
        console.log(day);

        let div1 = divs[0];
        let div2 = divs[1];
        image.setAttribute("src",imagen.src);
        div2.append(image);
        let div3 = divs[2];
        alert(nombre.value);
        h4.textContent = nombre.value;
        div3.append(h4);
        p.innerText = descripcion;
        div3.append(p);
        let div4 = divs[3];
        let small1 = small[0];
        let strong1 = strong[0];
        strong1.innerText = diasJoin;
        small1.append(strong1);
        div4.append(small1);
        let span1 = span[0];
        let span2 = span[1];

        if (diasJoin.indexOf(day) != -1)
        {
            div4.append(span1);
        }
        else
        {
            div4.append(span2);
        }

        div3.append(div4);
        let div5 = divs[4];
        let small2 = small[1];
        let strong2 = strong[1];
        strong2.innerText = telefono;
        small2.append(strong2);
        div5.append(small2);
        let div6 = divs[5];
        let small3 = small[2];
        small3.innerText = cocina;
        div6.append(small3);
        div3.append(div6);
        div2.append(div3);
        div1.append(div2);
        document.body.append(div1);*/
        
        
        


        /*let clon = temp.content.cloneNode(true);
        document.body.appendChild(clon);*/
        
        
        
    }
})