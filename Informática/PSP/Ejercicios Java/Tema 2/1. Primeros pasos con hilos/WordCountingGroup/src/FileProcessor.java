import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileProcessor implements Runnable {
    private File file;

    public FileProcessor(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        int wordCount=0;
        try (Stream<String> stream= Files.lines(Paths.get(file.getPath()))) {
           wordCount=stream.mapToInt(line->
               line.split("\\s").length
           ).sum();
        } catch (IOException e) {
            System.out.println("Error reading file: " + file.getName());
        }
        System.out.println("File " + file.getName() + " has " + wordCount + " words.");
    }


}
