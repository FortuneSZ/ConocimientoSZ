import java.io.File;

public class Main {
    public static void main(String[] args) {
        File dir = new File("./examples");
        File[] files = dir.listFiles((d, name) -> name.endsWith(".txt"));

        ThreadGroup fileProcessorGroup = new ThreadGroup("FileProcessorGroup");

        for (File file : files) {
            Thread t = new Thread(fileProcessorGroup, new FileProcessor(file));
            t.start();
        }

        while (fileProcessorGroup.activeCount() > 0) {
            try {
                Thread.sleep(100);
                System.out.println("Threre are  "+ fileProcessorGroup.activeCount()+" files pending to be processed" );
            } catch (InterruptedException e) {

            }
        }

        System.out.println("All files have been processed.");
    }
}