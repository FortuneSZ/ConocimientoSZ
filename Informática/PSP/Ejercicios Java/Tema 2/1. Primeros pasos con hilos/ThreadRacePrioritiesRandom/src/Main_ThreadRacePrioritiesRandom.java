

/* Main program */
public class Main_ThreadRacePrioritiesRandom
{
    public static void main(String[] args) 
    {
        RunnerThread tA = new RunnerThread("A", Thread.MAX_PRIORITY);
        RunnerThread tB = new RunnerThread("B", Thread.NORM_PRIORITY);
        RunnerThread tC = new RunnerThread("C", Thread.MIN_PRIORITY);
        
        tA.start();
        tB.start();
        tC.start();
        
        do
        {
            try
            {
                Thread.sleep(100);
            } catch (Exception e) {}
            System.out.print("Thread " + tA.getRunnerName() + ": " + tA.getRunnerNumber());
            System.out.print("\tThread " + tB.getRunnerName() + ": " + tB.getRunnerNumber());
            System.out.println("\tThread " + tC.getRunnerName() + ": " + tC.getRunnerNumber());
        } while (tA.isAlive() || tB.isAlive() || tC.isAlive());
        System.out.println("The race has finished");
    }
    
}
