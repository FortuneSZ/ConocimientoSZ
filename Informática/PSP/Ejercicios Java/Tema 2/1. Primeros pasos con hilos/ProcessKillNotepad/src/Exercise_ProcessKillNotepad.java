
import java.io.*;

public class Exercise_ProcessKillNotepad {

    public static void main(String[] args) 
    {
        String[] cmd = {"C:\\Windows\\notepad.exe"};
        ProcessBuilder pb = new ProcessBuilder(cmd);
        try
        {
            Process p = pb.start();
            Thread.sleep(10000);
            p.destroy();
            BufferedReader br = new BufferedReader(
               new InputStreamReader(p.getInputStream()));
        } catch (Exception e) {
            System.err.println("Exception occured:" + e.getMessage());
        }
    }        
}
