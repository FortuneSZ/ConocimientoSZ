package com.example.my3counters;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class HelloController
{
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;

    @FXML
    protected void onClickCountTo10()
    {
        Thread t1 = new count(label1,1,10,btn1);
        t1.start();
    }

    @FXML
    protected void onClickCountTo5()
    {
        Thread t1 = new count(label2,1,5,btn2);
        t1.start();
    }

    @FXML
    protected void onClickCountTo1()
    {
        Thread t1 = new count(label3,10,1,btn3);
        t1.start();
    }
}