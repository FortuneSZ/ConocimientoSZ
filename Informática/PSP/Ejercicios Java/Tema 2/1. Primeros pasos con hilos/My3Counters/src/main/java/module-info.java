module com.example.my3counters {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.my3counters to javafx.fxml;
    exports com.example.my3counters;
}