/*
 * This program creates 10 threads, each one with its own number, and launches
 * them all to get the multiplication tables of all 10 numbers.
 */
package multiplierthreads;

public class MultiplierThreadsMain
{
    public static void main(String[] args) 
    {
        for (int i = 1; i <= 10; i++)
        {
            Thread t = new MultiplierThread(i);
            t.start();
        }
    }
    
}
