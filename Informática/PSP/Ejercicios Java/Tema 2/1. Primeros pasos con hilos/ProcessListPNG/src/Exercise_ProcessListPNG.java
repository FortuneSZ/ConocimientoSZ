

import java.io.*;
import java.util.Scanner;

public class Exercise_ProcessListPNG {

    public static void main(String[] args) 
    {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Please introduce the chosen path (press Enter to finish):");
        String path = s.nextLine();        
        String line = "";
        Runtime rt = Runtime.getRuntime();
        try
        {
            Process p = rt.exec("find " + path + " -name *.png");
            BufferedReader br = new BufferedReader(
               new InputStreamReader(p.getInputStream()));
            System.out.println("Process output:");
            while ((line = br.readLine()) != null)
            {
                System.out.println(line);
            }
        } catch (Exception e) {
            System.err.println("Exception occured:" + e.getMessage());
        }
    }    
}
