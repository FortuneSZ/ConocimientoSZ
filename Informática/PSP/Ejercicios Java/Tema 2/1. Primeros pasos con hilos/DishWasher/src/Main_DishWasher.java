

public class Main_DishWasher
{
    public static void main(String[] args) 
    {
        // Simulate a process with N = 20 dishes
        DishPile dp = new DishPile();
        Washer w = new Washer(20, dp);
        Dryer d = new Dryer(20, dp);
        w.start();
        d.start();
        
    }
}
