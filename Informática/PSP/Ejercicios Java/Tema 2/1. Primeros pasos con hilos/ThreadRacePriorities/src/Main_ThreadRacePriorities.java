

/* Main program */
public class Main_ThreadRacePriorities
{
    public static void main(String[] args) 
    {
        RunnerThread tA = new RunnerThread("A");
        RunnerThread tB = new RunnerThread("B");
        RunnerThread tC = new RunnerThread("C");
        
        // These are the 3 lines that we only need to add to original project
        tA.setPriority(Thread.MAX_PRIORITY);
        tB.setPriority(Thread.NORM_PRIORITY);
        tC.setPriority(Thread.MIN_PRIORITY);
        
        tA.start();
        tB.start();
        tC.start();
        
        do
        {
            try
            {
                Thread.sleep(100);
            } catch (Exception e) {}
            System.out.print("Thread " + tA.getRunnerName() + ": " + tA.getRunnerNumber());
            System.out.print("\tThread " + tB.getRunnerName() + ": " + tB.getRunnerNumber());
            System.out.println("\tThread " + tC.getRunnerName() + ": " + tC.getRunnerNumber());
        } while (tA.isAlive() || tB.isAlive() || tC.isAlive());
        System.out.println("The race has finished");
    }
    
}
