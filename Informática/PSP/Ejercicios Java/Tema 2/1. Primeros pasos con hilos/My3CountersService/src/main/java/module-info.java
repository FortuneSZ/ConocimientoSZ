module com.example.my3countersservice {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.my3countersservice to javafx.fxml;
    exports com.example.my3countersservice;
}