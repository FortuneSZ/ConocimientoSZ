package com.example.my3countersservice;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class count extends Service
{
    Label number;
    int start;
    int end;
    Button btn;

    public count(Label Number,int start, int end,Button btn)
    {
        this.number = Number;
        this.start = start;
        this.end = end;
        this.btn = btn;
    }

    @Override
    protected Task createTask()
    {
        return new Task()
        {
            @Override
            protected Object call() throws Exception
            {
                btn.setDisable(true);
                if (start < end)
                {
                    for (int i = start; i<= end; i++)
                    {
                        int finalI = i;
                        try
                        {
                            Thread.sleep(1000);
                            Platform.runLater(() ->
                                    number.setText("Counting... " + finalI));
                        }
                        catch (InterruptedException e)
                        {
                            throw new RuntimeException(e);
                        }

                    }
                }
                else
                {
                    for (int i = start; i>= end;i--)
                    {
                        int finalI = i;
                        try
                        {
                            Thread.sleep(1000);
                            Platform.runLater(() ->
                                    number.setText("Counting... " + finalI));
                        }
                        catch (InterruptedException e)
                        {
                            throw new RuntimeException(e);
                        }
                    }
                }
                btn.setDisable(false);
                return null;
            }
        };
    }
}
