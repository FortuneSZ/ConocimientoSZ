public class AnimalConversation<T extends Animal, E extends Animal>
{
    private T animal1;
    private E animal2;

    public AnimalConversation(T animal1, E animal2)
    {
        this.animal1 = animal1;
        this.animal2 = animal2;
    }

    public void chat()
    {
        System.out.println(animal1.talk());
        System.out.println(animal2.talk());
    }
}
