import javax.swing.*;
import java.time.LocalDate;
import java.util.List;

public class Menu implements GetNutritionalValues
{
    private LocalDate date;
    private List<MenuElement> elements;

    /*Constructor*/

    public Menu(LocalDate date, List<MenuElement> elements)
    {
        this.date = date;
        this.elements = elements;
    }

    /*Getters and setters*/

    public LocalDate getDate()
    {
        return date;
    }

    public void setDate(LocalDate date)
    {
        this.date = date;
    }

    public List<MenuElement> getElements()
    {
        return elements;
    }

    public void setElements(List<MenuElement> elements)
    {
        this.elements = elements;
    }

    /*Methods*/

    public double getCalories()
    {
        double Calories = 0;
        return Calories;
    }

    public  double getCarbohydrates()
    {
        double carbohydrates = 0;
        return carbohydrates;
    }

    public double getFat()
    {
        double fat = 0;
        return fat;
    }

    public boolean hasMilk()
    {
        return true;
    }

    public boolean hasNuts()
    {
        return true;
    }

    public boolean hasEggs()
    {
        return true;
    }

    public boolean hasGluten()
    {
        return true;
    }
}
