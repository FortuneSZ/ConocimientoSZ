import java.util.*;

public class AlimentPicker<T extends MenuElement>
{
    private Set<T> aliments = new TreeSet()
    {
        public int compare(T element1, T element2)
        {
            return Double.compare(element1.getCalories(), element2.getCalories());
        }
    };

    public AlimentPicker()
    {

    }

    public void add(T al)
    {
        aliments.add(al);
    }

    public Aliment pickAliment(Double num)
    {
        Aliment alimento = null;
        Iterator<T> it = aliments.iterator();
        while (it.hasNext())
        {
            T aliment = it.next();
            if (aliment instanceof Aliment && aliment.getCalories() > num)
            {
                alimento = (Aliment) aliment;
                break;
            }
        }
        return alimento;
    }
}
