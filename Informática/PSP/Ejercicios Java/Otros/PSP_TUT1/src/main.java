import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class main
{
    public static void main(String[] args)
    {
        ArrayList<MenuElement> elements = new ArrayList<>();
        elements.add(new Aliment("A1","","",6,0,0,true,true,true,true));
        elements.add(new Aliment("A2","","",2,0,0,true,true,true,true));
        elements.add(new Aliment("A3","","",4,0,0,true,true,true,true));
        elements.add(new Aliment("A4","","",89,0,0,true,true,true,true));
        elements.add(new Aliment("A5","","",65,0,0,true,true,true,true));
        elements.add(new Aliment("A6","","",3,0,0,true,true,true,true));
        elements.add(new Aliment("A7","","",56,0,0,true,true,true,true));
        elements.add(new Aliment("A8","","",12,0,0,true,true,true,true));
        elements.add(new Aliment("A9","","",9,0,0,true,true,true,true));
        elements.add(new Aliment("A10","","",98,0,0,true,true,true,true));
        elements.add(new Dish("D1","",new ArrayList<Ingredient>()));
        elements.add(new Dish("D2","",new ArrayList<Ingredient>()));
        elements.add(new Dish("D3","",new ArrayList<Ingredient>()));

        Stream<MenuElement> elementStreamName = elements.stream().sorted(Comparator.comparing(el -> el.name));

        /*elements.sort(new Comparator<MenuElement>()
        {
            @Override
            public int compare(MenuElement o1, MenuElement o2) {
                return o1.name.compareTo(o2.name);
            }
        });*/

        System.out.print("All the menu elements \n");
        elementStreamName.forEach(System.out::print);

        /*for (int i = 0; i < elements.size(); i++)
        {
            System.out.print(elements.get(i));
        }*/

        Stream<MenuElement> elementStreamDesc = elements.stream().sorted(Comparator.comparing(el -> el.description));

        /*elements.sort(new Comparator<MenuElement>()
        {
            @Override
            public int compare(MenuElement o1, MenuElement o2) {
                return o1.description.compareTo(o2.description);
            }
        });*/

        System.out.print("All the Dishes \n");
        elementStreamDesc.forEach(System.out::print);

        /*for (int i = 0; i < elements.size(); i++)
        {
            if (elements.get(i) instanceof Dish)
            {
                System.out.print(elements.get(i));
            }
        }*/
        
        ArrayList<Aliment> Aliments = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++)
        {
            if (elements.get(i) instanceof Aliment)
            {
                Aliments.add((Aliment) elements.get(i));
            }
        }

        Aliments.sort(new Comparator<Aliment>()
        {
            @Override
            public int compare(Aliment o1, Aliment o2)
            {
                return Double.compare(o1.getCalories(),o2.getCalories());
            }
        });

        System.out.print("All the Aliments with nuts\n");
        for (int i = 0; i < Aliments.size(); i++)
        {
            System.out.print(Aliments.get(i));
        }
    }
}
