public class Aliment extends MenuElement implements GetNutritionalValues
{
    private String frequency;
    private double calories;
    private double fat;
    private double carbohydrates;
    private boolean nuts;
    private boolean milk;
    private boolean eggs;
    private boolean gluten;

    /*Constructor*/

    public Aliment(String name, String description, String frequency,
                   double calories, double fat, double carbohydrates,
                   boolean nuts, boolean milk, boolean eggs, boolean gluten)
    {
        this.name = name;
        this.description = description;
        this.frequency = frequency;
        this.calories = calories;
        this.fat = fat;
        this.carbohydrates = carbohydrates;
        this.nuts = nuts;
        this.milk = milk;
        this.eggs = eggs;
        this.gluten = gluten;
    }

    /*Getters and setters*/

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getFrequency()
    {
        return frequency;
    }

    public void setFrequency(String frequency)
    {
        this.frequency = frequency;
    }

    public void setCalories(double calories)
    {
        this.calories = calories;
    }

    public void setFat(double fat)
    {
        this.fat = fat;
    }

    public void setCarbohydrates(double carbohydrates)
    {
        this.carbohydrates = carbohydrates;
    }

    public void setNuts(boolean nuts)
    {
        this.nuts = nuts;
    }

    public void setMilk(boolean milk)
    {
        this.milk = milk;
    }

    public void setEggs(boolean eggs)
    {
        this.eggs = eggs;
    }

    public void setGluten(boolean gluten)
    {
        this.gluten = gluten;
    }

    /*Methods*/

    public double getCalories()
    {
        return this.calories;
    }

    public  double getCarbohydrates()
    {
        return this.carbohydrates;
    }

    public double getFat()
    {
        return this.fat;
    }

    public boolean hasMilk()
    {
        return this.milk;
    }

    public boolean hasNuts()
    {
        return this.nuts;
    }

    public boolean hasEggs()
    {
        return this.eggs;
    }

    public boolean hasGluten()
    {
        return this.gluten;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "frequency: " + frequency +
                ", calories: " + calories +
                ", fat: " + fat +
                ", carbohydrates: " + carbohydrates +
                ", nuts: " + nuts +
                ", milk: " + milk +
                ", eggs: " + eggs +
                ", gluten: " + gluten + "\n";
    }
}
