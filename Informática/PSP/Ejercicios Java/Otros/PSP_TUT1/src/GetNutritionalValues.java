public interface GetNutritionalValues
{
    double getCalories();
    double getCarbohydrates();
    double getFat();
    boolean hasMilk();
    boolean hasNuts();
    boolean hasEggs();
    boolean hasGluten();
}
