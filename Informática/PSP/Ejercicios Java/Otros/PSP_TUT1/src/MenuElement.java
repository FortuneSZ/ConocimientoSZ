public abstract class MenuElement implements GetNutritionalValues
{
    protected String name;
    protected String description;

    @Override
    public String toString() {
        return "name: " + name +
                ", description: " + description;
    }
}
