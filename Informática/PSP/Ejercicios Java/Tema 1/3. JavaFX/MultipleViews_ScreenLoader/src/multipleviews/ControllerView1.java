package multipleviews;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class ControllerView1 {


    @FXML
    private Button button1;

    @FXML
    void goToView1(ActionEvent event) throws IOException {
        ScreenLoader.loadScreen("main.fxml",(Stage) ((Node) event.getSource()).getScene().getWindow());
    }

}
