package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import sample.data.Contact;

import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class Controller implements Initializable
{
    public ListView lstContacts;
    public TextField txtName;
    public TextField txtEmail;
    public TextField txtPhoneNumber;
    public Button btnAdd;
    public Button btnRemove;
    ObservableList<Contact> contacts;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        contacts = FXCollections.observableArrayList(readContacts());
        lstContacts.setItems(contacts);

        lstContacts.getSelectionModel().selectedItemProperty().addListener(
            new ChangeListener<Contact>()
            {
                @Override
                public void changed(ObservableValue<? extends Contact> observable,
                                    Contact oldValue, Contact newValue) {
                    if (newValue != null)
                    {
                        txtName.setText(newValue.getName());
                        txtEmail.setText(newValue.getEmail());
                        txtPhoneNumber.setText(newValue.getPhoneNumber());
                    }
                }
            }
        );
    }

    private List<Contact> readContacts() {

        try {
            List<String> lines = Files.readAllLines(Paths.get("contacts.txt"));
            return lines.stream()
                    .map(line -> new Contact(line.split(":")[0],
                            line.split(":")[1],
                            line.split(":")[2]))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    private void saveContacts() {

        try (PrintWriter pw = new PrintWriter("contacts.txt"))
        {
            contacts.stream()
                    .forEach(contact -> {
                        pw.println(contact.getName() + ":" +
                                contact.getEmail() + ":" +
                                contact.getPhoneNumber());
                    });
        } catch (Exception e) {
        }
    }

    public void addContact(ActionEvent actionEvent)
    {
        if (txtName.getText().isEmpty() || txtEmail.getText().isEmpty() ||
                txtPhoneNumber.getText().isEmpty())
        {
            Alert dialog = new Alert(Alert.AlertType.ERROR);
            dialog.setTitle("Error");
            dialog.setHeaderText("Error adding data");
            dialog.setContentText("No field can be empty");
            dialog.showAndWait();
        } else {
            Contact newContact = new Contact(txtName.getText(),
                    txtEmail.getText(),
                    txtPhoneNumber.getText());
            contacts.add(newContact);
            saveContacts();
            Alert dialog = new Alert(Alert.AlertType.INFORMATION);
            dialog.setTitle("Success");
            dialog.setHeaderText("Adding contacts");
            dialog.setContentText("Contact successfully added");
            dialog.showAndWait();
        }
    }

    public void removeContact(ActionEvent actionEvent)
    {
        int position = lstContacts.getSelectionModel().getSelectedIndex();
        if (position >= 0)
        {
            contacts.remove(position);
            saveContacts();
        }
    }
}
