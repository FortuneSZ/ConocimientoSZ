package calculator;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    private Label label;
    @FXML
    private Label lblNumber1;
    @FXML
    private TextField txtNumber1;
    @FXML
    private Label lblOperation;
    @FXML
    private ChoiceBox<String> comboOperation;
    @FXML
    private Label lblNumber2;
    @FXML
    private TextField txtNumber2;
    @FXML
    private Button btnGo;
    @FXML
    private Label lblResult;
    @FXML
    private TextField txtResult;


    public void initialize()
    {
        comboOperation.setItems(FXCollections.observableArrayList("+", "-", "*", "/"));
        comboOperation.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnGoAction(ActionEvent event)
    {
        int num1, num2, result=0;
        char operation;

        // Get 1st and 2nd numbers
        try
        {
            num1 = Integer.parseInt(txtNumber1.getText());
        } catch (Exception e) {
            num1 = 0;
        }
        try
        {
            num2 = Integer.parseInt(txtNumber2.getText());
        } catch (Exception e) {
            num2 = 0;
        }
        // Get operation
        operation = comboOperation.getSelectionModel().getSelectedItem().charAt(0);
        // Calculate the result
        switch(operation)
        {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
        }
        // Print result in the corresponding text field
        txtResult.setText("" + result);
    }


}
