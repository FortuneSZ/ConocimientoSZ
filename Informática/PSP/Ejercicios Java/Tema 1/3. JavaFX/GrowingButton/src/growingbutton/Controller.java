/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package growingbutton;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.util.Duration;


public class Controller {
    
    @FXML
    private Button btnButton;

    public void initialize()
    {
        // Animation
        KeyFrame kf = new KeyFrame(Duration.millis(10), e ->
        {
           btnButton.setPrefWidth(btnButton.getPrefWidth() + 1);
           btnButton.setPrefHeight(btnButton.getPrefHeight() + 1);
        });
        
        Timeline t = new Timeline(kf);
        t.setCycleCount(200);
        
        // Event to start the animation
        btnButton.setOnMouseEntered(e -> 
        {
            t.play();
        });
        
        // Event to stop the animation
        btnButton.setOnMouseExited(e -> 
        {
            t.stop();
            // Recover original size
            btnButton.setPrefWidth(100);
            btnButton.setPrefHeight(50);
        });
    }    
    
}
