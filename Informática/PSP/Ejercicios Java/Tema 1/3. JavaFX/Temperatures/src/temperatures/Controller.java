package temperatures;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;

public class Controller {

    @FXML
    private BarChart<String, Number> chart;

    public void initialize() {

        String[] categories = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        double[] marks = { 9, 8, 14, 17, 21, 24, 28, 30, 26, 21, 16, 11 };

        chart.setTitle("Temperature average");

        XYChart.Series data = new XYChart.Series();
        data.setName("2017");
        for (int i = 0; i < categories.length; i++)
            data.getData().add(new XYChart.Data(categories[i], marks[i]));

        chart.getData().add(data);
    }


}
