package multipleviews;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {


    @FXML
    private Button view1;

    @FXML
    private Button view2;

    @FXML
    void goToView1(ActionEvent event) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("view1.fxml"));
        Scene view1Scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(view1Scene);
        stage.show();
    }

    @FXML
    void goToView2(ActionEvent event) {

    }
}
