package sample;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable
{
    public MenuItem menuOpen;
    public MenuItem menuSave;
    public MenuItem menuExit;
    public TextArea txtContents;
    public Label lblStatus;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        
    }

    public void openFile(ActionEvent actionEvent)
    {
        try
        {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open File");
            File selectedFile = fileChooser.showOpenDialog(null);

            if (selectedFile != null)
            {
                List<String> lines = Files.readAllLines(selectedFile.toPath());
                for(String line: lines)
                {
                    txtContents.appendText(line + "\n");
                }
                lblStatus.setText(lines.size() + " lines read from file");
            }
        } catch (Exception e) {}
    }

    public void saveFile(ActionEvent actionEvent)
    {
        PrintWriter pw = null;
        try
        {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save file");
            File selectedFile = fileChooser.showSaveDialog(null);

            if (selectedFile != null)
            {
                pw = new PrintWriter(new FileWriter(selectedFile));
                pw.print(txtContents.getText());
                lblStatus.setText("File successfully saved");
            }
        } catch (IOException e) {
            lblStatus.setText("Error saving file");
            e.printStackTrace();
        } finally {
            try
            {
                pw.close();
            } catch (Exception e) {}
        }
    }

    public void exit(ActionEvent actionEvent)
    {
        System.exit(0);
    }
}
