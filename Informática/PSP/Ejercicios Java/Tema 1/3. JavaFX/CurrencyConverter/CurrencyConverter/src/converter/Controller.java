package converter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;

public class Controller {

    private static double EUR_USD = 1.10;
    private static double EUR_GBP = 0.8;
    private static double USD_GBP = 0.7;

    @FXML
    private RadioMenuItem mnuEurUsd;
    @FXML
    private RadioMenuItem mnuEurGbp;
    @FXML
    private RadioMenuItem mnuUsdGbp;
    @FXML
    private RadioMenuItem mnuGbpEur;
    @FXML
    private RadioMenuItem mnuGbpUsd;
    @FXML
    private TextField txtMoney;
    @FXML
    private Label lblResult;
    @FXML
    private RadioMenuItem mnuUsdEur;
    @FXML
    private ToggleGroup radioGroup;

    public void initialize()
    {
    }

    @FXML
    private void txtMoneyKeyReleased(KeyEvent event)
    {
        double number, result;
        try
        {
            number = Double.parseDouble(txtMoney.getText());
        } catch (Exception e) {
            number = 0;
        }

        if (mnuEurGbp.isSelected())
        {
            result = number * EUR_GBP;
            lblResult.setText("" + number + " EUR = " + result + " GBP");
        } else if (mnuGbpEur.isSelected()) {
            result = number / EUR_GBP;
            lblResult.setText("" + number + " GBP = " + result + " EUR");
        } else if (mnuEurUsd.isSelected()) {
            result = number * EUR_USD;
            lblResult.setText("" + number + " EUR = " + result + " USD");
        } else if (mnuUsdEur.isSelected()) {
            result = number / EUR_USD;
            lblResult.setText("" + number + " USD = " + result + " EUR");
        } else if (mnuUsdGbp.isSelected()) {
            result = number * USD_GBP;
            lblResult.setText("" + number + " USD = " + result + " GBP");
        } else {
            result = number / USD_GBP;
            lblResult.setText("" + number + " GBP = " + result + " USD");
        }
    }

    @FXML
    private void clearData(ActionEvent event)
    {
        txtMoney.setText("");
        lblResult.setText("");
    }

}
