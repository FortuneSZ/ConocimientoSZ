package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private TextField txtNumber1;
    @FXML
    private TextField txtNumber2;
    @FXML
    private ComboBox<String> comboOperator;
    @FXML
    private Label lblResult;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        comboOperator.getItems().addAll("+", "-", "*", "/");

    }

    public void calculate(ActionEvent actionEvent) {

        int number1 = Integer.parseInt(txtNumber1.getText());
        int number2 = Integer.parseInt(txtNumber2.getText());
        String operator = comboOperator.getSelectionModel().getSelectedItem();

        switch(operator)
        {
            case "+":
                lblResult.setText(String.valueOf(number1 + number2));
                break;
            case "-":
                lblResult.setText(String.valueOf(number1 - number2));
                break;
            case "*":
                lblResult.setText(String.valueOf(number1 * number2));
                break;
            case "/":
                lblResult.setText(String.valueOf(number1 / number2));
                break;
        }
    }
}
