/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animationcredits;

import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class Controller {

    @FXML
    private Label lblName;

    public void initialize()
    {
        // Transition
        TranslateTransition t = new TranslateTransition(Duration.millis(5000), lblName);
        t.setFromX(-300);
        t.setToX(400);
        t.setCycleCount(Timeline.INDEFINITE);
        t.play();
    }    
    
}
