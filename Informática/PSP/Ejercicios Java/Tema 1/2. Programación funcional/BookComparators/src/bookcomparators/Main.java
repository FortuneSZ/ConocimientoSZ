package bookcomparators;

import java.util.*;

/**
 * This program creates a list of book objects and sorts them according to different criteria,
 * using lambda expressions
 */
public class Main
{
    public static void main(String[] args)
    {
        List<Book> books = new ArrayList();
        books.add(new Book("The Never Ending Story", 11.75f));
        books.add(new Book("Ender's game", 7.95f));
        books.add(new Book("The Lord of the Rings", 18.50f));
        books.add(new Book("Game of Thrones", 20.15f));

        Collections.sort(books, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
        System.out.println("List sorted by title (ascending):");
        for (Book b: books)
            System.out.println(b);

        Collections.sort(books, (b1, b2) -> Float.compare(b2.getPrice(), b1.getPrice()));
        System.out.println("List sorted by price (descending):");
        for (Book b: books)
            System.out.println(b);
    }
}
