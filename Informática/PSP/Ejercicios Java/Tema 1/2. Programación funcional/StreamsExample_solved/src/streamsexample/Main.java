package streamsexample;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Main program. It creates a list of Person objects and gets some final results using streams
 */

public class Main {

    public static void main(String[] args) {

        List<Person> people = new ArrayList<>();
        people.add(new Person("Nacho", 42));
        people.add(new Person("Juan", 70));
        people.add(new Person("Mario", 7));
        people.add(new Person("Laura", 4));
        people.add(new Person("Ana", 38));

        // Add here the code to print the names of adult people in the screen (one by line)

        System.out.println("Names of adult people (one by line):");
        people.stream()
              .filter(p -> p.getAge() >= 18)
              .map(p -> p.getName())
              .forEach(System.out::println);

        // Add here the code to get a sublist of all the adult people

        List<Person> adults = people.stream()
                                    .filter(p -> p.getAge() >= 18)
                                    .collect(Collectors.toList());
        System.out.println("List of adult people:");
        for(Person p: adults)
        {
            System.out.println(p);
        }

        // Add here the code to sort the list by ages (ascending) and get a sublist of the 3 first elements

        List<Person> firstThree = people.stream()
                                        .sorted((p1, p2) -> Integer.compare(p1.getAge(), p2.getAge()))
                                        .limit(3)
                                        .collect(Collectors.toList());
        System.out.println("List of 3 youngest people:");
        for(Person p: firstThree)
        {
            System.out.println(p);
        }

        // Add here the code to get the names, separated by commas, of adult people

        String adultNames = people.stream()
                                  .filter(p -> p.getAge() >= 18)
                                  .map(p -> p.getName())
                                  .collect(Collectors.joining(", "));
        System.out.println("Names of adult people separated by commas: " + adultNames);

        // Add here the code to get the sum of ages of the people in the list

        int sumAges = people.stream()
                            .map(p -> p.getAge())
                            .reduce(0, (a,b) -> a + b);
        System.out.println("Sum of ages: " + sumAges);

        // Add here the code to get the maximum age of the group

        Optional<Integer> maxAge = people.stream()
                                         .map(p -> p.getAge())
                                         .reduce(Integer::max);
        System.out.println("Maximum age:");
        System.out.println(maxAge.isPresent()?maxAge.get():"No max age");

        // Add here the code to get the age average

        OptionalDouble avgAge = people.stream()
                                      .mapToInt(p -> p.getAge()).average();
        System.out.println("Age average:");
        System.out.println(avgAge.isPresent()?avgAge.getAsDouble():"No ages");
    }

}
