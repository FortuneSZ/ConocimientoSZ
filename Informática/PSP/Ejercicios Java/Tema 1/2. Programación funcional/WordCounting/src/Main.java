import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;


public class Main
{
    public static void main(String[] args)
    {
        String palabra;

        Scanner sc = new Scanner(System.in);
        palabra = sc.nextLine();

        try(BufferedReader reader = new BufferedReader(
                new FileReader("text1.txt")))
        {
            int numVeces = reader.lines()
                    .filter(line -> line.contains(palabra))
                    .mapToInt(Line ->
                    {
                        int veces = 0;
                        int i = -1;

                        while ((i = Line.indexOf(palabra,i + 1)) != -1)
                        {
                            veces++;
                        }

                        return veces;
                    }).sum();

            System.out.println(numVeces);
        }
        catch (Exception e)
        {
            System.out.println("Error");
        }
    }
}