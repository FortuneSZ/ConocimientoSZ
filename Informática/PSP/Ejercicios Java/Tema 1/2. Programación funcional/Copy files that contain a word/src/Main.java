import javax.sound.sampled.Line;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args) throws IOException {

        String palabra;
        File location = new File("Ficheros2/");
        File [] contents = location.listFiles();
        Scanner sc = new Scanner(System.in);
        System.out.println("Escriba la palabra a buscar");
        palabra = sc.nextLine();

        if (!Files.exists(Paths.get("Ficheros2")))
        {
            Files.createDirectory(Paths.get("Ficheros2"));
        }
        else
        {
            for (int i = 0; i < contents.length;i++)
            {
                Files.delete(Paths.get(contents[i].getPath()));
            }
            Files.delete((Paths.get("Ficheros2")));
            Files.createDirectory(Paths.get("Ficheros2"));
        }

        try(Stream<Path> archivos = Files.list(Paths.get("Ficheros")))
        {
            archivos.forEach(archivo ->
            {
                try(BufferedReader reader = new BufferedReader(
                        new FileReader(archivo.toFile().getPath())))
                {
                    int encontrar = reader.lines()
                            .mapToInt(Line ->
                            {
                                int contador = 0;

                                int i = -1;

                                while ((i = Line.toUpperCase().indexOf(palabra.toUpperCase(),i + 1)) != -1)
                                {
                                    contador++;
                                }
                                return contador;
                            }).sum();

                    if (encontrar > 0)
                    {
                        Files.copy(archivo,Paths.get("Ficheros2/" + archivo.getFileName()),StandardCopyOption.REPLACE_EXISTING);
                    }
                } catch(Exception e)
                {
                    System.out.println("Error");
                }
            });
        }
        catch (Exception e)
        {
            System.out.println("Error");
        }
    }
}