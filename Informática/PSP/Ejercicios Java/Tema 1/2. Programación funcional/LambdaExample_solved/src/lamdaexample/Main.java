package lamdaexample;

import java.util.*;

/**
 * Main program. It creates a list of Person objects and sorts it using different types of comparators
 */

public class Main {

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Nacho", 42));
        people.add(new Person("Juan", 70));
        people.add(new Person("Mario", 7));
        people.add(new Person("Laura", 4));
        people.add(new Person("Ana", 38));
        people.add(new Person("Yeray", 4));

        // Sort list by name (ascending order), using PersonComparator class

        Collections.sort(people, new PersonComparator());

        System.out.println("People sorted by name (ascending), using PersonComparator:");
        for (Person p: people)
        {
            System.out.println(p);
        }

        // Sort list by age (descending) using an anonymous class

        Collections.sort(people, new Comparator<Person>() {

            @Override
            public int compare(Person p1, Person p2) {
                return Integer.compare(p2.getAge(), p1.getAge());
            }
        });

        System.out.println("People sorted by age (descending), using an anonymous class:");
        for (Person p: people)
        {
            System.out.println(p);
        }

        // Sort list by age (ascending) using a lambda expression

        Collections.sort(people, (p1, p2) -> Integer.compare(p1.getAge(), p2.getAge()));

        System.out.println("People sorted by age (ascending), using a lambda expression:");
        for (Person p: people)
        {
            System.out.println(p);
        }

        // Bonus track:
        // Sort list by age in ascending order and, if ages are the same, sort people by name
        // in ascending order

        // Step 1: define two comparators: one by age (ascending) and another one by name (ascending)
        Comparator<Person> compAge = (p1, p2) -> Integer.compare(p1.getAge(), p2.getAge());
        Comparator<Person> compNames = (p1, p2) -> p1.getName().compareTo(p2.getName());

        // Step 2: use "thenComparing" method from Comparator interface to join comparisons when sorting
        Collections.sort(people, compAge.thenComparing(compNames));

        // Step 3: show results
        System.out.println("People sorted by age (ascending) and then by name (ascending):");
        for (Person p: people)
        {
            System.out.println(p);
        }
    }
}
