package lamdaexample;

import java.util.*;

/**
 * Main program. It creates a list of Person objects and sorts it using different types of comparators
 */

public class Main {

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Nacho", 42));
        people.add(new Person("Juan", 70));
        people.add(new Person("Mario", 7));
        people.add(new Person("Laura", 4));
        people.add(new Person("Ana", 38));

        // Add here the code to sort list by name (ascending order), using PersonComparator class


        // Add here the code to sort list by age (descending) using an anonymous class


        // Add here the code to sort list by age (ascending) using a lambda expression


    }
}
