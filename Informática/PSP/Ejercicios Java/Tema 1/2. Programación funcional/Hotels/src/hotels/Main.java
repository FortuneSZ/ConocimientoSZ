package hotels;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This program creates a list of Hotels and then it filters and processes some information using streams
 */
public class Main
{
    public static void main(String[] args)
    {
        List<Hotel> hotels = new ArrayList<>();
        hotels.add(new Hotel("Meliá", "Alicante", 4.3f));
        hotels.add(new Hotel("Costa Narejos", "Los Alcázares", 3.8f));
        hotels.add(new Hotel("Estoril", "Estoril", 2.4f));
        hotels.add(new Hotel("Imperial", "Santander", 4.1f));
        hotels.add(new Hotel("Magic Natura", "Benidorm", 5f));
        hotels.add(new Hotel("Gran Sol", "Alicante", 3.6f));

        System.out.println("\nHotels sorted by rating in descending order:");
        hotels.stream()
                .sorted((h1, h2) -> Float.compare(h2.getRating(), h1.getRating()))
                .forEach(h -> System.out.println(h));

        System.out.println("\nHotels with rating > 3:");
        List<Hotel> hotelsRating3 = hotels.stream()
                .filter(h -> h.getRating() > 3)
                .collect(Collectors.toList());
        for (Hotel h: hotelsRating3)
            System.out.println(h);

        System.out.println("\nHotels with rating > 3:");
        hotels.stream()
                .filter(h -> h.getRating() > 3)
                .forEach(h -> System.out.println(h));

        System.out.println("\nHotel names from Alicante:");
        String names = hotels.stream()
                .filter(h -> h.getLocation().equals("Alicante"))
                .map(h -> h.getName())
                .collect(Collectors.joining(", ", "", ""));
        System.out.println(names);

        System.out.println("\nNumber of hotels with rating = 5:");
        long count = hotels.stream()
                .filter(h -> h.getRating() == 5)
                .count();
        System.out.println(count);



    }
}
