package moviemanagement;

import java.io.*;
import java.util.*;

/**
 * Main program to manage a list of movies with serialization
 */
public class Main
{
    public static final String MOVIES_FILE = "movies.dat";

    public static List<Movie> loadMovies()
    {
        List<Movie> movies = new ArrayList();

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(MOVIES_FILE)))
        {
            movies = (List<Movie>)ois.readObject();
        }
        catch (FileNotFoundException e)
        {
        }
        catch (Exception e)
        {
            System.err.println("Error reading movies");
        }

        return movies;
    }

    public static void saveMovies(List<Movie> movies)
    {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(MOVIES_FILE)))
        {
            oos.writeObject(movies);
        } catch (Exception e) {
            System.err.println("Error saving data");
        }
    }

    public static void addMovies(List<Movie> movies)
    {
        Scanner sc = new Scanner(System.in);
        String title, genre;

        do
        {
            System.out.println("Enter title for the new movie (empty to finish)");
            title = sc.nextLine();
            if (!title.isEmpty())
            {
                System.out.println("Enter genre for the new movie:");
                genre = sc.nextLine();
                movies.add(new Movie(title, genre));
            }
        }
        while(!title.isEmpty());
    }

    public static void printMovies(List<Movie> movies)
    {
        if (movies.size() == 0)
        {
            System.out.println("Your movie list is empty");
        }
        else
        {
            System.out.println("This is your movie list:\n");
            for(Movie m: movies)
                System.out.println(m);
        }
    }

    public static void main(String[] args)
    {
        List<Movie> movies = loadMovies();
        printMovies(movies);
        addMovies(movies);
        saveMovies(movies);
    }
}
