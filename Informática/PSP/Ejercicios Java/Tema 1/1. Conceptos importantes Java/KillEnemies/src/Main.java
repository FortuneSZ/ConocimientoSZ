import java.util.ArrayList;
import java.util.Collections;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    public static void main(String[] args)
    {
        ArrayList character = new ArrayList(10);
        character.add(new Friend());
        character.add(new Friend());
        character.add(new Friend());
        character.add(new Friend());
        character.add(new Friend());
        character.add(new Enemy());
        character.add(new Enemy());
        character.add(new Enemy());
        character.add(new Enemy());
        character.add(new Enemy());
        Collections.shuffle(character);

        for (int i = 0; i < character.size();i++)
        {
            if (character.get(i) instanceof Friend)
            {
                System.out.println("Character " + i + " is a friend! :-)");
            }
            else
            {
                ((Enemy)character.get(i)).kill();
            }
        }
    }
}