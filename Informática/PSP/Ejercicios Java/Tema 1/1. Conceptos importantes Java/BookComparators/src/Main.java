import java.util.*;

public class Main
{
    public static void main(String[] args)
    {
        List<Book> books = new ArrayList<>();
        books.add(new Book("Juramentada",30));
        books.add(new Book("It",25));
        books.add(new Book("Resident evil",12));


        Collections.sort(books, (b1,b2) -> b1.getTitle().compareTo(b2.getTitle()));

        for (int i = 0; i < books.size();i++)
        {
            System.out.println(books.get(i));
        }

        Collections.sort(books, (b1,b2) -> Double.compare(b1.getPrice(), b2.getPrice()));
        System.out.println();
        for (int i = 0; i < books.size();i++)
        {
            System.out.println(books.get(i));
        }
    }
}