public abstract class store
{
    private double cash;
    private double drinkPrice;

    public store(double drinkPrice)
    {
        this.cash = 0.0;
        this.drinkPrice = drinkPrice;
    }

    public double getCash()
    {
        return cash;
    }

    public void setCash(double cash)
    {
        this.cash = cash;
    }

    public double getDrinkPrice()
    {
        return drinkPrice;
    }

    public void setDrinkPrice(double drinkPrice)
    {
        this.drinkPrice = drinkPrice;
    }

    public abstract void  welcome();

    public double payDrinks(int numOfDrinks)
    {
        this.cash = numOfDrinks * this.drinkPrice;
        return this.cash;
    }
}
