//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    public static void main(String[] args)
    {
        LiquorStore store = new LiquorStore(8.95,20);
        store.welcome();
        double drinks = store.payDrinks(10);
        System.out.printf("%.2f ",drinks);
        System.out.println();

        store newstore = new store(8.95)
        {

            @Override
            public void welcome()
            {
                System.out.printf("Welcome to anonymous store! Our drink price is %.2f €",getDrinkPrice());
                System.out.println();
            }

            @Override
            public double payDrinks(int numOfDrinks)
            {
                return super.payDrinks(numOfDrinks);
            }
        };
        newstore.welcome();
        newstore.payDrinks(10);
        System.out.println(newstore.getCash());
    }
}