public class LiquorStore extends store
{
    private int tax;

    public LiquorStore(double drinkPrice,int tax)
    {
        super(drinkPrice);
        this.tax = tax;
    }


    @Override
    public void welcome()
    {
        System.out.println("Welcome to the liquor store");
    }

    @Override
    public double payDrinks(int numOfDrinks)
    {
        double notax = super.payDrinks(numOfDrinks);
        setCash(notax + (notax * this.tax/100.0));
        return getCash();
    }
}
