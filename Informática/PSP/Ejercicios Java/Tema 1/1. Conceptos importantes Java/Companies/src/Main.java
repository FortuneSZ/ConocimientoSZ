import java.util.Comparator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    public static void main(String[] args)
    {
        TreeMap<Company,TreeSet<Person>> companies = new TreeMap<>(new Comparator<Company>()
        {
            @Override
            public int compare(Company o1, Company o2) {
                return Double.compare(o2.getMoney(),o1.getMoney());
            }
        });

        Comparator<Person> personComp = new Comparator<Person>()
        {
            @Override
            public int compare(Person o1, Person o2) {
                return Integer.compare(o2.getAge(),o1.getAge());
            }
        };

        TreeSet<Person> people1 = new TreeSet<>(personComp);
        TreeSet<Person> people2 = new TreeSet<>(personComp);
        TreeSet<Person> people3 = new TreeSet<>(personComp);

        Company mercadona = new Company("Mercadona",50000);
        people1.add(new Person("Paco",18));
        people1.add(new Person("Saray",28));
        people1.add(new Person("Marcos",20));

        companies.put(mercadona,people1);


        Company Pepsi = new Company("Pepsi",30000);
        people2.add(new Person("Pedro",19));
        people2.add(new Person("Antonio",30));
        people2.add(new Person("Sandra",21));

        companies.put(Pepsi,people2);

        Company Nvidia = new Company("Nvidia",70000);
        people3.add(new Person("Lucas",27));
        people3.add(new Person("Sara",25));
        people3.add(new Person("Maite",23));

        companies.put(Nvidia,people3);

        for (Company c: companies.keySet())
        {
            System.out.println(c.getName() + " " + c.getMoney() + "$");

            for (Person p: companies.get(c))
            {
                System.out.println(p.getName() + " " + p.getAge() + " years");
            }
            System.out.println();
        }
    }
}