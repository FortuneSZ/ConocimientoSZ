import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    public static void main(String[] args)
    {
        ArrayList<Double> listarray = new ArrayList<>();
        LinkedList<Double> listlink = new LinkedList<>();
        Random rand = new Random();
        double num;
        int pos;

        Instant start = Instant.now();
        for (int i = 0; i < 100000;i++)
        {
            num = rand.nextDouble();
            listarray.add(0,num);
        }
        Instant end = Instant.now();
        Duration dur = Duration.between(start, end);
        System.out.printf("ArrayList: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            listarray.remove(0);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("ArrayList: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            pos = rand.nextInt(listarray.size());
            num = rand.nextDouble();
            listarray.add(pos,num);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("ArrayList: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            pos = rand.nextInt(listarray.size());
            listarray.remove(pos);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("ArrayList: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 100000;i++)
        {
            num = rand.nextDouble();
            listlink.add(0,num);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("Linkedlist: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            listlink.remove(0);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("Linkedlist: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            pos = rand.nextInt(listarray.size());
            num = rand.nextDouble();
            listlink.add(pos,num);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("Linkedlist: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            pos = rand.nextInt(listarray.size());
            listlink.remove(pos);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("Linkedlist: The operation ... takes: %dms\n", dur.toMillis());
    }
}