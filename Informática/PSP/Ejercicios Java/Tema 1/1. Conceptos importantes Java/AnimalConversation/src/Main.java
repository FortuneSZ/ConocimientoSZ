//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args)
    {
        AnimalConversation<Cat,Dog> conver1 = new AnimalConversation<>(new Cat("pipo"),new Dog("Buddy"));
        AnimalConversation<Cat,Sheep> conver2 = new AnimalConversation<>(new Cat("pedro"),new Sheep("Murcia"));
        conver1.chat();
        conver2.chat();
    }
}