public class Sheep extends Animal
{
    public Sheep(String name)
    {
        super(name);
    }

    @Override
    void talk()
    {
        System.out.println("Beeee");
    }
}
