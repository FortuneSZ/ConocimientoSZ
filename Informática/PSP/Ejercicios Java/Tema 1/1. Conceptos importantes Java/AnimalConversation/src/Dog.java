public class Dog extends Animal
{
    public Dog(String name)
    {
        super(name);
    }

    @Override
    void talk()
    {
        System.out.println("Wof wof");
    }
}
