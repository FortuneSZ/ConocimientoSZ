public class AnimalConversation<T extends Animal,E extends Animal>
{
    T animal1;
    E animal2;

    public AnimalConversation(T animal1, E animal2)
    {
        this.animal1 = animal1;
        this.animal2 = animal2;
    }

    public void chat()
    {
        animal1.talk();
        animal2.talk();
    }
}
