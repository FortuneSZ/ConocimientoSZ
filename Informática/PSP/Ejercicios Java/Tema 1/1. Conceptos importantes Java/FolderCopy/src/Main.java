import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Main
{
    public static void main(String[] args)
    {
        File location = new File("data/");
        File [] contents = location.listFiles();
        try
        {
            if (!Files.exists(Paths.get("copy")))
                Files.createDirectory(Paths.get("copy"));
            {
                for (int i = 0; i < contents.length; i++)
                {
                    System.out.println(contents[i].getName());
                    Files.copy(Paths.get(contents[i].getPath()),
                            Paths.get("copy/" + contents[i].getName()),
                            StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(Paths.get(contents[i].getPath()));
                }
                Files.delete((Paths.get(location.getPath())));

            }
        }
        catch (IOException e)
        {

        }
    }
}