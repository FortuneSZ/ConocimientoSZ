import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class RegexReader extends BufferedReader
{
    String regex1 = "^\\d{2}/\\d{2}/\\d{2}$";
    String regex2 = "^\\d{2}/\\d{2}/\\d{4}$";
    public RegexReader(Reader reader)
    {
        super(reader);
    }
    @Override
    public String readLine() throws IOException
    {
        String line = super.readLine();

        while (line != null && line.matches(regex1) || line.matches(regex2));
        {
            return line;
        }
    }

}
