import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main
{
    public static void main(String[] args)
    {
        String regex1 = "^\\d{2}/\\d{2}/\\d{2}$";
        String regex2 = "^\\d{2}/\\d{2}/\\d{4}$";

        try (RegexReader re = new RegexReader(new FileReader("file.txt")))
        {
            System.out.println(re.readLine());
        } catch (FileNotFoundException e)
        {
            throw new RuntimeException(e);
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}