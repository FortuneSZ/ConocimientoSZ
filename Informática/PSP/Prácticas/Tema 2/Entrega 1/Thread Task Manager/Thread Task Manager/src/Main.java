public class Main implements Runnable
{
    public static void main(String[] args) throws InterruptedException {
        Task t1 = new Task();
        Task t2 = new Task();
        Task t3 = new Task();

        t1.start();
        t2.start();
        t3.start();

        Thread.sleep(100);
        Thread T = new Thread(() ->
        {
            boolean allThreadsTerminated = false;

            do
            {
                System.out.println("T1 state: " +t1.getState());
                System.out.println("T2 state: " +t2.getState());
                System.out.println("T3 state: " +t3.getState());
                if(!t1.isAlive() && !t2.isAlive() && !t3.isAlive())
                {
                    allThreadsTerminated = true;
                    System.out.println("All threads terminated");
                }
                if (t1.getState() == Thread.State.TERMINATED && t2.getState() == Thread.State.TERMINATED
                && t3.getState() == Thread.State.TERMINATED)
                {
                    System.out.println("All threads terminated");
                }
            }
            while (!allThreadsTerminated);
        });
        T.start();
    }

    @Override
    public void run()
    {

    }
}