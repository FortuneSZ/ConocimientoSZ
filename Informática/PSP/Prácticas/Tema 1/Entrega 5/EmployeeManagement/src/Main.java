import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.IntToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    public static void main(String[] args)
    {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Paco","Finances",42000));
        employees.add(new Employee("Sara","Security",50200));
        employees.add(new Employee("Fran","Development",52500));
        employees.add(new Employee("Andrea","Marketing",41800));
        employees.add(new Employee("Mark","Design",51900));
        employees.add(new Employee("Lucy","Engineering",56000));
        employees.add(new Employee("Burnice","Engineering",58000));
        Stream<Employee> engyEmployees;

        System.out.println("Sort the employees by salary in ascending order" + "\n");
        employees.stream().sorted(Comparator.comparingDouble(Employee::getSalary)).forEach(System.out::println);

        System.out.println("the employees whose salary is greater than $50,000" + "\n");
        double TopSalary = 50000;
        employees.stream().filter(e -> e.getSalary() >= TopSalary).sorted(Comparator.comparingDouble(Employee::getSalary)).forEach(System.out::println);

        System.out.println("the employee with the highest salary" + "\n");
        employees.stream().sorted((e,e2) -> Double.compare(e2.getSalary(),e.getSalary())).limit(1).forEach(System.out::println);

        System.out.println("names of all employees in the Engineering department, separated by commas" + "\n");
        engyEmployees = employees.stream().filter(e -> e.getDepartment().equals("Engineering"));
        System.out.println(engyEmployees.map(Employee::getName).collect(Collectors.joining(",")));

        System.out.println("average salary of employees in the Marketing department" + "\n");
        System.out.println(employees.stream().filter(e -> e.getDepartment().equals("Marketing")).mapToDouble(Employee::getSalary).average().getAsDouble());

    }
}

