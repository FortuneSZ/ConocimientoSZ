import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args)
    {
        List<Book> libros = Arrays.asList(
                new Book("The crow","Poe",1810),
                new Book("it","Stephen king",2000),
                new Book("Murcia","Paco",2010),
                new Book("Ella no te ama","Ella",2024),
                new Book("Juramentada","Sanderson",2015),
                new Book("Perdona si te llamo amor","Federico",2006),
                new Book("Arcane","League of legends",2021)
        );

        System.out.println("Sort the books by year of publication in ascending order and display their titles\n");
        libros.stream().sorted(Comparator.comparingInt(Book::getYear)).forEach(lib -> System.out.println(lib.getTitle()));

        System.out.println("\nFilter the books to get those published after the year 2000 and print their titles and authors\n");
        libros.stream().filter(lib -> lib.getYear() > 2000).forEach(lib -> System.out.println(lib.getTitle() + ", " + lib.getAuthor()));

        System.out.println("\nFind the most recently published book and display its title and year\n");
        libros.stream().sorted((lib,lib2) -> Integer.compare(lib2.getYear(), lib.getYear())).limit(1).forEach(lib -> System.out.println(lib.getTitle() + " (" + lib.getYear() + ")"));

        System.out.println("\nPrint the titles of all books written by a specific author\n");
        libros.stream().filter(lib -> lib.getAuthor().equals("League of legends")).forEach(lib -> System.out.println(lib.getTitle()));

        System.out.println("\nGroup the books by author and print the number of books each author has written\n");
        Stream<String> authors = libros.stream().map(lib -> lib.getAuthor()).distinct();
        authors.forEach(au ->
                {
                    int books = 0;
                    for(int i = 0; i < libros.size();i++)
                    {
                        if(libros.get(i).getAuthor().equals(au))
                        {
                            books++;
                        }
                    }
                    System.out.println(au + ": " + books + " libros");
                }

        );
    }
}