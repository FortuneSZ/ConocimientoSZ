import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args)
    {
        List<Product> products = new ArrayList<>();
        products.add(new Product("Tv","Electronics",500));
        products.add(new Product("Laptop","Electronics",700));
        products.add(new Product("Table","Furniture",50));
        products.add(new Product("Chair","Furniture",25));
        products.add(new Product("Pants","Clothing",30));
        products.add(new Product("Hoodie","Clothing",40));
        products.add(new Product("Terarria","Videogame",12));
        products.add(new Product("Hollow Knight","Videogame",10));

        System.out.println("products to get those that belong to the Electronics category" +"\n");
        products.stream().filter(p -> p.getCategory().equals("Electronics")).forEach(e-> System.out.println(e.getName()));

        System.out.println("\n" + "total price of all products in the Furniture category" +"\n");
        System.out.println(products.stream().filter(p -> p.getCategory().equals("Furniture")).map(p -> p.getPrice()).reduce((double) 0,(p, p2) -> p + p2));

        System.out.println("\n" + "a list of all unique categories from the product list" +"\n");
        Stream<String> categories = products.stream().map(p -> p.getCategory()).distinct();
        categories.forEach(c -> System.out.println(c));

        System.out.println("\n" + "the cheapest product in the Clothing category and display its name and price" +"\n");
        products.stream().filter(p -> p.getCategory().equals("Clothing")).sorted((p,p2) -> Double.compare(p.getPrice(),p2.getPrice())).limit(1)
                .forEach(p-> System.out.println("Name: " + p.getName() + ", price: " + p.getPrice()));

        System.out.println("\n" + "Group the products by category and print the number of products in each category" +"\n");
        categories = products.stream().map(p -> p.getCategory()).distinct();
        categories.forEach(c ->
        {
            int times = 0;
            for(int i = 0; i < products.size();i++)
            {
                if(products.get(i).getCategory().equals(c))
                {
                    times++;
                }
            }
            System.out.println(c + ":" + times);
        });

    }
}