import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args)
    {
        List<Transaction> transactions = Arrays.asList(
                new Transaction(123456,"Credit",520),
                new Transaction(654321,"Debit",20),
                new Transaction(147852,"Credit",700),
                new Transaction(258741,"Debit",400),
                new Transaction(896523,"Credit",60),
                new Transaction(325698,"Debit",1000),
                new Transaction(159753,"Credit",350),
                new Transaction(357951,"Debit",900),
                new Transaction(179350,"Credit",650),
                new Transaction(842605,"Debit",584)
        );

        System.out.println("Filter the transactions to get those of type Credit and print their IDs and amounts\n");
        transactions.stream().filter(t -> t.getType().equals("Credit")).forEach(t -> System.out.println("Transaction " + t.getId() + ", amount: " + t.getAmount()));

        System.out.println("\nCalculate the total amount of Debit transactions\n");
        Stream<Transaction> devit = transactions.stream().filter(t -> t.getType().equals("Debit"));
        System.out.println(devit.count() + " debit transactions");

        System.out.println("\n Find the transaction with the highest amount and display its ID and type\n");
        transactions.stream().sorted((t,t2) -> Double.compare(t2.getAmount(),t.getAmount())).limit(1).forEach(t -> System.out.println("ID: " + t.getId() + ", Type: " + t.getType()));

        System.out.println("\nPrint the IDs of all transactions with an amount greater than $500\n");
        transactions.stream().filter(t -> t.getAmount() > 500).forEach(t -> System.out.println(t.getId()));

        System.out.println("\nGroup the transactions by type and print the total amount for each type\n");
        Stream<String> types = transactions.stream().map(lib -> lib.getType()).distinct();
        types.forEach(ty ->
        {
            int times = 0;
            for (int i = 0; i < transactions.size(); i++) {
                if (transactions.get(i).getType().equals(ty)) {
                    times++;
                }
            }
            System.out.println(ty + ": " + times);
        });
    }
}