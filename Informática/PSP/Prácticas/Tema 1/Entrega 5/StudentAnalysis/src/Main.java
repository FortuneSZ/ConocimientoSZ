import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args)
    {
        List<Student> students = new ArrayList<>();
        List<Double> grades1 = new ArrayList<>();
        grades1.add(8.0);
        grades1.add(9.0);
        grades1.add(10.0);
        List<Double> grades2 = new ArrayList<>();
        grades2.add(4.0);
        grades2.add(7.0);
        grades2.add(5.0);
        List<Double> grades3 = new ArrayList<>();
        grades3.add(2.0);
        grades3.add(1.0);
        grades3.add(3.0);
        List<Double> grades4 = new ArrayList<>();
        grades4.add(10.0);
        grades4.add(9.0);
        grades4.add(10.0);
        List<Double> grades5 = new ArrayList<>();
        grades5.add(6.0);
        grades5.add(9.0);
        grades5.add(5.0);
        students.add(new Student("Fran",25,grades1));
        students.add(new Student("John",21,grades2));
        students.add(new Student("Sergio",27,grades3));
        students.add(new Student("Miguel",25,grades4));
        students.add(new Student("Nath",30,grades5));

        System.out.println("Sort the students by age in descending order and display their names" + "\n");
        students.stream().sorted((e,e2) -> Integer.compare(e2.getAge(), e.getAge())).forEach(e -> System.out.println(e.getName()));
        System.out.println();
        System.out.println("Calculate the average grade of all students and print the result.\n");
        OptionalDouble averageGrade = students.stream()
                .flatMap(student -> student.getGrades().stream()) // Flatten grades into a single stream
                .mapToDouble(Double::doubleValue)                 // Convert to primitive double
                .average();
        if (averageGrade.isPresent()) {
            System.out.println("The average grade is: " + averageGrade.getAsDouble());
        } else {
            System.out.println("No grades available to calculate the average.");
        }
        System.out.println();
        System.out.println("Find the student with the highest average grade and display their name.\n");

        Optional<Student> topStudent = students.stream()
                .max(Comparator.comparingDouble(Student::getAverageGrade));
        topStudent.ifPresent(student -> System.out.println("Student with the highest average grade: "
                + student.getName()));

        System.out.println("\nPrint the names of students who are older than 20");
        System.out.println("\nStudents older than 20:\n");
        students.stream()
                .filter(student -> student.getAge() > 20)
                .forEach(student -> System.out.println(student.getName()));

       System.out.println("\nCreate a map where the key is the student's name and the value is their average grade");
        Map<String, Double> nameToAverageGradeMap = students.stream()
                .collect(Collectors.toMap(
                        Student::getName,
                        Student::getAverageGrade
                ));
        System.out.println("\nMap of student names to their average grades:\n");
        nameToAverageGradeMap.forEach((name, avgGrade) ->
                System.out.println(name + ": " + avgGrade));
    }
}