import java.util.List;

public class Student
{
    private String name;
    private int age;
    private List<Double> grades;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public List<Double> getGrades()
    {
        return grades;
    }

    public void setGrades(List grades)
    {
        this.grades = grades;
    }

    public Student(String name, int age, List<Double> grades)
    {
        this.name = name;
        this.age = age;
        this.grades = grades;
    }

    public double getAverageGrade() {
        return this.grades.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0); // Handle empty grades
    }
}
