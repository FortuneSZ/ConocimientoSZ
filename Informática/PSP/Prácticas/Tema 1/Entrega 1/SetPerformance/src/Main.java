import java.time.Duration;
import java.time.Instant;
import java.util.*;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    public static void main(String[] args)
    {
        HashSet<Double> hashS = new HashSet<>();
        TreeSet<Double> treeS = new TreeSet<>();
        Random rand = new Random();
        double num;
        int pos;

        Instant start = Instant.now();
        for (int i = 0; i < 100000;i++)
        {
            num = rand.nextDouble();
            hashS.add(num);
        }
        Instant end = Instant.now();
        Duration dur = Duration.between(start, end);
        System.out.printf("HashSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            if (hashS.contains(rand.nextDouble()))
            {
                //number in hash
            }
            else
            {
                //number not in hash
            }
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("HashSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            hashS.remove(i);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("HashSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (Double s: hashS)
        {
            //Number
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("HashSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 100000;i++)
        {
            num = rand.nextDouble();
            treeS.add(num);
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("TreeSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            if (treeS.contains(rand.nextDouble()))
            {
                //number in tree
            }
            else
            {
                //number not in tree
            }
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("TreeSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            treeS.pollFirst();
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("TreeSet: The operation ... takes: %dms\n", dur.toMillis());

        start = Instant.now();
        for (int i = 0; i < 50000;i++)
        {
            for (Double s: treeS)
            {
                //number
            }
        }
        end = Instant.now();
        dur = Duration.between(start, end);
        System.out.printf("TreeSet: The operation ... takes: %dms\n", dur.toMillis());

    }
}