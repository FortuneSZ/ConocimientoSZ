import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

public class Main
{
    public static void main(String[] args)
    {
        TreeMap<Student, HashSet<Teacher>> student = new TreeMap<>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2)
            {
                return Double.compare(o1.getGrade(), o2.getGrade());
            }
        });

        HashSet<Teacher> t1 = new HashSet<>();
        HashSet<Teacher> t2 = new HashSet<>();
        HashSet<Teacher> t3 = new HashSet<>();

        Student e1 = new Student("Paco",5);
        t1.add(new Teacher("Pedro","History"));
        t1.add(new Teacher("Mary","English"));

        student.put(e1,t1);

        Student e2 = new Student("Marta",7);
        t2.add(new Teacher("Saray","Math"));
        t2.add(new Teacher("Dexter","Chemistry"));

        student.put(e2,t2);

        Student e3 = new Student("Marcos",3);
        t3.add(new Teacher("Laura","Spanish"));
        t3.add(new Teacher("Alex","Physics"));

        student.put(e3,t3);

        Set data = student.entrySet();
        System.out.println("Students data");
        System.out.println(data);


    }
}