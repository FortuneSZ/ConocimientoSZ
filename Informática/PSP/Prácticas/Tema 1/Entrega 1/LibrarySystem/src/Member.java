public class Member
{
    private String name;
    private String memberID;

    public Member(String name, String memberID)
    {
        this.name = name;
        this.memberID = memberID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getMemberID()
    {
        return memberID;
    }

    public void setMemberID(String memberID)
    {
        this.memberID = memberID;
    }

    @Override
    public String toString()
    {
        return "Member{" +
                "name='" + name + '\'' +
                ", memberID='" + memberID + '\'' +
                '}';
    }
}
