import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

public class Main
{
    public static void main(String[] args)
    {
        HashMap<Book, LinkedList<Member>> Library = new HashMap();

        Book book1 = new Book("Marcus","Detroit");
        Book book2 = new Book("Lovecraft","The call of cthulhu");
        Book book3 = new Book("Stephen king","It");

        LinkedList<Member> members1 = new LinkedList<>();
        LinkedList<Member> members2 = new LinkedList<>();
        LinkedList<Member> members3 = new LinkedList<>();

        members1.add(new Member("Sara","2106"));
        members1.add(new Member("Fran","0421"));

        members2.add(new Member("Valeria","1904"));
        members2.add(new Member("John","2008"));

        members3.add(new Member("Elena","7845"));
        members3.add(new Member("Raquel","3111"));

        Library.put(book1,members1);
        Library.put(book2,members2);
        Library.put(book3,members3);

        Set data = Library.entrySet();
        System.out.println("Library data");
        System.out.println(data);

    }
}