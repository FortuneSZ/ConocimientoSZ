public class Bike extends Vehicle
{
    public Bike(String model)
    {
        super(model);
    }

    public String sound()
    {
        return "Brmm brmm";
    }
}
