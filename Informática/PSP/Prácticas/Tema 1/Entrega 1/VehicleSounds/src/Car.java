public class Car extends Vehicle
{
    public Car(String model)
    {
        super(model);
    }

    public String sound()
    {
        return "Vroom vroom";
    }
}
