public class Truck extends  Vehicle
{
    public Truck(String model)
    {
        super(model);
    }

    public String sound()
    {
        return "Honk honk";
    }
}
