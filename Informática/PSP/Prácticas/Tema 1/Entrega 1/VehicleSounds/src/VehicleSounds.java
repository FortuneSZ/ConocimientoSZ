public class VehicleSounds<T extends Vehicle,E extends Vehicle>
{
    T vehicle1;
    E vehicle2;

    public VehicleSounds(T v1, E v2)
    {
        this.vehicle1 = v1;
        this.vehicle2 = v2;
    }

    public void playSounds()
    {
        System.out.println(this.vehicle1.sound());
        System.out.println(this.vehicle2.sound());
    }
}
