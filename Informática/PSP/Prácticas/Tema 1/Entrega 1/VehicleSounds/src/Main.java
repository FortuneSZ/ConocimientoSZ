public class Main
{
    public static void main(String[] args)
    {
        VehicleSounds<Car,Bike> sound1 = new VehicleSounds<>(new Car("Tesla"),new Bike("Yamaha"));
        VehicleSounds<Car,Truck> sound2 = new VehicleSounds<>(new Car("Seat"),new Truck("yes"));

        sound1.playSounds();
        sound2.playSounds();
    }
}