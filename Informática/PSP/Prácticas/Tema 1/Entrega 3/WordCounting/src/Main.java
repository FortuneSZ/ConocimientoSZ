import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main
{
    private static int index;

    public static void main(String[] args)
    {
        int words = 0;
        try(Stream<String> stream = Files.lines(Paths.get("text3.txt")))
        {
            words = stream.filter(l -> l.contains("Alcohol")).mapToInt(s ->{
                int index = 0;
                int count = 0;
                while((index = s.indexOf("Alcohol")) != -1)
                {
                    s = s.substring(index+1);
                    count++;
                }
                return count;
            }).sum();
        } catch (IOException e)
        {
            throw new RuntimeException();
        }
        System.out.println(words);
    }
}