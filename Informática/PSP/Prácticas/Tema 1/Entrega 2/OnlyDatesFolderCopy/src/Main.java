import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Main
{
    public static boolean OnlyDatesFolderCopy(String line)
    {
        boolean date = false;
        Pattern datePattern = Pattern.compile( "^\\d{2}[-,/]\\d{2}[-,/]\\d{4}$");
        date = datePattern.matcher(line).matches();
        return date;
    }
    public static void main(String[] args) throws IOException {
        File location = new File("Folder/");
        File [] contents = location.listFiles();
        ArrayList<File> dateContents = new ArrayList<File>();
        Boolean dateFound = false;
        int dateCount = 0;

        if (!Files.exists(Paths.get("Dates")))
        {
            Files.createDirectory(Paths.get("Dates"));
        }
        else
        {
            Files.delete((Paths.get("Dates")));
            Files.createDirectory(Paths.get("Dates"));
        }
        for (int i = 0; i < contents.length; i++)
        {
            try (BufferedReader inputFile = new BufferedReader(
                    new FileReader((contents[i].toString()))))
            {
                System.out.println(contents[i].toString());
                String line;
                dateFound = false;
                while ((line = inputFile.readLine()) != null)
                {
                    System.out.println(OnlyDatesFolderCopy(line));
                    if (OnlyDatesFolderCopy(line) == true)
                    {
                        dateFound = true;
                    }
                }

                if (dateFound == true)
                {
                    dateContents.add(contents[i]);
                    dateCount++;
                }
            }
            catch (IOException fileError)
            {
                System.err.println(
                        "Error reading file: " +
                                fileError.getMessage() );
            }
            System.out.println();
        }
        for (int i = 0; i < dateContents.stream().count(); i++)
        {
            Files.copy(Paths.get(dateContents.get(i).getPath()),
                    Paths.get("Dates/" + dateContents.get(i).getName()),
                    StandardCopyOption.REPLACE_EXISTING);
        }

        for (int i = 0; i < contents.length;i++)
        {
            Files.delete(Paths.get(contents[i].getPath()));
        }
        Files.delete((Paths.get(location.getPath())));
    }
}