<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['ancho']) && isset($_POST['alto']) && isset($_FILES['imagen']) && !empty($_POST['alto']) && !empty($_POST['ancho']))
{

    $alto = $_POST["alto"];
    $ancho = $_POST["ancho"];

    if ($_FILES['imagen']['error'] == 0) 
    {
        $imagen = $_FILES['imagen']['tmp_name'];
        $nombreImagen = $_FILES['imagen']['name'];
        echo ("<img src='data:image/jpeg;base64," . base64_encode(file_get_contents($imagen)) . "' width='$ancho' height='$alto' alt='$nombreImagen'>");
    } 
    else 
    {
        echo "<p>Error</p>";
    }
} 
else 
{
    echo "<p>Parámetros incorrectos o faltantes</p>";
}
?>