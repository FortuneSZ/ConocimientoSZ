<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Ejercicio 4</title>
</head>
<body>
    <form action="procesador_formulario.php" method="post">
        <fieldset>
            <legend>Datos personales</legend>
            <label for="nombre">Nombre</label><br>
            <input type="text" id="nombre" name="nombre"><br><br>

            <label for="apellidos">Apellidos</label><br>
            <input type="text" id="apellidos" name="apellidos"><br><br>

            <label for="URL">URL página personal</label><br>
            <input type="url" id="URL" name="URL"><br><br>

            <label>Sexo</label><br>
            <input type="radio" id="masculino" name="sexo" value="masculino">
            <label for="masculino">Masculino</label><br><br>
            <input type="radio" id="femenino" name="sexo" value="femenino">
            <label for="femenino">Femenino</label><br><br>

            <label for="convivientes">Número de convivientes en el domicilio</label><br>
            <input type="number" id="convivientes" name="convivientes"><br><br>

            <label for="aficiones">Aficiones</label><br><br>
            <input type="checkbox" value="cantar" id="cantar" name="aficiones[]">
            <label for="cantar">Cantar</label><br>
            <input type="checkbox" value="bailar" id="bailar" name="aficiones[]">
            <label for="bailar">Bailar</label><br>
            <input type="checkbox" value="dibujar" id="dibujar" name="aficiones[]">
            <label for="dibujar">Dibujar</label><br>
            <input type="checkbox" value="jugar" id="jugar" name="aficiones[]">
            <label for="jugar">Jugar</label><br>
            <input type="checkbox" value="ir al cine" id="cine" name="aficiones[]">
            <label for="cine">Ir al cine</label><br><br>

            <label>Menú favorito</label><br><br>
            <select name="menu[]" multiple size="5">
                <option value="Italiana">Italiana</option>
                <option value="Mexicana">Mexicana</option>
                <option value="Turca">Turca</option>
                <option value="Española">Española</option>
                <option value="Americana">Americana</option>
            </select><br><br>

            <input type="submit" value="Enviar datos">
        </fieldset>
    
    </form>
</body>
</html>