<?php
if (isset($_POST["nombre"]) && isset($_POST["apellidos"]) && isset($_POST["URL"]) && isset($_POST["sexo"])
&& isset($_POST["convivientes"]) && isset($_POST["aficiones"]) && isset($_POST["menu"]))
{
    $valid = true;

    $nombre = trim($_POST["nombre"]);
    $nombre = stripslashes($nombre);
    if (empty($nombre))
    {
        $valid = false;
    }

    $apellidos = trim($_POST["apellidos"]);
    $apellidos = stripcslashes($apellidos);
    if (empty($apellidos))
    {
        $valid = false;
    }

    $url = trim($_POST["URL"]);

    if (!filter_var($url,FILTER_VALIDATE_URL) or empty($url))
    {
        $valid = false;
        echo("$url no es una URL no válida");
    }

    $sexo = $_POST["sexo"];
    if (empty($sexo))
    {
        $valid = false;
    }

    $convivientes = $_POST["convivientes"];
    if (empty($convivientes))
    {
        $valid = false;
    }

    $aficiones = $_POST["aficiones"];
    if (empty($aficiones))
    {
        $valid = false;
    }

    $menu = $_POST["menu"];
    if (empty($menu))
    {
        $valid = false;
    }

    if ($valid == true)
    {
        echo("<table border='1'>");
        echo("<tr>");
        echo("<th>Datos</th>");
        echo("<th>valor</th>");
        echo("</tr>");
        echo("<tr>");
        echo("<th>Nombre</th>");
        echo("<th>$nombre</th>");
        echo("</tr>");
        echo("<th>Apellidos</th>");
        echo("<th>$apellidos</th>");
        echo("</tr>");
        echo("</tr>");
        echo("<th>Url</th>");
        echo("<th>$url</th>");
        echo("</tr>");
        echo("</tr>");
        echo("<th>Sexo</th>");
        echo("<th>$sexo</th>");
        echo("</tr>");
        echo("</tr>");
        echo("<th>Convivientes</th>");
        echo("<th>$convivientes</th>");
        echo("</tr>");
        echo("</tr>");
        echo("<th>Aficiones</th>");
        echo("<th>");
        foreach($aficiones as $aficion)
        {
            echo("$aficion <br>");
        }
        echo("</th>");
        echo("</tr>");
        echo("</tr>");
        echo("<th>Menú favorito</th>");
        echo("<th>");
        foreach($menu as $menuitem)
        {
            echo("$menuitem <br>");
        }
        echo("</tr>");
        echo("</table>");
    }
    else
    {
        echo("Un elemento tiene un valor incorrecto");
    }
}
else
{
    echo("Rellena todos los campos del formulario antes de enviarlo");
}
?>