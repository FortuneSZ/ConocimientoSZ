<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3</title>
</head>
<body>
    <form action="procesador_pago.php" method="post">
        <fieldset>
            <legend><b>Formulario de pago</b> (los campos obligatorios se han marcado con *)</legend><br>
            <fieldset>
                <legend><b>Información de contacto</b></legend><br>
                <fieldset>
                    <legend>Profesión</legend>
                    <input type="radio" id="profesor" name="profesion" value="profesor">
                    <label for="profesor">Profesor</label><br><br>
                    <input type="radio" id="estudiante" name="profesion" value="estudiante">
                    <label for="estudiante">Estudiante</label><br><br>
                    <input type="radio" id="otro" name="profesion" value="otro">
                    <label for="otro">Otro</label><br><br>
                </fieldset><br>
                <fieldset>
                <legend>Tratamiento</legend>
                    <input type="radio" id="Sr/Sra" name="tratamiento" value="Sr/Sra">
                    <label for="Sr/Sra">Sr/Sra</label><br><br>
                    <input type="radio" id="Dr/Dra" name="tratamiento" value="Dr/Dra">
                    <label for="Dr/Dra">Dr/Dra</label><br><br>
                    <input type="radio" id="otro" name="tratamiento" value="otro">
                    <label for="otro">Otro</label><br><br>
                </fieldset><br>

                <label for="nombre">Nombre:*</label><br>
                <input type="text" id="nombre" name="nombre" required><br><br>
                <label for="correo">Correo:*</label><br>
                <input type="email" id="correo" name="correo" required><br><br>
                <label for="telefono">Número de teléfono:*</label><br>
                <input type="Tel" id="telefono" name="telefono" required><br><br>
                <label for="contraseña">Contraseña:*</label><br>
                <input type="password" id="contraseña" name="contraseña" required><br><br>
            </fieldset><br>

            <fieldset>
                <legend><b>Información de pago</b></legend><br>
                <label>Tipo de tarjeta</label>
                <select name="tarjeta">
                    <option value="visa">Visa</option>
                    <option value="mastercard">Mastercard</option>
                </select><br><br>

                <label for="numeroTarjeta">Número de tarjeta:*</label><br>
                <input type="number" id="numeroTarjeta" name="numeroTarjeta" required><br><br>
                <label for="caducidad">Fecha de caducidad:*</label><br>
                <input type="Date" id="caducidad" name="caducidad" required><br><br>
                <label for="paypal">Cuenta de paypal:*</label><br>
                <input type="email" id="paypal" name="paypal" required><br><br>

                <label>Método de pago preferido</label>
                <select name="pago">
                    <option value="tarjeta">Tarjeta de crédito</option>
                    <option value="paypal">Paypal</option>
                </select><br><br>
                </fieldset><br>
            <input type="submit" value="Validar el pago">
        </fieldset>
    
    </form>
</body>
</html>