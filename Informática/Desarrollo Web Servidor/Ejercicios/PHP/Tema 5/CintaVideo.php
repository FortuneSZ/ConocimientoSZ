<?php
require_once('Soporte.php');
class CintaVideo extends Soporte
{
    private $duracion;

    function __construct()
    {
        parent::__construct();
        $this->duracion = 107;
    }

    public function muestraResumen()
    {
        parent::muestraResumen();
        print("Película en VHS"."<br>");
        print($this->titulo ."<br>");
        print($this->getPrecio() . " € (IVa no incluido) <br>");
        print("Duración: " . $this->duracion . " minutos");
    }
}
?>