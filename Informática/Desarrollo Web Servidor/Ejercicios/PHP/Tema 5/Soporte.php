<?php
class Soporte
{
    public $titulo;
    protected $numero;
    private $precio;
    private const Iva = 21;

    function __construct()
    {
        $this->titulo = "The last of us part II";
        $this->numero = 21;
        $this->precio = 49.99;
    }

    // Getters
    public function getPrecio()
    {
        return $this->precio;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function getPrecioConIva()
    {
        return $this->precio * self::Iva / 100;
    }

    public function muestraResumen()
    {
        $precioConIva = $this->getPrecioConIva();
        print("<b>Titulo: " . $this->titulo . "</b><br>");
        print("Precio: " . $this->precio . "<br>");
        print("Precio IVA incluido: " . $this->precio+$precioConIva . "<br>");
    }
}
?>