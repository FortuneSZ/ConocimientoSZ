<?php
require_once('Soporte.php');
class Juego extends Soporte
{
    public $consola;
    private $minNumJugadores;
    private $maxNumJugadores;

    function muestraJugadoresPosibles()
    {
        if($this->maxNumJugadores == 1)
        {
            $jugadores = "Para un jugador";
        }
        elseif($this->minNumJugadores == $this->maxNumJugadores)
        {
            $jugadores = "Para $this->minNumJugadores jugadores";
        }
        else
        {
            $jugadores = "De $this->minNumJugadores a $this->maxNumJugadores jugadores";
        }
        return $jugadores;
    }

    function __construct()
    {
        parent::__construct();
        $this->consola = "PS4";
        $this->minNumJugadores = 1;
        $this->maxNumJugadores = 4;
    }

    public function muestraResumen()
    {
        $jugadores = $this->muestraJugadoresPosibles();
        parent::muestraResumen();
        print("Juego para: ".$this->consola."<br>");
        print($this->titulo ."<br>");
        print($this->getPrecio() . " € (IVa no incluido) <br>");
        print($jugadores);
    }
}
?>