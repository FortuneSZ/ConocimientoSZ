<?php
require_once('Soporte.php');
class Dvd extends Soporte
{
    public $idiomas;
    private $formatPantalla;

    function __construct()
    {
        parent::__construct();
        $this->idiomas = "es,en,fr";
        $this->formatPantalla = "16:9";
    }

    public function muestraResumen()
    {
        parent::muestraResumen();
        print("Película en DVD"."<br>");
        print($this->titulo ."<br>");
        print($this->getPrecio() . " € (IVa no incluido) <br>");
        print("Idiomas:" . $this->idiomas. "<br>");
        print("Formato pantalla:" . $this->formatPantalla);
    }
}
?>