<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 1</title>
        <style>
            h1
            {
                font-size: 5em;
                text-align: center;
            }
            p
            {
                font-size: 10em;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <?php
        $emoji = rand(128512,128586);
        echo "<h1> Random emoji </h1>";
        echo "<p> &#$emoji </p>";
        ?>
    </body>
</html>
