<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
            $nombre = "Fran";
            $apellido1 = "García";
            $apellido2 = "Sánchez";
            $email = "realSZmusic@gmail.com";
            $nacimiento = "04/09/1999";
            $telefono = "654145523";

            echo "<table border='*'>";
                echo "<tr>";
                    echo "<th> Parámetro </th>";
                    echo "<th> Valor </th>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td> Nombre </td>";
                    echo "<td> $nombre </td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td> 1º apellido </td>";
                    echo "<td> $apellido1 </td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td> 2º apellido </td>";
                    echo "<td> $apellido2 </td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td> Email </td>";
                    echo "<td> $email </td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td> Fecha de nacimiento </td>";
                    echo "<td> $nacimiento </td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td> Teléfono </td>";
                    echo "<td> $telefono </td>";
                echo "</tr>";
            echo "</table>";
        ?>
    </body>
</html>