<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 1</title>
        <style>
            img
            {
                height: 10em;
                Width: 10em;
            }
        </style>
    </head>
    <body>
    <?php
    $animales[0][0] = "Pingúino";
    $animales[0][1] = "pingu.jpeg";
    $animales[0][2] = "https://es.wikipedia.org/wiki/Spheniscidae";
    $animales[1][0] = "capybara";
    $animales[1][1] = "capybara.jpg";
    $animales[1][2] = "https://es.wikipedia.org/wiki/Hydrochoerus_hydrochaeris";
    $animales[2][0] = "alpaca";
    $animales[2][1] = "alpaca.jpg";
    $animales[2][2] = "https://es.wikipedia.org/wiki/Vicugna_pacos";
    $animales[3][0] = "Vaca de las tierras altas";
    $animales[3][1] = "toro escoces.png";
    $animales[3][2] = "https://es.wikipedia.org/wiki/Vaca_de_las_tierras_altas";
    $animales[4][0] = "zorro";
    $animales[4][1] = "zorro.jpg";
    $animales[4][2] = "https://es.wikipedia.org/wiki/Vulpini";

    $animal = rand(0,count($animales)-1);

    echo "<p>Actualice la página para ver un nuevo animal</p>";
    echo "<p><b>".$animales[$animal][0]."</b></p>";
    echo "<img src='IMG/". $animales[$animal][1]."'>";
    echo "<p><a href='".$animales[$animal][2]."'>Más información sobre este animal</a> en la wikipedia </p>";
?>
    </body>
</html>