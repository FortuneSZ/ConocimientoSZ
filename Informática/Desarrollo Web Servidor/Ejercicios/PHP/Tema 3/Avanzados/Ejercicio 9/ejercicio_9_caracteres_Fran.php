<?php
    $frase = "Ella no te ama";
    $chars = str_split($frase);
    $contador = 1;
    $palabraInpar = "";
    $numVocal = 0;
    $numA = 0;
    $numE = 0;
    $numI = 0;
    $numO = 0;
    $numU = 0;

    foreach($chars as $char)
    {
        if ($contador % 2 != 0)
        {
            $palabraInpar = $palabraInpar . $char;
        }

        if ($char == 'a' || $char == 'A')
        {
            $numA++;
        }

        if ($char == 'e' || $char == 'E')
        {
            $numE++;
        }

        if ($char == 'i' || $char == 'I')
        {
            $numI++;
        }

        if ($char == 'o' || $char == 'O')
        {
            $numO++;
        }

        if ($char == 'u' || $char == 'U')
        {
            $numU++;
        }

        if ($char == 'a' || $char == 'e' || $char == 'i' || $char == 'o' || $char == 'u' ||
        $char == 'A' || $char == 'E' || $char == 'I' || $char == 'O' || $char == 'U')
        {
            $numVocal++;
        }
        $contador++;
    }

    echo "<p> $palabraInpar </p>";
    echo "<p> En la frase $frase hay $numVocal vocales </p>";
    echo "<p> $numA a, $numE e, $numI i, $numO o y $numU u </p>";
?>