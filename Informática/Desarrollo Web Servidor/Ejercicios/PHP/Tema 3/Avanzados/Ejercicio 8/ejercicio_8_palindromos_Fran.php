<?php
    function Palindromo($palabra)
    {
        $inverso = strrev($palabra);

        if ($palabra == $inverso)
        {
            $palindromo = true;
        }
        else
        {
            $palindromo = false;
        }
        return $palindromo;
    }

    $palabras = array("Murcia","ada","sis","Sara","salas");

    foreach($palabras as $palabra)
    {
        if (Palindromo($palabra) == true)
        {
            echo "<p> $palabra es un palíndromo </p>";
        }
        else
        {
            echo "<p> $palabra no un palíndromo </p>";
        }
    }
    
?>