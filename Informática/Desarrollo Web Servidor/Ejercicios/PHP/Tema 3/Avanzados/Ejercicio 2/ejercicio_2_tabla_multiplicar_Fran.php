<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 3</title>
        <style>
            thead tr 
            {
                background-color: blue;
                color: white;
            }
            .orange
            {
                background-color: orange;
                color: white;
            }

        </style>
    </head>
    <body>
        <table border='*'>
            <thead>
                <tr>
                    <th> x </th>
                    <th> 0 </th>
                    <th> 1 </th>
                    <th> 2 </th>
                    <th> 3 </th>
                    <th> 4 </th>
                    <th> 5 </th>
                    <th> 6 </th>
                    <th> 7 </th>
                    <th> 8 </th>
                    <th> 9 </th>
                    <th> 10 </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    
                    for ($i = 0; $i <= 10; $i++)
                    {
                        echo "<tr>";
                        echo "<td class='orange'>" . $i  . "</td>";
                        for ($x = 0; $x <= 10; $x++)
                        {
                            echo "<td>" . $i * $x . "</td>";
                        }
                        echo "</tr>"; 
                    }
                ?>
            </tbody>
        </table>
        <?php
        ?>
    </body>
</html>