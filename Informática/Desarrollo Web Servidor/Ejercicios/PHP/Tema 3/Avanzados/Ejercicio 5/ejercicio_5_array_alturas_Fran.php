<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 5</title>
        <style>
            td
            {
                color: black;
            }
            .men
            {
                color: green;
            }
            .may
            {
                color: blue;
            }
        </style>
    </head>
    <body>
        <table border='*'>
            <?php
                for ($i = 0; $i < 6; $i++)
                {
                    for ($x = 0; $x < 9; $x++)
                    {
                        $numeros[$i][$x] = rand(100,999);
                    }
                }

                $mayor = 0;
                $colMayor = 0;
                $menor = 999;
                $filMenor = 0;
                
                for ($i = 0; $i < 6; $i++)
                {
                    for ($x = 0; $x < 9; $x++)
                    {
                         if ($numeros[$i][$x] > $mayor)
                         {
                            $mayor = $numeros[$i][$x];
                            $colMayor = $x;
                         }

                         if ($numeros[$i][$x] < $menor)
                         {
                            $menor = $numeros[$i][$x];
                            $filMenor = $i;
                         }
                    }
                }

                for ($i = 0; $i < 6; $i++)
                {
                    echo "<tr>";
                    for ($x = 0; $x < 9; $x++)
                    {
                        if ($i == $filMenor)
                        {
                            echo "<td class='men'>". $numeros[$i][$x]. "</td>";
                        }
                        else if ($x == $colMayor)
                        {
                            echo "<td class='may'>". $numeros[$i][$x]. "</td>";
                        }
                        else
                        {
                            echo "<td>". $numeros[$i][$x]. "</td>";  
                        }
                    }
                }
            ?>
        </table>
    </body>
</html>