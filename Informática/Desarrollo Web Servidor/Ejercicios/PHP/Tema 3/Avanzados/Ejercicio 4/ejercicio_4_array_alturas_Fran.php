
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 4</title>
        <style>
            
        </style>
    </head>
    <body>
        <table border='*'>
            <tr>
                <th> Nombre </th>
                <th> Altura </th>
            </tr>
            <?php
                $personas = array('Fran García' => 1.70, 'Sara Gordillo' => 1.70,
                'Marina Morales' => 1.50, 'John Ramirez' => 1.80, "Valeria Aroca" => 1.65);

                $media = 0;

                foreach($personas as $nombre=>$altura)
                {
                    echo "<tr>";
                    echo "<td> $nombre </td>";
                    echo "<td> $altura </td>";
                    echo "</tr>";
                    $media = $media + $altura;
                }

                $media = $media / count($personas);
                echo "<tr>";
                echo "<td> media de altura </td>";
                echo "<td> $media </td>";
                echo "</tr>";
                ?>
        </table>
    </body>
</html>
