<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ejercicio 4</title>
        <style>
            
        </style>
    </head>
    <body>
        <table border='*'>
            <tr>
                <th> Producto </th>
                <th> Precio euros </th>
                <th> Precio pesetas </th>
            </tr>
            <?php 
                require_once("ejercicio_7_conversor_Fran.php");

                $productos = array("Leche" => 1.5,"Lasaña" => 4,"Pizza" => 3);
                $sumaEuros = 0;
                $sumaPesetas = 0;

                foreach($productos as $producto=>$precio)
                {
                    $precioP = euros2pesetas($precio);
                    $sumaEuros = $sumaEuros + $precio;
                    $sumaPesetas = $sumaPesetas + $precioP;

                    echo "<tr>";
                    echo "<td> $producto </td>";
                    echo "<td> $precio </td>";
                    echo "<td> $precioP </td>";
                    echo "</tr>";
                }

                echo "<tr>";
                echo "<td> Suma de precios </td>";
                echo "<td> $sumaEuros </td>";
                echo "<td> $sumaPesetas </td>";
                echo "</tr>";
            ?>
        </table>
    </body>
</html>