<?php
function arrayPares(array &$array): int
{
    $pares = 0;
    foreach($array as $numero)
    {
        if ($numero%2 == 0)
        {
            $pares++;
        }
    }
    return $pares;
}

$numeros = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
$pares = arrayPares($numeros);
echo "Hay $pares números pares";
?>