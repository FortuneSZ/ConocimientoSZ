<?php

class Router 
{
    //almacena las rutas solicitadas por el usuario
    private $_uri = array();
    //almacena las acciones que deben realizarse en cada ruta
    private $_action = array();

    //método que agrega rutas al enrutador
    public function add($uri, $action = null) 
    {
        $this->_uri[] = '/' . trim($uri, '/');

        if ($action != null) 
        {
            $this->_action[] = $action;
        }
    }
    //función con la lógica principal del enrutador
    public function run() 
    {
        //comprueba si la URL solicitada por el cliente está informada sino la establece como /
        $uriGet = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
        //recorre el array uri, donde se guardan las rutas configuradas para redirecciones
        foreach ($this->_uri as $key => $value) 
        {   //si encuentra la uri del cliente con las configuradas llama a runAction -accion-con la uri original 
            if (preg_match("#^$value$#", $uriGet)) 
            {
                $action = $this->_action[$key]; //header()
                $this->runAction($action);
            }
        }
    }

    private function runAction($action) 
    {
        $action();

    }

}
?>