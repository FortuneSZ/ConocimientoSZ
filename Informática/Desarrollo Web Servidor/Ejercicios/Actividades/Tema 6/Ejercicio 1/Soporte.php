<?php
define("IVA",21);
class Soporte
{
    public $titulo = "";
    protected $numero = 0;
    private $precio = 0;

    public function __construct($t,$n,$p)
    {
        $this->titulo = $t;
        $this->numero = $n;
        $this->precio = $p;
    }


    public function getPrecio()
    {
        return $precio;
    }

    public function getPrecioConiva()
    {
        $IVA = IVA / 100;
        $precioIVA = $this->precio +  ($this->precio * $IVA);
        return $precioIVA;
    }

    public function getNumero()
    {
        return $numero;
    }

    public function muestraResumen()
    {
        echo "<p><b> " . $this->titulo . "</b></p>";
        echo "<p>Precio: " . $this->precio . "</p>";
        $precioIVA = $this->getPrecioConiva();
        echo "<p>Precio con IVA: " . $precioIVA . "</p>";
    }

}
?>