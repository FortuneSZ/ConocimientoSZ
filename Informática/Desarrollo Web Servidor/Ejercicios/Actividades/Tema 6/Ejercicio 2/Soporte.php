<?php
define("IVA",21);
class Soporte
{
    public $titulo = "";
    protected $numero = 0;
    protected $precio = 0;

    public function __construct($t,$n,$p)
    {
        $this->titulo = $t;
        $this->numero = $n;
        $this->precio = $p;
    }


    public function getPrecio()
    {
        return $precio;
    }

    public function getPrecioConiva()
    {
        $IVA = IVA / 100;
        $precioIVA = $this->precio +  ($this->precio * $IVA);
        return $precioIVA;
    }

    public function getNumero()
    {
        return $numero;
    }

    public function muestraResumen()
    {
        echo "<p><b> " . $this->titulo . "</b></p>";
        echo "<p>Precio: " . $this->precio . " euros </p>";
        $precioIVA = $this->getPrecioConiva();
        echo "<p>Precio con IVA: " . $precioIVA . " euros </p>";
    }
}

class CintaVideo extends Soporte
{
    private $duracion = 0;

    public function __construct($t,$n,$p,$d)
    {
        parent::__construct($t,$n,$p);
        $this->duracion = $d;
    }

    public function muestraResumen()
    {
        parent::muestraResumen();
        echo "<p> Película en VHS: </p>";
        echo "<p> $this->titulo </p>";
        echo "<p> $this->precio € (IVA no incluido) </p>";
        echo "<p> Duración: $this->duracion minutos </p>";
    }
}
?>