<?php
function intercambia(&$a,&$b)
{
    $aux = $a;
    $a = $b;
    $b = $aux;
}
$a = 2;
$b = 3;

echo ("Valor original de a = $a" . "<br>");
echo ("Valor original de b = $b" . "<br>");
intercambia($a,$b);
echo ("Valor actual de a = $a" . "<br>");
echo ("Valor actual de b = $b" . "<br>");
?>