<?php

function calculaDescuento ($precio,$descuento = 0)
{
    $descontar = ($precio * $descuento) / 100;
    return ($precio - $descontar);
}

function intercambia(&$a,&$b)
{
    $aux = $a;
    $a = $b;
    $b = $aux;
}

function cuenta($a, $b)
{
    if ($a < $b)
    {
        for ($i = $a; $i <= $b; $i++)
        {
            echo ($i);
            if ($i != $b)
            {
                echo (",");
            }
        }
    }
    else
    {
        for ($i = $b; $i <= $a; $i++)
        {
            echo ($i);
            if ($i != $a)
            {
                echo (",");
            }
        }
    }
}

function saludo($n)
{
    echo ("Hola, $n");
}
?>