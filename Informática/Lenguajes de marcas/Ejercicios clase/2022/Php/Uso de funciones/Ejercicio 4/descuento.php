<?php
function calculaDescuento ($precio,$descuento = 0)
{
    $descontar = ($precio * $descuento) / 100;
    return ($precio - $descontar);
}
$productoA = 250;
$productoB = 85;

echo ("Producto de precio $productoA con un descuento del 10% quedaría en " . calculaDescuento($productoA,10) . "<br>");
echo ("Producto de precio $productoB sin descuento quedaría en " . calculaDescuento($productoB));
?>