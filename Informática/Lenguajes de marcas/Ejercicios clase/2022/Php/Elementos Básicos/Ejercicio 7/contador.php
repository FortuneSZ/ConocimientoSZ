<h1>Contadores</h1>

<p> Este contador va del 1 al 100: </p>

<p><?php
for ($i = 1; $i <= 100; $i++)
{
    echo ($i);
    if ($i != 100)
    {
        echo (",");
    }
}
echo ("<br>");?></p>

<p> Este otro va del 10 al 0: </p>

<p>
<?php
$x = 10;

while ($x >= 0)
{
    echo ($x);
    if ($x != 0)
    {
        echo ("-");
    }
    $x--;
}
?>
</p>