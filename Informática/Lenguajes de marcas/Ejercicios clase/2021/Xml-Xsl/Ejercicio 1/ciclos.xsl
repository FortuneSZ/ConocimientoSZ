<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head>
<title> Ciclos </title>
</head>
<body>
	<ul>
		<xsl:for-each select="ciclo/modulo">
		<li><xsl:value-of select="nombre"/> (<xsl:value-of select="horas/@valor"/> horas)</li>
		</xsl:for-each>
	</ul>
</body>
</html>
</xsl:template>
</xsl:stylesheet>