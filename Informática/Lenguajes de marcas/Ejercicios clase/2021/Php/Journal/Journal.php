<!doctype html>
<html lang = "en" >
	<head>
		<title> My Trip Around the USA on a Segway </title>
	</head>
	<body>
	<?php 
	$Parrafoin = "<p>";
	$Parrafofin = "</p>";
	$h1 = "<h1>";
	$h1fin = "</h1>";
	$h2 = "<h2>";
	$h2fin = "</h2>";
	$img = "<img ";
	$imgroute = "journal/segway2.jpg";
	$imgalt1 = '"Tony photographing next to his segway"';
	$imgalt2 = '"Tony climbed on his segway"';
	$imgroute2 = "journal/segway1.jpg";
	$imgend = ">";
	$ol = "<ol>";
	$olend = "</ol>";
	$ul = "<ul>";
	$ulend = "</ul>";
	$li = "<li>";
	$liend = "</li>";
	$block = "<blockquote>";
	$blockend = "</blockquote>";
	$br = "<br/>";
	$q = "<q>";
	$qend = "</q>";
	
	echo $h1, "Segway'n USA",$h1fin;
	
	echo $Parrafoin,"Documenting my trip around the US on my very own Segway!",$Parrafofin;
	
	echo $h2,"August 20, 2012",$h2fin;
	
	echo $img,"src=",$imgroute," alt=",$imgalt1,$imgend;
	
	echo $Parrafoin,"Well I made it 1200 miles already, and I passed
		through some interesting places on the way:",$Parrafofin;
		
	echo $ol;
	echo $li,"Walla Walla, WA",$liend;
	echo $li,"Magic City, ID",$liend;
	echo $li,"Bountiful, UT",$liend;
	echo $li,"Last Chance, CO",$liend;
	echo $li,"Truth or Consequences, NM",$liend;
	echo $li,"Why, AZ",$liend;
	echo $olend;
	
	echo $h2,"July 14, 2012",$h2fin;
	
	echo $Parrafoin,"I saw some Burma Shave style signs on the side of the
	road today:",$Parrafofin;
	
	echo $block;
	echo "Passing cars,",$br;
	echo "When you can't see,",$br;
	echo "May get you,",$br;
	echo "A glimpse,",$br;
	echo "Of eternity.",$br;
	echo $blockend;
	
	echo $Parrafoin,"I definitely won't be passing any cars.",$Parrafofin;
	
	echo $h2,"June 2, 2012",$h2fin;
	
	echo $img,"src=",$imgroute2," alt=",$imgalt2,$imgend;
	
	echo $Parrafoin,"My first day of the trip!  I can't believe I finally got
	everything packed and ready to go.  Because I'm on a Segway,
	I wasn't able to bring a whole lot with me:";
	
	echo $ul;
	echo $li,"cellphone",$liend;
	echo $li,"iPod",$liend;
	echo $li,"digital camera",$liend;
	echo $li,"a protein bar",$liend;
	echo $ulend;
	echo $Parrafofin;
	
	echo $Parrafoin,"Just the essentials.  As
	Lao Tzu would have said,",$q,"A journey of a 
	thousand miles begins with one Segway.",$qend,$Parrafofin;
	?>
	</body>
</html>	