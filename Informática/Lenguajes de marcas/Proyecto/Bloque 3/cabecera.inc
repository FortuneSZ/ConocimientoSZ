<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Cooking companions</title>
	<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
	<!-- Estilos -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="CSS/index.css">
	<!-- Fuentes insertadas -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=La+Belle+Aurore&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Creepster&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Satisfy&display=swap" rel="stylesheet">
	
</head>
<body>
	<!-- Encabezado -->
    <header>
		<!-- Logo y nombre -->
		<div id="logo">
		<img src="img/Cooking_Companions_Logo.png" alt="" id="logoimg">
		<h1> Cooking companions </h1>
		</div>
		<!-- Barra de navegación -->
		<nav> 
			<p> <a href="index.php">Inicio</a> </p>
			<p> <a href="form_receta.php">Nueva receta</a> </p>
		</nav>
	</header>