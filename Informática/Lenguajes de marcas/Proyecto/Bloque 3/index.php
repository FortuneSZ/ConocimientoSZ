<?php include('cabecera.inc'); ?>
<?php include('basedatos.inc'); ?>
	<main class="container-fluid">
		<!-- Barra de búsqueda -->
		<section id="seachbar">
			<p>Buscar recetas:<input type="text" placeholder="Escribe la receta aquí" id="buscador"><button id="buscar">Buscar</button>
			<button id="mostrar">Ver opciones de filtrado</button><button id="ocultar">ocultar opciones de filtrado</button><button id="borrar">Borrar filtros</button></p>

			<form action="index.php" method="post">
				<p id ="filtrado">Dificultad <select id="dificultad" name="dificultad">
					<option value="cualquiera">cualquiera</option>
					<option value="baja">baja</option>
					<option value="media">media</option>
					<option value="alta">alta</option>
				</select>Tiempo total inferior a (min) <input type="text" id="tiempo" name="tiempo"></p>
			</form>
		</section>
		
		<div class="row">
		<?php
		if (!isset($_POST['dificultad']) && !isset($_POST['tiempo']))
		{
			$consulta = $pdo->prepare("SELECT * FROM recetas ORDER BY 'titulo'");
			$consulta->execute();
			while($registro = $consulta->fetch())
			{
				$id = $registro['id'];
				$direccion = ('detalle.php?id='.$id . '"');
				$imagen = $registro['imagen'];
				echo ('<div class="col-sm-5 p-2">');
				echo ("<h2>" . $registro['titulo'] . "</h2>");
				echo ('<a href="'.$direccion.">" . '<img src=' . $imagen . " class='imgtext' alt=''>" . '</a>');
				echo ('<p class="recipetext">' . $registro['resumen'] . '</p></div>');
			}
		}
		else if (!isset($_POST['dificultad']) && isset($_POST['tiempo']))
		{
			$dif = $_POST['dificultad'];
			$tiem = $_POST['tiempo'];
			$consulta = $pdo->prepare("SELECT * FROM recetas WHERE coccion > '$tiem' ORDER BY 'titulo'");
			$consulta->execute();
			while($registro = $consulta->fetch())
			{
				$id = $registro['id'];
				$direccion = ('detalle.php?id='.$id . '"');
				$imagen = $registro['imagen'];
				echo ('<div class="col-sm-5 p-2">');
				echo ("<h2>" . $registro['titulo'] . "</h2>");
				echo ('<a href="'.$direccion.">" . '<img src=' . $imagen . " class='imgtext' alt=''>" . '</a>');
				echo ('<p class="recipetext">' . $registro['resumen'] . '</p></div>');
			}
		}
		else if (isset($_POST['dificultad']) && !isset($_POST['tiempo']))
		{
			$dif = $_POST['dificultad'];
			$tiem = $_POST['tiempo'];
			$consulta = $pdo->prepare("SELECT * FROM recetas WHERE dificultad = '$dif' ORDER BY 'titulo'");
			$consulta->execute();
			while($registro = $consulta->fetch())
			{
				$id = $registro['id'];
				$direccion = ('detalle.php?id='.$id . '"');
				$imagen = $registro['imagen'];
				echo ('<div class="col-sm-5 p-2">');
				echo ("<h2>" . $registro['titulo'] . "</h2>");
				echo ('<a href="'.$direccion.">" . '<img src=' . $imagen . " class='imgtext' alt=''>" . '</a>');
				echo ('<p class="recipetext">' . $registro['resumen'] . '</p></div>');
			}
		}
		else
		{
			$dif = $_POST['dificultad'];
			$tiem = $_POST['tiempo'];
			$consulta = $pdo->prepare("SELECT * FROM recetas WHERE dificultad = '$dif' AND coccion > '$tiem' ORDER BY 'titulo'");
			$consulta->execute();
			while($registro = $consulta->fetch())
			{
				$id = $registro['id'];
				$direccion = ('detalle.php?id='.$id . '"');
				$imagen = $registro['imagen'];
				echo ('<div class="col-sm-5 p-2">');
				echo ("<h2>" . $registro['titulo'] . "</h2>");
				echo ('<a href="'.$direccion.">" . '<img src=' . $imagen . " class='imgtext' alt=''>" . '</a>');
				echo ('<p class="recipetext">' . $registro['resumen'] . '</p></div>');
			}
		}
		
		?>
		<!--</div> --> 
	</main>
	<!-- Pie de página --> 
	<footer>
		<p> Contacto:</p>
		<p>Teléfono: 654145599</p>
		<p>Correo: realSZmusic@hotmail.com</p>
	</footer>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src ="JS/inicio.js"></script>
</body>
</html>