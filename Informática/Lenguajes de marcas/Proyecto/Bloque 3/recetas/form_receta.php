<?php include('cabecera.inc'); ?>
	<!-- Formulario -->
	<main class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1>Nueva receta </h1>
			</div>
		</div>
		<form action="mipagina.php" method="post">
			<div class="row">
			
				<div class="col-xxl-12">
					<label class="form-label"> Título de la receta </label>
					<input class="form-control" type="text" placeholder="Título de la receta" id="titulo" required>
				</div>
				
				<div class="col-xxl-12">	
					<label class="form-label"> Comensales </label>
					<input class="form-control" type="number" placeholder="Comunesales" min="1" max="8" id="comensales" required>
				</div>
				
				<div class="col-xxl-12">	
					<label class="form-label"> Tiempo de preparación (minutos) </label>
					<input class="form-control" type="number" placeholder="Tiempo preparación (min)" id="tiempoprep" required>
				</div>
				
				<div class="col-xxl-12">	
					<label class="form-label"> Tiempo de cocción (minutos) </label>
					<input class="form-control" type="number" placeholder="Tiempo cocción (min)" id="tiempococ">
				</div>
				
				<div class="col-xxl-12">	
					<label class="form-label"> Dificultad </label>
					<select class="form-control" name="Dificultad">
					<option value="Alta"> Alta </option>
					<option value="Media"> Media </option>
					<option value="Baja" selected> Baja </option>
					</select>
				</div>
				
				<div class="col-xxl-12">	
					<label class="form-label"> Resumen </label>
					<input class="form-control" type="text" id="resumen" required> </input>
				</div>
				
				<div class="col-xxl-12">	
					<label class="form-label"> Descripción </label>
					<input class="form-control" id="desc" required> </input>
				</div>
				
				<div class="col-xxl-6">	
					<input type="submit" value="Enviar" class="form-control btn btn-dark">
				</div>
				
				<div class="col-xxl-6">	
					<input type="reset" value="Borrar" class="form-control btn btn-dark">
				</div>
			</div>
		</form>
	</main>
	<!-- Pie de página --> 
	<footer>
		<p> Contacto:</p>
		<p>Teléfono: 654145599</p>
		<p>Correo: realSZmusic@hotmail.com</p>
	</footer>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src ="../js/formulario.js"></script>
</body>
</html>