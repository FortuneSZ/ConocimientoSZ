<?php include('cabecera.inc'); ?>
<?php include('basedatos.inc'); ?>
	<!-- Receta detallada -->
	<main class="container-fluid">
		<?php
		$consulta = $pdo->prepare("SELECT * FROM recetas ORDER BY 'titulo'");
		$consulta->execute();
		while($registro = $consulta->fetch())
		{
			
		}
		?>
		<h1>spaghetti carbonara </h1>
		<div class="row">
		
			<div class="col-sm-5 p-2" id="img">
				<img src="../img/espaguetis-carbonara.jpg" class="imgtext" alt="">
			</div>
			<!-- Ingredientes -->
			<div class="col-sm-6 p-2">
				<h1> Ingredientes </h1>
				<ul id="ingredientes">
					<li>400g de espagueti</li>
					<li> huevos</li>
					<li> 3 yemas de huevo </li>
					<li>150g de queso curado (parmesano, manchego…)</li>
					<li>120g de panceta</li>
					<li>sal y pimienta</li>
					<li>un pequeño chorrito de aceite de oliva</li>
				</ul>
			</div>
			
		</div>
		
		<div class="row">		
			<!-- Detalles -->
			<div class="col-sm-5 p-2" id="detalles">
				<p> 4 comensales </p>
				<p> Tiempo de preparación: 5 min </p>
				<p> Tiempo de cocción: 12 min </p>
				<p> Dificultad: Baja </p>
			</div>			
			<!-- Preparación -->
			<div class="col-sm-6 p-2">
				<h1> Preparación </h1>
				<p>No utilices bacon o bacon ahumado para hacer esta receta. Busca el auténtico <i>guanciale italiano</i>, hecho con la papada del cerdo y si no, 
				utiliza en su lugar una buena <b>panceta fresca, retirando la piel</b>. </p>
				<p>Córtala en trocitos de pequeño tamaño y fríe esos trocitos hasta que comiencen a dorarse.</p>
				<p>Por otro lado <b>bate las 3 yemas de huevo con el huevo y el queso pecorino recién rallado hasta hacer una mezcla algo espesa</b>. Dale unos toques de pimienta negra a esa mezcla.</p>
				<p>Entre tanto, pon la pasta a cocer en agua abundante con sal, y sácala cuando falten un par de minutos para que esté al dente.</p>
				<p> En la sartén donde habíamos reservado la panceta o el guanciale salteado, agregaremos la pasta escurrida a la que añadimos la mezcla de huevos, queso y pimienta, mezclando bien.</p> 
				<p>Agregamos también un <b>cucharón del agua de cocción de la pasta</b>. </p>
				<p> Removemos mientras se van integrando los distintos ingredientes y se va haciendo una especie de salsa al <b>cuajar ligeramente el huevo con el caldo caliente de la cocción</b>. </p>
				<p>Servimos espolvoreando de un poco más de pecorino rallado y dando un par de vueltas más de pimienta molida. </p>
			</div>
		</div>
	</main>
	<!-- Pie de página --> 
	<footer>
		<p> Contacto:</p>
		<p>Teléfono: 654145599</p>
		<p>Correo: realSZmusic@hotmail.com</p>
	</footer>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src ="js/otros.js"></script>
</body>
</html>