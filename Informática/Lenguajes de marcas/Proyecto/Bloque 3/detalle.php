<?php include('cabecera.inc'); ?>
<?php include('basedatos.inc'); ?>
	<!-- Receta detallada -->
	<main class="container-fluid">
	<?php
		$idReceta = $_REQUEST["id"];
		$consulta = $pdo->prepare("SELECT * FROM recetas WHERE id = $idReceta");
		$consulta->execute();
		while($registro = $consulta->fetch())
		{
			echo ('<h1>'.$registro['titulo'].'</h1>');
			echo ('<div class="row">');
			echo ('<div class="col-sm-5 p-2" id="img">');
			echo ('<img src="'.$registro['imagen'].'" class="imgtext" alt="">');
			echo ('</div>');

			echo ('<div class="col-sm-6 p-2">');
			echo ('<h1> Ingredientes </h1>');
			echo ('<ul id="ingredientes">');
			echo ('<li>400g de espagueti</li>');
			echo ('<li> huevos</li>');
			echo ('<li> 3 yemas de huevo </li>');
			echo ('<li>150g de queso curado (parmesano, manchego…)</li>');
			echo ('<li>120g de panceta</li>');
			echo ('<li>sal y pimienta</li>');
			echo ('<li>un pequeño chorrito de aceite de oliva</li>');
			echo ('</ul>');
			echo ('</div>');
			echo ('</div>');

			echo ('<div class="row">');
			echo ('<div class="col-sm-5 p-2" id="detalles">');
			echo ('<p> 4 comensales </p>');
			echo ('<p> Tiempo de preparación: '.$registro['preparacion'].'min </p>');
			echo ('<p> Tiempo de cocción: '.$registro['coccion'].'min </p>');
			echo ('<p> Dificultad: '.$registro['dificultad'].'</p>');

			echo ('</div>');
			echo ('<div class="col-sm-6 p-2">');
			echo ('<h1> Preparación </h1>');
			echo ('<p>'.$registro['resumen'].'</p>');
			echo ('</div>');
			echo ('</div>');
		}
		?>
	</main>
	<!-- Pie de página --> 
	<footer>
		<p> Contacto:</p>
		<p>Teléfono: 654145599</p>
		<p>Correo: realSZmusic@hotmail.com</p>
	</footer>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src ="js/otros.js"></script>
</body>
</html>