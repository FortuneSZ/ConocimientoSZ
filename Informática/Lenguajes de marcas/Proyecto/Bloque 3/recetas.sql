-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2023 a las 20:58:00
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `recetas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE `recetas` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `resumen` text NOT NULL,
  `preparacion` int(11) NOT NULL,
  `coccion` int(11) NOT NULL,
  `dificultad` varchar(20) NOT NULL,
  `imagen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id`, `titulo`, `resumen`, `preparacion`, `coccion`, `dificultad`, `imagen`) VALUES
(1, 'carne en salsa', 'Hoy quiero enseñaros a preparar la mejor receta de carne en salsa, para disfrutar de su textura melosa y tierna y de su sabor tradicional.\r\n\r\n', 20, 30, 'media', 'https://i.ytimg.com/vi/QKZhGYEjJgc/maxresdefault.jpg'),
(2, 'ensaladilla rusa\r\n', 'La tradicional receta de ensaladilla rusa con patata, zanahoria, huevo y atún con mayonesa.\n\n', 10, 15, 'baja', 'https://cocina-casera.com/wp-content/uploads/2018/05/ensaladilla-rusa-700x485.jpg'),
(3, 'spaghetti carbonara', 'Los espaguetis a la carbonara es probablemente la forma más internacional de preparar esta pasta. La auténtica salsa carbonara de italia contiene yema de huevo, queso y bacon. No tiene nata, ingrediente que le solemos añadir en España.\r\n\r\n', 10, 5, 'bajo', 'https://img.jamieoliver.com/jamieoliver/recipe-database/oldImages/large/1558_1_1436795948.jpg?tr=w-800,h-1066'),
(4, 'tortilla española\r\n', 'La tradicional receta de tortilla de patatas o tortilla española, un plato básico de la cocina española a base de patatas, huevo y cebolla.\r\n\r\n', 20, 20, 'media', 'https://www.cocinavital.mx/wp-content/uploads/2019/04/tortilla-espanola-clasica-fb.jpg'),
(5, 'pollo', 'es pollo', 5, 10, 'Baja', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `recetas`
--
ALTER TABLE `recetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
