<?php include('basedatos.inc'); 
if (isset($_POST['titulo']) && isset($_POST['comensales']) && isset($_POST['prep']) && isset($_POST['coc']) && isset($_POST['Dificultad']) && isset($_POST['resumen']))
{
    $insercion = $pdo->prepare("INSERT INTO recetas(titulo, resumen, preparacion, coccion, dificultad)" .
            " VALUES(:titulo, :resumen, :preparacion, :coccion, :dificultad)");
            $insercion->bindParam(':titulo', $_POST['titulo']);
            $insercion->bindParam(':resumen', $_POST['resumen']);
            $insercion->bindParam(':preparacion', $_POST['prep']);
            $insercion->bindParam(':coccion', $_POST['coc']);
            $insercion->bindParam(':dificultad', $_POST['Dificultad']);
            $insercion->execute();
            header("Location:index.php");
}
?>

