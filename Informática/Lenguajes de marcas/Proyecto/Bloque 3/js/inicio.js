
//Función que elimina las recetas en pantalla
function BorrarRecetas()
{
    let elem = document.getElementsByClassName("row");
    let div1 = elem[0];
    let recetas1 = document.getElementsByClassName("col-sm-6 p-2")
    let recetas2 = document.getElementsByClassName("col-sm-5 p-2")
    do
    {
        if (recetas1.length != 0)
        {
            let receta = recetas1[0];
            div1.removeChild(receta);
        }
        if (recetas2.length != 0)
        {
            let receta2 = recetas2[0];
            div1.removeChild(receta2);    
        }
        
    }
    while(recetas1.length != 0 || recetas2.length != 0);
}

    let mostrar = document.getElementById("mostrar");
    let ocultar = document.getElementById("ocultar");
    let filtrado = document.getElementById("filtrado");
    let borrar = document.getElementById("borrar");
    //Función que muestra las opciones de búsqueda
    mostrar.addEventListener("click", function()
    {
        mostrar.style.display = "none";
        ocultar.style.display = "inline";
        filtrado.style.display = "inline";
        
    })
    //Función que oculta las opciones de búsqueda
    ocultar.addEventListener("click",function()
    {
        ocultar.style.display = "none";
        mostrar.style.display = "inline";
        filtrado.style.display = "none";
    })
    borrar.addEventListener("click",function()
    {
        let textobuscar = document.getElementById("buscador");
       let display = document.getElementById("dificultad");
       let eleccion = display.options[display.selectedIndex].innerText;
       let tiempo = document.getElementById("tiempo");

       textobuscar.value = "";
       display.value = "cualquiera";
       tiempo.value = "";

    })

