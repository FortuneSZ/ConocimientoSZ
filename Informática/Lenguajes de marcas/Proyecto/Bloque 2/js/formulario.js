let titulo = document.getElementById("titulo");
titulo.addEventListener("invalid",function()
{
    titulo.style.backgroundColor = "red";
    titulo.style.border = "3px solid red";
})
titulo.addEventListener("input",function()
{
    titulo.style.backgroundColor = "white";
    titulo.style.border = "0px solid red";
});

let comensales = document.getElementById("comensales");
comensales.addEventListener("invalid",function()
{
    comensales.setCustomValidity("El número de comensales debe estar entre 1 y 8");
    comensales.style.border = "3px solid red";
})
comensales.addEventListener("input",function()
{
    comensales.setCustomValidity("");
    comensales.style.border = "0px solid red";
});

let preparacion = document.getElementById("tiempoprep");
preparacion.addEventListener("invalid",function()
{
    if (preparacion.value > 0)
    {
        preparacion.setCustomValidity("");
        preparacion.style.border = "0px solid red";
    }
    else
    {
        preparacion.setCustomValidity("El tiempo de preparación debe ser positivo mayor que 0");
        preparacion.style.border = "3px solid red";
    }
});

let coccion = document.getElementById("tiempococ");
coccion.addEventListener("input",function()
{
    if (coccion.value != "")
    {
        if (coccion.value < 0)
        {
            coccion.setCustomValidity("El tiempo de preparación debe ser positivo");
            coccion.style.border = "3px solid red";         
        }
        else
        {
            coccion.setCustomValidity("");
            coccion.style.border = "0px solid red";
        }
    }
    else
    {
        coccion.setCustomValidity("");
        coccion.style.border = "0px solid red";
    }
});

let resumen = document.getElementById("resumen");
resumen.addEventListener("invalid",function()
{
    resumen.style.border = "3px solid red";
})
resumen.addEventListener("input",function()
{
    resumen.style.border = "0px solid red";
});

let descripcion = document.getElementById("desc");
descripcion.addEventListener("invalid",function()
{
    descripcion.style.border = "3px solid red";
})
descripcion.addEventListener("input",function()
{
    descripcion.style.border = "0px solid red";
});
