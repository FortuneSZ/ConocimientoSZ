<?php
    include "includes/cabecera.inc";
    include('includes/conexion.inc');
?>
    <section id="formulario">
        <h1>Nueva entrada</h1>
        <p>Rellena los datos a continuación para modificar un registro...</p>
       
        <form action="modificarEntrada.php" method="post" enctype="multipart/form-data">
            <label class="nuevo">
                <span>Curso:</span>
                <input type="text" name="curso" id="curso" size="20"required>
            </label>
            <label class="nuevo">
                <span>Nombre alumno:</span>
                <input type="text" name="alumno" id="alumno" size="30" required>
            </label>
            <label class="nuevo">
                <span>Nota:</span>
                <input type="number" name="nota" id="nota" min="1" max="10" required>
            </label>
            <input name="submit" type="submit" value="Modificar datos">
        </form>

        <?php
        if (isset($_REQUEST['curso']) && isset($_REQUEST['alumno'])
        && isset($_REQUEST['nota']))
        {
            $curso = $_REQUEST['curso'];
            $alumno = $_REQUEST['alumno'];
            $nota = $_REQUEST['nota'];

            $modification = $pdo->prepare("UPDATE notas 
            SET nota = $nota WHERE curso = '$curso' AND alumno = '$alumno'");
            $modification->execute();
            header("Location:inicio.php");
        }
        ?>
</body>
</html>