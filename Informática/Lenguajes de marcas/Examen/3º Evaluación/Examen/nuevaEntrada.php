<?php
    include "includes/cabecera.inc";
    include('includes/conexion.inc');
?>
    <section id="formulario">
        <h1>Nueva entrada</h1>
        <p>Rellena los datos a continuación para introducir un registro...</p>
       
        <form action="nuevaEntrada.php" method="post" enctype="multipart/form-data">
            <label class="nuevo">
                <span>Código:</span>
                <input type="text" name="codigo" id="codigo" size="5"required>
            </label>
            <label class="nuevo">
                <span>Curso:</span>
                <input type="text" name="curso" id="curso" size="20"required>
            </label>
            <label class="nuevo">
                <span>Nombre alumno:</span>
                <input type="text" name="alumno" id="alumno" size="30" required>
            </label>
            <label class="nuevo">
                <span>Imagen:</span>
                <input type="text" name="imagen" id="imagen">
            </label>
            <label class="nuevo">
                <span>Nota:</span>
                <input type="number" name="nota" id="nota" min="1" max="10" required>
            </label>
            <input name="submit" type="submit" value="Guardar datos">
        </form>

        <?php
        if (isset($_REQUEST['codigo']) && isset($_REQUEST['curso']) && isset($_REQUEST['alumno'])
        && isset($_REQUEST['imagen']) && isset($_REQUEST['nota']))
        {
            $codigo = $_REQUEST['codigo'];
            $curso = $_REQUEST['curso'];
            $alumno = $_REQUEST['alumno'];
            $imagen = $_REQUEST['imagen'];
            $nota = $_REQUEST['nota'];

            $insercion = $pdo->prepare("INSERT INTO notas(codigo, curso, alumno, foto, nota)" .
            " VALUES(:codigo, :curso, :alumno, :foto, :nota)");
            $insercion->bindParam(':codigo', $codigo);
            $insercion->bindParam(':curso', $curso);
            $insercion->bindParam(':alumno', $alumno);
            $insercion->bindParam(':foto', $imagen);
            $insercion->bindParam(':nota', $nota);
            $insercion->execute();
            header("Location:inicio.php");
        }
        ?>
</body>
</html>