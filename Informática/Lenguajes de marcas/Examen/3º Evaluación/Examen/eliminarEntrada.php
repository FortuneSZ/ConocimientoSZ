<?php
    include "includes/cabecera.inc";
    include('includes/conexion.inc');
?>
    <section id="formulario">
        <h1>Nueva entrada</h1>
        <p>Rellena los datos a continuación para borrar un registro...</p>
       
        <form action="eliminarEntrada.php" method="post" enctype="multipart/form-data">
            <label class="nuevo">
                <span>Código alumno:</span>
                <input type="text" name="codigo" id="codigo" size="50"required>
            </label>
            <input name="submit" type="submit" value="Eliminar notas">
        </form>

        <?php
        if (isset($_REQUEST['codigo']))
        {
            $codigo = $_REQUEST['codigo'];
            $delete = $pdo->prepare("DELETE FROM notas WHERE codigo = $codigo");
            $delete->execute();
            header("Location:inicio.php");
        }
        ?>
</body>
</html>