<?php include('includes/conexion.inc'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/estilo_login.css"/>
</head>
 
<body>
    
    <?php
        session_start();
        // Se comprueba si ya se habia iniciado la sesion y en ese caso le redirigimos a la pagina de inicio
        if(isset($_SESSION['loginUsuario']))
        {
            header("Location:a inicio.php");
        }
    ?>
   
    <section>
        <div id="formulario_login">
            <h2>Login</h2>
            <form action="index.php" method="POST">
                <div class="campo">
                    <label for="usuario">Usuario:</label>
                    <input type='text' name="usuario" maxlength="20"/>
                </div>
                <div class="campo">
                    <label for="password">Contraseña:</label>
                    <input type='password' name="password" maxlength="20"/>
                </div>
                <div class="boton">
                    <input type="submit" id="login" name="login" value="Validar">
                </div>
				 <div id="enlaceRegistro">
                    <a href="nuevoAdministrador.php"> Registrarse </a>
                </div>
            </form> 
               
            <?php
                if (isset($_REQUEST['errores']))
                {
                    $errores = $_REQUEST['errores'];
                }
                else
                {
                    $errores = 0;
                }

                if ($errores >= 3)
                {
                    echo ("<p>Rindete</p>");
                }

                if (isset($_REQUEST['usuario']) && isset($_REQUEST['password']))
                {
                    $usuario = $_REQUEST['usuario'];
                    $contraseña = $_REQUEST['password'];
                    $entrar = false;
                    
                    $consulta = $pdo->prepare("SELECT * FROM administradores");
                    $consulta->execute();
                    while($registro = $consulta->fetch())
                    {
                        if ($registro['login'] == $usuario && $registro['contraseña'] == $contraseña)
                        {
                            $entrar = true;
                        }
                    }
                    if ($entrar == true)
                    {
                        $_SESSION["loginUsuario"] = $user;
                        header("Location:inicio.php");
                    }
                    else
                    {
                        $errores++;
                        header("Location:index.php?errores=".$errores);
                    }
                }
                if(!empty($_GET['errores'])) 
                {
            ?>
                <p class="errorUsuario">Usuario o pasword incorrectos</p>
            <?php } ?>
        </div>
    </section>
</body>
</html> 