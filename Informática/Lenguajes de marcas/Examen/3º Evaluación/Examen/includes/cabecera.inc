<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Academia Herculana</title>
    <link rel="stylesheet" href="./css/estilos.css">
</head>
<body>
    <header>
		<h1> <font color="blue">Academia Herculana</font></h1>
        <nav>
            <ul>
                <li><a href="inicio.php">Inicio</a></li>
                <li><a href="nuevaEntrada.php">Nueva entrada</a></li>
                <li><a href="eliminarEntrada.php">Borrar entrada</a></li>
				<li><a href="mostrarEntrada.php">Mostrar entrada</a></li>
				<li><a href="modificarEntrada.php">Modificar entrada</a></li>
            </ul>
        </nav>
    </header>