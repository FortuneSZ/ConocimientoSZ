<?php
    include "includes/cabecera.inc";
    include('includes/conexion.inc');
?>
    <section id="formulario">
        <h1>Nueva entrada</h1>
        <p>Rellena los datos a continuación para mostrar un registro...</p>
       
        <form action="mostrarEntrada.php" method="post" enctype="multipart/form-data">
            <label class="nuevo">
                <span>Código alumno:</span>
                <input type="text" name="codigo" id="codigo" size="50"required>
            </label>
            <input name="submit" type="submit" value="mostrar registro">
        </form>
        <br>

        <?php
        if (isset($_REQUEST['codigo']))
        {
            $codigo = $_REQUEST['codigo'];
            $consulta = $pdo->prepare("SELECT * FROM notas WHERE codigo = $codigo");
            $consulta->execute();
            while($registro = $consulta->fetch())
            {
                echo ('<section class="curso">');
                echo ('<h3>Curso:' . $registro['curso'] . '</h3>');
                echo ('<h4>Nota media de:</h4>');
                echo ('<p><img src="imagenes/' . $registro['foto'] . '"></p>');
                echo ('<p>' . $registro['alumno'] . '</p>');
                echo ('<p>' . $registro['nota'] . '</p>');
            }
        }
        ?>
</body>
</html>