<?php
    include "includes/cabecera.inc";
    include('includes/conexion.inc');
?>
    <section id="favoritos">
        <h1>Notas medias de los alumnos</h1>
        <p>En esta sección aparecen las notas medias de los alumnos. Puedes dar de alta nuevos alumnos, buscar,... desde el menú superior.</p>
        <section id="resultados">
                <!-- Aquí dejo un contenido estático que sirva
                 de guía a cómo debe quedar el listado. Este contenido
                 deberá quitarse y/o reemplazarse  -->
           <?php 
            $consulta = $pdo->prepare("SELECT * FROM notas GROUP BY alumno");
            $consulta->execute();
            while($registro = $consulta->fetch())
            {
                $media = 0;
                $numnotas = 0;
                $nombre = $registro['alumno'];
                echo ('<section class="curso">');
                echo ('<h4>Nota media de:</h4>');
                echo ('<p><img src="imagenes/' . $registro['foto'] . '"></p>');
                echo ('<p>' . $registro['alumno'] . '</p>');
                $consulta2 = $pdo->prepare("SELECT * FROM notas");
                $consulta2->execute();
                while($registro2 = $consulta2->fetch())
                {
                    if ($registro2['alumno'] === $registro['alumno'])
                    {
                        $media += $registro2['nota'];
                        $numnotas++;
                    }
                }
                $media/= $numnotas;
                echo ('<p>' . $media . '</p>');
                echo ('<p><a href="ficha.php' . '?nombre='.$nombre . '">Ver ficha</a></p>');
                echo ('</section>');
            } 
            ?>
        </section>
		<section class="final">
			<p><a href="guardarEntradas.php">Almacenar en fichero</a></p>
			<p><a href="cerrar_sesion.php">Cerrar conexión</a></p>
		</section>
    </section>
</body>
</html>