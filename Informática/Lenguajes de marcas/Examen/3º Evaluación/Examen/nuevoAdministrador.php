<?php
include('includes/conexion.inc');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    
    <link rel="stylesheet" type="text/css" href="css/estilo_login.css" media="screen"/>    
    <title>Registro</title>
</head>

<body>
    <section>
        <div id="formulario_registro">

            <h2>Formulario registro</h2>
            <form action="#" method=POST>
                <div class="campoFormulario">
                    <label for="usuario">Usuario: <span class="obligatorio">*</span></label>
                    <input type='text' id="usuario" name="usuario" maxlength="15" required/>
                </div>
                <div class="campoFormulario">
                    <label for="password">Contraseña: <span class="obligatorio">*</span></label>
                    <input type='password' id="password" name="password" maxlength="20" required/>
                </div>
                <div class="campoFormulario">
                    <label for="password2">Repita la Contraseña: <span class="obligatorio">*</span></label>
                    <input type='password' id="password2" name="password2" maxlength="20" required/>
                </div>
                <div class="campoFormulario">
                    <label for="email">Email: <span class="obligatorio">*</span></label>
                    <input type='text' id="email" name="email" maxlength="30" required/>
                </div>
                <div class="campoFormulario">
                    <label for="nombre">Nombre:</label>
                    <input type='text' id="nombre" name="nombre" maxlength="20" required/>
                </div>
                <div class="campoFormulario">
                    <label for="apellidos">Apellidos:</label>
                    <input type='text' id="apellidos" name="apellidos" maxlength="30" required/>
                </div>
                <div class="campoFormulario">
                    <label for="edad">Edad:</label>
                    <input type='text' id="edad" name="edad" maxlength="30" required/>
                </div>
                <div class="campoFormulario">
                    <label for="telefono">Telefono: <span class="obligatorio">*</span></label>
                    <input type='text' id="telefono" name="telefono" maxlength="9" required/>
                </div>


                <div class="botonFormulario">
                    <input type="submit" id="registrar" name="registrar" value="Registrarse">
                </div>  
            </form> 
            
            <?php
            if (isset($_REQUEST['usuario']) && isset($_REQUEST['password']) && isset($_REQUEST['password2']) && isset($_REQUEST['email'])
            && isset($_REQUEST['nombre']) && isset($_REQUEST['apellidos']) && isset($_REQUEST['edad']) && isset($_REQUEST['telefono']))
            {
                if ($_REQUEST['password'] == $_REQUEST['password2'])
                $login = $_REQUEST['usuario']
                $contraseña = $_REQUEST['password'];

                $insercion = $pdo->prepare("INSERT INTO administradores(login, contraseña)" .
                " VALUES(:login, :contraseña)");
                $insercion->bindParam(':login', $login);
                $insercion->bindParam(':contraseña', $contraseña);
                $insercion->execute();
                header("Location:inicio.php");
            }
            ?>
        </div>
    </section>
</body>
</html>