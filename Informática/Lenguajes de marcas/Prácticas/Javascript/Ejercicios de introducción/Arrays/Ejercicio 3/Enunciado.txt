Ejercicio 3

Crea tres arrays, uno que contenga el nombre del alumno, otro con las notas
obtenidas por un alumno y otro con las asignaturas en las que ha obtenido cada
una de las notas. Seguidamente, asocia cada una de las notas a las asignaturas
de modo que la nota situada en la posición 0 del array corresponda a la asignatura situada en la posición 0.
Realiza el mismo ejercicio, y guárdalo como ejercicio3a.html, pero usando un
solo array que contenga en cada posición los tres datos a la vez.
Se debe visualizar el contenido en ambos casos con el siguiente formato:
El alumno …….. tiene un ….. en el módulo …..