Ejercicio 5

Realiza un programa que pida n (introducido por el usuario) números enteros
positivos y muestre el siguiente mensaje:

La suma de números introducidos es:
Se han sumado un total de:

a. Si se introduce algún número negativo, se finalizará la ejecución (break).
b. Si se introduce algún número negativo, no se tendrá en cuenta (continue).