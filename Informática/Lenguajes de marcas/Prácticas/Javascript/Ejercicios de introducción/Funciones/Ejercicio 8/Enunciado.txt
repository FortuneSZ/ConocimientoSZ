Ejercicio 8

El Hércules C.F. en su afán de reducir gastos nos solicita que realicemos una
función para determinar en qué categoría estarían las personas que vengan al
club. Si está por debajo de 9 años se dice que es jugador para infantil, menor de
12 cadete, menor de 18 juvenil, menor de 67 adult@ y mayor de dicha edad
ancian@. Realiza una función que dada la edad de una persona nos devuelva la
palabra “infantil”, “cadete”, “juvenil”, “adult@” o “ancian@”.