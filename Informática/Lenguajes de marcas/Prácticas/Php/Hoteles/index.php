<?php include('cabecera.inc'); ?>
<h1> Listado de hoteles </h1>

<form action="index.php" method="post">
    <label for="provincia"> Provincia:
        <select name="provincia">
            <option value="Cualquiera" checked="checked"> Cualquiera </option> 
            <option value="Alicante"> Alicante </option>
            <option value="Murcia"> Murcia </option>
            <option value="Sevilla"> Sevilla </option>
            <option value="Valencia"> Valencia </option>  
        </select>
        Estrellas:
        <select name="estrellas">
            <option value="Cualquiera" checked="checked"> Cualquiera </option> 
            <option value="2"> 2 o más </option>
            <option value="3"> 3 o más </option>
            <option value="4"> 4 o más </option>  
            <option value="5"> 5 </option>  
        </select>
        <input type="submit" value="Buscar">
    </label>
</form>

<?php 
$fichero = "hoteles.txt";
if (file_exists($fichero))
{
    $hoteles = file($fichero);
    for($i = 0; $i < count($hoteles); $i++)
    {
        $partes = preg_split("/[;]+/",$hoteles[$i]);
        $ListaHoteles[$i]['nombre'] = $partes[0];
        $ListaHoteles[$i]['provincia'] = $partes[1];
        $ListaHoteles[$i]['estrellas'] = $partes[2];
    }
}

function Tabla($ListaHoteles,$i)
{
    echo ("<tr>");
    echo ("<td>" . $ListaHoteles[$i]['nombre'] . "</td>");
    echo ("<td>" . $ListaHoteles[$i]['provincia'] . "</td>");
    echo ("<td>" . $ListaHoteles[$i]['estrellas'] . "</td>");
    echo ("</tr>");
}
if (isset($_POST['provincia']) && isset($_POST['estrellas'])) 
{
    $provincia = $_REQUEST["provincia"];
    $estrellas = $_REQUEST["estrellas"];

    echo ('<table>');
    echo ('<tr>');
    echo ('<th>' . 'Nombre' . '</th>');
    echo ('<th>' . 'Provincia' . '</th>');
    echo ('<th>' . 'Estrellas' . '</th>');
    echo ('</tr>');
    for ($i = 0; $i < count($ListaHoteles); $i++)
    {
        if ($provincia != 'Cualquiera' && $estrellas != 'Cualquiera')
        {
            if ($ListaHoteles[$i]['provincia'] == $provincia && $ListaHoteles[$i]['estrellas'] >= intval($estrellas))
            {
                Tabla($ListaHoteles,$i);
            }
        }
        else
        {
            if ($provincia == 'Cualquiera')
            {
                if ($ListaHoteles[$i]['estrellas'] >= intval($estrellas))
                {
                    Tabla($ListaHoteles,$i);
                }
            }
            else if ($estrellas == 'Cualquiera')
            {
                if ($ListaHoteles[$i]['provincia'] == $provincia)
                {
                    Tabla($ListaHoteles,$i);
                }
            }
            else
            {
                Tabla($ListaHoteles,$i);
            }
        }

    }
    echo('</table>');
}
?>
</body>
</html>