<!DOCTYPE html>
<html lang="es">
<head>
    <title>Hoteles</title>
    <link rel="stylesheet" href="css/estilos.css">

</head>
<body>   
    <form action="login.php" method="post">
        <label for="login"> login:
            <input name="login">
        </label>
        <br>
        <label for="password"> password:
            <input name="password">
        </label>   
        <br>
        <input type="submit" value="Entrar">
    </form>

    <?php
    session_start();
    if (isset($_REQUEST["error"]))
    {
        $error = $_REQUEST["error"];
        echo $error;
    }
    if (isset($_REQUEST["login"]) && isset($_REQUEST["password"]))
    {
        $user = $_REQUEST["login"];
        $contra = $_REQUEST["password"];
        $host = "localhost";
        $nombreBD = "hoteles";
        $usuario = "root";
        $password = ""; 
        $entrar = false;
        
        $pdo = new PDO("mysql:host=$host;dbname=$nombreBD;charset=utf8", $usuario, $password);

        $consulta = $pdo->prepare("SELECT * FROM usuarios");
        $consulta->execute();
        while($registro = $consulta->fetch())
        {
            if ($registro['login'] == $user && $registro['password'] == $contra)
            {
                $entrar = true;
            }
        }
        if ($entrar == true)
        {
            $_SESSION["loginUsuario"] = $user;
            header("Location:index.php");
        }
        else
        {
            $error =  'Credenciales incorrectas';
            header("Location:login.php?error=".$error);
        }
    }
?>
</body>
</html>