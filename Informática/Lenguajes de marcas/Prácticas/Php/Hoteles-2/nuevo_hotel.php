<?php include('cabecera.inc'); ?>
    <h1> Alta de nuevo hotel </h1>
    <form action="guardar_hotel.php" method="post">
        <label for="nombre"> Nombre del hotel:
            <input type="text" name="nombre">
        </label>
        <br>
        <label for="provincia">Provincia:
            <select name="provincia">
                <option value="Alicante"> Alicante </option>
                <option value="Murcia"> Murcia </option>
                <option value="Sevilla"> Sevilla </option>
                <option value="Valencia"> Valencia </option>  
            </select>
        </label>
        <br>
        <label for="estrellas">
        Estrellas:
            <select name="estrellas">
                <option value="1" checked="checked"> 1 </option> 
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>  
                <option value="5">5</option>  
            </select>
        </label>
        <br>
        <label>
            <input type="submit" value="Guardar">
        </label>
</form>
<?php 
session_start();
if (isset($_SESSION['loginUsuario']))
{

}
else
{
    header("Location:login.php");
}
?>
</body>
</html>




