<?php include('cabecera.inc'); ?>
<h1> Listado de hoteles </h1>

<form action="index.php" method="post">
    <label for="provincia"> Provincia:
        <select name="provincia">
            <option value="Cualquiera" checked="checked"> Cualquiera </option> 
            <option value="Alicante"> Alicante </option>
            <option value="Murcia"> Murcia </option>
            <option value="Sevilla"> Sevilla </option>
            <option value="Valencia"> Valencia </option>  
        </select>
        Estrellas:
        <select name="estrellas">
            <option value="Cualquiera" checked="checked"> Cualquiera </option> 
            <option value="2"> 2 o más </option>
            <option value="3"> 3 o más </option>
            <option value="4"> 4 o más </option>  
            <option value="5"> 5 </option>  
        </select>
        <input type="submit" value="Buscar">
    </label>
</form>

<?php 
$host = "localhost";
$nombreBD = "hoteles";
$usuario = "root";
$password = "";
$i = 0;

$pdo = new PDO("mysql:host=$host;dbname=$nombreBD;charset=utf8", $usuario, $password);

    $consulta = $pdo->prepare("SELECT * FROM hoteles");
    $consulta->execute();
    while($registro = $consulta->fetch())
    {
       // echo $registro['nombre'];
        $ListaHoteles[$i]['nombre'] = $registro['nombre'];
       // echo $registro['provincia'];
        $ListaHoteles[$i]['provincia'] = $registro['provincia'];
       // echo $registro['estrellas'];
        $ListaHoteles[$i]['estrellas'] = $registro['estrellas'];
        $i++;
    }

function Tabla($registro,$i)
{
    echo ("<tr>");
    echo ("<td>" . $registro['nombre'] . "</td>");
    echo ("<td>" . $registro['provincia'] . "</td>");
    echo ("<td>" . $registro['estrellas'] . "</td>");
    echo ("</tr>");
}
if (isset($_POST['provincia']) && isset($_POST['estrellas'])) 
{
    $provincia = $_REQUEST["provincia"];
    $estrellas = $_REQUEST["estrellas"];

    echo ('<table>');
    echo ('<tr>');
    echo ('<th>' . 'Nombre' . '</th>');
    echo ('<th>' . 'Provincia' . '</th>');
    echo ('<th>' . 'Estrellas' . '</th>');
    echo ('</tr>');
  
        if ($provincia != 'Cualquiera' && $estrellas != 'Cualquiera')
        {
            $consulta = $pdo->prepare("SELECT * FROM hoteles WHERE provincia = '$provincia' AND estrellas >= $estrellas");
            $consulta->execute();
            while($registro = $consulta->fetch())
            {
                Tabla($registro,$i);
            }
        }
        else
        {
            if ($provincia == 'Cualquiera' && $estrellas != 'Cualquiera')
            {
                $consulta = $pdo->prepare("SELECT * FROM hoteles WHERE  estrellas >= $estrellas");
                $consulta->execute();
                while($registro = $consulta->fetch())
                {
                    Tabla($registro,$i);
                }
            }
            else if ($estrellas == 'Cualquiera' && $provincia != 'Cualquiera')
            {
                $consulta = $pdo->prepare("SELECT * FROM hoteles WHERE provincia = '$provincia'");
                $consulta->execute();
                while($registro = $consulta->fetch())
                {
                    Tabla($registro,$i);
                }
            }
            else
            {
                $consulta = $pdo->prepare("SELECT * FROM hoteles");
                $consulta->execute();
                while($registro = $consulta->fetch())
                {
                    Tabla($registro,$i);
                }
            }
        }
    echo('</table>');
}
?>
</body>
</html>