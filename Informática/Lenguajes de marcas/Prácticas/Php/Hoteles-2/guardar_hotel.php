<?php
session_start();
if (isset($_SESSION['loginUsuario']))
{
    if (isset($_REQUEST['provincia']) && isset($_REQUEST['estrellas']) && $_REQUEST['nombre'] != '')
    {
        $nombre = $_REQUEST["nombre"];
        $provincia = $_REQUEST["provincia"];
        $estrellas = $_REQUEST["estrellas"];
        $valido = true;

        if (strlen($nombre) > 100 || strlen($provincia) > 50 || $estrellas < 1 || $estrellas > 5)
        {
            $valido = false;
        }

        if ($valido == true)
        {
            $user = $_REQUEST["login"];
            $contra = $_REQUEST["password"];
            $host = "localhost";
            $nombreBD = "hoteles";
            $usuario = "root";
            $password = ""; 
            $entrar = false;
            
            $pdo = new PDO("mysql:host=$host;dbname=$nombreBD;charset=utf8", $usuario, $password);

            $insercion = $pdo->prepare("INSERT INTO hoteles(nombre, provincia, estrellas)" .
            " VALUES(:nombre, :provincia, :estrellas)");
            $insercion->bindParam(':nombre', $nombre);
            $insercion->bindParam(':provincia', $provincia);
            $insercion->bindParam(':estrellas', $estrellas);
            $insercion->execute();
            header("Location:index.php");
        }
        else
        {
            $error = "Valores incorrectos";
            header("Location:error.php?error=".$error);
        }
    }
    else
    {
        $error = "Los campos no pueden estar vacíos";
        header("Location:error.php?error=".$error);
    }
    
}
else
{
    header("Location:login.php");
}

?>