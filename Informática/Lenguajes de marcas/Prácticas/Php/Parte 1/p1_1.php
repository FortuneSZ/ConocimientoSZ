<?php

$suma = 0;
$multi = 1;

for ($i = 1; $i <= 10; $i++)
{
    $suma += $i;
    $multi *= $i;
}

echo "La suma de los 10 primeros número naturales es " . $suma . "<br>";
echo "El producto de los 10 primeros números naturales es " . $multi;
?>