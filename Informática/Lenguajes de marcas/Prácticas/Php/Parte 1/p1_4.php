<?php
$i = 1;
$sumaPares = 0;
$productoImpares = 1;
do
{
    
    if ($i % 2 == 0)
    {
        $sumaPares += $i;
    }
    else
    {
        $productoImpares *= $i;
    }
    $i++;
}
while($i <= 20);

echo ("La suma de los primeros 10 números pares es " . $sumaPares . "<br>");
echo ("el producto de los primeros 10 números impares es " . $productoImpares . "<br>");
?>