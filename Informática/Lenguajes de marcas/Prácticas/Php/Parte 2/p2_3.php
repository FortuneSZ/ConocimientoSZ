<?php
$nif = "48523614n";
$valido = true;

for ($i = 0; $i < strlen($nif); $i++)
{
    $num = $nif[$i];
    $asciiNum = ord($num);

    if ($i != 8 && ($asciiNum < 48 || $asciiNum > 57))
    {
        $valido = false;
    }
    if ($i == 8 && ($asciiNum < 65 || ($asciiNum > 90 && $asciiNum < 97) || $asciiNum > 122))
    {
        $valido = false;
    }
}

if ($valido == true)
{
    $nif = strtoupper($nif);
    echo ("$nif");
}
else
{
    echo ("No valido");
}
?>