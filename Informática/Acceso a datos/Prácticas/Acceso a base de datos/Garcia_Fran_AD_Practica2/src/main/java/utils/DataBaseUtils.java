package utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.postgresql.util.PSQLException;

import clases.Episodio;
import clases.Lugar;
import clases.Personaje;

/**
 * Clase DataBaseUtils 
 * Clase encargada de la gestión de la base de datos
 * @author Francisco Javier García Sánchez
 */

public class DataBaseUtils 
{
	
	/**
	 * Método EmptyTables
	 * vacía las tablas de la base de datos al iniciarse el programa
	 * @author Francisco Javier García Sánchez
	 */
	
	public static void EmptyTables()
	{
		
		/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
		
		try 
		{
			
			/**
			 * Conexión con la base de datos
			 */
			
			Connection con = p.connectionAdmin();
			
			try (CallableStatement statement = con.prepareCall("CALL truncatetables()")) 
			{
			    statement.execute();
			}
			catch(PSQLException  e)
			{
				System.out.println(e.getLocalizedMessage());
			}
			con.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Método UploadCharacters
	 * Método que se encarga de subir los personajes obtenidos de la api a su respectiva tabla de la base de datos
	 * @param personajes Lista de personajes obtenidos de la api
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
	public static void uploadCharacters(List<Personaje> personajes) throws SQLException
	{
		
		/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
		
		/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
				
		/**
		 * Id del origen del personaje
		 */
		
		int idOrigen = 0;
		
		/**
		 * Id de la ubicación del personaje
		 */
		
		int idUbicacion = 0;
	    
		for(int i = 0; i < personajes.size();i++)
		{
			
			/**
			 * Id del origen obtenida de la lista de personajes, en caso de ser null se asigna 0 como valor
			 */
			
			Integer origenId = (personajes.get(i).getOrigen() != null) ? personajes.get(i).getOrigen().getId() : 0;
			idOrigen = FindPlace("origen",con,origenId,idOrigen);
			
			/**
			 * Id de la ubicación obtenida de la lista de personajes, en caso de ser null se asigna 0 como valor
			 */
			
			Integer ubicacionId = (personajes.get(i).getOrigen() != null) ? personajes.get(i).getUbicacion().getId() : 0;
			idUbicacion = FindPlace("ubicación",con,ubicacionId,idUbicacion);
			
			InsertCharacters(personajes,con,i,idOrigen,idUbicacion);
		}
		System.out.println();
		con.close();
	}
	
	/**
	 * Método UploadPlaces
	 * Método que se encarga de subir los lugares obtenidos de la api a su respectiva tabla de la base de datos
	 * @param lugares Lista de lugares obtenida de la api
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
	public static void UploadPlaces(List<Lugar> lugares) throws SQLException
	{
		
		/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
		
		/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
		
		InsertVoidPlace(con);
	    
		for(int i = 0; i < lugares.size();i++)
		{
			InsertPlace(lugares,con,i);
		}
		System.out.println();
		con.close();
	}
	
	/**
	 * Método UploadEpisodes
	 * Sube los episodios obtenidos de la api a la base de datos
	 * @param episodes Lista de episodios obtenidos de la api
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void UploadEpisodes(List<Episodio> episodios) throws SQLException
	{
		/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
		
		/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
		
		for(int i = 0; i < episodios.size();i++)
		{
			InsertEpisode(episodios,con,i);
		}
		System.out.println();
		con.close();
	}
	
	/**
	 * Método uploadCharacterInEpisode
	 * Método que rellena la tabla characterInEpisode de la base de datos
	 * @param id_char Id del personaje
	 * @param id_episode Id del episodio
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void UploadCharacterInEpisode(int id_char,int id_episode) throws SQLException
	{
		/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
		
		/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
		
		InsertCharacterInEpisode(id_char,id_episode,con);
		con.close();
		
		con.close();
	}
	
	/**
	 * Método FindPlace 
	 * se encarga de encontrar la Id del lugar especificado
	 * @param tipo Tipo de lugar que queremos buscar
	 * @param con Conexión con la base de datos
	 * @param Id Id obtenida de la lista de personajes para buscar el lugar
	 * @param IdResult ID resultande del lugar buscado
	 * @return Id resultande del lugar buscado
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
	public static int FindPlace(String tipo,Connection con, int Id, int IdResult) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("SELECT findChar(?)"))
		{
			statement.setInt(1, Id);
			try(ResultSet idOrigenSet = statement.executeQuery();)
			{
				if (idOrigenSet.next()) 
				{
					IdResult = idOrigenSet.getInt(1);
		        }
				else 
		        {
		            IdResult = 0;
		        }
				return IdResult;
			}
		}
	}
	
	/**
	 * Método InsertCharacter 
	 * Método para insertar los personajes obtenidos desde la api a su respectiva tabla de la base de datos
	 * @param personajes Lista de personajes obtenidos desde la api
	 * @param con Conexión con la base de datos
	 * @param i Posición en la lista
	 * @param idOrigen Id del origen del personaje
	 * @param idUbicacion Id de la ubicación del personaje
	 * * @throws SQLException 
	 * @author Francisco Javier García Sánchez
	 */
	
	public static void InsertCharacters(List<Personaje> personajes,Connection con, int i, int idOrigen, int idUbicacion) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("CALL InsertCharacter(?,?,?,?,?,?,?,?)")) 
		{
			statement.setInt(1, personajes.get(i).getId());
			statement.setString(2, personajes.get(i).getNombre());
			statement.setString(3, personajes.get(i).getEstado());
			statement.setString(4, personajes.get(i).getEspecies());
			statement.setString(5, personajes.get(i).getTipo());
			statement.setString(6, personajes.get(i).getGenero());
			statement.setInt(7, idOrigen);
			statement.setInt(8, idUbicacion);
		    statement.execute();
		}
		catch(PSQLException  e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch(Exception e)
		{
			System.out.println("error al añadir el personaje " + personajes.get(i).getNombre());
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Método InsertCharacter
	 * Método para insertar un personaje obtenidos desde la api a su respectiva tabla de la base de datos
	 * @param per Objeto de tipo personaje, personaje a añadir a la base de datos
	 * @param con Conexión con la base de datos
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void InsertCharacter(Personaje per,Connection con) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("CALL InsertCharacter(?,?,?,?,?,?,?,?)")) 
		{
			statement.setInt(1, per.getId());
			statement.setString(2, per.getNombre());
			statement.setString(3, per.getEstado());
			statement.setString(4, per.getEspecies());
			statement.setString(5, per.getTipo());
			statement.setString(6, per.getGenero());
			statement.setInt(7, per.getOrigen().getId());
			statement.setInt(8, per.getUbicacion().getId());
		    statement.execute();
		}
		catch(PSQLException  e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch(Exception e)
		{
			System.out.println("error al añadir el personaje " + per.getNombre());
			System.out.println(e.getLocalizedMessage());
		}
		con.close();
	}
	
	/**
	 * Método InsertCharacterInEpisode
	 * Permite rellenar la tabla de la base de datos que contiene los personajes en episodios
	 * @param id_char Id del personaje
	 * @param id_episode Id del episodio
	 * @param con Conexión con la base de datos
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void InsertCharacterInEpisode(int id_char,int id_episode,Connection con) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("CALL insertCharacterInEpisode(?,?)")) 
		{
			statement.setInt(1, id_char);
			statement.setInt(2, id_episode);
		    statement.execute();
		}
		catch(PSQLException  e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch(Exception e)
		{
			System.out.println("error al añadir");
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Método InsertVoidPlace 
	 * inserta un lugar "vacío" en la base de datos para los personajes que no tienen un origen conocido
	 * @param con Conexión con la base de datos
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
	public static void InsertVoidPlace(Connection con) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("CALL insertVoidPlace()")) 
		{
			statement.execute();
		}
	}
	
	/**
	 * Método InsertPlace 
	 * inserta los lugares en su tabla correspondiente de la base de datos
	 * @param lugares Lista de lugares obtenidos de la api
	 * @param con Conexión con la base de datos
	 * @param i Posición en la lista
	 * @author Francisco Javier García Sánchez
	 * @throws SQLException 
	 */
	
	public static void InsertPlace(List<Lugar> lugares,Connection con,int i) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("CALL insertPlace(?,?,?,?)")) 
		{
			statement.setInt(1, lugares.get(i).getId());
			statement.setString(2, lugares.get(i).getNombre());
			statement.setString(3, lugares.get(i).getTipo());
			statement.setString(4, lugares.get(i).getDimension());
		    statement.executeUpdate();
		}
		catch(PSQLException  e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch(Exception e)
		{
			System.out.println("error al añadir el Lugar " + lugares.get(i).getNombre());
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Método InsertEpisode
	 * Método que sirve para rellenar la tabla de la base de datos que almacena los episodios
	 * @param episodios Lista de episodios
	 * @param con COnexión de la base de datos
	 * @param i Posición en la lista
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void InsertEpisode(List<Episodio> episodios, Connection con, int i) throws SQLException
	{
		try (CallableStatement statement = con.prepareCall("CALL insertEpisode(?,?,?,?)")) 
		{
			statement.setInt(1, episodios.get(i).getId());
			statement.setString(2, episodios.get(i).getNombre());
			statement.setDate(3, (Date) episodios.get(i).getEmisión());
			statement.setString(4, episodios.get(i).getEpisodio());
		    statement.executeUpdate();
		}
		catch(PSQLException  e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch(Exception e)
		{
			System.out.println("error al añadir el episodio " + episodios.get(i).getNombre());
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Método obtainLocation
	 * Permite obtener una ubicación basada en su id
	 * @param con COnexión con la base de datos
	 * @param locationChar Id de la ubicación
	 * @return lug Lugar obtenido de la búsqueda en la base de datos en base al id de la ubicación
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static Lugar obtainLocation(Connection con, int locationChar) throws SQLException
	{
		/**
		 * Lugar a obtener de la base de datos
		 */
		Lugar lug = null;
		try (CallableStatement statement = con.prepareCall("SELECT * FROM selectAllFromLocation(?)"))
    	{
			statement.setInt(1,locationChar);
    		try(ResultSet idOrigenSet = statement.executeQuery())
			{
    			if (idOrigenSet.next())
    			{
    				String nameLocation = idOrigenSet.getString("name");
        			String typeLocation = idOrigenSet.getString("type");
        			String dimensionLocation = idOrigenSet.getString("dimension");
        			lug = new Lugar(locationChar,nameLocation,typeLocation,dimensionLocation);
    			}	
			}
    		catch(Exception e)
    		{
    			System.out.println("Error");
    		}
    	}
		return lug;
	}
}
