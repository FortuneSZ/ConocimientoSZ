package clases;

import java.io.Serializable;

/**
 * Clase Lugar
 * @author Fran García Sánchez 
 */

public class Lugar implements Serializable
{
	
	/**
	 * ID del lugar
	 */
	
	private int id;
	
	/**
	 * nombre Nombre del lugar
	 */
	
	private String nombre;
	
	/**
	 * Tipo de lugar
	 */
	
	private String tipo;
	
	/**
	 * Dimensión en la que se encuentra el lugar
	 */
	
	private String dimensión;
	
	/**
	 * Construcor parametrizado de la clase Lugar
	 * @param id Id del lugar
	 * @param nombre Nombre del lugar
	 * @param tipo Tipo del lugar
	 * @param dimensión Dimensión del lugar
	 * @author Fran García Sánchez
	 */
	
	public Lugar(int id,String nombre,String tipo,String dimensión)
	{
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.dimensión = dimensión;
	}
	
	/**
	 * Método toString
	 * Método toString de la clase Lugar
	 * @return La forma predefinida de mostrar el contenido de la clase
	 * @author Fran García Sánchez
	 */
	
	@Override
	public String toString()
	{
		System.out.println("-----------------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("Dimensión: " + this.dimensión);
		System.out.println("-----------------------");
		return "";
	}
	
	/**
	 * Método getId
	 * Método getter del atributo id
	 * @return la id del lugar
	 * @author Fran García Sánchez
	 */
	
	public int getId()
	{
		return id;
	}
	
	/**
	 * Método getNombre
	 * Método getter del atributo nombre
	 * @return el nombre del lugar
	 * @author Fran García Sánchez
	 */
	
	public String getNombre()
	{
		return nombre;
	}
	
	/**
	 * Método setNombre
	 * Método setter del atributo nombre
	 * @param nombre Nombre que asignar al atributo nombre
	 * @author Fran García Sánchez
	 */
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	/**
	 * Método getDimension
	 * Método getter del atributo dimensión
	 * @return la dimensión a la que pertenece el lugar
	 * @author Fran García Sánchez
	 */
	
	public String getDimension()
	{
		return dimensión;
	}
	
	/**
	 * Método setDimension
	 * Método setter del atributo dimensión
	 * @param dimension Dimensión que asignar al atributo dimensión
	 * @author Fran García Sánchez 
	 */
	
	public void setDimension(String dimension)
	{
		this.dimensión = dimension;
	}
	
	/**
	 * Método getTipo
	 * Método getter del atributo tipo
	 * @return el tipo de lugar
	 * @author Fran García Sánchez
	 */
	
	public String getTipo()
	{
		return tipo;
	}
}
