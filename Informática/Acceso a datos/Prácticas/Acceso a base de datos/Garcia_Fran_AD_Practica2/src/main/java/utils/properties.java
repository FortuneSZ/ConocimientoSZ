package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Clase properties
 * Determina las propiedades de la conexión con la base de datos
 * @author Francisco Javier García Sánchez
 */
public class properties 
{
	/**
	 * Método connection
	 * Conecta con la base de datos
	 * @return La conexión con la base de datos
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
	public Connection connection() throws SQLException
	{
		/**
		 * Url para conectar con la base de datos
		 */
		
		String url = "jdbc:postgresql://localhost:5432/serie";
		
		/**
		 * Instanciación de la clase Properties para determinar las propiedades de la conexión
		 */
		
		Properties props = new Properties();
		props.setProperty("user", "dev");
		props.setProperty("password", "Fortune99");
		
		/**
		 * Conexión con la base de datos
		 */
		
		Connection con = DriverManager.getConnection(url, props);
		return con;
	}
	
	/**
	 * Método connectionAdmin
	 * Conecta con la base de datos como administrador
	 * @return La conexión con la base de datos
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
	public Connection connectionAdmin() throws SQLException
	{
		
		/**
		 * Url para conectar con la base de datos
		 */
		
		String url = "jdbc:postgresql://localhost:5432/serie";
		
		/**
		 * Instanciación de la clase Properties para determinar las propiedades de la conexión
		 */
		
		Properties props = new Properties();
		props.setProperty("user", "postgres");
		props.setProperty("password", "Fortune99");
		
		/**
		 * Conexión con la base de datos
		 */
		
		Connection con = DriverManager.getConnection(url, props);
		return con;
	}
}
