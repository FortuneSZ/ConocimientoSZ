package com.FranGarcia.Garcia_Fran_AD_Practica2;

import java.rmi.AccessException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import clases.Lugar;
import clases.Personaje;
import utils.DataBaseUtils;
import utils.JsonUtils;
import utils.properties;

/**
 * Clase App
 * Clase principal del programa
 * @author Francisco Javier García Sánchez
 */

public class App 
{
	
	/**
	 * Método main
	 * Método principal del programa
	 * @param args
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	
    public static void main(String[] args) throws SQLException 
    {
    	
    	/**
    	 * Instanciación de la clase DataBaseUtils, empleada para manejar la base de datos
    	 */
    	
    	DataBaseUtils database = new DataBaseUtils();
    	
    	/**
    	 * Instanciación de la clase JsonUtils, empleada para manejar y obtener JSON de la api
    	 */
    	
    	JsonUtils json = new JsonUtils();
    	
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	
    	/**
    	 * Elección del menú
    	 */
    	
    	int eleccion = 0;
    	
        do
        {
        	try
        	{
        		eleccion = 0;
            	menu();
        		eleccion = sc.nextInt();
        		switch(eleccion)
            	{
            		case 1:
            			fillDataBase(database,json);
            		break;
            		
            		case 2:
            			añadirPersonaje();
            		break;
            		
            		case 3:
            			BuscarPersonaje();
            		break;
            		
            		case 4:
            			BuscarPersonajeSinEpisodio();
                	break;
                	
            		case 5:
            			System.out.println("Has elegido salir");
                	break;
                	
                	default:
                		System.out.println("Opción incorrecta");
                	break;
            	}
        	}
        	catch(Exception e)
        	{
        		System.out.println("Error: valor no numérico");
        		System.out.println();
        		sc.nextLine();
        	}
        	
        }
        while(eleccion != 5);
    }
    
    /**
     * Método menu
     * Muestra un menú de opciones
     * @author Francisco Javier García Sánchez
     */
    
    public static void menu()
    {
    	System.out.println("1. Rellenar BD");
    	System.out.println("2. Añadir Personaje");
    	System.out.println("3. Búsqueda por texto");
    	System.out.println("4. Personajes sin episodio");
    	System.out.println("5. Salir");
    	System.out.println();
    }
    
    /**
     * Método menuAñadirPersonaje
     * Muestra un menú para la creación de un personaje que añadir
     * @author Francisco Javier García Sánchez
     */
    public static void menuAñadirPersonaje()
    {
    	System.out.println();
    	System.out.println("1. Nombre");
    	System.out.println("2. Estado");
    	System.out.println("3. Especies");
    	System.out.println("4. Tipo");
    	System.out.println("5. Género");
    	System.out.println("6. Id origen");
    	System.out.println("7. Id ubicación");
    	System.out.println("8. Terminar");
    	System.out.println();
    }
    
    /**
     * Método fillDataBase
     * Método que se encarga de llamar a los diferentes métodos para rellenar las tablas de la base de datos
     * @throws SQLException
     * @author Francisco Javier García Sánchez
     */
    
    public static void fillDataBase(DataBaseUtils database,JsonUtils json) throws SQLException
    {
    	database.EmptyTables();
    	json.obtenerLugares();
    	json.obtenerPersonajes();
    	json.obtenerEpisodios();
    	json.obtenerPersonajesEnEpisodios();
		System.out.println();
    }
    
    /**
     * Método añadirPersonaje
     * Método que permite añadir un personaje personalizado a la base de datos
     * @throws SQLException
     * @author Francisco Javier García Sánchez
     */
    public static void añadirPersonaje() throws SQLException
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	/**
    	 * Opción elegida del emnú de opciones
    	 */
    	int eleccion = 0;
    	
    	/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
    	
    	/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
		
		/**
		 * Id del personaje a añadir
		 */
		
		int IdChar = 0;
    	
    	try (CallableStatement statement = con.prepareCall("SELECT findCharSize()"))
    	{
    		try(ResultSet idOrigenSet = statement.executeQuery())
			{
    			if (idOrigenSet.next()) 
    			{
    				IdChar = idOrigenSet.getInt(1) + 1;
    			}
			}
    	}
    	
    	/**
    	 * Nombre del personaje a añadir
    	 */
    	
    	String nameChar = "";
    	
    	/**
    	 * Estado del personaje a añadir
    	 */
    	
    	String stateChar = "Alive";
    	
    	/**
    	 * Especies del personaje a añadir
    	 */
    	
    	String speciesChar = "Human";
    	
    	/**
    	 * Tipo del personaje a añadir
    	 */
    	
    	String typeChar = "";
    	
    	/**
    	 * Género del personaje a añadir
    	 */
    	String genderChar = "Male";
    	
    	/**
    	 * Origen del personaje a añadir
    	 */
    	int originChar = 0;
    	
    	/**
    	 * Ubicación del personaje a añadir
    	 */
    	int locationChar = 0;

    	do
    	{
    		eleccion = 0;
        	menuAñadirPersonaje();
        	try
        	{
            	eleccion = sc.nextInt();
            	switch(eleccion)
            	{
            		case 1:
            			nameChar = elegirNombre();
            		break;
            		
            		case 2:
            			stateChar = elegirEstado();
            		break;
            		
            		case 3:
            			speciesChar = elegirEspecie();
            		break;
            		
            		case 4:
            			typeChar = elegirTipo();
            		break;
            		
            		case 5:
            			genderChar = elegirGenero();
            		break;
            		
            		case 6:
            			originChar= elegirOrigen(con);
            		break;
            		
            		case 7:
            			locationChar = elegirUbicacion(con);
            		break;
            		
            		case 8:
            			/**
            			 * Objeto de tipo lugar que almacena el lugar de origen del personaje a añadir
            			 */
            			Lugar origin = DataBaseUtils.obtainLocation(con,originChar);
            			/**
            			 * Objeto de tipo lugar que almacena la ubicación del personaje a añadir
            			 */
            			Lugar location = DataBaseUtils.obtainLocation(con,locationChar);
            			/**
            			 * Personaje a añadir a la base de datos
            			 */
            			Personaje per = new Personaje(IdChar,nameChar,stateChar,speciesChar,typeChar,genderChar,origin,location);
            			DataBaseUtils.InsertCharacter(per,con);
            			System.out.println("Personaje añadido correctamente");

            			con.close();
            		break;
            	}
        	}
        	catch(Exception e)
        	{
        		System.out.println("La elección tiene que ser numérica");
        		System.out.println();
        		sc.nextLine();
        	}
        	
    	}
    	while(eleccion != 8);
    }
    
    /**
     * Método BuscarPersonaje
     * Busca un personaje en la base de datos en base a unos parámetros introducidos por el usuario
     * @throws SQLException
     * @author Francisco Javier García Sánchez
     */
    public static void BuscarPersonaje() throws SQLException
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
    	
    	/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
    	System.out.println("Introduzca el atributo por el que buscar");
    	/**
    	 * Atributo que emplear para la búsqueda en la base de datos
    	 */
    	String atribute = sc.nextLine();
    	System.out.println("Introduzca el texto a buscar");
    	/**
    	 * texto con el que comparar el atributo para la búsqueda en la base de datos
    	 */
    	String querry = sc.nextLine();
    	try( PreparedStatement statement = con.prepareStatement("SELECT * FROM selectFromCharacter(?,?)"))
    	{
    		statement.setString(1, atribute);
    		statement.setString(2, querry);
    		try (ResultSet rs = statement.executeQuery()) 
    		{
            	System.out.println();
                while (rs.next()) 
                {
                	/**
                	 * Id del personaje a mostrar
                	 */
                	String idChar = rs.getString("id");
                	/**
                	 * Nombre del personaje a mostrar
                	 */
        			String nameChar = rs.getString("name");
        			/**
        			 * Estado del personaje a mostrar
        			 */
        			String statusChar = rs.getString("status");
        			/**
        			 * Especie del personaje a mostrar
        			 */
        			String speciesChar = rs.getString("species");
        			/**
        			 * Tipo del personaje a mostrar
        			 */
        			String typeChar = rs.getString("type");
        			/**
        			 * Género del personaje a mostrar
        			 */
        			String genderChar = rs.getString("gender");
        			/**
        			 * Id del origen del personaje a mostrar
        			 */
        			String originChar =  rs.getString("id_origin");
        			/**
        			 * Id de la ubicación del personaje a mostrar
        			 */
        			String locationChar =  rs.getString("id_location");
                	System.out.println("ID: " + idChar + ", Nombre: " + nameChar + ", Estado: " + statusChar + ", Especie: " + speciesChar + ", Tipo: " + typeChar + ", Género: " + genderChar + ", Origen: " + originChar + ", Ubicación: " + locationChar);
                }
            	System.out.println();
            	con.close();
    		}
    	}
    	catch(Exception e)
    	{
    		System.out.println("Error, atributo no encontrado");
    		System.out.println();
    	}
    }
    
    /**
     * Método BuscarPersonajeSinEpisodio
     * Busca todos los personajes que no tengan ningún episodio
     * @throws SQLException
     * @author Francisco Javier García Sánchez
     */
    public static void BuscarPersonajeSinEpisodio() throws SQLException
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	/**
		 * Instanciación de la clase properties, que se encarga de la conexión con la base de datos
		 */
		
		properties p = new properties();
    	
    	/**
		 * Conexión con la base de datos
		 */
		
		Connection con = p.connection();
    	
    	try( PreparedStatement statement = con.prepareStatement("SELECT * FROM characterWithoutEpisode()"))
    	{
    		try (ResultSet rs = statement.executeQuery()) 
    		{

    			System.out.println("Personajes sin episodio");
    			System.out.println();
                while (rs.next()) 
                {
                	/**
                	 * Id del personaje a mostrar
                	 */
                	String idChar = rs.getString("id");
                	/**
                	 * Nombre del personaje a mostrar
                	 */
            		String nameChar = rs.getString("name");
            		/**
            		 * Estado del personaje a mostrar
            		 */
            		String statusChar = rs.getString("status");
            		/**
            		 * Especie del personaje a mostrar
            		 */
            		String speciesChar = rs.getString("species");
            		/**
            		 * Tipo del personaje a mostrar
            		 */
            		String typeChar = rs.getString("type");
            		/**
            		 * Género del personaje a mostrar
            		 */
            		String genderChar = rs.getString("gender");
            		/**
            		 * Id del lugar de origen del personaje a mostrar
            		 */
            		String originChar =  rs.getString("id_origin");
            		/**
            		 * Id de la ubicación del personaje a mostrar
            		 */
            		String locationChar =  rs.getString("id_location");
                    System.out.println("ID: " + idChar + ", Nombre: " + nameChar + ", Estado: " + statusChar + ", Especie: " + speciesChar + ", Tipo: " + typeChar + ", Género: " + genderChar + ", Origen: " + originChar + ", Ubicación: " + locationChar);
                }
                System.out.println();
                con.close();
    		}
    	}
    	catch(Exception e)
    	{
    		System.out.println("Error");
    		System.out.println();
    	}
    }
    
    /**
     * Método elegirNombre
     * Permite elegir el nombre del personaje a añadir
     * @return name El nombre elegido para el personaje
     * @author Francisco Javier García Sánchez
     */
    public static String elegirNombre()
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	
    	System.out.println("Elige el nombre del personaje");
    	/**
    	 * Nombre del personaje
    	 */
    	String name = sc.nextLine();
		return name;
    }
    
    /**
     * Método elegirEstado
     * Permite elegir el estado del personaje a añadir
     * @return state El estado elegido para el personaje
     * @author Francisco Javier García Sánchez
     */
    
    public static String elegirEstado()
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	
    	/**
    	 * Estado del personaje a añadir
    	 */
    	
    	String state = "Alive";
    	
    	System.out.println("Elige el estado del personaje");
    	System.out.println("Opciones disponibles: vivo, muerto, desconocido");
    	String eleccion = sc.nextLine();
    	switch(eleccion.toLowerCase())
    	{
    	case "vivo":
    		state = "Alive";
    	break;
    	
    	case "muerto":
    		state = "Dead";
    	break;
    	
    	case "desconocido":
    		state = "Unknown";
    	break;
    	
    	default:
    		System.out.println("Opción incorrecta");
    	}
		return state;
    }
    
    /**
     * Método elegirEspecie
     * Permite elegir las especies del personaje a añadir
     * @return species la especie elegida para el personaje
     * @author Francisco Javier García Sánchez
     */
    
    public static String elegirEspecie()
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	
    	System.out.println("Elige la especie del personaje");
    	/**
    	 * Especie del personaje
    	 */
    	String species = sc.nextLine();
		return species;
    }
    
    /**
     * Método elegirTipo
     * Permite elegir el tipo del personaje a añadir
     * @return type el tipo elegida para el personaje
     * @author Francisco Javier García Sánchez
     */
    
    public static String elegirTipo()
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	
    	System.out.println("Elige el tipo del personaje");
    	/**
    	 * Tipo del personaje
    	 */
    	String type = sc.nextLine();
		return type;
    }
    
    /**
     * Método elegirGenero
     * Permite elegir el género del personaje a añadir
     * @return gender El género elegido para el personaje
     * @author Francisco Javier García Sánchez
     */
    
    public static String elegirGenero()
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);
    	
    	/**
    	 * género del personaje a añadir
    	 */
    	
    	String gender = "Male";
    	
    	System.out.println("Elige el género del personaje");
    	System.out.println("Opciones disponibles: masculino, femenino, desconocido, sin género");
    	String eleccion = sc.nextLine();
    	switch(eleccion.toLowerCase())
    	{
    	case "masculino":
    		gender = "Male";
    	break;
    	
    	case "femenino":
    		gender = "Female";
    	break;
    	
    	case "desconocido":
    		gender = "Unknown";
    	break;
    	
    	case "sin género":
    		gender = "Genderless";
    	break;
    	
    	default:
    		System.out.println("Opción incorrecta");
    	}
		return gender;
    }
    
    /**
     * Método elegirOrigen
     * Permite elegir el origen del personaje a añadir
     * @return origin el origen elegida para el personaje
     * @throws SQLException 
     * @author Francisco Javier García Sánchez
     */
    
    public static int elegirOrigen(Connection con) throws SQLException
    {
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	
    	Scanner sc = new Scanner(System.in);   		
    	
    	/**
    	 * Id máxima de lugares almacenados en la base de datos
    	 */
    	int idmax = 0;
    	
    	try (CallableStatement statement = con.prepareCall("SELECT findLocationSize()"))
    	{
    		try(ResultSet idOrigenSet = statement.executeQuery())
			{
    			if (idOrigenSet.next()) 
    			{
    				idmax = idOrigenSet.getInt(1) -1;
    			}
    			
			}
    	}
    	
    	System.out.println("Elige el id del origen del personaje");
    	System.out.println("La id máxima seleccionable actualmente es: " + idmax);
    	/**
    	 * Id del lugar de origen del personaje
    	 */
    	int origin = sc.nextInt();
    	if (origin <= idmax)
    	{
    		return origin;
    	}
    	else
    	{
    		System.out.println("Id no válida");
    		return 0;
    	}
    }
    	
    	/**
         * Método elegirUbicacion
         * Permite elegir la ubicación del personaje a añadir
         * @return location la ubicación elegida para el personaje
         * @throws SQLException 
         * @author Francisco Javier García Sánchez
         */
        
        public static int elegirUbicacion(Connection con) throws SQLException
        {
        	/**
        	 * Instanciación de un scanner para leer la opción intruducida por el usuario
        	 */
        	
        	Scanner sc = new Scanner(System.in);   		
        	
        	/**
        	 * Id máxima de lugares almacenados en la base de datos
        	 */
        	int idmax = 0;
        	
        	try (CallableStatement statement = con.prepareCall("SELECT findLocationSize()"))
        	{
        		try(ResultSet idOrigenSet = statement.executeQuery())
    			{
        			if (idOrigenSet.next()) 
        			{
            			idmax = idOrigenSet.getInt(1) -1;
        			}
    			}
        	}
        	
        	System.out.println("Elige el id de la ubicación del personaje");
        	System.out.println("La id máxima seleccionable actualmente es: " + idmax);
        	/**
        	 * Id de la ubicación del personaje
        	 */
        	int location = sc.nextInt();
        	if (location <= idmax)
        	{
        		return location;
        	}
        	else
        	{
        		System.out.println("Id no válida");
        		return 0;
        	}
		
    }
}
