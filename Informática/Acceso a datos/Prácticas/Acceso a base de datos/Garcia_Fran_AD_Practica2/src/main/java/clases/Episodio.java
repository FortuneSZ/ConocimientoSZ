package clases;

import java.util.Date;

import org.json.simple.JSONArray;
/**
 * Clase Episodio
 * @author Fran García Sánchez
 */

public class Episodio 
{
	
	/**
	 * Id del episodio
	 */
	
	private int id;
	
	/**
	 * Nombre del episodio
	 */
	
	private String nombre;
	
	/**
	 * Fecha en la cual fue emitido por primera vez el episodio
	 */
	
	private Date emisión;
	
	/**
	 * Código del episodio
	 */
	
	private String episodio;
	
	/**
	 * Array de personajes que aparecen en el episodio
	 */
	private JSONArray personajes;

	/**
	 * Constructor parametrizado de la clase Episodio
	 * @param id Id del episodio
	 * @param nombre Nombre del episodio
	 * @param emisión Fecha de emisión del episodio
	 * @param episodio Código del episodio
	 * @author Fran García Sánchez
	 */
	
	public Episodio(int id,String nombre,Date emisión,String episodio)
	{
		this.id = id;
		this.nombre = nombre;
		this.emisión = emisión;
		this.episodio = episodio;
	}
	
	/**
	 * Constructor parametrizado de la clase Episodio que añade el array de personajes
	 * @param id Id del episodio
	 * @param nombre Nombre del episodio
	 * @param emisión Fecha de emisión del episodio
	 * @param episodio Código del episodio
	 * @param personajes Array de personajes
	 * @author Fran García Sánchez
	 */
	
	public Episodio(int id,String nombre,Date emisión,String episodio,JSONArray personajes)
	{
		this.id = id;
		this.nombre = nombre;
		this.emisión = emisión;
		this.episodio = episodio;
		this.personajes = personajes;
	}
	
	/**
	 * Método getId
	 * Método getter del atributo id
	 * @return El id del episodio
	 * @author Fran García Sánchez
	 */
	
	public int getId()
	{
		return id;
	}
	
	/**
	 * Método getNombre
	 * Método getter del atributo nombre
	 * @return El nombre del episodio
	 * @author Fran García Sánchez
	 */
	
	public String getNombre()
	{
		return nombre;
	}
	
	/**
	 * Método getEmisión
	 * Método getter del atributo emisión
	 * @return La fecha de emisión del episodio
	 * @author Fran García Sánchez
	 */
	
	public Date getEmisión()
	{
		return emisión;
	}
	
	/**
	 * Método getEpisodio
	 * Método getter del atributo episodio
	 * @return El código del episodio
	 * @author Fran García Sánchez
	 */
	public String getEpisodio()
	{
		return episodio;
	}
	
	/**
	 * Método getPersonajes
	 * Método getter del atributo personajes
	 * @return el array de personajes del episodio
	 * @author Fran García Sánchez
	 */
	public JSONArray getPersonajes()
	{
		return personajes;
	}
}
