package clases;

import java.io.Serializable;

/**
 * Clase Personaje
 * @author Fran García Sánchez
 */

public class Personaje implements Serializable
{
	
	/**
	 * id Id del personaje
	 */
	
	private int id;
	
	/**
	 * nombre Nombre del personaje
	 */
	
	private String nombre;
	
	/**
	 * estado Estado del personaje(vivo/muerto) 
	 */
	
	private String estado;
	
	/**
	 * especies Especie u especies a las que el personaje pertenece
	 */
	
	private String especies;
	
	/**
	 * tipo Tipo de personaje
	 */
	
	private String tipo;
	
	/**
	 * género Género del personaje
	 */
	
	private String género;
	
	/**
	 * origen Lugar de origen del personaje
	 */
	
	private Lugar origen;
	
	/**
	 * ubicación Lugar actual donde se encuentra el personaje
	 */
	
	private Lugar ubicación;
	
	/**
	 * Constructor parametrizado de la clase Personaje
	 * @param id Id del personaje
	 * @param nombre Nombre del personaje
	 * @param estado Estado del personaje
	 * @param especies Especies del personaje
	 * @param tipo Tipo del personaje
	 * @param género Género del personaje
	 * @param origen Origen del personaje
	 * @param ubicación Ubicación del personaje
	 * @author Fran García Sánchez
	 */
	
	public Personaje(int id,String nombre,String estado,String especies,String tipo,String género,
			Lugar origen,Lugar ubicación)
	{
		this.id = id;
		this.nombre = nombre;
		this.estado = estado;
		this.especies = especies;
		this.tipo = tipo;
		this.género = género;
		this.origen = origen;
		this.ubicación = ubicación;
	}
	
	/**
	 * Método getId
	 * Método getter del atributo id
	 * @return el id del personaje
	 * @author Fran García Sánchez
	 */
	
	public int getId()
	{
		return id;
	}
	
	/**
	 * Método getNombre
	 * Método getter del atributo nombre
	 * @return el nombre del personaje
	 * @author Fran García Sánchez
	 */
	
	public String getNombre()
	{
		return nombre;
	}
	
	/**
	 * Método setNombre
	 * Método setter del atributo nombre
	 * @param nombre Nombre que asignar al atributo nombre
	 * @author Fran García Sánchez
	 */
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	/**
	 * Método getEstado
	 * Método getter del atributo estado
	 * @return el estado del personaje
	 * @author Fran García Sánchez
	 */
	
	public String getEstado()
	{
		return estado;
	}
	
	/**
	 * Método getEspecies
	 * Método getter del atributo especies
	 * @return la especie u especies a las que pertenece el personaje
	 * @author Fran García Sánchez
	 */
	
	public String getEspecies()
	{
		return especies;
	}
	
	/**
	 * Método getTipo
	 * Método getter del atributo tipo
	 * @return el tipo del personaje
	 * @author Fran García Sánchez
	 */
	
	public String getTipo()
	{
		return tipo;
	}
	
	/**
	 * Método setTipo
	 * Método setter del atributo tipo
	 * @param tipo Tipo que asignar al atributo tipo
	 * @author Fran García Sánchez
	 */
	
	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}
	
	/**
	 * Método getGenero
	 * Método getter del atributo género
	 * @return el género del personaje
	 * @author Fran García Sánchez
	 */
	
	public String getGenero()
	{
		return género;
	}
	
	/**
	 * Método getOrigen
	 * Método getter del atributo origen
	 * @return el lugar de origen del personaje
	 * @author Fran García Sánchez
	 */
	
	public Lugar getOrigen()
	{
		return origen;
	}
	
	/**
	 * Método getUbicacion
	 * Método getter del atributo ubicación
	 * @return ubicación del personaje
	 * @author Fran García Sánchez
	 */
	
	public Lugar getUbicacion()
	{
		return ubicación;
	}
	
	/**
	 * Método toString
	 * Método toString de la clase Personaje
	 * @return La forma predefinida de mostrar el contenido de la clase
	 * @author Fran García Sánchez
	 */
	
	@Override
	public String toString() 
	{
		System.out.println("-----------------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Estado: " + this.estado);
		System.out.println("Especie: " + this.especies);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("Género: " + this.género);
		System.out.println("Origen:");
		System.out.println(this.origen.toString());
		System.out.println("Ubicación:");
		System.out.println(this.ubicación.toString());
		System.out.println("-----------------------");
		return "";
	}
}
