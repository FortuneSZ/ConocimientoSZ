package utils;

import static java.lang.Math.toIntExact;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import clases.Episodio;
import clases.Lugar;
import clases.Personaje;

/**
 * Clase JsonUtils encargada de la gestión de JSON obtenido de la api
 * @author Francisco Javier García Sánchez
 */
public class JsonUtils 
{
	/**
	 * Método readUrlList
	 * Obtiene un objeto JSONObject leyendo la url proporcionada
	 * @param web Url de la página
	 * @return Objeto JSONObject obtenido de la url
	 * @author Francisco Javier García Sánchez
	 */
	public static JSONObject readUrlList(String web) 
	{
		/**
		 * Objeto que será devuelto con la información leída
		 */
		JSONObject jsonobj = null;

		try 
		{
			/**
			 * Url de la api a la que conectarnos
			 */
			URL url = new URL(web);
			URLConnection uc = url.openConnection();
			uc.setRequestProperty("User-Agent", "PostmanRuntime/7.20.1");
			uc.connect();
			
			try 
			{
				jsonobj = (JSONObject) new JSONParser().parse(new BufferedReader(new InputStreamReader(uc.getInputStream())));
			} 
			catch (org.json.simple.parser.ParseException e) 
			{
				e.printStackTrace();
			}
				
			return jsonobj;
			
		} 
		catch (Exception e) 
		{
			System.out.println("No se ha podido leer la URL: " + web);
			e.printStackTrace();
		}
		return jsonobj;
	}
	
	/**
	 * Método readUrlSize
	 * Obtiene la cantidad de objetos obtenidos de la api
	 * @param web Url de la api
	 * @return La cantidad de obtetos dentro del objeto obtenido de la api
	 */
	public static JSONObject readUrlSize(String web) 
	{
		
		JSONObject jsonobj = readUrlList(web);
		JSONObject size;
				
		size = (JSONObject) jsonobj.get("info");

		return size;
	}
	
	public static String clearFormat(String texto)
	{
		String nombre = texto;
		if (texto.contains("'"))
		{
			nombre = texto.replace("'", " ");
		}
		return nombre;
	}
	
	/**
	 * Método obtenerPersonaje
	 * Método que recoge la información de todos los personajes de la api, los añade a una lista y los pasa al gestor de base de datos para posteriormente añadirlos a ella
	 * @throws SQLException 
	 * @author Francisco Javier García Sánchez
	 */
	public static void obtenerPersonajes() throws SQLException
	{
		/**
		 * Instanciación de la clase DataBaseUtils que gestiona la base de datos
		 */
		DataBaseUtils database = new DataBaseUtils();
		/**
		 * Lista de personajes donde almacenar los lugares obtenidos desde la api
		 */
		List<Personaje> personajes = new ArrayList<Personaje>();
		/**
		 * Número del personaje a buscar
		 */
		int numeroPersonaje = 1;
		/**
		 * Objeto que contiene todos los personajes de la api
		 */
		JSONObject array =  readUrlSize("https://rickandmortyapi.com/api/character/");
		/**
		 * Instrucción para obtener la cantidad de personajes que contiene el objeto
		 */
		String sizeString = "" +  array.get("count");
		/**
		 * Cantidad de personajes que contiene el objeto
		 */
		int size = Integer.parseInt(sizeString);
		do
		{
			try
			{
				/**
				 * Url de la api a la que nos conectaremos para obtener el personaje
				 */
				String url = "https://rickandmortyapi.com/api/character/" + numeroPersonaje;
				/**
				 * Objeto del personaje buscado
				 * @see Método readUrlList
				 */
				JSONObject per = readUrlList(url);
				/**
				 * Objeto Personaje obtenido en base a la información del objeto JSONObject
				 */
				Personaje personaje = crearPersonaje(per);
				personajes.add(personaje);
			}
			catch(Exception e)
			{
				e.getLocalizedMessage();
			}
			numeroPersonaje++;
		}
		while(numeroPersonaje <= size);
		
		for (int i = 0; i < personajes.size();i++)
		{
			/**
			 * Objeto personaje el cual arreglar el formato
			 */
			Personaje per = personajes.get(i);
			/**
			 * Nombre del personaje
			 */
			String nombre = per.getNombre();
			/**
			 * Tipo del personaje
			 */
			String tipo = per.getTipo();
			per.setNombre(clearFormat(nombre));
			per.setTipo(clearFormat(tipo));
		}
		
		database.uploadCharacters(personajes);
		System.out.println("Personajes añadidos correctamente");
	}
	
	/**
	 * Método obtenerLugares
	 * Obtiene la lista completa de lugares de la api
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void obtenerLugares() throws SQLException
	{
		/**
		 * Instanciación de la clase DataBaseUtils que gestiona la base de datos
		 */
		DataBaseUtils database = new DataBaseUtils();
		/**
		 * Lista de lugares donde almacenar los lugares obtenidos desde la api
		 */
		List<Lugar> lugares = new ArrayList<Lugar>();
		/**
		 * Número del lugar a buscar
		 */
		int numeroLugar = 1;
		/**
		 * Objeto que contiene todos los lugares de la api
		 */
		JSONObject array =  readUrlSize("https://rickandmortyapi.com/api/location/");
		/**
		 * Instrucción para obtener la cantidad de lugares que contiene el objeto
		 */
		String sizeString = "" +  array.get("count");
		/**
		 * Cantidad de lugares que contiene el objeto
		 */
		int size = Integer.parseInt(sizeString);
		do
		{
			try
			{
				/**
				 * Url de la api a la que nos conectaremos para obtener el personaje
				 */
				String url = "https://rickandmortyapi.com/api/location/" + numeroLugar;
				/**
				 * Objeto del personaje buscado
				 * @see Método readUrlList
				 */
				JSONObject lug = readUrlList(url);
				/**
				 * Objeto Personaje obtenido en base a la información del objeto JSONObject
				 */
				Lugar lugar = crearLugar(lug);
				lugares.add(lugar);
			}
			catch(Exception e)
			{
				e.getLocalizedMessage();
			}
			numeroLugar++;
		}
		while(numeroLugar <= size);
		
		for (int i = 0; i < lugares.size();i++)
		{
			/**
			 * Objeto lugar el cual arreglar el formato
			 */
			Lugar place = lugares.get(i);
			/**
			 * Nombre del lugar
			 */
			String name = place.getNombre();
			/**
			 * Dimensión del lugar
			 */
			String dimension = place.getDimension();
			place.setNombre(clearFormat(name));
			place.setDimension(clearFormat(dimension));
		}
		database.UploadPlaces(lugares);
		System.out.println("Lugares obtenidos correctamente");
	}
	
	/**
	 * Método obtenerEpisodios
	 * Obtiene la lista completa de episodios de la api
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void obtenerEpisodios() throws SQLException
	{
		/**
		 * Instanciación de la clase DataBaseUtils que gestiona la base de datos
		 */
		DataBaseUtils database = new DataBaseUtils();
		/**
		 * Lista de episodios donde almacenar los episodios obtenidos desde la api
		 */
		List<Episodio> episodios = new ArrayList<Episodio>();
		/**
		 * Número del episodio a buscar
		 */
		int numeroEpisodio = 1;
		/**
		 * Objeto que contiene todos los episodios de la api
		 */
		JSONObject array =  readUrlSize("https://rickandmortyapi.com/api/episode/");
		/**
		 * Instrucción para obtener la cantidad de episodios que contiene el objeto
		 */
		String sizeString = "" +  array.get("count");
		/**
		 * Cantidad de episodios que contiene el objeto
		 */
		int size = Integer.parseInt(sizeString);
		do
		{
			try
			{
				/**
				 * Url de la api a la que nos conectaremos para obtener el personaje
				 */
				String url = "https://rickandmortyapi.com/api/episode/" + numeroEpisodio;
				/**
				 * Objeto del personaje buscado
				 * @see Método readUrlList
				 */
				JSONObject ep = readUrlList(url);
				/**
				 * Objeto Personaje obtenido en base a la información del objeto JSONObject
				 */
				Episodio episodio = crearEpisodio(ep);
				episodios.add(episodio);
			}
			catch(Exception e)
			{
				e.getLocalizedMessage();
			}
			numeroEpisodio++;
		}
		while(numeroEpisodio <= size);
		
		database.UploadEpisodes(episodios);
		System.out.println("Episodios obtenidos correctamente");
	}
	
	/**
	 * Método obtenerPersonajesEnEpisodios
	 * Obtiene la lista completa de episodios de la api y busca por cada episodio los personajes que aparecieron
	 * @throws SQLException
	 * @author Francisco Javier García Sánchez
	 */
	public static void obtenerPersonajesEnEpisodios() throws SQLException
	{
		/**
		 * Instanciación de la clase DataBaseUtils que gestiona la base de datos
		 */
		DataBaseUtils database = new DataBaseUtils();
		/**
		 * Lista de episodios donde almacenar los episodios obtenidos desde la api
		 */
		List<Episodio> episodios = new ArrayList<Episodio>();
		/**
		 * Número del episodio a buscar
		 */
		int numeroEpisodio = 1;
		/**
		 * Objeto que contiene todos los episodios de la api
		 */
		JSONObject array =  readUrlSize("https://rickandmortyapi.com/api/episode/");
		/**
		 * Instrucción para obtener la cantidad de episodios que contiene el objeto
		 */
		String sizeString = "" +  array.get("count");
		/**
		 * Cantidad de episodios que contiene el objeto
		 */
		int size = Integer.parseInt(sizeString);
		do
		{
			try
			{
				/**
				 * Url de la api a la que nos conectaremos para obtener el personaje
				 */
				String url = "https://rickandmortyapi.com/api/episode/" + numeroEpisodio;
				/**
				 * Objeto del personaje buscado
				 * @see Método readUrlList
				 */
				JSONObject ep = readUrlList(url);
				/**
				 * Objeto Personaje obtenido en base a la información del objeto JSONObject
				 */
				Episodio episodio = crearEpisodioConPersonajes(ep);
				episodios.add(episodio);
			}
			catch(Exception e)
			{
				e.getLocalizedMessage();
			}
			numeroEpisodio++;
		}
		while(numeroEpisodio <= size);
		
		for (int i = 0; i < size; i++)
		{
			for (int x = 0; x < episodios.get(i).getPersonajes().size();x++)
			{
				/**
				 * Id del episodio
				 */
				int id_ep = episodios.get(i).getId();
				JSONObject personaje = readUrlList((String) episodios.get(i).getPersonajes().get(x));
				/**
				 * Id del personaje
				 */
				int id_char = toIntExact((long)personaje.get("id"));
				database.UploadCharacterInEpisode(id_char,id_ep);
			}
		}
		
		System.out.println("Personajes en episodios obtenidos correctamente");
	}
	
	/**
	 * Método crearPersonaje
	 * crea un objeto de tipo personaje
	 * @param per Objeto Json del personaje obtenido desde la api
	 * @return Objeto de tipo personaje
	 * @author Francisco Javier García Sánchez
	 */
	public static Personaje crearPersonaje(JSONObject per)
	{
		/**
		 * Variable que almacena el id del objeto
		 */
		int id =  toIntExact((long)per.get("id"));
		/**
		 * Variable que almacena el nombre del objeto
		 */
		String nombre = (String) per.get("name");
		/**
		 * Variable que almacena el estado del objeto
		 */
		String estado = (String) per.get("status");
		/**
		 * Variable que almacena la especie del objeto
		 */
		String especie = (String) per.get("species");
		/**
		 * Variable que almacena el tipo del objeto
		 */
		String tipo = (String) per.get("type");
		/**
		 * Variable que almacena el tipo del objeto
		 */
		String genero = (String) per.get("gender");
		/**
		 * Variable que almacena el origen del objeto
		 */
		Lugar origen = getOrigin(per);
		
		/**
		 * Variable que almacena la ubicación del objeto
		 */
		Lugar ubicacion = getLocation(per);
		
		/**
		 * Variable que almacena la imagen del objeto
		 */	
		return new Personaje(id,nombre,estado,especie,tipo,genero,origen,ubicacion);
	}
	
	/**
	 * Método crearLugar
	 * Crea un objeto de tipo Lugar
	 * @param lug Objeto del lugar obtenido de la api
	 * @return Un objeto de tipo Lugar
	 * @author Francisco Javier García Sánchez
	 */
	public static Lugar crearLugar(JSONObject lug)
	{
		/**
		 * variable que almacena el id de la ubicación
		 */
		int idUbicacion = toIntExact((long)lug.get("id"));
		/**
		 * Variable que almacena el nombre de la ubicación
		 */
		String nombreUbicacion = (String) lug.get("name");
		/**
		 * Variable que almacena el tipo de la ubicación
		 */
		String typeUbicacion = (String) lug.get("type");
		/**
		 * Variable que almacena la dimensión de la ubicación
		 */
		String dimensionUbicacion = (String) lug.get("dimension");
		/**
		 * Objeto ubicación del tipo Lugar a devolver por la función
		 */
		Lugar ubicacion = new Lugar(idUbicacion,nombreUbicacion,typeUbicacion,dimensionUbicacion);
		return ubicacion;
	}
	
	/**
	 * Método crearEpisodio
	 * Crea un objeto de tipo Episodio
	 * @param ep Objeto del episodio obtenido de la api
	 * @return El episodio
	 * @throws ParseException
	 * @author Francisco Javier García Sánchez
	 */
	public static Episodio crearEpisodio(JSONObject ep) throws ParseException
	{
		
		/**
		 * variable que almacena el id del episodio
		 */
		
		int idEpisodio = toIntExact((long)ep.get("id"));
		/**
		 * Variable que almacena el nombre del episodio
		 */
		
		String nombreEpisodio = (String) ep.get("name");
		
		/**
		 * Variable que almacena el formato de la fecha
		 */
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		
		/**
		 * Variable que almacena la fecha de emisión del episodio
		 */
		
		Date emisionEpisodioApi = format.parse((String) ep.get("air_date"));
		java.sql.Date emisionEpisodio = new java.sql.Date(emisionEpisodioApi.getTime());
		/**
		 * Variable que almacena el código del episodio
		 */
		String codigoEpisodio = (String) ep.get("episode");
		/**
		 * Objeto episodio del tipo Episodio a devolver por la función
		 */
		Episodio episodio = new Episodio(idEpisodio,nombreEpisodio,emisionEpisodio,codigoEpisodio);
		return episodio;
	}
	
	/**
	 * Método crearEpisodioConPersonajes
	 * Crea un objeto de tipo Episodio recogiendo también los personajes que aparecen en este
	 * @param ep Objeto del episodio
	 * @return El episodio
	 * @throws ParseException
	 * @author Francisco Javier García Sánchez
	 */
	public static Episodio crearEpisodioConPersonajes(JSONObject ep) throws ParseException
	{
		/**
		 * Objeto episodio del tipo Episodio a devolver por la función
		 */
		Episodio episodio = crearEpisodio(ep);
		/**
		 * Objeto JSONArray que contiene los èrsonajes aparecidos en el episodio
		 */
		JSONArray personajes = (JSONArray) ep.get("characters");
		episodio = new Episodio(episodio.getId(),episodio.getNombre(),episodio.getEmisión(),episodio.getEpisodio(),personajes);
		
		return episodio;
	}
	
	/**
	 * Método getOrigin
	 * Obtiene el objeto origin del interior del objeto enviado por parámetros
	 * @param per objeto JSONObject obtenido de la api
	 * @return Objeto origen de tipo Lugar en base a los parámetros obtenidos
	 * @author Francisco Javier García Sánchez
	 */
	public static Lugar getOrigin(JSONObject per) 
	{
		/**
		 * Objeto de tipo Lugar que devolverá el método
		 */
	    Lugar origen;
	    /**
	     * Objeto origen obtenido del personaje
	     */
	    JSONObject origenObjeto = (JSONObject) per.get("origin");

	    if (origenObjeto != null) 
	    {
	    	/**
	    	 * Url del origen obtenida del objeto, para pdoer buscar dicha ubicación en la api
	    	 */
	        String urlOrigen = (String) origenObjeto.get("url");

	        // Verificamos que la URL no sea vacía antes de hacer la solicitud
	        if (urlOrigen != null && !urlOrigen.isEmpty()) 
	        {
	            origenObjeto = readUrlList(urlOrigen);
	        } 
	        else 
	        {
	            origenObjeto = null; // Evita hacer la solicitud si la URL es vacía
	        }

	        if (origenObjeto != null) 
	        {
	        	/**
	        	 * Id del origen
	        	 */
	            int idorigen = toIntExact((long) origenObjeto.get("id"));
	            /**
	        	 * Nombre del origen
	        	 */
	            String nombreOrigen = (String) origenObjeto.get("name");
	            /**
	        	 * Tipo del origen
	        	 */
	            String type = (String) origenObjeto.get("type");
	            /**
	        	 * Dimensión del origen
	        	 */
	            String dimension = (String) origenObjeto.get("dimension");

	            origen = new Lugar(idorigen, nombreOrigen, type, dimension);
	        } 
	        else 
	        {
	            origen = new Lugar(0, "Unknown", "Unknown", "Unknown");
	        }
	    }
	    else 
	    {
	        origen = new Lugar(0, "Unknown", "Unknown", "Unknown");
	    }
	    
	    return origen;
	}
	
	/**
	 * Método getLocation
	 * Obtiene el objeto location del interior del objeto enviado por parámetros
	 * @param per objeto JSONObject obtenido de la api
	 * @return Objeto ubicación de tipo Lugar en base a los parámetros obtenidos
	 * @author Francisco Javier García Sánchez
	 */
	public static Lugar getLocation(JSONObject per) 
	{
		/**
		 * Objeto de tipo Lugar que devolverá el método
		 */
	    Lugar ubicacion;
	    /**
	     * Objeto de la ubicación obtenida del personaje
	     */
	    JSONObject ubicacionObjeto = (JSONObject) per.get("location");

	    if (ubicacionObjeto != null) 
	    {
	    	/**
	    	 * Url de la ubicación obtenida del objeto, para pdoer buscar dicha ubicación en la api
	    	 */
	        String urlUbicacion = (String) ubicacionObjeto.get("url");

	        // Si la URL es vacía o null, devolvemos una ubicación por defecto
	        if (urlUbicacion == null || urlUbicacion.isEmpty()) 
	        {
	            return new Lugar(0, "Unknown", "Unknown", "Unknown");
	        }

	        // Intentamos leer la URL solo si es válida
	        ubicacionObjeto = readUrlList(urlUbicacion);

	        if (ubicacionObjeto != null) 
	        {
	        	/**
	        	 * Id de la ubicación
	        	 */
	            int idUbicacion = toIntExact((long) ubicacionObjeto.get("id"));
	            /**
	        	 * Nombre de la ubicación
	        	 */
	            String nombreUbicacion = (String) ubicacionObjeto.get("name");
	            /**
	        	 * Tipo de la ubicación
	        	 */
	            String typeUbicacion = (String) ubicacionObjeto.get("type");
	            /**
	        	 * Dimensión de la ubicación
	        	 */
	            String dimensionUbicacion = (String) ubicacionObjeto.get("dimension");

	            ubicacion = new Lugar(idUbicacion, nombreUbicacion, typeUbicacion, dimensionUbicacion);
	        }
	        else
	        {
		    	ubicacion = new Lugar(0, "Unknown", "Unknown", "Unknown");
	        }
	    }
	    else
	    {
	    	ubicacion = new Lugar(0, "Unknown", "Unknown", "Unknown");
	    }

	    return ubicacion;
	}
}
