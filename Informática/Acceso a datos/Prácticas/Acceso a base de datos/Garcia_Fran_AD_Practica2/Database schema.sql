/*DROPS
DROP DATABASE Serie;
DROP TABLE character_in_episode;
DROP TABLE episode;
DROP TABLE character;
DROP TABLE location;
DROP PROCEDURE truncateTables;
DROP PROCEDURE InsertCharacter;
DROP PROCEDURE insertVoidPlace;
DROP PROCEDURE insertPlace;
DROP PROCEDURE insertEpisode;
DROP PROCEDURE insertCharacterInEpisode;
DROP FUNCTION findChar;
DROP FUNCTION findCharSize;
DROP FUNCTION findLocationSize;
DROP FUNCTION selectAllFromLocation;
DROP FUNCTION selectFromCharacter;
DROP FUNCTION characterWithoutEpisode;
*/

/*Base de datos*/

CREATE DATABASE Serie;

/*Tablas*/

/*Creación de la tabla ubicación*/
CREATE TABLE location 
(
	Id INTEGER PRIMARY KEY,
	Name VARCHAR,
	Type VARCHAR,
	Dimension VARCHAR
);

/*Creación de la tabla personajes*/
CREATE TABLE character
(
	Id INTEGER PRIMARY KEY,
	Name VARCHAR,
	Status VARCHAR,
	Species VARCHAR,
	Type VARCHAR,
	Gender VARCHAR,
	Id_Origin INTEGER REFERENCES location(Id),
	Id_Location INTEGER REFERENCES location(Id)
);

/*Creación de la tabla episodios*/
CREATE TABLE episode
(
	Id INTEGER PRIMARY KEY,
	Name VARCHAR,
	Air_Date DATE,
	Episode VARCHAR
);

/*Creación de la tabla de personajes en episodios*/
CREATE TABLE character_in_episode
(
	Id_Character INTEGER REFERENCES character(Id),
	Id_Episode INTEGER REFERENCES episode(Id),
	PRIMARY KEY(Id_Character,Id_Episode)
);

/*Procedimientos*/

/*Procedimiento para vaciar las tablas*/
CREATE OR REPLACE PROCEDURE truncateTables()
LANGUAGE plpgsql
AS
$$

BEGIN
TRUNCATE TABLE location CASCADE;
TRUNCATE TABLE character CASCADE;
TRUNCATE TABLE episode CASCADE;
TRUNCATE TABLE character_in_episode CASCADE;
END;
$$;

/*Procedimiento para insertar personajes*/
CREATE OR REPLACE PROCEDURE InsertCharacter(id INTEGER, name VARCHAR, status VARCHAR, species VARCHAR, type VARCHAR, gender VARCHAR, Id_Origin INTEGER, Id_Location INTEGER)
LANGUAGE plpgsql
AS
$$
BEGIN
INSERT INTO character(Id,Name,Status,Species,Type,Gender,Id_Origin,Id_Location) 
VALUES(id,name,status,species,type,Gender,Id_Origin,Id_Location);

END;
$$;

/*Procedimiento para insertar un lugar vacío para los personajes sin origen conocido*/
CREATE OR REPLACE PROCEDURE insertVoidPlace()
LANGUAGE plpgsql
AS
$$

BEGIN
INSERT INTO location(Id,Name) VALUES(0,'unknown');
END;
$$;

/*Procedimiento para insertar lugares*/
CREATE OR REPLACE PROCEDURE insertPlace(id_place INTEGER,name_place VARCHAR,type_place VARCHAR,dimension_place VARCHAR)
LANGUAGE plpgsql
AS
$$

BEGIN
INSERT INTO location(Id,Name,Type,Dimension) VALUES(id_place,name_place,type_place,dimension_place);
END;
$$;

/*Procedimiento para insertar episodios*/
CREATE OR REPLACE PROCEDURE insertEpisode(id_ep INTEGER,name_ep VARCHAR,air_ep DATE,code_ep VARCHAR)
LANGUAGE plpgsql
AS
$$

BEGIN
INSERT INTO episode(Id,Name,Air_Date,Episode) VALUES(id_ep,name_ep,air_ep,code_ep);
END;
$$;

/*Procedimiento para insertar personajes en episodios*/
CREATE OR REPLACE PROCEDURE insertCharacterInEpisode(id_cha INTEGER,id_ep INTEGER)
LANGUAGE plpgsql
AS
$$

BEGIN
INSERT INTO character_in_episode(Id_Character,Id_Episode) VALUES(id_cha,id_ep)
ON CONFLICT (Id_Character, Id_Episode) DO NOTHING;
END;
$$;

/*Funciones*/

/*Función para encontrar la id de un lugar(origen o ubicación) de un personaje*/
CREATE OR REPLACE FUNCTION findChar(ID_Char INTEGER)
RETURNS INTEGER
LANGUAGE plpgsql
AS
$$
DECLARE
    id_place INTEGER;
BEGIN
    SELECT Id INTO id_place FROM location WHERE Id = ID_Char;
    RETURN id_place;
END;
$$;

/*Función para obtener el tamaño de la tabla de personajes*/
CREATE OR REPLACE FUNCTION findCharSize()
RETURNS INTEGER
LANGUAGE plpgsql
AS
$$
DECLARE
    charSize INTEGER;
BEGIN
    SELECT COUNT(*) INTO charSize FROM character;
    RETURN charSize;
END;
$$;

/*Función para obtener el tamaño de la tabla de ubicaciones*/
CREATE OR REPLACE FUNCTION findLocationSize()
RETURNS INTEGER
LANGUAGE plpgsql
AS
$$
DECLARE
    locationSize INTEGER;
BEGIN
    SELECT COUNT(Id) INTO locationSize FROM location;
    RETURN locationSize;
END;
$$;

/*Función para obtener toda la información de un lugar determinado*/
CREATE OR REPLACE FUNCTION selectAllFromLocation(id_location INTEGER)
RETURNS TABLE(name VARCHAR, type VARCHAR, dimension VARCHAR)
LANGUAGE plpgsql
AS
$$
BEGIN
	RETURN QUERY
    SELECT location.Name::VARCHAR,location.Type::VARCHAR,location.Dimension::VARCHAR
	FROM location 
	WHERE location.Id = id_location;
END;
$$;

/*Función para obtener todos los personajes sin episodio*/
CREATE OR REPLACE FUNCTION characterWithoutEpisode()
RETURNS TABLE(id INTEGER,name VARCHAR, status VARCHAR, species VARCHAR,type VARCHAR,gender VARCHAR, id_origin INTEGER, id_location INTEGER)
LANGUAGE plpgsql
AS
$$
BEGIN
	RETURN QUERY
    SELECT * FROM character c
WHERE NOT EXISTS
(SELECT Id_Character FROM character_in_episode p
WHERE c.Id = p.Id_Character);

END;
$$;

SELECT * FROM characterWithoutEpisode();
/*Función para obtener los personajes en base a un criterio determinado*/
CREATE OR REPLACE FUNCTION selectFromCharacter(atribute VARCHAR,querry VARCHAR)
RETURNS TABLE(id INTEGER,name VARCHAR, status VARCHAR, species VARCHAR,type VARCHAR,gender VARCHAR, id_origin INTEGER, id_location INTEGER)
LANGUAGE plpgsql
AS
$$
DECLARE
    column_type VARCHAR;
BEGIN
	EXECUTE FORMAT('SELECT pg_typeof(%I) FROM "character" LIMIT 1', atribute) INTO column_type;
	
	IF column_type IN ('text', 'character varying', 'char') THEN
        RETURN QUERY EXECUTE FORMAT(
            'SELECT Id, Name, Status, Species, Type, Gender, Id_Origin, Id_Location
            FROM character
            WHERE %I LIKE %L', atribute, '%' || querry || '%'
        );
    ELSE
        RETURN QUERY EXECUTE FORMAT(
            'SELECT Id, Name, Status, Species, Type, Gender, Id_Origin, Id_Location
            FROM "character"
            WHERE %I = %L', atribute, querry
        );
    END IF;
END;
$$;
DROP FUNCTION selectFromCharacter;

/*Permisos y usuarios*/

CREATE USER dev WITH PASSWORD 'Fortune99';
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO dev;

/*Inserts*/

INSERT INTO location(Id,Name) VALUES(0,'unknown');