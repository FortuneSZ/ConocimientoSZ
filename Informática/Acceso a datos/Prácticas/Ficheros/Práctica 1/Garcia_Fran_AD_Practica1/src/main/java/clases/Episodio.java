package clases;

import java.util.Date;

import org.json.simple.JSONArray;
/**
 * Clase Episodio
 * @author Fran García Sánchez
 */
public class Episodio 
{
	/**
	 * Id del episodio
	 */
	private int id;
	/**
	 * Nombre del episodio
	 */
	private String nombre;
	/**
	 * Fecha en la cual fue emitido por primera vez el episodio
	 */
	private Date emisión;
	/**
	 * Código del episodio
	 */
	private String episodio;
	/**
	 * Personajes que aparecen en el episodio
	 */
	private JSONArray personajes;
	
	/**
	 * Constructor parametrizado de la clase Episodio
	 * @param id
	 * @param nombre
	 * @param emisión
	 * @param episodio
	 * @param personajes
	 */
	public Episodio(int id,String nombre,Date emisión,String episodio,JSONArray personajes)
	{
		this.id = id;
		this.nombre = nombre;
		this.emisión = emisión;
		this.episodio = episodio;
		this.personajes = personajes;
	}
	
	/**
	 * Método getId
	 * Método getter del atributo id
	 * @return El id del episodio
	 */
	public int getId()
	{
		return id;
	}
	/**
	 * Método getNombre
	 * Método getter del atributo nombre
	 * @return El nombre del episodio
	 */
	public String getNombre()
	{
		return nombre;
	}
	/**
	 * Método getEmisión
	 * Método getter del atributo emisión
	 * @return La fecha de emisión del episodio
	 */
	public Date getEmisión()
	{
		return emisión;
	}
	/**
	 * Método getEpisodio
	 * Método getter del atributo episodio
	 * @return El código del episodio
	 */
	public String getEpisodio()
	{
		return episodio;
	}
	/**
	 * Método getPersonajes
	 * Método getter del atributo personajes
	 * @return Los personajes que aparecen en el episodio
	 */
	public JSONArray getPersonajes()
	{
		return personajes;
	}
}
