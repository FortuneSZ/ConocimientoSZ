package clases;

import java.io.Serializable;

import org.json.simple.JSONArray;

/**
 * Clase Personaje
 * @author Fran García Sánchez
 */
public class Personaje implements Serializable
{
	/**
	 * id Id del personaje
	 */
	private int id;
	/**
	 * nombre Nombre del personaje
	 */
	private String nombre;
	/**
	 * estado Estado del personaje(vivo/muerto) 
	 */
	private String estado;
	/**
	 * especies Especie u especies a las que el personaje pertenece
	 */
	private String especies;
	/**
	 * tipo Tipo de personaje
	 */
	private String tipo;
	/**
	 * género Género del personaje
	 */
	private String género;
	/**
	 * origen Lugar de origen del personaje
	 */
	private Lugar origen;
	/**
	 * ubicación Lugar actual donde se encuentra el personaje
	 */
	private Lugar ubicación;
	/**
	 * imagen Imagen del personaje
	 */
	private String imagen;
	/**
	 * episodios Episodios en los cuales aparece el personaje
	 */
	private JSONArray episodios;
	
	/**
	 * Constructor parametrizado de la clase Personaje
	 * @param id
	 * @param nombre
	 * @param estado
	 * @param especies
	 * @param tipo
	 * @param género
	 * @param origen
	 * @param ubicación
	 * @param imagen
	 * @param episodios
	 */
	public Personaje(int id,String nombre,String estado,String especies,String tipo,String género,
			Lugar origen,Lugar ubicación,String imagen,JSONArray episodios)
	{
		this.id = id;
		this.nombre = nombre;
		this.estado = estado;
		this.especies = especies;
		this.tipo = tipo;
		this.género = género;
		this.origen = origen;
		this.ubicación = ubicación;
		this.imagen = imagen;
		this.episodios = episodios;
	}
	
	/**
	 * Método getId
	 * Método getter del atributo id
	 * @return el id del personaje
	 */
	public int getId()
	{
		return id;
	}
	/**
	 * Método getNombre
	 * Método getter del atributo nombre
	 * @return el nombre del personaje
	 */
	public String getNombre()
	{
		return nombre;
	}
	/**
	 * Método getEstado
	 * Método getter del atributo estado
	 * @return el estado del personaje
	 */
	public String getEstado()
	{
		return estado;
	}
	/**
	 * Método getEspecies
	 * Método getter del atributo especies
	 * @return la especie u especies a las que pertenece el personaje
	 */
	public String getEspecies()
	{
		return especies;
	}
	/**
	 * Método getTipo
	 * Método getter del atributo tipo
	 * @return el tipo del personaje
	 */
	public String getTipo()
	{
		return tipo;
	}
	/**
	 * Método getGenero
	 * Método getter del atributo género
	 * @return el género del personaje
	 */
	public String getGenero()
	{
		return género;
	}
	/**
	 * Método getOrigen
	 * Método getter del atributo origen
	 * @return el lugar de origen del personaje
	 */
	public Lugar getOrigen()
	{
		return origen;
	}
	/**
	 * Método getUbicacion
	 * Método getter del atributo ubicación
	 * @return ubicación del personaje
	 */
	public Lugar getUbicacion()
	{
		return ubicación;
	}
	/**
	 * Método getImagen
	 * Método getter del atributo imagen
	 * @return la imagen del personaje
	 */
	public String getImagen()
	{
		return imagen;
	}
	/**
	 * Método getEpisodios
	 * Método getter del atributo episodios
	 * @return los episodios en los que aparece el personaje
	 */
	public JSONArray getEpisodios()
	{
		return episodios;
	}
	
	/**
	 * Método toString
	 * Método toString de la clase Personaje
	 * @return La forma predefinida de mostrar el contenido de la clase
	 */
	@Override
	public String toString() 
	{
		System.out.println("-----------------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Estado: " + this.estado);
		System.out.println("Especie: " + this.especies);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("Género: " + this.género);
		System.out.println("Origen:");
		System.out.println(this.origen.toString());
		System.out.println("Ubicación:");
		System.out.println(this.ubicación.toString());
		System.out.println("Imagen: " + this.imagen);
		System.out.println("Episodios:");
		for(int i =0; i < this.episodios.size();i++)
		{
			System.out.println("    -"+this.episodios.get(i));
		}
		System.out.println("-----------------------");
		return "";
	}
}
