package clases;

import java.io.Serializable;

import org.json.simple.JSONArray;

/**
 * Clase Lugar
 * @author Fran García Sánchez 
 */
public class Lugar implements Serializable
{
	/**
	 * ID del lugar
	 */
	private int id;
	/**
	 * nombre Nombre del lugar
	 */
	private String nombre;
	/**
	 * Tipo de lugar
	 */
	private String tipo;
	/**
	 * Dimensión en la que se encuentra el lugar
	 */
	private String dimensión;
	/**
	 * Personaje que residen en ese lugar
	 */
	private JSONArray habitantes;
	
	/**
	 * Constructor parametrizado de la clase Lugar
	 * @param id
	 * @param nombre
	 * @param tipo
	 * @param dimensión
	 * @param residentes
	 */
	public Lugar(int id,String nombre,String tipo,String dimensión,JSONArray residentes)
	{
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.dimensión = dimensión;
		this.habitantes = residentes;
	}
	
	/**
	 * Método toString
	 * Método toString de la clase Lugar
	 * @return La forma predefinida de mostrar el contenido de la clase
	 */
	@Override
	public String toString()
	{
		System.out.println("-----------------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("Dimensión: " + this.dimensión);
		if(this.habitantes != null)
		{
			System.out.println("Habitantes:");
			for(int i = 0; i < this.habitantes.size();i++)
			{
				System.out.println("    -"+this.habitantes.get(i));
			}
		}
		System.out.println("-----------------------");
		return "";
	}
	
	/**
	 * Método getId
	 * Método getter del atributo id
	 * @return la id del lugar
	 */
	public int getId()
	{
		return id;
	}
	/**
	 * Método getNombre
	 * Método getter del atributo nombre
	 * @return el nombre del lugar
	 */
	public String getNombre()
	{
		return nombre;
	}
	/**
	 * Método getDimension
	 * Método getter del atributo dimensión
	 * @return la dimensión a la que pertenece el lugar
	 */
	public String getDimension()
	{
		return dimensión;
	}
	/**
	 * Método getTipo
	 * Método getter del atributo tipo
	 * @return el tipo de lugar
	 */
	public String getTipo()
	{
		return tipo;
	}
	/**
	 * Método getHabitantes
	 * Método getter del atributo habitantes
	 * @return los habitantes del lugar
	 */
	public JSONArray getHabitantes()
	{
		return habitantes;
	}
}
