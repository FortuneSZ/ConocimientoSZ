package com.FranGarcia.Garcia_Fran_AD_Practica1;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import utils.JsonUtils;
import utils.SerializarUtils;
import utils.XmlUtils;
import clases.Personaje;

/**
 * Clase principal de la aplicación
 * @author Fran García Sánchez
 */
public class App 
{
	/**
	 * Método main de la aplicación principal
	 * Muestra un menú de opciones y mediante y switch el usuario selecciona su opción deseada
	 * @param args
	 * @throws ParseException
	 */
    public static void main(String[] args) throws ParseException 
    {
    	String lista = "personajes.dat"; // Ruta del archivo a verificar
        File archivo = new File(lista);
    	/**
    	 * Número del personaje a buscar
    	 */
    	int personaje;
    	/**
    	 * Instanciación de la clase XmlUtils
    	 */
    	XmlUtils xml = new XmlUtils();
    	/**
    	 * Instanciación de la clase JsonUtils
    	 */
    	JsonUtils js = new JsonUtils();
    	/**
    	 * Instanciación de un scanner para leer la opción intruducida por el usuario
    	 */
    	Scanner sc = new Scanner(System.in);
    	/**
    	 * Lista de personajes,instanciada en base del fichero de personajes serializados
    	 */
    	List<Personaje> personajes = new ArrayList<Personaje>();
    	if(!archivo.exists())
    	{
    		try 
    		{
				archivo.createNewFile();
				
			} 
    		catch (IOException e) 
    		{
				e.printStackTrace();
			}
    	}
    	else if (archivo.length() > 0)
    	{
    		personajes = SerializarUtils.deSerializarListaPersonajes("./personajes.dat");
    	}
    	
    	/**
    	 * Opción elegida por el usuario
    	 */
    	int eleccion = 0;
    	do
    	{
    		menu();
    		System.out.println("Seleccione la opción deseada");
    		eleccion = sc.nextInt();
    		switch(eleccion)
    		{
    			case 1:
    				System.out.println("Introduzca el número del episodio");
    				int episodio = sc.nextInt();
    				xml.crearXml(episodio);
    				enter();
    			break;
    			
    			case 2:
    				xml.procesarXml();
    				enter();
        		break;
        		
    			case 3:

    				System.out.println("Introduzca el número del personaje");
        			personaje = sc.nextInt();
        			JsonUtils.obtenerPersonaje(personaje,personajes);

    				enter();
    				
        		break;
        		
    			case 4:
    				String ruta = "./";
    				SerializarUtils.serializarListaPersonajes(personajes,ruta);
    				enter();
        		break;
        		
    			case 5:
    				if (archivo.exists())
    				{
    					if (archivo.length() > 1)
    					{
    						List<Personaje> personajesMostrar = SerializarUtils.deSerializarListaPersonajes("./personajes.dat");
            				for(int i = 0; i < personajesMostrar.size();i++)
            				{
            					System.out.println(personajesMostrar.get(i));
            				}
            				enter();
    					}
    					else
    					{
    						System.out.println("El fichero de personajes está vacío");
        					enter();
    					}
    				}
    				else
    				{
    					System.out.println("El fichero de personajes no existe");
    					enter();
    				}
    				
        		break;
        		
    			case 6:
    				System.out.println("Introduzca el número del personaje");
    				personaje = sc.nextInt();
    				JsonUtils.localizacionPersonaje(personaje);
    				enter();
        		break;
        		
    			case 7:
    				System.out.println("Has seleccionado salir");
        		break;
    		}
    	}
    	while(eleccion != 7);
        
    }
    /**
     * Método que muestra el menú de opciones
     */
    public static void menu()
    {
    	System.out.println("1. Conversor XML");
    	System.out.println("2. Mostrar XML");
    	System.out.println("3. Obtener Personaje");
    	System.out.println("4. Guardar Personajes");
    	System.out.println("5. Mostrar Personajes");
    	System.out.println("6. Localización del Personaje");
    	System.out.println("7. Salir");
    	System.out.println();
    }
    
    public static void enter()
    {
    	Scanner sc = new Scanner(System.in);
    	System.out.println();
    	System.out.println("Presione enter para continuar");
		 sc.nextLine();
    }
}
