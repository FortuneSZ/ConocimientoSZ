package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import clases.Personaje;

/**
 * Clase SerializarUtils
 * Contiene todos los métodos necesarios para serializar y desrializar personajes
 * @author Fran García Sánchez
 */
public class SerializarUtils 
{
	/**
	 * Método serializarListaPersonajes
	 * Permite serializar una lista de personajes
	 * @param objEntrada Lista de personajes a serializar
	 * @param rutaa Ruta donde se sitúa o desea situar el fichero resultante
	 */
	public static void serializarListaPersonajes(List<Personaje> objEntrada, String rutaa) 
	{
		/**
		 * Fichero creado con la ruta y el nombre del fichero
		 */
		File destino = new File(rutaa + "personajes.dat");
		
		try (FileOutputStream salida = new FileOutputStream(destino);
				ObjectOutputStream ficheroSalida = new ObjectOutputStream(salida)) 
		{

			ficheroSalida.writeObject(objEntrada);

		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		System.out.println();
		System.out.println("Serialización realizada correctamente");
	}
	/**
	 * Método deSerializarListaPersonajes
	 * Permite deserializar una lista de personajes
	 * @param rutaa Ruta desde donde se deserializará la lista
	 * @return Lista de personajes deserializada
	 */
	public static List<Personaje> deSerializarListaPersonajes(String rutaa) {
		/**
		 * Lista de personajes a devolver por la función
		 */
		List<Personaje> objeto = null;
		/**
		 * Fichero desde el cual deserializar la lista
		 */
		File origen = new File(rutaa);

		try (FileInputStream entrada = new FileInputStream(origen);
				ObjectInputStream ficheroEntrada = new ObjectInputStream(entrada)) {

			objeto = (List<Personaje>) ficheroEntrada.readObject();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return objeto;
	}
}
