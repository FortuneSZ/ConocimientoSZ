package utils;

import static java.lang.Math.toIntExact;

import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.text.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import javax.xml.transform.Result;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import clases.Episodio;
/**
 * CLase XmlUtils
 * Contiene todos los métodos para procesar o escribir XML
 * @author Fran García Sánchez
 */
public class XmlUtils 
{
	/**
	 * Instanciación de un scanner para leer la opción intruducida por el usuario
	 */
	Scanner sc = new Scanner(System.in);
	String ruta = "resultado.xml"; // Ruta del archivo a verificar
    File archivo = new File(ruta);
	
	/**
	 * Método crearXml
	 * Crea un fichero XML en base a un número de episodio introducido por el usuario
	 * @param numEpisodio id del episodio a buscar en la api
	 * @throws ParseException
	 */
	public void crearXml(int numEpisodio) throws ParseException
	{
		/**
		 * Objeto de tipo episodio leido desde la api
		 */
		Episodio epi = JsonUtils.leerEpisodio(numEpisodio);
		if (epi != null)
		{
			try
			{
				/**
				 * Objeto que crea el objeto capaz de crear contenido XML
				 */
				  DocumentBuilderFactory fábricaCreadorDocumento = DocumentBuilderFactory.newInstance();
				  /**
				   * Objeto capaz de crear contenido XML
				   */
				  DocumentBuilder creadorDocumento = fábricaCreadorDocumento.newDocumentBuilder();
				  /**
				   * Documento creado para almacenar el XML
				   */
				  Document documento = creadorDocumento.newDocument();
				  /**
				   * Elemento raiz del documento
				   */
				  Element elementoRaiz = documento.createElement("EPISODIOS");
				  documento.appendChild(elementoRaiz);
				  /**
				   * Elemento episodio del documento
				   */
				  Element elementoEpisodio = documento.createElement("EPISODIO");
				  elementoRaiz.appendChild(elementoEpisodio);
				  /**
				   * Elemento id del documento
				   */
				  Element elementoId = documento.createElement("ID");
				  elementoEpisodio.appendChild(elementoId);
				  /**
				   * Texto del elemento id del documento
				   */
				  Text textoId = documento.createTextNode(""+epi.getId());
				  elementoId.appendChild(textoId);
				  /**
				   * Elemento nombre del documento
				   */
				  Element elementoNombre = documento.createElement("NOMBRE");
				  elementoEpisodio.appendChild(elementoNombre);
				  /**
				   * Texto del elemento nombre del documento
				   */
				  Text textoNombre = documento.createTextNode(epi.getNombre());
				  elementoNombre.appendChild(textoNombre);
				  /**
				   * Elemento emision del documento
				   */
				  Element elementoEmision = documento.createElement("EMISION");
				  elementoEpisodio.appendChild(elementoEmision);
				  /**
				   * Texto del elemento emisión del documento
				   */
				  Text textoEmision = documento.createTextNode(""+epi.getEmisión());
				  elementoEmision.appendChild(textoEmision);
				  /**
				   * Elemento personajes del documento
				   */
				  Element elementoPersonajes = documento.createElement("PERSONAJES");
				  elementoEpisodio.appendChild(elementoPersonajes);
				  for(int i = 0; i < epi.getPersonajes().size();i++)
				  {
					  /**
					   * Elemento personaje del documento
					   */
					  Element elementoPersonaje = documento.createElement("PERSONAJE");
					  elementoPersonajes.appendChild(elementoPersonaje);
					  /**
					   * Texto del elemento personaje del documento
					   */
					  Text textoPersonaje = documento.createTextNode(""+epi.getPersonajes().get(i));
					  elementoPersonajes.appendChild(textoPersonaje);
				  }
				  /**
				   * Objeto capaz de crear el objeto capaz de transformar los elementos creados en un archivo XML
				   */
				  TransformerFactory fábricaTransformador = TransformerFactory.newInstance();
				  /**
				   * objeto capaz de transformar los elementos creados en un archivo XML
				   */
				  Transformer transformador = fábricaTransformador.newTransformer();
				  transformador.setOutputProperty(OutputKeys.INDENT, "yes");
				  /**
				   * Fichero de origen
				   */
				  Source origen = new DOMSource(documento);
				  /**
				   * Fichero de destino
				   */
				  Result destino = new StreamResult("resultado.xml");
				  transformador.transform(origen, destino);
				  System.out.println();
				  System.out.println("Operación realizada exitosamente");
				  System.out.println();
			}
			catch (ParserConfigurationException ex) 
			{
			    System.out.println("ERROR: No se ha podido crear el generador de documentos XML\n"+ex.getMessage());
			    ex.printStackTrace();
			    System.out.println();
			}
			catch (TransformerConfigurationException ex) 
			{
			    System.out.println("ERROR: No se ha podido crear el transformador del documento XML\n"+ex.getMessage());
			    ex.printStackTrace();
			    System.out.println();
			}
			catch (TransformerException ex) 
			{
			    System.out.println("ERROR: No se ha podido crear la salida del documento XML\n"+ex.getMessage());
			    ex.printStackTrace();
			    System.out.println();
			}
		}
	}
	/**
	 * Método procesarXml
	 * Método que procesa un archivo XML y lo muestra por pantalla
	 */
	public void procesarXml()
	{
		try 
		{
			System.out.println();
			 /**
			  * Objeto capaz de crear el objeto capaz de procesar XML
			  */
		 SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		 /**
		  * objeto capaz de procesar XML
		  */
		 SAXParser saxParser = saxParserFactory.newSAXParser();
		 /**
		  * Manejador del SAXParser
		  */
		 DefaultHandler manejadorEventos = new DefaultHandler()
		 {
			 /**
			  * Variable que almacena la etiqueta actual
			  */
			 String etiquetaActual = "";
			 /**
			  * Variable que almacena el contenido de la etiqueta
			  */
			 String contenido = "";
			 // Método que se llama al encontrar inicio de etiqueta: '<'
			 public void startElement(String uri, String localName,
			 String qName, Attributes attributes)
			 throws SAXException 
			 {
				 etiquetaActual = qName;
			 }
			 // Obtiene los datos entre '<' y '>'
			 public void characters(char ch[], int start, int length)
			 throws SAXException 
			 {
				 contenido = new String(ch, start, length);
			 }
			 // Llamado al encontrar un fin de etiqueta: '>'
			 public void endElement(String uri, String localName, String qName)
			 throws SAXException 
			 {
				 if (etiquetaActual != "") 
				 {
					 System.out.println(" " + etiquetaActual +
					 ": "+ contenido);
					 etiquetaActual = "";
				 }
			 }
		 };
		 // Cuerpo de la función: trata de analizar el fichero deseado
		 // Llamará a startElement(), endElement() y character()
		 if (archivo.exists())
		 {
			 saxParser.parse("resultado.xml", manejadorEventos);
		 }
		 else
		 {
			 System.out.println("El archivo XML no existe");
			 System.out.println();
		 }
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
