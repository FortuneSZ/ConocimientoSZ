package utils;

import static java.lang.Math.toIntExact;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import clases.Episodio;
import clases.Lugar;
import clases.Personaje;

/**
 * Clase JsonUtils
 * contiene todos los métodos relacionados con la lectura o tratamiento de ficheros JSON
 * @author Fran García Sánchez
 */
public class JsonUtils 
{
	/**
	 * Instanciación de un scanner para leer la opción intruducida por el usuario
	 */
	static Scanner sc = new Scanner(System.in);
	/**
	 * Método readUrlList
	 * Este método lee información de una api especificada mediante parámetros y devuelve un objeto de tipo JSONObject con la información leída
	 * @param web Url de la api a la que conectarnos para obtener la información
	 * @return El objeto JSONObject con la información leída de la api
	 */
	public static JSONObject readUrlList(String web) 
	{
		/**
		 * Objeto que será devuelto con la información leída
		 */
		JSONObject jsonobj = null;

		
		try 
		{
			/**
			 * Url de la api a la que conectarnos
			 */
			URL url = new URL(web);
			URLConnection uc = url.openConnection();
			uc.setRequestProperty("User-Agent", "PostmanRuntime/7.20.1");
			uc.connect();
			
			try 
			{
				jsonobj = (JSONObject) new JSONParser().parse(new BufferedReader(new InputStreamReader(uc.getInputStream())));
			} 
			catch (org.json.simple.parser.ParseException e) 
			{
				e.printStackTrace();
			}
				
			return jsonobj;
			
		} 
		catch (Exception e) 
		{
			System.out.println("No se ha podido leer la URL: " + web);
			System.out.println(e.getStackTrace() + " " + e.getCause());
		}
		return jsonobj;
	}
	
	/**
	 * Método obtenerPersonaje
	 * Método que recoge la información de un personaje especificado por el usuario y crea y devuelve un objeto de tipo Personaje
	 * @param numeroPersonaje Id del personaje a buscar en la api
	 * @param personajes Lista de Personajes del programa principal
	 */
	public static void obtenerPersonaje(int numeroPersonaje,List<Personaje> personajes)
	{
		/**
		 * Variable que determina si el personaje a buscar se encuentra ya o no en la lista de Personajes
		 */
		boolean encontrado = false;
		/**
		 * Url de la api a la que nos conectaremos para obtener el personaje
		 */
		String url = "https://rickandmortyapi.com/api/character/" + numeroPersonaje;
		/**
		 * Objeto del personaje buscado
		 * @see Método readUrlList
		 */
		JSONObject per = readUrlList(url);
		/**
		 * Objeto Personaje obtenido en base a la información del objeto JSONObject
		 */
		Personaje personaje = crearPersonaje(per);
		if(personajes.size() != 0)
		{
			for(int i = 0; i < personajes.size();i++)
			{
				if (personaje.getId() == personajes.get(i).getId())
				{
					encontrado = true;
				}
			}
			
			if(encontrado == false)
			{
				personajes.add(personaje);
			}
			else
			{
				System.out.println("Personaje ya presente en la lista");
			}
		}
		else
		{
			personajes.add(personaje);
		}
	}
	
	/**
	 * Método crearPersonaje
	 * Crear un objeto de tipo Personaje en base la información almacenada en un objeto JSONObject recopilada de la api
	 * @param per objeto JSONObject obtenido de la api
	 * @return el personaje buscado por el usuario
	 */
	public static Personaje crearPersonaje(JSONObject per)
	{
		/**
		 * Variable que almacena el id del objeto
		 */
		int id =  toIntExact((long)per.get("id"));
		/**
		 * Variable que almacena el nombre del objeto
		 */
		String nombre = (String) per.get("name");
		/**
		 * Variable que almacena el estado del objeto
		 */
		String estado = (String) per.get("status");
		/**
		 * Variable que almacena la especie del objeto
		 */
		String especie = (String) per.get("species");
		/**
		 * Variable que almacena el tipo del objeto
		 */
		String tipo = (String) per.get("type");
		/**
		 * Variable que almacena el tipo del objeto
		 */
		String genero = (String) per.get("gender");
		/**
		 * Variable que almacena el origen del objeto
		 */
		Lugar origen = getOrigin(per);
		
		/**
		 * Variable que almacena la ubicación del objeto
		 */
		Lugar ubicacion = getLocation(per);
		
		/**
		 * Variable que almacena la imagen del objeto
		 */
		String imagen = (String) per.get("image");
		/**
		 * Variable que almacena los episodios del objeto
		 */
		JSONArray episodios = (JSONArray) per.get("episode");
		
		return new Personaje(id,nombre,estado,especie,tipo,genero,origen,ubicacion,imagen,episodios);
	}
	/**
	 * Método getOrigin
	 * Obtiene el objeto origin del interior del objeto enviado por parámetros
	 * @param per objeto JSONObject obtenido de la api
	 * @return Objeto origen de tipo Lugar en base a los parámetros obtenidos
	 */
	public static Lugar getOrigin(JSONObject per)
	{
		/**
		 * Objeto origen del tipo Lugar a devolver por la función
		 */
		Lugar origen;
		/**
		 * Variable que almacena el objeto obtenido al obtener el origen del objeto
		 */
		JSONObject origenObjeto = (JSONObject) per.get("origin");
		if (origenObjeto != null)
		{
			/**
			 * url del objeto origenObjeto, empleada para obtener los datos de este
			 */
			String urlOrigen = (String) origenObjeto.get("url");
			origenObjeto = readUrlList(urlOrigen);
			if (origenObjeto != null)
			{
				/**
				 * variable que almacena el id del origen
				 */
				int idorigen = toIntExact((long)origenObjeto.get("id"));
				/**
				 * Variable que almacena el nombre del origen
				 */
				String nombreOrigen = (String) origenObjeto.get("name");
				/**
				 * variable que almacena el tipo del origen
				 */
				String type = (String) origenObjeto.get("type");
				/**
				 * Variable que almacena la dimensión del origen
				 */
				String dimension = (String) origenObjeto.get("dimension");
				/**
				 * Variable que almacena los residentes del origen
				 */
				JSONArray residentes = (JSONArray) origenObjeto.get("residents");
				origen = new Lugar(idorigen,nombreOrigen,type,dimension,residentes);
			}
			else
			{
				origen = new Lugar(0,"Unknown","Unknown","Unknown",null);
			}
			
		}
		else
		{
			origen = new Lugar(0,"Unknown","Unknown","Unknown",null);
		}
		return origen;
	}
	/**
	 * Método getLocation
	 * Obtiene el objeto origin del interior del objeto enviado por parámetros
	 * @param per objeto JSONObject obtenido de la api
	 * @return Objeto ubicación de tipo Lugar en base a los parámetros obtenidos
	 */
	public static Lugar getLocation(JSONObject per)
	{
		/**
		 * Variable que almacena el objeto obtenido al obtener el origen del objeto
		 */
		JSONObject UbicacionObjeto = (JSONObject) per.get("location");
		/**
		 * url del objeto origenObjeto, empleada para obtener los datos de este
		 */
		String urlUbicacion = (String) UbicacionObjeto.get("url");
		UbicacionObjeto = readUrlList(urlUbicacion);
		/**
		 * variable que almacena el id de la ubicación
		 */
		int idUbicacion = toIntExact((long)UbicacionObjeto.get("id"));
		/**
		 * Variable que almacena el nombre de la ubicación
		 */
		String nombreUbicacion = (String) UbicacionObjeto.get("name");
		/**
		 * Variable que almacena el tipo de la ubicación
		 */
		String typeUbicacion = (String) UbicacionObjeto.get("type");
		/**
		 * Variable que almacena la dimensión de la ubicación
		 */
		String dimensionUbicacion = (String) UbicacionObjeto.get("dimension");
		/**
		 * Variable que almacena los residentes de la ubicación
		 */
		JSONArray residentesUbicacion = (JSONArray) UbicacionObjeto.get("residents");
		/**
		 * Objeto ubicación del tipo Lugar a devolver por la función
		 */
		Lugar ubicacion = new Lugar(idUbicacion,nombreUbicacion,typeUbicacion,dimensionUbicacion,residentesUbicacion);
		return ubicacion;
	}
	/**
	 * Método localizacionPersonaje
	 * Obtiene un personaje de la api y obtiene y muestra la información básica de su ubicación
	 * @param numeroPersonaje id del personaje a buscar en la api
	 */
	public static void localizacionPersonaje(int numeroPersonaje)
	{
		try
		{
			/**
			 * Url de la api a la que nos conectaremos para obtener el personaje
			 */
			String url = "https://rickandmortyapi.com/api/character/" + numeroPersonaje;
			/**
			 * Objeto del personaje buscado
			 * @see Método readUrlList
			 */
			JSONObject per = readUrlList(url);
			/**
			 * Objeto lugar obtenido del objeto del personaje
			 */
			Lugar lugar = getLocation(per);
			System.out.println("El personaje " + per.get("name") + " se encuentra en " + lugar.getNombre() +", de tipo " + lugar.getTipo() +
					", situada en la dimensión " + lugar.getDimension());
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println("Número de personaje no válido");
		}
		
	}
	/**
	 * Método crearEpisodio
	 * Crear un objeto de tipo episodio en base a un objeto JSONObject obtenido de la api
	 * @param ep objeto JSONObject obtenido de la api
	 * @return Objeto de tipo Episodio
	 * @throws ParseException
	 */
	public static Episodio crearEpisodio(JSONObject ep) throws ParseException
	{
		/**
		 * Variable que almacena el id del objeto
		 */
		int id =  toIntExact((long)ep.get("id")) ;
		/**
		 * Variable que almacena el nombre del objeto
		 */
		String nombre = (String) ep.get("name");
		/**
		 * Variable que almacena el formato de la fecha
		 */
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		/**
		 * Variable que almacena la fecha de emisión del objeto
		 */
		Date emision =  format.parse((String) ep.get("air_date"));
		/**
		 * Variable que almacena el código de episodio del objeto
		 */
		String episodio = (String) ep.get("episode");
		/**
		 * variable que almacena los personajes del objeto
		 */
		JSONArray personajes = (JSONArray) ep.get("characters");
		/**
		 * Objeto de tipo Episodio que devuelve el método
		 */
		return new Episodio(id,nombre,emision,episodio,personajes);
	}
	/**
	 * Método leerEpisodio
	 * Lee un episodio de la api en base al número mandando por el usuario, lo procesa y devuelve un objeto de tipo Episodio
	 * @param numEpisodio id del episodio a buscar en la api
	 * @return Objeto de tipo Episodio
	 * @throws ParseException
	 */
	public static Episodio leerEpisodio(int numEpisodio) throws ParseException
	{
		/**
		 * Url del episodio a buscar en la api
		 */
		String url = "https://rickandmortyapi.com/api/episode/" + numEpisodio;
		/**
		 * Objeto leido de la api
		 */
		try
		{
			JSONObject ep = JsonUtils.readUrlList(url);
			/**
			 * Objeto de tipo Episodio que devuelve el método
			 */
			Episodio episodio = crearEpisodio(ep);
			return episodio;
		}
		catch(Exception e)
		{
			System.out.println("Número de episodio no válido");
			System.out.println();
			return null;
		}
		
	}
}
