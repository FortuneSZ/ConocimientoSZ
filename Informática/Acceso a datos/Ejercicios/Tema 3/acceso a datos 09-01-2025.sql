CREATE DOMAIN reglaPrecio AS NUMERIC(7,2)
CHECK (VALUE >= 0); 

ALTER TABLE evento
ALTER COLUMN precio Type reglaPrecio;

CREATE DOMAIN nombreSinComas AS varchar(50)
CHECK (VALUE NOT LIKE '%,%');

CREATE TYPE ubicacion AS (nombre nombreSinComas,latitud FLOAT,longitud FLOAT);

ALTER TABLE evento
ADD Ubicacion ubicacion;

INSERT INTO evento(nombre,descripcion,precio,fecha,ubicacion.nombre,ubicacion.latitud,ubicacion.longitud) VALUES('Evento en Madrid','Evento en Madrid',25,'2025-01-07','Madrid',456,789);

CREATE TYPE diaSemana
AS ENUM ( 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado',
'Domingo'); 

ALTER TABLE evento
ADD Dia diaSemana;

INSERT INTO evento(nombre,descripcion,precio,fecha,ubicacion.nombre,ubicacion.latitud,ubicacion.longitud,dia) VALUES('Evento en Murcia','Evento en Murcia',15,'2025-01-08','Murcia',300,478,'Miércoles');

CREATE OR REPLACE FUNCTION cogerDia(fecha_evento DATE)
RETURNS diaSemana AS $$
DECLARE
    diaTexto TEXT;
BEGIN
    diaTexto := TO_CHAR(fecha_evento, 'Day');
	diaTexto := TRIM(BOTH FROM INITCAP(diaTexto));
    CASE diaTexto
        WHEN 'Monday' THEN RETURN 'Lunes';
        WHEN 'Tuesday' THEN RETURN 'Martes';
        WHEN 'Wednesday' THEN RETURN 'Miércoles';
        WHEN 'Thursday' THEN RETURN 'Jueves';
        WHEN 'Friday' THEN RETURN 'Viernes';
        WHEN 'Saturday' THEN RETURN 'Sábado';
        WHEN 'Sunday' THEN RETURN 'Domingo';
        ELSE
            RAISE EXCEPTION 'Día no válido: %', diaTexto;
    END CASE;
END;
$$ LANGUAGE plpgsql;

SELECT * from evento;

ALTER TABLE evento
ADD COLUMN etiqueta TEXT[];

UPDATE evento
SET etiqueta = '{"Etiqueta 1","Etiqueta 2"}'
WHERE nombre LIKE '%Evento%' OR NOMBRE LIKE '%evento%';

UPDATE evento 
SET etiqueta[3] = 'Etiqueta 3'
WHERE nombre LIKE '%Evento%' OR NOMBRE LIKE '%evento%';

SELECT * from evento;

