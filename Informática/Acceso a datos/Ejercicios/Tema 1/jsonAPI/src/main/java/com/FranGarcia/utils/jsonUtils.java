package com.FranGarcia.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class jsonUtils 
{
	public static void apiModelos() 
	{

		 Object obj;
		 try 
		 {
			 // parseado el fichero "profesor.json"
			 obj = new JSONParser().parse(new FileReader("profesor.json"));
			 // casteando obj a JSONObject
			 JSONObject jo = (JSONObject) obj;
			 // cogiendo el nombre y el apellido
			 String nombre = (String) jo.get("nombre");
			 String apellido = (String) jo.get("apellido");
			 System.out.println(nombre);
			 System.out.println(apellido);
			 // cogiendo la edad como long
			 long edad = (long) jo.get("edad");
			 System.out.println(edad);
			 // cogiendo direccion
			 Map domicilio = ((Map) jo.get("domicilio"));
			 // iterando direccion Map
			 Iterator<Map.Entry> itr1 = domicilio.entrySet().iterator();
			 while (itr1.hasNext()) 
			 {
				 Map.Entry pareja = itr1.next();
				 System.out.println(pareja.getKey() + " : " + pareja.getValue());
			 } 
			// cogiendo números de teléfonos
			 JSONArray ja = (JSONArray) jo.get("numerosTelefonos");
			 // iterando números de teléfonos
			 Iterator itr2 = ja.iterator();
			 while (itr2.hasNext()) 
			 {
				 itr1 = ((Map) itr2.next()).entrySet().iterator();
				while (itr1.hasNext()) 
				{
				 Map.Entry pareja = itr1.next();
				 System.out.println(pareja.getKey() + " : " + pareja.getValue());
				}
			 }
		 } 
		 catch (FileNotFoundException e) 
		 {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 } 
		 catch (IOException e) 
		 {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 } 
		 catch (ParseException e) 
		 {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
	}
	
	public static JSONObject readUrlList(String web) {
			
			JSONObject jsonobj = null;
	
			
			try {
				// USAREMOS UN OBJETO URL PARA VALIDAR EL String RECIBIDO
				URL url = new URL(web);
				// URLConnection NBOS PERMITIRÁ INTERACTUAR CON LA URL
				URLConnection uc = url.openConnection();
				// POR EJEMPLO PASARLE CABECERAS DE PETICIÓN 
				// PARA ESPECIFICAR CLIENTE (Postman)
				uc.setRequestProperty("User-Agent", "PostmanRuntime/7.20.1");
				// VERIFICA LA CONEXIÓN CON LA URL
				uc.connect();
				
				// LEEREMOS CON UN BufferedReader
				// COMO HEMOS APRENDIDO PODEMOS COLOCARLO EN UN try-with-resources PARA QUE SE CIERRE
				try {
						jsonobj = (JSONObject) new JSONParser().parse(new BufferedReader(new InputStreamReader(uc.getInputStream())));
					} 
					catch (org.json.simple.parser.ParseException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				
				// System.out.println(lineas);
				return jsonobj;
				
			} 
			catch (Exception e) 
			{
				System.out.println("No se ha podido leer la URL: " + web);
				System.out.println(e.getStackTrace() + " " + e.getCause());
			}
			return jsonobj;
		}
	
	public static JSONArray readUrlArray(String web) {
		
		JSONObject jsonobj = readUrlList(web);
		JSONArray characters = null;
				
		characters = (JSONArray) jsonobj.get("results");

		return characters;
	}

	
	public static JSONObject readUrlListAuth(String web, String token) {
		
		JSONObject jsonobj = null;
		try 
		{
			URL url = new URL(web);
			URLConnection uc = url.openConnection();
			
			// EN ESTA CABECERA NECESITAREMOS ANYADIR UN TOKEN DE AUTH
			uc.setRequestProperty("User-Agent", "PostmanRuntime/7.20.1");
			uc.setRequestProperty("X-Auth-Token", token);
			
			uc.connect();
			
			try 
			{
				jsonobj = (JSONObject) new JSONParser().parse(new BufferedReader(new InputStreamReader(uc.getInputStream())));
			} 
			catch (org.json.simple.parser.ParseException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		// System.out.println(lineas);
		return jsonobj;
		
		} 
		catch (Exception e) 
		{
			System.out.println("No se ha podido leer la URL: " + web);
			System.out.println(e.getStackTrace() + " " + e.getCause());
		}
		return jsonobj;
	}
}
