package com.FranGarcia.jsonAPI;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.FranGarcia.utils.jsonUtils;

/**
 * Hello world!
 */
public class App 
{
	public static void getCharacter(JSONObject character)
	{
		String homeworldText = (String) character.get("homeworld");
        JSONObject film;
        JSONObject specie;
        JSONObject starship;
        JSONObject homeworld = jsonUtils.readUrlList(homeworldText);
        JSONArray films = (JSONArray) character.get("films");
        JSONArray species = (JSONArray) character.get("species");
        JSONArray starships = (JSONArray) character.get("starships");
        
        System.out.println("Name: " + character.get("name"));
        System.out.println("height: " + character.get("height"));
        System.out.println("mass: " + character.get("mass"));
        System.out.println("hair color: " + character.get("hair_color"));
        System.out.println("skin color: " + character.get("skin_color"));
        System.out.println("eye color: " + character.get("eye_color"));
        System.out.println("birth year: " + character.get("birth_year"));
        System.out.println("gender: " + character.get("gender"));
        System.out.println("homeworld: " + homeworld.get("name"));
        System.out.println();

        System.out.println("films:");
        for(int i = 0; i < films.size(); i++)
        {
        	film = jsonUtils.readUrlList((String) films.get(i));
        	System.out.println(film.get("title"));
        	
        }
        System.out.println();
        if (species.size() >= 1)
        {
        	System.out.println("species:");
        	for(int i = 0; i < species.size(); i++)
            {
        		specie = jsonUtils.readUrlList((String) species.get(i));
            	System.out.println(specie.get("name"));
            	
            }
        }
        System.out.println();
        if (starships.size() >= 1)
        {
        	System.out.println("starships:");
        	for(int i = 0; i < starships.size(); i++)
            {
        		starship = jsonUtils.readUrlList((String) starships.get(i));
            	System.out.println(starship.get("name"));
            	
            }
        }
        System.out.println();

	}
    public static void main(String[] args) 
    {
    	String footballDataApiKey = "bfec7f5bd0ce4f0881e6305f5b1afaf3";
        jsonUtils js = new jsonUtils();
        JSONObject character = jsonUtils.readUrlList("https://swapi.dev/api/people/4/?format=json");
        JSONArray characters = jsonUtils.readUrlArray("https://swapi.dev/api/people/?page=1");
        String url = "https://swapi.dev/api/people/?page=1";
        int page = 1;
        do
        {
        	for(int i = 0; i < characters.size();i++)
            {
            	getCharacter((JSONObject) characters.get(i));
            	System.out.println("-----------------------");
            }
        	page++;
        	if (page < 10)
        	{
        		url = "https://swapi.dev/api/people/?page=" + page;
            	characters = jsonUtils.readUrlArray(url);
        	}
        }
        while (page < 10);
        System.out.println(jsonUtils.readUrlListAuth("https://api.football-data.org/v4/competitions/SA/scorers"
       		 , footballDataApiKey));
        
        /*System.out.println(jsonUtils.readUrlList("https://swapi.dev/api/people/4/?format=json"));
        System.out.println(jsonUtils.readUrlListAuth("https://api.football-data.org/v4/competitions/SA/scorers", "SET_API_KEY"));*/
    }
}
