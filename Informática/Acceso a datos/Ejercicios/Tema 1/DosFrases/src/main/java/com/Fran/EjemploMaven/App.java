package com.Fran.EjemploMaven;

import java.io.*; 
import java.io.PrintWriter; 
import java.io.IOException;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner sc = new Scanner(System.in);
        String Frase1 = sc.nextLine();
        String Frase2 = sc.nextLine();
        PrintWriter printWriter = null;
        try
        {
        	printWriter = new PrintWriter(new BufferedWriter(new FileWriter("dosFrases.txt",true)));
        	printWriter.println(Frase1);
        	printWriter.println(Frase2);
        }
        catch (IOException errorDeFichero)
        {
        	errorDeFichero.printStackTrace();

        }
        finally 
        {             
        	if ( printWriter != null )  
        	{                 
        		printWriter.close();             
        	} 
        }
    }
}
