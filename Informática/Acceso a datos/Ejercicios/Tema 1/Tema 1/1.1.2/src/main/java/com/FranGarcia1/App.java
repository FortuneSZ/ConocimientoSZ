package com.FranGarcia1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Hello world!
 */
public class App 
{
    public static void main(String[] args) throws IOException 
    {
    	Scanner sc = new Scanner(System.in);
    	String frase;
    	PrintWriter print = null;
    	ArrayList<String> frases = new ArrayList<String>();
    	if (Files.exists(Paths.get("fichero.txt")))
    	{
    		Files.delete(Paths.get("fichero.txt"));
    	}
    	System.out.println("Introduce frases");
    	do
    	{
    		frase = sc.nextLine();
    		frases.add(frase);
    	}
    	while(frase != "");
    	
    	 try 
    	 {
    		 print = new PrintWriter(new BufferedWriter(
    	 new FileWriter("fichero.txt", true)));
    	 System.out.println(frases.size());
	        for(int i = 0; i < frases.size()-1;i++)
	        {
	        	System.out.println(frases.get(i));
	        	String a = frases.get(i);
	        	print.println(a);
	        }
	        
	    }
		catch(Exception e)
		{
			System.out.println("error");
		}
    	 finally 
    	 {
    		 if ( print != null ) 
    	     {
    			 print.close();
    	     }
    	 }
        
    }
}
