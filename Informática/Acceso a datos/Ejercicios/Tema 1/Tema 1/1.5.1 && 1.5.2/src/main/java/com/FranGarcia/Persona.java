package com.FranGarcia;

import java.io.Serializable;

public class Persona implements Serializable
{
	private String nombre;
	private String email;
	private String nacimiento;
	
	public Persona(String nombre,String email,String nacimiento)
	{
		this.nombre = nombre;
		this.email = email;
		this.nacimiento = nacimiento;
	}
	
	public void escribir() 
	{
		 System.out.println(this.toString());
	} 

	
	@Override
    public String toString()
    {
        return "nombre: " + this.nombre + ", apellidos: " + this.email + ", nota: " + this.nacimiento;
    }
}
