package com.FranGarcia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App 
{
    public static void main(String[] args) throws IOException, ClassNotFoundException 
    {
    	List<Persona> personas = new ArrayList();
    	System.out.println("Serialización");
    	System.out.println();
        File fichero = new File("personas.dat");
		 FileOutputStream ficheroSalida =
		 new FileOutputStream(fichero);
		 ObjectOutputStream ficheroObjetos =
		 new ObjectOutputStream(ficheroSalida);
		 Persona a = new Persona("Paco", "paco@gmail.com","07/08/2001");
		 personas.add(a);
		 a = new Persona("Sara", "sargormar@hotmail.com","21/06/1998");
		 personas.add(a);
		 a = new Persona("Marcos", "marcos2@hotmail.com","13/02/1999");
		 personas.add(a);
		 ficheroObjetos.writeObject(personas);
		 ficheroObjetos.close();
		 
		 System.out.println("Deserialización");
		 
		 File fichero2 = new File("personas.dat");
		 FileInputStream ficheroSalida2 =
		 new FileInputStream(fichero2);
		 ObjectInputStream ficheroObjetos2 =
		 new ObjectInputStream(ficheroSalida2);
		 List<Persona> c = (List<Persona>) ficheroObjetos2.readObject();
		 for (int i = 0; i < c.size();i++)
		 {
			 System.out.println(c.get(i));
		 }
		 
		 ficheroObjetos2.close();

		 
		 
    }
}
