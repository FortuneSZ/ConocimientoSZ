package com.FranGarcia1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Hello world!
 */
public class App 
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce dos frases");
        String frase1 = sc.nextLine();
        String frase2 = sc.nextLine();
        
        PrintWriter printWriter = null;
        try {
        printWriter = new PrintWriter(new BufferedWriter(
        new FileWriter("fichero.txt", true)));
        printWriter.println (frase1);
        printWriter.println (frase2);
        }
        catch (IOException e) {
        e.printStackTrace();
        }
        finally {
        if ( printWriter != null ) 
        {
        	printWriter.close();
        }
        }
    }
}
