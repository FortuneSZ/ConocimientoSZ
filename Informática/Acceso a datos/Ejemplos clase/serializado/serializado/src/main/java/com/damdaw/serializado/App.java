package com.damdaw.serializado;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.damdaw.entities.Alumno;
import com.damdaw.entities.Ciudad;
import com.damdaw.utils.serializarUtils;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		String ruta = new String();

		ruta = "./resources/";

		// v1.0
		Alumno aluSer = new Alumno("2º DAM", "Jose María García", 7.8);
		serializarUtils.serializarAlumno(aluSer, ruta);

		// UN OBJETO SERIALIZADO EN UN FICHERO NO SE LEE COMO TEXTO PLANO, LO DESERIALIZAMOS
		Alumno aluDes = serializarUtils.deSerializarAlumno(ruta + "ejemplo.obj");
		System.out.println(aluDes);

		// v1.1
		List<Alumno> alusSer = new ArrayList<Alumno>();
		alusSer.add(new Alumno("2º DAM", "Jose María García", 7.8));
		alusSer.add(new Alumno("1º DAM", "Ma Dolores Ruiz", 7.9));
		alusSer.add(new Alumno("2º ASIR", "Alex Houston", 9.8));

		serializarUtils.serializarListaAlumnos(alusSer, ruta);

		// HAY QUE CREAR UN NUEVO OBJETO PORQUE NO MANEJAMOS EL uid
		List<Alumno> alusDes = new ArrayList<Alumno>();

		alusDes = serializarUtils.deSerializarListaAlumnos(ruta + "ejemplo.obj");
		alusDes.forEach(alu -> System.out.println(alu));

		// v2.0 CON OBJETOS GENÉRICOS
		serializarUtils.serializarObjeto(alusSer, ruta);

		List<Alumno> alusObj = new ArrayList<Alumno>();

		alusObj = serializarUtils.deSerializarObjeto(ruta + "ejemplo.obj");
		alusObj.forEach(alu -> System.out.println(alu));

		// AHORA PODRÍAMOS USAR OTROS OBJETOS, COMO CIUDADES
		List<Ciudad> ciudades = new ArrayList<Ciudad>();
		ciudades.add(new Ciudad("A", "Alicante"));
		ciudades.add(new Ciudad("Gr", "Granada"));

		serializarUtils.serializarObjeto(ciudades, ruta);

		List<Ciudad> ciudadesObj = new ArrayList<Ciudad>();
		ciudadesObj = serializarUtils.deSerializarObjeto(ruta + "ejemplo.obj");
		ciudadesObj.forEach(alu -> System.out.println(alu));
	}
}
