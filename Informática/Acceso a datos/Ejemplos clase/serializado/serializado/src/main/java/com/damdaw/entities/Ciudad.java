package com.damdaw.entities;

import java.io.Serializable;

public class Ciudad implements Serializable {

	private String codPostal;
	private String nombre;
	
	/**
	 * 
	 */
	public Ciudad() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param codPostal
	 * @param nombre
	 */
	public Ciudad(String codPostal, String nombre) {
		super();
		this.codPostal = codPostal;
		this.nombre = nombre;
	}

	/**
	 * @return the codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}

	/**
	 * @param codPostal the codPostal to set
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Ciudad [codPostal=" + codPostal + ", nombre=" + nombre + "]";
	}
	
	

}
