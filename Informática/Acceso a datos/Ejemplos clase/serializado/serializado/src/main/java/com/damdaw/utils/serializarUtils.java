package com.damdaw.utils;

import java.io.*;
import java.util.List;

import com.damdaw.entities.Alumno;

public class serializarUtils {

	/**
	 * v1.0 SERIALIZAR/DESERIALIZAR 1 OBJETO 
	 * SERIALIZAR 1 ALUMNO PASADO COMO PARÁMETRO A UN ARCHIVO .obj
	 * 
	 * @param objEntrada EL Alumno QUE QUEREMOS SERIALIZAR
	 * @param rutaa      LA RUTA DONDE SE ALMACENARÁ EL .obj RESULTADO
	 */
	public static void serializarAlumno(Alumno objEntrada, String rutaa) {

		// CAMBIO LA EXTENSIÓN POR NO CONFUNDIR. NO SE PUEDE LEER COMO TEXTO
		File destino = new File(rutaa + "ejemplo.obj");

		// UTILIZO UN TRY WITH RESOURCES PARA AHORRARME CERRAR LOS Stream
		try (FileOutputStream salida = new FileOutputStream(destino);
				ObjectOutputStream ficheroSalida = new ObjectOutputStream(salida)) {

			ficheroSalida.writeObject(objEntrada);
			// PARA DEBUG QUE ESTOY LEYENDO LO QUE QUIERO
			// System.out.println("Objeto serializado: " + objEntrada);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * v1.0 SERIALIZAR/DESERIALIZAR 1 OBJETO 
	 * DESERIALIZAR 1 ALUMNO DESDE UN ARCHIVO SERIALIZADO PASADO COMO PARÁMETRO 
	 * PIDE ANYADIR EL SuppressWarnings EN EL readObject POR EL CASTEO SUCIO
	 * 
	 * @param rutaa LA RUTA COMPLETA DONDE SE ENCUENTRA EL ARCHIVO SERIALIZADO
	 * @return DEVUELVE UN Alumno LEÍDO DEL FICHERO
	 */
	@SuppressWarnings("unchecked")
	public static Alumno deSerializarAlumno(String rutaa) {

		// UTILIZO UN OBJETO GENÉRICO
		Alumno objeto = null;
		File origen = new File(rutaa);

		try (FileInputStream entrada = new FileInputStream(origen);
				ObjectInputStream ficheroEntrada = new ObjectInputStream(entrada)) {

			// SE CASTEA A TIPO T EN SUCIO
			objeto = (Alumno) ficheroEntrada.readObject();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return objeto;
	}

	/**
	 * v1.1 SERIALIZAR/DESERIALIZAR LISTAS 
	 * SERIALIZAR LISTA DE ALUMNOS PASADA COMO PARÁMETRO A UN ARCHIVO .obj
	 * 
	 * @param objEntrada LA LISTA CON LOS ALUMNOS QUE QUEREMOS SERIALIZAR
	 * @param rutaa      LA RUTA DONDE SE ALMACENARÁ EL .obj RESULTADO
	 */
	public static void serializarListaAlumnos(List<Alumno> objEntrada, String rutaa) {

		File destino = new File(rutaa + "ejemplo.obj");

		try (FileOutputStream salida = new FileOutputStream(destino);
				ObjectOutputStream ficheroSalida = new ObjectOutputStream(salida)) {

			ficheroSalida.writeObject(objEntrada);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * v1.1 SERIALIZAR/DESERIALIZAR LISTAS 
	 * DESERIALIZAR LISTAS DE ALUMNOS DESDE UN ARCHIVO SERIALIZADO PASADO COMO PARÁMETRO
	 * 
	 * @param rutaa LA RUTA COMPLETA DONDE SE ENCUENTRA EL ARCHIVO SERIALIZADO
	 * @return DEVUELVE UNA LISTA DE ALUMNOS LEÍDOS DEL FICHERO
	 */
	@SuppressWarnings("unchecked")
	public static List<Alumno> deSerializarListaAlumnos(String rutaa) {

		List<Alumno> objeto = null;
		File origen = new File(rutaa);

		try (FileInputStream entrada = new FileInputStream(origen);
				ObjectInputStream ficheroEntrada = new ObjectInputStream(entrada)) {

			objeto = (List<Alumno>) ficheroEntrada.readObject();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return objeto;
	}

	/**
	 * v2.0 SERIALIZAR/DESERIALIZAR OBJETOS GENÉRICOS 
	 * SERIALIZAR UN OBJETO GENÉRICO PASADO COMO PARÁMETRO A UN ARCHIVO .obj 
	 * PODEMOS USAR CUALQUIER OBJETO, INCLUÍDAS LAS LISTAS
	 * 
	 * @param <T>        LE ESPECIFICAMOS EL TIPO <T> PARA PODER USAR OBJETOS GENÉRICOS
	 * @param objEntrada EL OBJETO QUE QUEREMOS SERIALIZAR. PUEDE SER 1 O UNA LISTA
	 * @param rutaa      LA RUTA DONDE SE ALMACENARÁ EL .obj RESULTADO
	 */
	public static <T> void serializarObjeto(T objEntrada, String rutaa) {

		File destino = new File(rutaa + "ejemplo.obj");

		try (FileOutputStream salida = new FileOutputStream(destino);
				ObjectOutputStream ficheroSalida = new ObjectOutputStream(salida)) {

			ficheroSalida.writeObject(objEntrada);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * v2.0 SERIALIZAR/DESERIALIZAR OBJETOS GENÉRICOS 
	 * DESERIALIZAR UN OBJETO GENÉRICO DESDE UN ARCHIVO SERIALIZADO PASADO COMO PARÁMETRO 
	 * PODEMOS USAR CUALQUIER OBJETO, INCLUÍDAS LAS LISTAS
	 * 
	 * @param <T>   LE ESPECIFICAMOS EL TIPO <T> PARA PODER USAR OBJETOS GENÉRICOS
	 * @param rutaa LA RUTA COMPLETA DONDE SE ENCUENTRA EL ARCHIVO SERIALIZADO
	 * @return DEVUELVE UN OBJETO GENÉRICO COMO RESULTADO. PUEDE SER 1 SOLO O UNA LISTA
	 */
	@SuppressWarnings("unchecked")
	public static <T> T deSerializarObjeto(String rutaa) {

		// UTILIZO UN OBJETO GENÉRICO
		T objeto = null;
		File origen = new File(rutaa);

		try (FileInputStream entrada = new FileInputStream(origen);
				ObjectInputStream ficheroEntrada = new ObjectInputStream(entrada)) {

			// SE CASTEA A TIPO T EN SUCIO
			objeto = (T) ficheroEntrada.readObject();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return objeto;
	}
}
