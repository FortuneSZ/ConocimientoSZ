package com.damdaw.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

public class ApiUtils {

	/**
	 * LEER DATOS DE UNA API SIN NECESIDAD DE TOKEN Y DEVOLVER UNA LISTA CON LAS LÍNEAS LEÍDAS
	 * 
	 * @param web URL DE LA API QUE QUEREMOS CONSULTAR CON LOS PARÁMETROS QUE NECESITEMOS
	 * @return DEVOLVEMOS UNA List<String> PARA ALMACENAR LO LEIDO
	 */
	public static List<String> readUrlList(String web) {
		
		List<String> lineas = null;
		
		try {
			// USAREMOS UN OBJETO URL PARA VALIDAR EL String RECIBIDO
			URL url = new URL(web);
			// URLConnection NBOS PERMITIRÁ INTERACTUAR CON LA URL
			URLConnection uc = url.openConnection();
			// POR EJEMPLO PASARLE CABECERAS DE PETICIÓN 
			// PARA ESPECIFICAR CLIENTE (Postman)
			uc.setRequestProperty("User-Agent", "PostmanRuntime/7.20.1");
			// VERIFICA LA CONEXIÓN CON LA URL
			uc.connect();
			
			// LEEREMOS CON UN BufferedReader
			// COMO HEMOS APRENDIDO PODEMOS COLOCARLO EN UN try-with-resources PARA QUE SE CIERRE
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream(), StandardCharsets.UTF_8))) {
	            // CONVERTIMOS EL Stream A STRING CON lines Y A LISTA CON collect
				lineas = reader.lines().collect(Collectors.toList());
	        }
			
			// System.out.println(lineas);
			return lineas;
			
		} catch (Exception e) {
			System.out.println("No se ha podido leer la URL: " + web);
			System.out.println(e.getStackTrace() + " " + e.getCause());
		}
		return lineas;
	}
	
	/**
	 * LEER DATOS DE UNA API QUE REQUIERE AUTENTICACIÓN Y DEVOLVER UNA LISTA CON LAS LÍNEAS LEÍDAS
	 * 
	 * @param web URL DE LA API QUE QUEREMOS CONSULTAR CON LOS PARÁMETROS QUE NECESITEMOS
	 * @param token TOKEN DE AUTENTICACIÓN DE LA API ÚNICO Y PERSONAL
	 * @return DEVOLVEMOS UNA List<String> PARA ALMACENAR LO LEIDO
	 */
	public static List<String> readUrlListAuth(String web, String token) {
		
		List<String> lineas = null;
		try {
			URL url = new URL(web);
			URLConnection uc = url.openConnection();
			
			// EN ESTA CABECERA NECESITAREMOS ANYADIR UN TOKEN DE AUTH
			uc.setRequestProperty("User-Agent", "PostmanRuntime/7.20.1");
			uc.setRequestProperty("X-Auth-Token", token);
			
			uc.connect();
			
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream(), StandardCharsets.UTF_8))) {
	            lineas = reader.lines().collect(Collectors.toList());
	        }
			
			// System.out.println(lineas);
			return lineas;
		} catch (Exception e) {
			System.out.println("No se ha podido la leer la URL: " + web);
			System.out.println(e.getStackTrace() + " " + e.getCause());
		}
		return lineas;
	}

}
