package com.damdaw.getAPI;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.damdaw.utils.ApiUtils;

/**
 * GET DATA FROM APIs
 */
public class App {
    public static void main(String[] args) {
    	
		System.out.println(ApiUtils.readUrlList("https://swapi.dev/api/people/4/?format=json"));
		System.out.println(ApiUtils.readUrlList("https://swapi.dev/api/people/?format=json"));

		System.out.println(ApiUtils.readUrlListAuth("https://api.football-data.org/v4/competitions/SA/scorers"
		 , "SET_API_KEY"));

		
		 // FILTRAR A MANO (NO RECOMENDADO) 
		List<String> personaje =
		  ApiUtils.readUrlList("https://swapi.dev/api/people/4/?format=json");
		  personaje.forEach(tag -> { 
			  String name = null; 
			  String birthYear = null;
		  String url = null;
		  
		  // Extraer name 
		  Pattern namePattern =
		  Pattern.compile("\"name\":\"([^\"]+)\""); 
		  Matcher nameMatcher =
		  namePattern.matcher(tag); if (nameMatcher.find()) { name = nameMatcher.group(1); }
		  
		  // Extraer birth_year 
		  Pattern birthYearPattern =
		  Pattern.compile("\"birth_year\":\"([^\"]+)\""); 
		  Matcher birthYearMatcher =
		  birthYearPattern.matcher(tag); 
		  if (birthYearMatcher.find()) { birthYear = birthYearMatcher.group(1); }
		  
		  // Extraer url 
		  Pattern urlPattern = Pattern.compile("\"url\":\"([^\"]+)\"");
		  Matcher urlMatcher = urlPattern.matcher(tag); 
		  if (urlMatcher.find()) { url = urlMatcher.group(1); }
		  
		  System.out.println("Name: " + name); System.out.println("Birth Year: " +
		  birthYear); System.out.println("URL: " + url); 
		  });
    }
}
