package com.damdaw.ficherosJ8;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.damdaw.entities.Alumno;
import com.damdaw.utils.J8FilesUtils;

/**
 * MANAGING FILES IN JAVA 8
 *
 */
public class App {
	public static void main(String[] args) throws IOException {

		String ruta = new String();

		ruta = "./resources/";

		// v1.0
		J8FilesUtils.leerFicheroJ8SinErrores(ruta + "Maslineas.txt");
		// v1.1
		J8FilesUtils.leerFicheroJ8TryClasico(ruta + "Maslineas.txt");
		// v1.2
		J8FilesUtils.leerFicheroJ8TryResources(ruta + "Maslineas.txt");
		// v1.3
		J8FilesUtils.leerFicheroJ8readAllLines(ruta + "Maslineas.txt");

		/* v1.4 TRASTEAR CON LAMBDAS
		 * VAMOS A LEER EL FICHERO CON ALUMNOS,
		 * FILTRAR LOS QUE SEAN DE 1º DAM 
		 * Y MOSTRARLO POR PANTALLA USANDO TODO EL RATO LAMBDAS
		 */
		J8FilesUtils.getLineasFichero(ruta+"alumnos.txt") 
		// NECESITAMOS stream() PARA PODER FILTRAR 
			.stream()
			.filter(alu -> alu.contains("1º DAM")) 
			.forEach(alu -> System.out.println(alu));
		
		// SI QUISIÉRAMOS ALMACENARLO EN UNA VARIABLE ACTUARÍAMOS ASÍ 
		List<String> alumnos1o = J8FilesUtils.getLineasFichero(ruta+"alumnos.txt").stream()
				.filter(alu -> alu.contains("1º DAM")) 
		  // ES UN METODO void POR LO QUE PARA ALMACENAR EL RESULTADO NECESITAMOS collect Y COMO ES UNA LISTA  Collectors.toList() 
				.collect(Collectors.toList()); 
		// AHORA QUE LO TENEMOS ALMACENADO PODEMOS REPRESENTARLO EN PANTALLA O LO QUE NECESITEMOS
		alumnos1o.forEach(alu -> System.out.println(alu));
		
		/* TRASTEAR CON LAMBDAS v2.0
		 * AHORA USAREMOS NUESTRA CLASE Alumnos PARA ALMACENAR LOS DATOS EN OBJETOS
		 */
		List<String> alumnos1er = J8FilesUtils.getLineasFichero(ruta+"alumnos.txt").stream()
				.filter(alu -> alu.contains("1º DAM"))
				.collect(Collectors.toList());
				  
		//NOS CREAMOS NUESTRA LISTA DE TIPO Alumno 
		List<Alumno> alumnos = new ArrayList<Alumno>();
		// AHORA QUE LO TENEMOS ALMACENADO PODEMOS GUARDARLO EN NUESTRA CLASE
		alumnos1er 
		// DE NUEVO USAMOS stream PARA ACTUAR SOBRE LA VARIABLE 
			.stream()
			.forEach(alu -> alumnos.add(new Alumno( 
				// DEBEREMOS DIFERENCIAR CADA LINEA POR UN SEPARADOR Y DETERMINAR LA POSICION 
				// LA POSICION ES IMPORTANTE SEGÚN MONTEMOS EL CONSTRUCTOR 
				alu.split(";")[0], alu.split(";")[1], 
				// LA NOTA ES DE TIPO Double POR LO QUE NO PODEMOS ALMACENAR EL String SIN MAS 
				// PODEMOS SIMPLEMENTE CONVERTIR LAS , EN . PARA RESPETAR EL FORMATO POR DEFECTO
				Double.parseDouble(alu.split(";")[2].replace(',', '.'))) ));
				// SI QUISIÉRAMOS USAR UN FORMATO CONCRETO EN VEZ DE SIMPLEMENTE CONVERTIR LAS ,
				//(NumberFormat.getInstance(Locale.GERMANY).parse(alu.split(";")[2])).doubleValue()));
				 
		alumnos.forEach(alu->System.out.println(alu));
		
		// v2.0
		J8FilesUtils.escribirFicheroJ8(ruta);
		// v2.1
		J8FilesUtils.escribirFicheroJ8Files(ruta);
		// v2.2
		J8FilesUtils.duplicarFicheroJ8(ruta, "Maslineas.txt");
	}
}
