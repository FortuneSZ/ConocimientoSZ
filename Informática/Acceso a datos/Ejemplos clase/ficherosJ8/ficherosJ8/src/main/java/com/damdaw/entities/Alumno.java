package com.damdaw.entities;

import java.io.Serializable;
import java.util.Objects;

public class Alumno implements Serializable {
	
	private String curso;
	private String nombre;
	private double nota;

	/**
	 * 
	 */
	public Alumno() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param curso
	 * @param nombre
	 * @param nota
	 */
	public Alumno(String curso, String nombre, double nota) {
		super();
		this.curso = curso;
		this.nombre = nombre;
		this.nota = nota;
	}

	/**
	 * @return the curso
	 */
	public String getCurso() {
		return curso;
	}

	/**
	 * @param curso the curso to set
	 */
	public void setCurso(String curso) {
		this.curso = curso;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the nota
	 */
	public double getNota() {
		return nota;
	}

	/**
	 * @param nota the nota to set
	 */
	public void setNota(double nota) {
		this.nota = nota;
	}

	@Override
	public String toString() {
		return "Alumno [curso=" + curso + ", nombre=" + nombre + ", nota=" + nota + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(curso, nombre, nota);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumno other = (Alumno) obj;
		return Objects.equals(curso, other.curso) && Objects.equals(nombre, other.nombre);
	}
	
}