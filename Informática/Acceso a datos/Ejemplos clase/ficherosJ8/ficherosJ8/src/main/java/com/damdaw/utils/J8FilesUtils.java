package com.damdaw.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class J8FilesUtils {

	/**
	 * v1.0 LEER TEXTO EN Java 8 
	 * LECTURA DE FICHERO DE TEXTO USANDO Stream USADO EN JAVA v8 SIN CONTROLAR EXCEPCIONES
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 * @throws IOException PUEDE TIRAR EXCEPCIÓN LA LECTURA DEL ARCHIVO
	 */
	public static void leerFicheroJ8SinErrores(String archivo) throws IOException {

		// COMPROBAMOS SI EL FICHERO EXISTE USANDO LA CLASES Path Y Files
		Path ruta = Paths.get(archivo);
		if (!Files.exists(ruta)) {
			System.out.println("No he encontrado el fichero");

			return;
		}

		System.out.println("--Leyendo fichero...--");
		System.out.println();

		// LEO TODAS LAS LINEAS DEL ARCHIVO, NO NECESITO BUCLE
		Stream<String> lineas = Files.lines(ruta);

		// IMPRIMO LAS LINEAS LEIDAS UNA A UNA USANDO forEach
		// forEach NECESITA COMO PARAMETRO UN METODO QUE ADMITA STRING
		// PARA REFERENCIAR UN MÉTODO DE System.out PODEMOS USAR ::
		lineas.forEach(System.out::println);

		// PODEMOS HACER LO MISMO USANDO FUNCIONES LAMBDA
		// lineas.forEach((s)->System.out.println(s));

		System.out.println();
		System.out.println("--Fin de la lectura.--");
	}

	/**
	 * v1.1 LEER TEXTO EN Java 8 CON CONTROL CLASICO 
	 * LECTURA DE FICHERO DE TEXTO USANDO Stream USADO EN JAVA v8 CON CONTROL DE EXCEPCIONES CLÁSICO
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void leerFicheroJ8TryClasico(String archivo) {

		Path ruta = Paths.get(archivo);
		if (!Files.exists(ruta)) {
			System.out.println("No he encontrado el fichero");

			return;
		}

		// SACO LA CREACIÓN DE Stream PARA REFERENCIARLA
		Stream<String> lineas = null;

		System.out.println("--Leyendo fichero...--");
		System.out.println();

		try {
			lineas = Files.lines(ruta);
			lineas.forEach(System.out::println);
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} finally {
			// TENEMOS QUE COMPROBAR QUE EL STREAM NO SEA NULL
			if (lineas != null) {
				// TAMBIÉN TENEMOS QUE CAPTURAR QUE close() NO TIRE EXCEPCIÓN
				try {
					lineas.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		System.out.println();
		System.out.println("--Fin de la lectura.--");
	}

	/**
	 * v1.2 LEER TEXTO EN Java 8 CON TRY-WITH RESOURCES 
	 * LECTURA DE FICHERO DE TEXTO USANDO Stream USADO EN JAVA v8 CON CONTROL DE EXCEPCIONES PARAMETRIZADO (WITH RESOURCES)
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void leerFicheroJ8TryResources(String archivo) {

		Path ruta = Paths.get(archivo);
		if (!Files.exists(ruta)) {
			System.out.println("No he encontrado el fichero");

			return;
		}

		System.out.println("--Leyendo fichero...--");
		System.out.println();

		// NO HACE FALTA SACAR LA CREACIÓN DE Stream PARA REFERENCIARLA ANYADIMOS EL CHARSET
		try (Stream<String> lineas = Files.lines(ruta, Charset.defaultCharset())) {
			// EN ESTA OCASION USAREMOS FUNCIONES LAMBDA PARA IR FAMILIARIZANDONOS
			lineas.forEach((s) -> System.out.println(s));
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} // YA NO HACE FALTA CERRAR EL ARCHIVO PORQUE USA AutoCloseable

		System.out.println();
		System.out.println("--Fin de la lectura.--");
	}

	/**
	 * v1.3 LEER TODAS LAS LINEAS EN Java 8 
	 * LECTURA DE FICHERO DE TEXTO USANDO readAllLines USADO EN JAVA v8 CON CHARSET DEFINIDO
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void leerFicheroJ8readAllLines(String archivo) {

		Path ruta = Paths.get(archivo);
		if (!Files.exists(ruta)) {
			System.out.println("No he encontrado el fichero");

			return;
		}

		System.out.println("--Leyendo fichero...--");
		System.out.println();

		try {
			// AHORA UTILIZAMOS readAllLines PARA GUARDAR LAS LINEAS LEIDAS EN UNA LISTA DE STRINGS
			List<String> lineas = Files.readAllLines(ruta, StandardCharsets.UTF_8);
			lineas.forEach((s) -> System.out.println(s));
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} // COMO AHORA NO USAMOS UN Stream, NO HACE FALTA CERRAR RECURSOS, POR ESO NO TENEMOS PARÁMETROS EN EL TRY

		System.out.println();
		System.out.println("--Fin de la lectura.--");
	}

	/**
	 * v1.4 LEER TEXTO Y DEVOLVER List 
	 * DEVOLVER LAS LINEAS DE UN FICHERO DE TEXTO EN UNA List<String>
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 * @return DEVUELVE List<String> DE LAS LINEAS LEIDAS
	 */
	public static List<String> getLineasFichero(String archivo) {

		List<String> lineas = null;

		Path ruta = Paths.get(archivo);
		if (!Files.exists(ruta)) {
			System.out.println("No he encontrado el fichero");

			return null;
		}

		try {
			// DIRECTAMENTE DEVOLVEMOS EL RESULTADO DE readAllLines PORQUE DEVUELVE List<String>
			lineas = Files.readAllLines(ruta, StandardCharsets.UTF_8);
			// YA NO VAMOS A IMPRIMIR POR PANTALLA
			// .forEach((s) -> System.out.println(s));
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		}

		return lineas;
	}

	/**
	 * v2.0 ESCRIBIR TEXTO EN Java 8 
	 * ESCRITURA DE UN FICHERO EN JAVA v8 USANDO BufferedWriter
	 * 
	 * @param rutaa RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void escribirFicheroJ8(String rutaa) {

		// VAMOS A UTILIZAR UN ARRAY DE String PARA ALMACENAR LAS LINEAS A ESCRIBIR. LO USAREMOS MAS TARDE
		String[] lineas = new String[] { "Hola!", "Java 8", "hasta luego!" };

		Path ruta = Paths.get(rutaa + "ejemplo.txt");

		System.out.println("--Escribiendo fichero...--");
		System.out.println();

		// SI EL ARCHIVO NO EXISTE LO CREAMOS
		if (!Files.exists(ruta)) {
			try (BufferedWriter br = Files.newBufferedWriter(ruta, Charset.defaultCharset(),
					StandardOpenOption.CREATE)) {
				Arrays.stream(lineas).forEach((s) -> {
					try {
						br.write(s);
						br.newLine();
					} catch (IOException errorDeFichero) {
						System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
					}
				});
			} catch (IOException errorDeFichero) {
				System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
			}
		}
		// SI EL ARCHIVO EXISTE AÑADIMOS EL TEXTO AL FINAL
		else {
			try (BufferedWriter br = Files.newBufferedWriter(ruta, Charset.defaultCharset(),
					StandardOpenOption.APPEND)) {
				Arrays.stream(lineas).forEach((s) -> {
					try {
						br.write(s);
						br.newLine();
					} catch (IOException errorDeFichero) {
						System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
					}
				});
			} catch (IOException errorDeFichero) {
				System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
			}
		}

		System.out.println();
		System.out.println("--Fin de la escritura.--");
	}

	/**
	 * v2.1 ESCRBIR TEXTO EN Java 8 CON Files 
	 * ESCRITURA DE UN FICHERO EN JAVA v8 USANDO Files.write
	 * 
	 * @param rutaa RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void escribirFicheroJ8Files(String rutaa) {

		// VAMOS A UTILIZAR UNA LISTA DE String PORQUE FILES NECESITA UN ITERABLE
		List<String> lineas = new ArrayList<>();
		lineas.add("Hola!");
		lineas.add("Java 8");
		lineas.add("Files");

		Path ruta = Paths.get(rutaa + "ejemplo.txt");

		System.out.println("--Escribiendo fichero...--");
		System.out.println();

		try {
			if (!Files.exists(ruta)) {
				// write USARÁ LA LISTA PARA LAS LINEAS Y LA CODIFICACION QUE PONGAMOS
				Files.write(ruta, lineas, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
			} else {
				Files.write(ruta, lineas, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
			}
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		}

		System.out.println();
		System.out.println("--Fin de la escritura.--");
	}

	/**
	 * v2.3 DUPLICAR TEXTO EN Java 8 
	 * DUPLICAR UN FICHERO EN JAVA v8
	 * 
	 * @param rutaa   RUTA DONDE CREAREMOS EL ARCHIVO DE TEXTO RESULTANTE
	 * @param entrada NOMBRE DEL ARCHIVO QUE VAMOS A COPIAR. USARÁ LA MISMA RUTA PARA ORIGEN Y DESTINO
	 */
	public static void duplicarFicheroJ8(String rutaa, String entrada) {

		Path origen = Paths.get(rutaa + entrada);

		if (!Files.exists(origen)) {
			System.out.println("No he encontrado el fichero original");

			return;
		}

		Path destino = Paths.get(rutaa + "copia.txt");

		System.out.println("--duplicando fichero...--");
		System.out.println();

		try {
			List<String> lineas = Files.readAllLines(origen, StandardCharsets.UTF_8);

			Files.write(destino, lineas, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} // YA NO HACE FALTA CERRAR EL ARCHIVO PORQUE USA AutoCloseable

		System.out.println();
		System.out.println("--Archivo duplicado.--");
	}

}
