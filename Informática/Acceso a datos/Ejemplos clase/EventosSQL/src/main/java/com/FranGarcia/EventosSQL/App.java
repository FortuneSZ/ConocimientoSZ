package com.FranGarcia.EventosSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import utils.properties;

/**
 * Hello world!
 */
public class App 
{
	public static void main(String[] args) throws ClassNotFoundException,
	SQLException 
	{
		String querr = """
			    SELECT id, nombre, descripcion
			    FROM evento
			    ORDER BY fecha
			    """;
		properties p = new properties();
		Connection con = p.connection();
		try (
			  // UTILIZO MI VARIABLE PRIVADA PROPERTIES CON EL USUARIO dev
			    
			    Statement statement = con.createStatement();
			    ResultSet rs = statement.executeQuery(querr)) {
			   System.out.println("ID" + "\t" + "Nombre" + "\t" + "Descripción");
			   System.out.println("-----------------------------------------");

			   while (rs.next()) {
			    System.out.println(
			      rs.getString("id") + "\t " + rs.getString("nombre") + "\t " + rs.getString("descripcion"));
			   }
			  } catch (SQLException e) {
			   e.printStackTrace();
			  }
	}

}
