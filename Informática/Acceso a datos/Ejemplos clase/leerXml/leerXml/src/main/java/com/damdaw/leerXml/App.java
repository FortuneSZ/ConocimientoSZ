package com.damdaw.leerXml;

import java.util.List;

import com.damdaw.entities.Noticia;
import com.damdaw.utils.XmlUtils;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) {

		String ruta = new String();
		ruta = "./resources/";

		// v1.0
		XmlUtils.procesarXmlSax(ruta, "asignaturas.xml");
		// v1.1
		XmlUtils.procesarXmlDom(ruta, "asignaturas.xml");

		// v1.2 Procesar XML del RSS del Marca
		XmlUtils.procesarXmlMarca(ruta, "portada.xml");
		// v1.3 Procesar URL CON EL RSS del RSS del Marca
		XmlUtils.procesarURLMarca("https://e00-marca.uecdn.es/rss/portada.xml").stream()
				.filter(noti -> noti.getTitulo().contains("Madrid")).forEach(noti -> System.out.println(noti));

		// v2.0 Objetos genéricos
		List<Noticia> noticias = XmlUtils.procesarURLListaObjeto("https://e00-marca.uecdn.es/rss/portada.xml",
				Noticia.class);
		noticias.forEach(System.out::println);
	}
}
