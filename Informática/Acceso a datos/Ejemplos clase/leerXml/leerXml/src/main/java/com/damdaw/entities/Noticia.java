package com.damdaw.entities;

import java.io.Serializable;
import java.util.Objects;

import org.w3c.dom.Element;

public class Noticia implements Serializable {

	private String titulo;
	private String guid;
	private String imagen;	
	
	/**
	 * 
	 */
	public Noticia() {
		super(); 
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @param titulo
	 * @param guid
	 * @param imagen
	 */
	public Noticia(String titulo, String guid, String imagen) {
		super();
		this.titulo = titulo;
		this.guid = guid;
		this.imagen = imagen;
	}


	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}
	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}
	/**
	 * @return the imagen
	 */
	public String getImagen() {
		return imagen;
	}
	/**
	 * @param imagen the imagen to set
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}


	@Override
	public int hashCode() {
		return Objects.hash(guid, imagen, titulo);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Noticia other = (Noticia) obj;
		return Objects.equals(guid, other.guid) && Objects.equals(imagen, other.imagen)
				&& Objects.equals(titulo, other.titulo);
	}


	@Override
	public String toString() {
	    return "Noticia:\n" +
	           "Título: " + titulo + "\n" +
	           "URL: " + guid + "\n" +
	           "URL Imagen: " + imagen +
	           "\n";
	}
	
	// CREAMOS UN CONSTRUCTOR QUE RECIBE UN Element Y BUSCAMOS LAS ETIQUETAS QUE NOS INTERESAN
    public Noticia(Element element) {
        this.titulo = element.getElementsByTagName("title").item(0).getTextContent();
        this.guid = element.getElementsByTagName("guid").item(0).getTextContent();
        this.imagen = element.getElementsByTagName("media:content")
            .item(0).getAttributes().getNamedItem("url").getTextContent();
    }

}
