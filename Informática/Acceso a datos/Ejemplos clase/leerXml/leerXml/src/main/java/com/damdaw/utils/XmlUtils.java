package com.damdaw.utils;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.damdaw.entities.Noticia;

public class XmlUtils {

	/**
	 * v1.0 LEER CON SAX 
	 * LEER UN ARCHIVO XML UTILIZANDO LA LIBRERÍA SAX PASÁNDOLO COMO PARÁMETRO 
	 * LECTURA SECUENCIAL. NO TENEMOS TODO EL ARBOL 
	 * USARLO PARA VOLCAR A UN ARCHIVO O UNA ESTRUCTURA DINÁMICA. 
	 * ÚTIL SI LEEMOS ARCHIVOS GIGANTES
	 * 
	 * @param rutaa   EL DIRECTORIO DONDE SE ALMACENAN LOS ARCHIVOS XML QUE PODEMOS USAR
	 * @param archivo EL ARCHIVO CONCRETO QUE QUEREMOS LEER
	 */
	public static void procesarXmlSax(String rutaa, String archivo) {
		try {
			// 1. CREAR OBJETO TIPO SAXParserFactory
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			// 2. CREAR UN SAXParser A PARTIR DEL SAXParserFactory
			SAXParser saxParser = saxParserFactory.newSAXParser();

			// 3. CREAR EL MANEJADOR DE EVENTOS DefaultHandler
			DefaultHandler manejadorEventos = new DefaultHandler() {
				String etiquetaActual = "";
				String contenido = "";

				// 3.1 CREAR MÉTODO PARA APERTURA DE ETIQUETA: '<'
				public void startElement(String uri, String localName, String qName, Attributes attributes)
						throws SAXException {
					// MEMORIZAMOS EL NOMBRE DE LA ETIQUETA PARA MOSTRAR DESPUÉS
					etiquetaActual = qName;
					// SI LA ETIQUETA ES "asignatura" MOSTRAREMOS SU ATRIBUTO "id" EN PANTALLA
					if (etiquetaActual == "asignatura" || etiquetaActual == "item")
						System.out.println("Mostrando " + etiquetaActual + ": " + attributes.getValue("id"));
				}

				// 3.2 CREAR MÉTODO PARA EL CONTENIDO DENTRO DE UNA ETIQUETA:'<' y '>'
				public void characters(char ch[], int start, int length) throws SAXException {
					contenido = new String(ch, start, length);
				}

				// 3.3 CREAR MÉTODO PARA CIERRE DE ETIQUETA: '>'
				public void endElement(String uri, String localName, String qName) throws SAXException {

					if (etiquetaActual != "") {
						System.out.println(" " + etiquetaActual + ": " + contenido);
						etiquetaActual = "";
					}
				}
			};
			// 4. LLAMADA AL METODO parse CON EL FICHERO Y EL DefaultHandler
			// DefaultHandler LLAMARÁ a startElement(), endElement() y character()
			saxParser.parse(rutaa + archivo, manejadorEventos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * v1.1 LEER CON DOM 
	 * LEER UN ARCHIVO XML UTILIZANDO LA LIBRERÍA PARA OBJECT MODEL PASÁNDOLO COMO PARÁMETRO 
	 * GUARDAMOS EL ARBOL COMPLETO 
	 * PODEMOS ACCEDER A CUALQUIER DATO CON getElementsByTagName o getAttribute(nombre)
	 * 
	 * @param rutaa   EL DIRECTORIO DONDE SE ALMACENAN LOS ARCHIVOS XML QUE PODEMOS USAR
	 * @param archivo EL ARCHIVO CONCRETO QUE QUEREMOS LEER
	 */
	public static void procesarXmlDom(String rutaa, String archivo) {

		try {
			// 1. CREAR OBJETO TIPO DocumentBuilderFactory
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			// 2. CREAR UN DocumentBuilder A PARTIR DEL DocumentBuilderFactory
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			// 3. CREAR UN Document Y PASARLE EL ARCHIVO COMO OBJETO File
			File inputFile = new File(rutaa + archivo);
			// USAR EL METODO parse DE DocumentBuilder PARA PASARLE EL ARCHIVO
			Document doc = dBuilder.parse(inputFile);
			// 3.1 CON normalize() NOS ASEGURAMOS QUE EL XML ESTÁ BIEN CONSTRUIDO
			doc.getDocumentElement().normalize();

			// 3.2 PODEMOS REVISAR CUAL ES EL ELEMENTO BASE
			System.out.println("Elemento base : " + doc.getDocumentElement().getNodeName());

			// 4. OBTENEMOS LA LISTA DE NODOS CON LA ETIQUETA "asignatura" Y LA ALMACENAMOS EN UNA NodeList
			NodeList nList = doc.getElementsByTagName("asignatura");

			// ANYADO SALTO DE LINEA
			System.out.println();
			System.out.println("Recorriendo asignaturas...");

			// 5. RECORRO LA NodeList CON UN for HASTA LA LONGITUD DE LA LISTA
			for (int temp = 0; temp < nList.getLength(); temp++) {
				// 5.1 CREAMOS UN OBJETO Node PARA CADA "asignatura"
				Node nNode = nList.item(temp);
				// COMPROBAMOS SI EL Node ES DE TIPO Element DE HTML O XML
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					// CASTEAMOS A Element EN SUCIO PARA PODER USAR LOS METODOS
					Element eElement = (Element) nNode;

					// SI EL NODO TIENE ATRIBUTOS USAREMOS getAttribute
					System.out.println("Codigo: " + eElement.getAttribute("id"));
					// SI SON ETIQUETAS USAREMOS getElementsByTagName
					System.out.println("Nombre: " + eElement.getElementsByTagName("nombre").item(0).getTextContent());
					System.out.println(
							"Ciclo: " + eElement.getElementsByTagName("cicloFormativo").item(0).getTextContent());
					System.out.println("Curso: " + eElement.getElementsByTagName("curso").item(0).getTextContent());
					System.out
							.println("Profesor: " + eElement.getElementsByTagName("profesor").item(0).getTextContent());

					// ANYADO UN SALTO DE LINEA
					System.out.println();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * v1.2 LEER XML DE MARCA.COM LEER UN ARCHIVO XML DESDE EL RSS DE MARCA USANDO DOM
	 * 
	 * @param rutaa   EL DIRECTORIO DONDE SE ALMACENAN LOS ARCHIVOS XML QUE PODEMOS USAR
	 * @param archivo EL ARCHIVO CONCRETO QUE QUEREMOS LEER
	 */
	public static void procesarXmlMarca(String rutaa, String archivo) {

		try {
			File inputFile = new File(rutaa + archivo);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			// CON normalize() NOS ASEGURAMOS QUE EL XML ESTÁ BIEN CONSTRUIDO
			doc.getDocumentElement().normalize();
			System.out.println("Elemento base : " + doc.getDocumentElement().getNodeName());

			// ALMACENAREMOS TODAS LAS NOTICIAS EN UNA NodeList
			// LAS NOTICIAS SE IDENTIFICAN COMO item
			NodeList nList = doc.getElementsByTagName("item");

			// ANYADO SALTO DE LINEA
			System.out.println();
			System.out.println("Recorriendo Noticias del Marca...");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				// IMPRIMIREMOS CADA VEZ QUE VEAMOS UN Element
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					// CASTEAMOS A Element EN SUCIO
					Element eElement = (Element) nNode;

					System.out.println("Título: " + eElement.getElementsByTagName("title").item(0).getTextContent());
					System.out.println("URL de la noticia: "
							// stripIndent QUITA LOS ESPACIOS SI EL XML ESTÁ TABULADO
							+ eElement.getElementsByTagName("guid").item(0).getTextContent().stripIndent()); 
					System.out.println("URL de la imagen: " + eElement.getElementsByTagName("media:content").item(0)
							.getAttributes().getNamedItem("url").getTextContent());
					// ANYADO UN SALTO DE LINEA
					System.out.println();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * v1.3 LEER URL DEL XML DE MARCA.COM 
	 * LEER UNA URL DEL RSS DE MARCA USANDO DOM Y DEVOLVER UNA LISTA DE NOTICIAS
	 * 
	 * @param url LA URL DESDE LA QUE DESCARGAREMOS EL XML DEL MARCA
	 * @return DEVOLVEREMOS UNA List<Noticia> LEÍDO DEL XML
	 */
	public static List<Noticia> procesarURLMarca(String url) {

		List<Noticia> noticias = new ArrayList<Noticia>();

		try {
			// NO VAMOS A USAR ARCHIVO ESTA VEZ, SINO QUE PASAMOS UNA URL
			// File inputFile = new File(url);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(url);
			// CON normalize() NOS ASEGURAMOS QUE EL XML ESTÁ BIEN CONSTRUIDO
			doc.getDocumentElement().normalize();
			System.out.println("Elemento base : " + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("item");

			// ANYADO SALTO DE LINEA
			System.out.println();
			System.out.println("Recorriendo Noticias del Marca...");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					noticias.add(new Noticia(eElement.getElementsByTagName("title").item(0).getTextContent(),
							eElement.getElementsByTagName("guid").item(0).getTextContent(),
							eElement.getElementsByTagName("media:content").item(0).getAttributes().getNamedItem("url")
									.getTextContent()));
					/*
					 * COMPROBAR SI EXISTE UNA URL DE IMAGEN PARA QUE NO DE ERROR String foto =
					 * eElement.getElementsByTagName("media:content") .item(0)!=null?
					 * eElement.getElementsByTagName("media:content")
					 * .item(0).getAttributes().getNamedItem("url").getTextContent() :"";
					 * 
					 * noticias.add(new Noticia(
					 * eElement.getElementsByTagName("title").item(0).getTextContent(),
					 * eElement.getElementsByTagName("guid").item(0).getTextContent(), foto ));
					 */
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return noticias;

	}

	/**
	 * V2.0 LECTURA DE XML CON OBJETOS GENÉRICOS 
	 * LEEMOS EL XML DESDE LA URL Y DEVOLVEREMOS UNA LISTA DE OBJETOS GENÉRICOS, POR LO QUE PODREMOS USAR CUALQUIERA 
	 * NECESITAREMOS DEVOLVER UNA LISTA SÍ O SÍ PARA USAR EL MÉTODO .add() 
	 * NECESITAMOS UNA Class<T> QUE NOS PERMITA INVOCAR AL CONSTRUCTOR DEL OBJETO QUE QUEREMOS
	 * 
	 * @param <T>   LE ESPECIFICAMOS EL TIPO <T> PARA PODER USAR OBJETOS GENÉRICOS
	 * @param url   LA URL DESDE LA QUE DESCARGAREMOS EL XML DEL MARCA
	 * @param clase ESPECIFICAREMOS LA CLASE DE OBJETO QUE UTILIZAREMOS PARA ALMACENAR LO LEÍDO DE LA URL. 
	 * 				DICHA CLAS NECESITA TENER UN CONSTRUCTOR QUE RECIBA Element COMO PARÁMETRO
	 * @return DEVOLVEREMOS UNA List<T> CON OBJETOS GENÉRICOS QUE CAMBIARÁ SEGÚN LA CLASE
	 */
	public static <T> List<T> procesarURLListaObjeto(String url, Class<T> clase) {

		List<T> listaObj = new ArrayList<>();

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(url);
			// CON normalize() NOS ASEGURAMOS QUE EL XML ESTÁ BIEN CONSTRUIDO
			doc.getDocumentElement().normalize();
			System.out.println("Elemento base : " + doc.getDocumentElement().getNodeName());

			// LAS NOTICIAS SE IDENTIFICAN COMO item
			// SI TRABAJÁSEMOS CON OTRO XML PODRÍA LLAMARSE DE OTRA MANERA
			NodeList nList = doc.getElementsByTagName("item");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					// UTILIZAREMOS REFLEXIÓN PARA BUSCAR UN CONSTRUCTOR QUE RECIBA Element
					Constructor<T> constructor = clase.getConstructor(Element.class);
					// DEBEREMOS CREAR UN CONSTRUCTOR EN NUESTRA CLASE QUE RECIBA Element
					// Y QUE BUSQUE LAS CADENAS QUE NOS INTERESAN
					// newInstance INVOCARA AL CONSTRUCTOR SI LO ENCUENTRA Y LE PASARÁ eElement
					listaObj.add(constructor.newInstance(eElement));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listaObj;
	}

}
