package com.damdaw.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class J5FilesUtils {

	/**
	 * v1.0 LEER TEXTO EN Java 5 LECTURA DE FICHERO DE TEXTO USANDO BufferedReader
	 * USADO EN JAVA v5
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void leerFicheroBReader(String archivo) {
		// COMPROBAMOS QUE EL FICHERO EXISTE
		if (!(new File(archivo)).exists()) {
			System.out.println("No he encontrado el fichero");

			return;
		}

		// SI EXISTE, USAMOS BUFFEREDREADER PARA LEERLO
		System.out.println("--Leyendo fichero...--");
		// ANYADO UN 2o SALTO DE LÍNEA PARA DIFERENCIAR
		System.out.println();
		try {
			BufferedReader ficheroEntrada = new BufferedReader(new FileReader(new File(archivo)));
			String linea = ficheroEntrada.readLine();

			// MIENTRAS readLine() DEVUELVA LÍNEAS SEGUIMOS LEYENDO
			while (linea != null) {
				System.out.println(linea);
				linea = ficheroEntrada.readLine();
			}

			/*
			 * FORMA SIMPLIFICADA String linea=null;
			 * 
			 * while ((linea=ficheroEntrada.readLine()) != null) {
			 * System.out.println(linea); }
			 */

			// SIEMPRE CERRAMOS EL FICHERO DESPUÉS DE ABRIRLO
			ficheroEntrada.close();
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		}

		// ANYADO UN 2o SALTO DE LÍNEA PARA DIFERENCIAR
		System.out.println();
		System.out.println("--Fin de la lectura.--");
	}

	/**
	 * v1.1 LEER BINARIOS EN Java 5 
	 * LECTURA DE FICHERO DE TEXTO USANDO FileInputStream USADO EN JAVA v5
	 * 
	 * @param archivo RUTA AL ARCHIVO QUE VAMOS A LEER, INCLUYENDO EL ARCHIVO
	 */
	public static void leerFicheroFIStream(String archivo) {

		if (!(new File(archivo)).exists()) {
			System.out.println("No he encontrado el fichero");

			return;
		}

		System.out.println("--Leyendo fichero...--");
		System.out.println();

		try {
			// AHORA UTILIZAREMOS FileInputStream PARA EL ARCHIVO
			FileInputStream ficheroEntrada = new FileInputStream(new File(archivo));
			// COMO LA LECTURA DE FileInputStream ES BIT A BIT AHORA LEERÁ ENTEROS

			// PUESTO QUE VAMOS A LEER TEXTO UTILIZAREMOS InputStreamReader PARA LA LECTURA DE CADA DATO
			InputStreamReader textoEntrada = new InputStreamReader(ficheroEntrada);
			// ALMACENAREMOS LOS CARACTERES EN UN StringBuffer PARA MOSTRAR LA LINEA POR CONSOLA
			StringBuffer linea = new StringBuffer();
			int dato = textoEntrada.read();

			// MIENTRAS read() DEVUELVA DISTINTO DE -1 SEGUIMOS LEYENDO
			while (dato != -1) {

				// CUANDO DETECTAMOS UN SALTO DE LÍNEA IMPRIMIMOS LA LINEA Y REINICIAMOS EL BUFFER
				if (dato == '\n') {
					System.out.println(linea.toString());
					linea.setLength(0);
					// SI NO HAY SALTO, ALAMCENAMOS EL BIT LEÍDO EN EL BUFFER
				} else {
					linea.append((char) dato);
				}

				dato = textoEntrada.read();
			}

			// SI LA LINEA NO TIENE SALTO, SE IMPRIME LA ÚLTIMA LINEA ALMACENADA
			if (linea.length() > 0) {
				System.out.println(linea.toString());
			}
			ficheroEntrada.close();
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		}

		System.out.println();
		System.out.println("--Fin de la lectura.--");
	}

	/**
	 * v2.0 ESCRIBIR TEXTO EN Java 5 ESCRITURA DE UN FICHERO DE TEXTO EN JAVA 5
	 * 
	 * @param rutaa RUTA DONDE CREAREMOS EL ARCHIVO DE TEXTO RESULTANTE
	 */
	public static void escribirFicheroJ5(String rutaa) {

		// DECLARAMOS EL PrintWriter FUERA PARA PODER REFERENCIARLO EN TRY/CATCH/FINALLY
		PrintWriter escribe = null;

		System.out.println("--Escribiendo fichero...--");
		System.out.println();

		try {
			// LE PASAREMOS COMO PARAMETRO UNA RUTA ABSOLUTA QUE TERMINE CON EL NOMBRE DE ARCHIVO
			escribe = new PrintWriter(rutaa + "ejemplo.txt");

			// USAMOS println PARA ESCRIBIR
			escribe.println("Hola!");
			escribe.println("y...");
			escribe.println("hasta luego!");
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} finally {
			// NOS ASEGURAMOS QUE SIEMPRE CERRAMOS EL PrintWriter
			if (escribe != null) {
				escribe.close();
			}
		}

		System.out.println();
		System.out.println("--Fin de la escritura--");
	}

	/**
	 * v2.1 ESCRIBIR SIN SOBREESCRIBIR EN Java 5 
	 * ESCRITURA DE UN FICHERO DE TEXTO EN JAVA 5 SIN SOBREESCRIBIR
	 * 
	 * @param rutaa RUTA DONDE CREAREMOS EL ARCHIVO DE TEXTO RESULTANTE
	 */
	public static void escribirFicheroJ5BufferedWriter(String rutaa) {

		PrintWriter escribe = null;

		System.out.println("--Escribiendo fichero...--");
		System.out.println();

		try {
			// AHORA UTILIZAREMOS UN BufferedWriter Y UN FileWriter
			// SEGUIMOS USANDO PrintWriter PARA PODER USAR println
			// EL 2o PARAM DE FileWriter DETERMINA SI ANYADE CONTENIDO AL FINAL (true) O SI ESCRIBE DESDE EL PRINCIPIO
			escribe = new PrintWriter(new BufferedWriter(new FileWriter(rutaa + "ejemplo.txt", true)));

			escribe.println("Hola otra vez!");
			escribe.println("ahora...");
			escribe.println("adios!");
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} finally {
			if (escribe != null) {
				escribe.close();
			}
		}

		System.out.println();
		System.out.println("--Fin de la escritura--");
	}

	/**
	 * v2.2 DUPLICAR EN Java 5 
	 * DUPLICAR UN ARCHIVO DE TEXTO EN JAVA 5
	 * 
	 * @param rutaa   RUTA DONDE CREAREMOS EL ARCHIVO DE TEXTO RESULTANTE
	 * @param entrada NOMBRE DEL ARCHIVO QUE VAMOS A COPIAR. USARÁ LA MISMA RUTA PARA ORIGEN Y DESTINO
	 */
	public static void duplicarFicheroJ5(String rutaa, String entrada) {

		// ALMACENAMOS NUESTRO ARCHIVO DESTINO EN UNA VARIABLE
		String salida = "copia.txt";
		// DEJAMOS EL TAMANYO DEL BUFFER EN UNA VARIABLE FINAL
		final int BUFFER_SIZE = 512 * 1024;

		// DECLARAMOS LAS InputStream OutputStream FUERA PARA REFERENCIARLAS
		InputStream origen = null;
		OutputStream destino = null;

		System.out.println("--duplicando fichero...--");
		System.out.println();

		try {
			// USAREMOS LA MISMA RUTA PARA AMBOS FICHEROS, PERO DIFERENTES NOMBRES DE
			// ARCHIVO
			origen = new FileInputStream(rutaa + entrada);
			destino = new FileOutputStream(rutaa + salida);

			// NECESITAREMOS ESPECIFICAR UN BUFFER DE LECTURA PARA OPTIMIZARLA
			byte[] buf = new byte[BUFFER_SIZE];
			int dato;

			while ((dato = origen.read(buf)) != -1) {
				destino.write(buf, 0, dato);
			}
		} catch (IOException errorDeFichero) {
			System.out.println("Ha habido problemas: " + errorDeFichero.getMessage());
		} finally {
			// NOS ASEGURAMOS DE CERRAR AMBOS STREAMS
			// HAY QUE PONERLOS ENTRE TRY/CATCH POR SI FALLA EL CIERRE
			try {
				if (origen != null) {
					origen.close();
				}
				if (destino != null) {
					destino.close();
				}
			} catch (IOException errorDeFichero) {
				System.err.println("Error al cerrar los streams: " + errorDeFichero.getMessage());
			}
		}

		System.out.println();
		System.out.println("--Fin de la escritura--");
	}
}
