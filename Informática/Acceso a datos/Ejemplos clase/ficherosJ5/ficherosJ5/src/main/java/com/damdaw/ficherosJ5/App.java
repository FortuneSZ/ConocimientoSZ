package com.damdaw.ficherosJ5;

import java.io.IOException;

import com.damdaw.utils.J5FilesUtils;

/**
 * MANAGING FILES IN JAVA 5
 *
 */
public class App 
{
	public static void main(String[] args) throws IOException {

		String ruta = new String();

		ruta = "./resources/";

		// v1.0
		J5FilesUtils.leerFicheroBReader(ruta + "Maslineas.txt");
		// v1.1
		J5FilesUtils.leerFicheroFIStream(ruta + "Maslineas.txt");
		// v2.0
		J5FilesUtils.escribirFicheroJ5(ruta);
		// v2.1
		J5FilesUtils.escribirFicheroJ5BufferedWriter(ruta);
		// v2.2
		J5FilesUtils.duplicarFicheroJ5(ruta, "ejemplo.txt");
	}
}
