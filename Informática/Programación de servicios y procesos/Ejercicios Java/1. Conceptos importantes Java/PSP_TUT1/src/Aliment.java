public class Aliment extends MenuElement implements GetNutritionalValues
{
    String name;
    String description;
    String frequency;
    double calories;
    double fat;
    double carbohydrates;
    boolean nuts;
    boolean milk;
    boolean eggs;
    boolean gluten;

    /*Constructor*/

    public Aliment(String name, String description, String frequency,
                   double calories, double fat, double carbohydrates,
                   boolean nuts, boolean milk, boolean eggs, boolean gluten)
    {
        this.name = name;
        this.description = description;
        this.frequency = frequency;
        this.calories = calories;
        this.fat = fat;
        this.carbohydrates = carbohydrates;
        this.nuts = nuts;
        this.milk = milk;
        this.eggs = eggs;
        this.gluten = gluten;
    }

    /*Getters and setters*/

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getFrequency()
    {
        return frequency;
    }

    public void setFrequency(String frequency)
    {
        this.frequency = frequency;
    }

    public void setCalories(double calories)
    {
        this.calories = calories;
    }

    public void setFat(double fat)
    {
        this.fat = fat;
    }

    public void setCarbohydrates(double carbohydrates)
    {
        this.carbohydrates = carbohydrates;
    }

    public void setNuts(boolean nuts)
    {
        this.nuts = nuts;
    }

    public void setMilk(boolean milk)
    {
        this.milk = milk;
    }

    public void setEggs(boolean eggs)
    {
        this.eggs = eggs;
    }

    public void setGluten(boolean gluten)
    {
        this.gluten = gluten;
    }

    /*Methods*/

    public double getCalories()
    {
        double Calories = 0;
        return Calories;
    }

    public  double getCarbohydrates()
    {
        double carbohydrates = 0;
        return carbohydrates;
    }

    public double getFat()
    {
        double fat = 0;
        return fat;
    }

    public boolean hasMilk()
    {
        return true;
    }

    public boolean hasNuts()
    {
        return true;
    }

    public boolean hasEggs()
    {
        return true;
    }

    public boolean hasGluten()
    {
        return true;
    }
}
