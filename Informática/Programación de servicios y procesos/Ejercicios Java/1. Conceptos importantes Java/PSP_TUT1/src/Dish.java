import java.util.List;

public class Dish extends MenuElement implements GetNutritionalValues
{
    String name;
    String description;
    List<Ingredient> ingedients;

    /*Constructor*/

    public Dish(String name, String description, List<Ingredient> ingedients)
    {
        this.name = name;
        this.description = description;
        this.ingedients = ingedients;
    }

    /*Getters and setters*/

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public List<Ingredient> getIngedients()
    {
        return ingedients;
    }

    public void setIngedients(List<Ingredient> ingedients)
    {
        this.ingedients = ingedients;
    }

    /*Methods*/

    public double getCalories()
    {
        double Calories = 0;
        return Calories;
    }

    public  double getCarbohydrates()
    {
        double carbohydrates = 0;
        return carbohydrates;
    }

    public double getFat()
    {
        double fat = 0;
        return fat;
    }

    public boolean hasMilk()
    {
        return true;
    }

    public boolean hasNuts()
    {
        return true;
    }

    public boolean hasEggs()
    {
        return true;
    }

    public boolean hasGluten()
    {
        return true;
    }
}
