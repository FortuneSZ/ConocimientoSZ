#Lista en la que cada elemento de la lista es una lista con dos elementos, tamaño y peso

# Crear una lista de listas con el tamaño y peso de varios objetos

Lista = [[9, 10], [3, 7], [9, 9], [7, 8], [5, 10]]

#Utilizando https://docs.python.org/3/howto/sorting.html y las "key functions", haz que esa lista se ordene por mayor altura y en caso de igualdad, por menor peso.

# Ordenar la lista por mayor altura y menor peso

Lista_ordenada = sorted(Lista, key=lambda x: (-x[0], x[1]))

# Mostrar la lista ordenada

print("Lista ordenada por mayor altura y menor peso:")

print(Lista_ordenada)

#Que son las key functions

#Las key functions son funciones que se utilizan para personalizar el ordenamiento de los 
# elementos de una lista.

#En el caso de la función sorted, se puede pasar un argumento key que es una función que
# se aplica a cada elemento de la lista antes de ordenarlos.

#Por ejemplo, en el caso de la lista de listas con el tamaño y peso de varios objetos,
# se puede utilizar una key function que ordene los elementos por mayor altura y en caso de
# igualdad, por menor peso.

#Esto permite personalizar el ordenamiento de la lista de acuerdo a los criterios deseados.