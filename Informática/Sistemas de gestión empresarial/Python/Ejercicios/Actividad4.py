#Funcionamiento del operador is

# is es un operador de comparación que verifica si dos variables apuntan al mismo objeto en memoria. 
# A diferencia de ==, que verifica si dos variables son iguales, pero no necesariamente el mismo 
# objeto en memoria.

# Ejemplo de uso de is:
print("Ejemplos de is")
x = [1, 2, 3]

y = x

print(x is y) # True

z = [1, 2, 3]

print(x is z) # False

# En este caso, x y y apuntan al mismo objeto en memoria, por lo que x is y devuelve True.

# Sin embargo, x y z son dos objetos diferentes, por lo que x is z devuelve False.

#Funcionamiento del operador not

# not es un operador lógico que invierte el valor de una expresión booleana.

# Ejemplo de uso de not:
print("Ejemplos de not")
x = True

print(not x) # False

y = False

print(not y) # True

# En este caso, not invierte el valor de la variable x de True a False, y de la variable y de False a True.

#Funcionamiento del operador in

# in es un operador de pertenencia que verifica si un valor está presente en una secuencia, 
# como una lista, tupla, cadena, etc.

# Ejemplo de uso de in:

lista = [1, 2, 3, 4, 5]
print("Ejemplos de in")
print(3 in lista) # True

print(6 in lista) # False

# En este caso, 3 está presente en la lista, por lo que 3 in lista devuelve True.