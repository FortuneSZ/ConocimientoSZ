#pasar varios parámetros por consola a un programa

# Importamos el módulo sys

import sys

# Obtenemos los argumentos pasados por consola

argumentos = sys.argv

# Mostramos los argumentos

print("Argumentos pasados por consola:")
print(argumentos)

# Mostramos el primer argumento

print("Primer argumento:")
print(argumentos[1])

# Mostramos el segundo argumento

print("Segundo argumento:")
print(argumentos[2])

#sobrecarga de funciones

# Definimos una función que suma números

def suma(*numeros):
    return sum(numeros)

print("Sobrecarga de funciones")
print(suma(2, 3))
print(suma(2, 3, 4))
print(suma(2, 3, 4, 5))
print(suma())

