#Almacenar un usuario y una contraseña usando una lista

# Crear una lista vacía para almacenar los usuarios y contraseñas   

usuarios = []

# Crear una función para almacenar un usuario y una contraseña en la lista

def almacenar_usuario(usuario, contraseña):

    #pasamos la contraseña a formato hash

    contraseña = hash(contraseña)

    # Crear un diccionario con el usuario y la contraseña

    usuario_contraseña = {"usuario": usuario, "contraseña": contraseña}

    # Agregar el diccionario a la lista de usuarios

    usuarios.append(usuario_contraseña)

# Llamar a la función para almacenar un usuario y una contraseña

almacenar_usuario("usuario1", "contraseña1")
almacenar_usuario("usuario2", "contraseña2")
almacenar_usuario("usuario3", "contraseña3")
almacenar_usuario("usuario4", "contraseña4")
almacenar_usuario("usuario5", "contraseña5")

# Mostrar la lista de usuarios y contraseñas
print("Obtener usuario y contraseña de usuario4")
print(usuarios[3])

#almacenar un usuario y contraseña usando un diccionario

# Crear un diccionario vacío para almacenar los usuarios y contraseñas

usuarios = {}

# Crear una función para almacenar un usuario y una contraseña en el diccionario

def almacenar_usuario(usuario, contraseña):

    #pasamos la contraseña a formato hash

    contraseña = hash(contraseña)

    # Agregar el usuario y la contraseña al diccionario

    usuarios[usuario] = contraseña

# Llamar a la función para almacenar un usuario y una contraseña

almacenar_usuario("usuario1", "contraseña1")
almacenar_usuario("usuario2", "contraseña2")
almacenar_usuario("usuario3", "contraseña3")
almacenar_usuario("usuario4", "contraseña4")
almacenar_usuario("usuario5", "contraseña5")

# Mostrar el diccionario de usuarios y contraseñas
print("Obtener contraseña de usuario1")
print(usuarios.get("usuario1"))