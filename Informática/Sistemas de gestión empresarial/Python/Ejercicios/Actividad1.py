# En primer lugar creamos una lista

Lista = [1, 2, 3, 4]

# Clonamos la lista
Clone = Lista[:] 

# Muestra de que el clon es una copia de la lista

print(f"Lista = {Lista} y Clone = {Clone}")

# Diferencia entre shallow copy y deep copy

# Shallow copy

# Importamos la librería copy
import copy

# Creamos una lista con sublistas

Lista = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# Clonamos la lista

Clone = copy.copy(Lista)

# Cambiamos un valor de la lista original

Lista[0][0] = 0

# Mostramos la lista original y la clonada
print("Shallow copy")
print(f"Lista = {Lista} y Clone = {Clone}")

#Como se puede observar, al cambiar un valor de la lista original, también se cambia en la clonada,
# esto se debe a que la clonación es superficial, es decir, solo se clona la lista principal,
#  pero no las sublistas.

# Deep copy

# Clonamos la lista

Clone = copy.deepcopy(Lista)

# Cambiamos un valor de la lista original

Lista[0][0] = 1

# Mostramos la lista original y la clonada
print("Deep copy")
print(f"Lista = {Lista} y Clone = {Clone}")

# En este caso, al cambiar un valor de la lista original, no se cambia en la clonada,
# esto se debe a que la clonación es profunda, es decir, se clona la lista principal y las sublistas.

#Añadir un elemento a la lista

Lista.append(10)

# Mostramos la lista
print("Lista modificada")
print(f"Lista = {Lista}")

# Eliminar un elemento de la lista

Lista.remove(10)

# Mostramos la lista

print("Lista modificada")
print(f"Lista = {Lista}")

#Crear una nueva lista con los últimos cuatro elementos de la lista original
Lista2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
NuevaLista = Lista2[-4:]

# Mostramos la nueva lista

print("Nueva lista")
print(f"NuevaLista = {NuevaLista}")

#convertir las palabras de una cadena separadas por espacios en una lista

Cadena = "Hola mundo"

# Convertimos la cadena en una lista

Lista = Cadena.split()

# Mostramos la lista

print("Lista de palabras")
print(f"Lista = {Lista}")

"""
Comentario
Multiínea
"""