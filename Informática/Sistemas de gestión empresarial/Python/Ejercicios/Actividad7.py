#Definición de la clase car, con atributos matrícula y color, y métodos imprimir, cambiar_color y cambiar_matrícula.
import random


class Car:

    def __init__(self, matricula, color):
        self.matricula = matricula
        self.color = color

    def imprimir(self):
        print(f"Matrícula: {self.matricula}, Color: {self.color}")

    def cambiar_color(self, nuevo_color):
        self.color = nuevo_color

    def cambiar_matricula(self, nueva_matricula):
        self.matricula = nueva_matricula

#Crea un objeto de la clase Car y prueba

coche = Car(1234, "Rojo")

coche.imprimir()

"""
En segundo lugar, haz que el programa pida un número "n" por teclado y se creen "n"
instancias de la clase, donde cada instancia:
Cada "matrícula" tendrá un número consecutivo desde 1 hasta "n".
El "color" será para cada instancia un color aleatorio obtenido de esta lista ["red",
"white", "black", "pink", "blue"]
"""

n = int(input("Introduce el número de instancias: "))
colores = ["red", "white", "black", "pink", "blue"]


"""
Finalmente, el programa deberá imprimir los valores de las 10 primeras instancias. En
caso de que "n" sea menor que 10, solo imprimirá "n" instancias.
"""

for i in range(1, min(10, n)+1):
    coche = Car(i, colores[random.randint(0, 4)])
    coche.imprimir()    