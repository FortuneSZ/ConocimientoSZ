#Función que recibe dos números y devuelve la suma de ambos

def suma(a, b):
    print("La suma de" , a , "y" , b , "es:" , a + b)
    return a + b

print(suma(2, 3))

#función que recibe una lista y la modifica duplicando todos los valores, sin devolver nada

def duplicar(lista):
    for i in range(len(lista)):
        lista[i] = lista[i] * 2
    print(lista)

lista = [1, 2, 3, 4, 5]
print(lista)
duplicar(lista)

#función que recibe una lista y devuelve una copia de la lista con todos los valores duplicados

def duplicar(lista):

    lista_duplicada = []
    for i in range(len(lista)):
        lista_duplicada.append(lista[i] * 2)
    return lista_duplicada

lista_duplicada = duplicar(lista)
print(lista)
print(lista_duplicada)