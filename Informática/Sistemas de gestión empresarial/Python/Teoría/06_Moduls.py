####################################################
## 6. Módulos
####################################################
# Puedes importar módulos
# Importa todas las funciones y hay que referirse a ellas con math.función
import math
print(math.sqrt(16)) # => 4.0

# Puedes obtener funciones específicas desde un módulo
from math import ceil, floor
print(ceil(3.7)) # => 4.0
print(floor(3.7))# => 3.0

# Precaución: Esto NO es recomendable
# Importa todas las funciones y variables definidas en el módulo math directamente al espacio de nombres actual.
from math import *
sqrt(16) == sqrt(16) # => True

# Puedes acortar los nombres de los módulos
import math as m
math.sqrt(16) == m.sqrt(16) # => True

# Los módulos de Python son sólo archivos ordinarios de Python.
# Puedes escribir tus propios módulos e importarlos. El nombre del módulo
# es el mismo del nombre del archivo.
# Puedes encontrar que funciones y atributos definen un módulo.
import math
print(dir(math))

## Futuro comentar desde aquí <<<<<<<<<<<<<<<< ##
# La clase en la misma ubicación que este archivo
from Humanoide import Humano
# Ahora puedes usar la clase Humano en tu script
persona = Humano("Maria",15)
print(persona.decir("Hola,\n ¿cómo estás?"))

# La clase en una subcarpeta
from Clases.Humano import Humano
# Otras opciones
# import Clases.Humano # >>> Nos obligaria a anteponer "Humano en su uso": 
#                        >>>    persona = Humano.Humano("Pablo", 15)
# from Clases.Humano import * # >>> mismo efecto pero NO recomendado por tema legibilidad
#                               >>> - espacio de nombres contaminado (método ajenos)

# Instanciando la clase Humano desde el módulo Humano
persona = Humano("Pablo", 15)
print(persona.decir("Hola,\n ¿Humano sin from?"))

# >>> Comenta línea superiores y descomenta esto y __init__.py de "Clases"
# # La clase en una subcarpeta
# from Clases import Humano
# persona = Humano("Pablo", 15)
# print(persona.decir("Hola,\n ¿Humano sin from?"))

