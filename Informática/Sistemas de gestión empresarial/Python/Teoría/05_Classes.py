####################################################
## 5. Clases
####################################################
# Heredamos de object para obtener una clase.
class Humano:
    # Un atributo de clase es compartido por todas las instancias de esta clase
    _especie = "H. sapiens" # por convención con "_" NO debe ser accedido desde fuera de la propia clase (privado)

    # Constructor basico
    def __init__(self, nombre, edad):
        # Asigna el argumento al atributo nombre de la instancia
        self._nombre = nombre
        self._edad = edad

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, valor):
        # Agregar lógica adicional si es necesario antes de establecer el nombre
        self._nombre = valor

    @property
    def edad(self):
        return self._edad

    @edad.setter
    def edad(self, valor):
        # Agregar lógica adicional si es necesario antes de establecer la edad
        self._edad = valor

    # Un metodo de instancia. Todos los metodos toman self como primer argumento
    def decir(self, msg):
        return "%s: %s" % (self.nombre, msg) #% es el operador de formato

    # Un metodo de clase es compartido a través de todas las instancias
    # Son llamados con la clase como primer argumento
    @classmethod
    def get_especie(cls): #cls se refiere a la propia clase. se usa por "convencion"
        return cls._especie
    
    @classmethod
    def set_especie(cls, nueva_especie):
        cls._especie = nueva_especie

    # Un metodo estatico es llamado sin la clase o instancia como referencia
    # No reciben un parámetro implícito self ni cls. Son como funciones regulares definidas dentro de una clase.
    @staticmethod
    def dormir():
        return "*durmiendo*"

# Instancia una clase
i = Humano(nombre="Edu",edad="20")
print(i.decir("hola")) # imprime "edu: hola"
j = Humano("Paula",33)
print(j.decir("bienvenido")) #imprime "Paula: bienvenido"

# Llama nuestro método de clase
print(f"Los humanos son: {i.get_especie()}") # => Los humanos son: H. sapiens

# Cambia los atributos compartidos
Humano.set_especie("H. neanderthalensis")
i.get_especie()
j.get_especie()
print(f"Los humanos son: {i.get_especie()}") 
print(f"Los humanos son: {j.get_especie()}") 

# Llama al método estático
Humano.dormir() # => "*durmiendo*"

#Herencia
class Animal:
    def __init__(self, nombre):
        self.nombre = nombre

    def hablar(self):
        raise NotImplementedError("La subclase debe implementar este método")

class Perro(Animal):
    def hablar(self):
        return "Guau!"

class Gato(Animal):
    def hablar(self):
        return "Miau!"

# Uso de las clases
perro = Perro("Firulais")
gato = Gato("Misifú")

print(perro.hablar())  # Imprime: Guau!
print(gato.hablar())   # Imprime: Miau!
