# Función de orden superior que crea funciones de descuento
def crear_funcion_descuento(porcentaje_descuento):
    def aplicar_descuento(precio):
        return precio - (precio * (porcentaje_descuento / 100))
    return aplicar_descuento

# Crear funciones de descuento específicas
descuento_ropa = crear_funcion_descuento(10)  # 10% de descuento para ropa
descuento_electronica = crear_funcion_descuento(15)  # 15% de descuento para electrónica

# Aplicar los descuentos a productos específicos
precio_ropa = 100  # Precio inicial de un artículo de ropa
precio_final_ropa = descuento_ropa(precio_ropa)
print(f"Precio final de la ropa después del descuento: ${precio_final_ropa}")

precio_electronica = 1000  # Precio inicial de un artículo electrónico
precio_final_electronica = descuento_electronica(precio_electronica)
print(f"Precio final de electrónica después del descuento: ${precio_final_electronica}")