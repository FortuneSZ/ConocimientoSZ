﻿using System.Runtime.Intrinsics;

class Vector3D
{
    private double x;
    private double y;
    private double z;

    public Vector3D(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double X
    {
        get
        {
            return x;
        }
        set
        {
            x = value;
        }
    }

    public double Y
    {
        get
        {
            return y;
        }
        set
        {
            y = value;
        }
    }

  public double Z
    {
        get
        {
            return z;
        }
        set
        {
            z = value;
        }
    }

    public bool Igual(Vector3D v2)
    {
        if (this.x.Equals(v2.x) && this.y.Equals(v2.y) && this.z.Equals(v2.z))
        {
            Console.WriteLine("Los vectores {0} y {1} son iguales", this, v2);
            return true;
            
        }
        else
        {
            Console.WriteLine("Los vectores {0} y {1} no son iguales", this, v2);
            return false;
        }
    }

    public double NormaMax() 
    {
        return Math.Sqrt((Math.Pow(this.x, 2) + Math.Pow(this.y, 2) + Math.Pow(this.z, 2)));
    }

    public override string ToString()
    {
        return "(" + this.x + "," + this.y + "," + this.z + ") ";
    }




}
