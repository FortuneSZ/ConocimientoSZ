﻿class main
{
    static void Main(string[] args)
    {
        Vector3D v1 = new Vector3D(3, 5, 7);
        Vector3D v2 = new Vector3D(3, 5, 7);
        Vector3D v3 = new Vector3D(2, 4, 6);

        v1.Igual(v2);
        v2.Igual(v3);

        Console.WriteLine("La norma del vector {0} es {1}",v1,v1.NormaMax());
        Console.WriteLine("La norma del vector {0} es {1}", v2, v2.NormaMax());
        Console.WriteLine("La norma del vector {0} es {1}", v3, v3.NormaMax());
    }
}