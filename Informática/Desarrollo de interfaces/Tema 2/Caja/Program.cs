﻿class principal
{
    public static void main()
    {
        Caja c1 = new Caja(1, 2, 3);
        Caja c2 = new Caja(2, 3, 4);
        Caja c3 = c1 + c2;
        Caja c4 = c2 - c1;

        /*Console.WriteLine(c1);
        Console.WriteLine(c2);
        Console.WriteLine(c3);
        Console.WriteLine(c4);*/

        Caja[] cajas = new Caja[4];
        cajas[0] = c1;
        cajas[1] = c2;
        cajas[2] = c3;
        cajas[3] = c4;
        Array.Sort(cajas);

        for (int i = 0; i < cajas.Length; i++)
        {
            Console.WriteLine(cajas[i]);
        }
    }
}
