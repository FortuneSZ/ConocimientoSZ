﻿class Caja :IComparable<Caja>
{
    private int alto;
    private int largo;
    private int ancho;

    public int Alto
    {
        get
        {
            return alto;
        }
        set
        {
            try
            {
                Convert.ToInt32(value);
                alto = value;
            }
            catch(Exception e)
            {
                Console.WriteLine("Tipo de dato no válido");
                Console.WriteLine(e.Message);
            }
            
        }
    }

    public int Largo
    {
        get
        {
            return largo;
        }
        set
        {
            try
            {
                Convert.ToInt32(value);
                largo = value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Tipo de dato no válido");
                Console.WriteLine(e.Message);
            }
            
        }
    }

    public int Ancho
    {
        get
        {
            return ancho;
        }
        set
        {
            try
            {
                ancho = value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Tipo de dato no válido");
                Console.WriteLine(e.Message);
            }

        }
    }

    public Caja(int alto, int largo, int ancho)
    {
        this.alto = alto;
        this.largo = largo;
        this.ancho = ancho;
    }

    public static Caja operator +(Caja c1,Caja c2)
    {
        Caja c3 = new Caja(c1.alto + c2.alto, c1.largo + c2.largo, c1.ancho + c2.ancho);
        return c3;
    }

    public static Caja operator -(Caja c1, Caja c2)
    {
        Caja c3 = new Caja(c1.alto - c2.alto, c1.largo - c2.largo, c1.ancho - c2.ancho);
        return c3;
    }

    public int CompareTo(Caja c1)
    {
        return this.ancho.CompareTo(c1.ancho);
    }

    public override string ToString()
    {
        Console.WriteLine("Alto: {0}, Largo: {1}, Ancho: {2}", this.alto, this.largo, this.ancho);
        return "";
    }
}

