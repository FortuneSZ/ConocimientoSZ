﻿//clase principal
class Principal
{
    static void Main()
    {
        Modulo m1 = new Modulo("interfaces", 240, 9);
        Modulo m2 = new Modulo("psp", 300, 8);
        Modulo m3 = new Modulo("móviles", 270, 9);
        List<Modulo> modulos = new List<Modulo> { m1, m2, m3 };

        Persona p1 = new Alumno("Fran", "García", "Sánchez", new DateTime(1999, 9, 4), 25, modulos);

        Console.WriteLine(p1);
    }
}

