﻿//clase persona
class Persona
{
    //atributos
    protected string name;
    protected string firstName;
    protected string lastName;
    protected DateTime birthDate;
    protected int age;

    //constructor parametrizado
    public Persona(string name, string fisrtName, string lastName, DateTime birthDate, int age)
    {
        this.name = name;
        this.firstName = fisrtName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.age = age;
    }

    //getters y setters
    public string Name
    {
        get
        {
            return this.name;
        }
        set
        {
            this.name = value;
        }
    }

    public string Firstname
    {
        get
        {
            return this.firstName;
        }
        set
        {
            this.firstName = value;
        }
    }

    public string Lastname
    {
        get
        {
            return this.lastName;
        }
        set
        {
            this.lastName = value;
        }
    }

    public DateTime BirthDate
    {
        get
        {
            return this.birthDate;
        }
        set
        {
            this.birthDate = value;
        }
    }

    public int Age
    {
        get
        {
            return this.age;
        }
        set
        {
            this.age = value;
        }
    }

    //método tostring
    public override string ToString()
    {
        return "Nombre: " + name + ", apellidos: " + firstName + " " + lastName 
            + ", fecha de nacimiento: " + birthDate.ToString("d") + ", edad: " + age;
    }
}

