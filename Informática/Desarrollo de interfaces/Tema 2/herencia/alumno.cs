﻿//clase persona
class Alumno : Persona
{
    //atributos
    List<Modulo> modulos = new List<Modulo>();

    //constructor parametrizado
    public Alumno(string name, string firstname, string lastName, DateTime birthDate, int age, List<Modulo> modulos) 
        : base(name, firstname, lastName,birthDate,age)
    {
        this.name = name;
        this.firstName = firstname; 
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.age = age;
        this.modulos = modulos;
    }

    //método tostring
    public override string ToString()
    {
        Console.WriteLine(base.ToString());
        Console.WriteLine("Lista de módulos:");
        for (int i = 0; i < modulos.Count; i++) 
        {
            Console.WriteLine(modulos[i].ToString()); 
        }
        return "";
    }
}

