﻿//Clase modulo
class Modulo
{
    //atributos
    protected string nombre;
    protected int horas;
    protected double nota;

    //constructor parametrizado
    public Modulo(string nombre, int horas, double nota)
    {
        this.nombre = nombre;
        this.horas = horas;
        this.nota = nota;
    }

    //getters y setters
    public string Nombre
    {
        get
        {
            return Nombre;
        }
        set
        {
            
            this.nombre = value;
            
        }
    }

    public int Horas
    {
        get
        {
            return horas;
        }
        set
        {
            this.horas = value;
        }
    }

    public double Nota
    {
        get
        {
            return nota;
        }
        set
        {
            this.nota = value;
        }
    }

    //método tostring
    public override string ToString()
    {
        return "Nombre: " + nombre + ", horas: " + horas + ", nota: " + nota;
    }
}

