﻿//Clase complejo
class Complejo
{
    //Atributos
    private double real;
    private double imaginaria;

    //Getters y setters
    public double Real
    { 
        get 
        {
            return real; 
        } 
        set 
        { 
            real = value; 
        } 
    }

    public double Imaginaria
    {
        get
        {
            return imaginaria;
        }
        set
        {
            imaginaria = value;
        }
    }

    public Complejo(double real, double imaginaria)
    {
        this.real = real;
        this.imaginaria = imaginaria;
    }

    //Métodos

    public Complejo Suma(Complejo c)
    {
        double r = 0, i = 0;
        r = this.real + c.real;
        i = this.imaginaria + c.imaginaria;
        return new Complejo(r, i);
    }

    public Complejo Resta(Complejo c)
    {
        double r = 0, i = 0;
        r = this.real - c.real;
        i = this.imaginaria - c.imaginaria;
        return new Complejo(r, i);
    }

    public Complejo Multiplicación(Complejo c)
    {
        double r = 0, i = 0;
        r = this.real * c.real - this.imaginaria * c.imaginaria;
        i = this.real * c.imaginaria + this.imaginaria * c.real;
        return new Complejo(r, i);
    }

    public Complejo Multiplicación(int x)
    {
        double r = 0, i = 0;
        r = x * this.real;
        i = x * this.imaginaria;
        return new Complejo(r, i);
    }

    //método tostring
    public override string ToString()
    {
        return this.real + "+" + this.imaginaria + "i";
    }
}

