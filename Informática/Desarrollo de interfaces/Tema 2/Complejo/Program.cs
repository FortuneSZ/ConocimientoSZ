﻿class main
{ 
    public static void Main(string[] args)
    {
        Complejo c1 = new Complejo(7, 3);
        Complejo c2 = new Complejo(1, 2);


        Console.WriteLine("La suma de {0} y {1} es {2}", c1, c2, c1.Suma(c2));
        Console.WriteLine("La resta de {0} y {1} es {2}", c1, c2, c1.Resta(c2));
        Console.WriteLine("La multiplicación de {0} y {1} es {2}", c1, c2, c1.Multiplicación(c2));
        Console.WriteLine("La multiplicación de {0} y {1} es {2}", c1, 5, c1.Multiplicación(5));
    }
}



