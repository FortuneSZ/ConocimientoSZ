﻿HashSet<int> conjunto = new HashSet<int>() { 1, 2, 3 };
HashSet<int> conjunto2 = new HashSet<int>() { 3, 4, 5 };

Console.WriteLine("Conjunto inicial");
Conjunto conj = new Conjunto(conjunto);
Console.WriteLine(conj);

Console.WriteLine("Función vaciar");
conj.Vaciar();
Console.WriteLine(conj);

Console.WriteLine("Función agregar");
conj.Agregar(4);
Console.WriteLine(conj);

Console.WriteLine("Función copiar");
conj.Copiar(conjunto2);
Console.WriteLine(conj);

Console.WriteLine("Función EsMiembro");
Console.WriteLine("4: " + conj.EsMiembro(4));
Console.WriteLine("1: " + conj.EsMiembro(1));
Console.WriteLine();

Console.WriteLine("Función EsIgual");
Console.WriteLine("conj = conjunto1: " + conj.EsIgual(conjunto));
Console.WriteLine("conj = conjunto2: " + conj.EsIgual(conjunto2));
Console.WriteLine();

Console.WriteLine("Función EsVacío");
Console.WriteLine(conj.EsVacio());
conj.Vaciar();
Console.WriteLine(conj.EsVacio());
Console.WriteLine();

Console.WriteLine("Función Cardinal");
Console.WriteLine(conj.Cardinal());
conj.Agregar(1);
conj.Agregar(2);
conj.Agregar(3);
Console.WriteLine(conj.Cardinal());
Console.WriteLine();

Console.WriteLine("Función unión");
conjunto2 = new HashSet<int>() { 4, 5, 6 };
HashSet<int> conjunto3 = conj.Unión(conjunto2);
Conjunto conj2 = new Conjunto(conjunto3);
Console.WriteLine(conj2);
Console.WriteLine();

Console.WriteLine("Función intersección");
conjunto3 = conj.Intersección(conjunto2);
conj2 = new Conjunto(conjunto3);
Console.WriteLine(conj2);
Console.WriteLine();

Console.WriteLine("Función diferencia");
conjunto2 = new HashSet<int>() { 5,6,7 };
conjunto3 = conj.Diferencia(conjunto2);
conj2 = new Conjunto(conjunto3);
Console.WriteLine(conj2);
Console.WriteLine();

Console.WriteLine("Función diferencia simétrica");
conjunto2 = new HashSet<int>() { 4,5, 6, 7 };
conjunto3 = conj.DiferenciaSimétrica(conjunto2);
conj2 = new Conjunto(conjunto3);
Console.WriteLine(conj2);
Console.WriteLine();
