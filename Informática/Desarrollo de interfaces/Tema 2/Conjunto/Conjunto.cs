﻿//Clase conjunto
class Conjunto
{
    #region Atributos
    //atributos
    private HashSet<int> conjunto;
    #endregion

    #region Constructor
    //Constructor parametrizado
    public Conjunto(HashSet<int> conjunto)
    {
        this.conjunto = conjunto;
    }
    #endregion

    #region Métodos
    //Métodos

    public void Vaciar()
    {
        conjunto.Clear();
    }

    public void Agregar(int n)
    {
        conjunto.Add(n);
    }

    public void Eliminar(int n)
    {
        conjunto.Remove(n);
    }

    public void Copiar(HashSet<int> c2)
    {
        this.conjunto = c2;
    }

    public bool EsMiembro(int n)
    {
        bool miembro = false;

        foreach (int i in conjunto)
        {
            if (i == n)
            {
                miembro = true;
            }
        }

        return miembro;
    }

    public bool EsIgual(HashSet<int> c2)
    {
        bool igual = false;

        if (this.conjunto.Equals(c2))
        {
            igual = true;
        }

        return igual;
    }

    public override string ToString()
    {
        Console.WriteLine("Valores del conjunto:");
        foreach(int i in conjunto)
        {
            Console.WriteLine(i);
        }
        return "";
    }

    public bool EsVacio()
    { 
        bool vacio = false;
        if (this.conjunto.Count == 0 )
        {
            vacio = true;
        }

        return vacio;
    }

    public int Cardinal()
    {
        return this.conjunto.Count();
    }

    public HashSet<int> Unión(HashSet<int> c2)
    {
        HashSet<int> newconj = this.conjunto;
        newconj.UnionWith(c2);
        return newconj;
    }

    public HashSet<int> Intersección(HashSet<int> c2)
    {
        HashSet<int> newconj = this.conjunto;
        newconj.IntersectWith(c2);
        return newconj;
    }

    public HashSet<int> Diferencia(HashSet<int> c2)
    {
        HashSet<int> newconj = this.conjunto;
        newconj.ExceptWith(c2);
        return newconj;
    }

    public HashSet<int> DiferenciaSimétrica(HashSet<int> c2)
    {
        HashSet<int> newconj = this.conjunto;
        newconj.SymmetricExceptWith(c2);
        return newconj;
    }
    #endregion
}

