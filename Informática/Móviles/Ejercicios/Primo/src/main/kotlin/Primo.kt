fun Primo(x:Int) :Boolean
{
    var contador:Int = 0
    var primo:Boolean = false
    for (i in 1 .. x)
    {
        if (x % i == 0)
        {
            contador++
        }
    }

    if (contador == 2)
    {
        primo = true
    }
    else
    {
        primo = false
    }
    return primo
}

fun main(args: Array<String>)
{
    var numero:Int = readLine()!!.toInt()
    if (Primo(numero)) println("$numero es un número primo") else println("$numero no es un número primo")


}