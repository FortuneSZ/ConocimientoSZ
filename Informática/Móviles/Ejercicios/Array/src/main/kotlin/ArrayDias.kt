fun main(args: Array<String>)
{
    val DiasSemana = arrayOf("Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo")
    val dia:Int = readLine()!!.toInt()

    if (dia >= 1 && dia <= 7)
    {
        print(DiasSemana[dia-1])
    }
    else
    {
        println("1 : Lunes")
        println("2 : Martes")
        println("3 : Miercoles")
        println("4 : Jueves")
        println("5 : Viernes")
        println("6 : Sabado")
        println("7 : Domingo")
    }

}