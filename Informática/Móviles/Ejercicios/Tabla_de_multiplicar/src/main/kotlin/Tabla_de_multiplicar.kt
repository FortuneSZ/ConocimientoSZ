fun main(args: Array<String>)
{
    println("Escriba un número del que desea ver la tabla de multiplicar")
    var num:Int = readLine()!!.toInt()

    println("Tabla de multiplicar del $num")
    for (i in 1..10)
    {
        var multi = num * i
        println("$num X $i = $multi")
    }
}