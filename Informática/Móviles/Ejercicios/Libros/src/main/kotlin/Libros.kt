class Libro(Autor : String, Título : String, Año: Int)
{
    var autor : String = "Anónimo"
        get()
        {
            return field
        }
        set(Autor)
        {
            field = Autor
        }
    var título : String = "No indicado"
        get()
        {
            return field
        }
        set(Título)
        {
            field = Título
        }
    var año : Int = -1
        get()
        {
            return field
        }
        set(Año)
        {
            field = Año.toInt()
        }
    init
    {
        this.autor = Autor
        this.título = Título
        this.año = Año
    }


    override fun toString(): String
    {
        return "$título-$autor-$año"
    }
}

fun main(args: Array<String>)
{
    var libro = Libro("It","Stephen King",1986)
    println(libro.toString())

}