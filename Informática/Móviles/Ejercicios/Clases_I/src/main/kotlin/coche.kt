data class coche(val Matricula2 : String,val Modelo2 : String,val Año2 : Int)
{
    var matricula : String = ""
    var modelo : String = ""
    var año : Int = 0

    init
    {
        this.matricula = Matricula2
        this.modelo = Modelo2
        this.año = Año2
    }

    override fun toString(): String {
        return "$modelo,$año,$matricula"
    }
}

fun main(args: Array<String>)
{
    var coche1 = coche("123456","Murcia",2021)
    var coche2 = coche("456789","Alicante",2023)
    println(coche1.toString());
    println(coche2.toString());
}