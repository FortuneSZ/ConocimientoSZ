package edu.frangarcia.demo_03

import android.util.Log
import androidx.lifecycle.ViewModel
import edu.frangarcia.demo_03.model.Items

/**
* Class MainViewModel.kt
* Clase donde se encuentra la lógica de la aplicación
* @autor Fran García Sánchez
* */
class MainViewModel : ViewModel()
{
    private var _fragmentShowed: String? = null

    val fragmentShowed: String?
        get() = _fragmentShowed

    init
    {
        Log.i("MainViewModel", "Init")
    }

    /**
     * Método setFragmentShowed
     * Método que marca el fragment mostrado
     * @author Fran García Sánchez
     */
    fun setFragmentShowed(fragmentName: String)
    {
        _fragmentShowed = fragmentName
    }

    /**
     * Método addItem
     * Método que añade un Item a la lista
     */
    fun addItem(item: Items)
    {
        Items.items.add(item)
        Log.i("MainViewModel", "addItem: $item")
    }

    /**
     * Método fetchItems
     * Método que muestra los Items de la lista
     */
    fun fetchItems(): MutableList<Items>
    {
        Log.i("MainViewModel", "fetchItems: ${Items.items.size}")
        return Items.items.filter { it.archived == false }.toMutableList()
    }

    /**
     * Método fetchArchivedItems
     * Método que muestra los Items de la lista de archivados
     */
    fun fetchArchivedItems(): MutableList<Items>
    {
        Log.i("MainViewModel", "fetchArchivedItems: ${Items.items.size}")
        return Items.items.filter { it.archived }.toMutableList()
    }

    /**
     * Método archiveItem
     * Método que archiva un Item
     */
    fun archiveItem(item: Items, callback: () -> Unit)
    {
        Items.items.remove(item)
        item.archived = true
        Items.items.add(item)


        callback()
        Log.i("MainViewModel", "archiveItem: $item")
    }
}