package edu.frangarcia.demo_03.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import edu.frangarcia.demo_03.MainViewModel
import edu.frangarcia.demo_03.model.Items
import edu.frangarcia.demo_03.databinding.ItemsBinding


/**
 * Clase ItemsAdapter.kt
 * Adaptador para la lista de Items
 * @author Fran García Sánchez
 */

class ItemsAdapter() : ListAdapter<Items, ItemsAdapter.ViewHolder>(ItemsDiffCallback())
{

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        private val binding = ItemsBinding.bind(view)
        private val viewModel = MainViewModel()

        /**
         * Método bind
         * Método que adhiere la información del objeto al mismo
         * @author Fran García Sánchez
         */
        fun bind(item: Items)
        {
            binding.tvId.text = String.format(item.id.toString())
            binding.tvTitle.text = item.title


            if (item.archived)
            {
                binding.iconArchive.visibility = View.GONE
            }
            else
            {
                binding.iconArchive.visibility = View.VISIBLE
            }

            Glide.with(binding.root)
                .load(item.image)
                .transform(FitCenter(), RoundedCorners(16))
                .into(binding.ivItem)


            binding.root.setOnClickListener {
                MaterialAlertDialogBuilder(binding.root.context)
                    .setTitle(item.title)
                    .setMessage(item.description)
                    .setPositiveButton("Ok", null)
                    .show()
            }


            binding.iconArchive.setOnClickListener {
                viewModel.archiveItem(item) {
                    submitList(viewModel.fetchItems())
                }
            }
        }
    }

    /**
     * Override del método onCreateViewHolder
     * Método que especifica que pasa al crearse el ViewHolder
     * @author Fran García Sánchez
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        return ViewHolder(
            ItemsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )
    }

    //vinculación de datos con la vista y ViewHolder
    /**
     * override del método onBindViewHolder
     * Método que dicta que pasa al unir los datos a un objeto
     * @author Fran García Sánchez
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        holder.bind(getItem(position))
    }
}

//Comprobación de objetos
/**
 * Clase ItemsDiffCallback
 * @author Fran García Sánchez
 */
class ItemsDiffCallback: DiffUtil.ItemCallback<Items>()
{
    /**
     * Método areItemsTheSame
     * Método que compara un objeto antiguo y uno nuevo que se intenta añadir
     * y dice si son el mismo o no
     */
    override fun areItemsTheSame(oldItem: Items, newItem: Items): Boolean
    {
        return oldItem.id == newItem.id
    }

    /**
     * Método areContentsTheSame
     * Método que compara un objeto antiguo y uno nuevo que se intenta añadir
     * y dice si su contenido es el mismo
     */
    override fun areContentsTheSame(oldItem: Items, newItem: Items): Boolean
    {
        return oldItem == newItem
    }

}