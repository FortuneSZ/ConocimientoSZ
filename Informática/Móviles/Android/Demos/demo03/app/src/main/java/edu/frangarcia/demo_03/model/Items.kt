package edu.frangarcia.demo_03.model

/**
 * clase de datos Items.kt
 * donde se encuentra la información de los items
 * @autor Fran García Sánchez
 */

data class Items(var id: Int = 0, var title: String, var description: String, var image: String = "",
                 var archived: Boolean = false)
{
    companion object
    {
        var identifier: Int = 0
        val items: MutableList<Items> = mutableListOf()
    }
    init
    {
        id = ++identifier
        image = "https://picsum.photos/200/200?image=$id"
    }
}