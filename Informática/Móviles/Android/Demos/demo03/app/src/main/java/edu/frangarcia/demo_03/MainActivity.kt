package edu.frangarcia.demo_03


import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import edu.frangarcia.demo_03.FragmentAdd
import edu.frangarcia.demo_03.FragmentArchive
import edu.frangarcia.demo_03.FragmentList
import edu.frangarcia.demo_03.MainViewModel
import edu.frangarcia.demo_03.adapters.ItemsAdapter
import edu.frangarcia.demo_03.databinding.ActivityMainBinding


/**
* Class MainActivity.kt
* Actividad principal donde se cargan los fragments
* @autor Fran García Sánchez
*
* */

class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding
    private val adapter = ItemsAdapter()
    private val TAG = MainActivity::class.java.simpleName

    private lateinit var fragmentList: FragmentList
    private lateinit var fragmentAdd: FragmentAdd
    private lateinit var fragmentArchive: FragmentArchive

    private val mainViewModel: MainViewModel by viewModels()

    /**
     * override del método onCreate
     * Método que Dicta que sucede al crearse la aplicación
     * @author Fran García Sánchez
     */
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Log.i(TAG, "onCreate")
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, 0)
            insets
        }
        fragmentList = FragmentList()
        fragmentAdd = FragmentAdd()
        fragmentArchive = FragmentArchive()

        //donde se carga el fragmento
        if(mainViewModel.fragmentShowed == null){
            loadFragment(fragmentList)
        } else {
            when(mainViewModel.fragmentShowed){
                fragmentList.javaClass.simpleName -> loadFragment(fragmentList)
                fragmentAdd.javaClass.simpleName -> loadFragment(fragmentAdd)
                fragmentArchive.javaClass.simpleName -> loadFragment(fragmentArchive)
            }
        }
    }

    // Acordarse que es polimorfismo de Fragment
    /**
     * Método loadFragment
     * Método que se encarga de cargar el fragment seleccionado
     */
    private fun loadFragment(fragment: Fragment)
    {
        supportFragmentManager.beginTransaction()
            .replace(binding.mFrameLayout.id, fragment)
            .commit()

        mainViewModel.setFragmentShowed(fragment.javaClass.simpleName)
    }

    /**
     * override del método onStart
     * Método que dicta que sucede cuando se inicia la aplicación
     * @author Fran García Sánchez
     */
    override fun onStart()
    {
        super.onStart()
        Log.i(TAG, "onStart")
        binding.mBottomNavView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.op_list -> loadFragment(fragmentList)
                R.id.op_archive -> loadFragment(fragmentArchive)
                R.id.op_add -> loadFragment(fragmentAdd)
            }
            true
        }
    }
}