package edu.frangarcia.demo_03

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import edu.frangarcia.demo_03.model.Items
import edu.frangarcia.demo_03.databinding.AddFragmentBinding

/**
 * Clase FragmentAdd.kt
 * FragmentAdd where the items are added
 * @autor Fran García Sánchez
 * */

class FragmentAdd : Fragment() {
    private val TAG = FragmentAdd::class.java.simpleName
    private lateinit var binding: AddFragmentBinding

    private val sharedViewModel: MainViewModel by activityViewModels()

    /**
     * override del método onCreateView
     * Método que dicta que sucede cuando se crea la vista
     * @author Fran García Sánchez
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i(TAG, "onCreateView")
        binding = AddFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * override del método onViewCreated
     * Método que dicta que sucede una vez se ha creado la vista
     * @author Fran García Sánchez
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        Log.i(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)

        binding.btnSave.setOnClickListener {
            binding.tilTitle.error = null
            binding.tilDesc.error = null

            val title = binding.tietTitle.text.toString().trim()
            val description = binding.tietDesc.text.toString().trim()

            if(title.isEmpty())
            {
                binding.tilTitle.error = getString(R.string.error_title)
                return@setOnClickListener
            }

            if(description.isEmpty())
            {
                binding.tilDesc.error = getString(R.string.error_desc)
                return@setOnClickListener
            }

            sharedViewModel.addItem(Items(title = title, description = description))

            binding.tietTitle.text?.clear()
            binding.tietDesc.text?.clear()


            val imm = requireActivity().getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /**
     * override del método onResume
     * Muestra un log cuando se continúa la ejecución
     * @author Fran García Sánchez
     */
    override fun onResume()
    {
        super.onResume()
        Log.i(TAG, "onResume")
    }

    /**
     * override del método onPause
     * Muestra un log cuando se pausa la ejecución
     * @author Fran García Sánchez
     */
    override fun onPause()
    {
        super.onPause()
        Log.i(TAG, "onPause")
    }

    /**
     * override del método onDestroy
     * Muestra un logo cuando se destruye el fragment
     * @author Fran García Sánchez
     */
    override fun onDestroy()
    {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
    }
}