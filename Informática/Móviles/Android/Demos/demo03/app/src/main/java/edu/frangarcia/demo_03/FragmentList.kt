package edu.frangarcia.demo_03

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import edu.frangarcia.demo_03.adapters.ItemsAdapter
import edu.frangarcia.demo_03.model.Items
import edu.frangarcia.demo_03.databinding.ListFragmentBinding


/**
 * Clase FragmentList.kt
 * Clase donde los Items son mostrados
 * @autor Fran García Sánchez
 * */

class FragmentList : Fragment()
{
    private val TAG = FragmentList::class.java.simpleName
    private lateinit var binding: ListFragmentBinding

    private val sharedViewModel: MainViewModel by activityViewModels()
    private val adapter = ItemsAdapter()
    /**
     * override del método onCreateView
     * Método que dicta que sucede cuando se crea la vista
     * @author Fran García Sánchez
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i(TAG, "onCreateView")
        Log.d(TAG, "onCreateView: ${Items.items.size}")
        binding = ListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
    /**
     * override del método onViewCreated
     * Método que dicta que sucede una vez se ha creado la vista
     * @author Fran García Sánchez
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        Log.i(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        binding.mRecycled.layoutManager = LinearLayoutManager(context)
        binding.mRecycled.adapter = adapter
        adapter.submitList(sharedViewModel.fetchItems())
    }
    /**
     * override del método onResume
     * Muestra un log cuando se continúa la ejecución
     * @author Fran García Sánchez
     */
    override fun onResume()
    {
        super.onResume()
        Log.i(TAG, "onResume")
        adapter.submitList(sharedViewModel.fetchItems())
    }
    /**
     * override del método onPause
     * Muestra un log cuando se pausa la ejecución
     * @author Fran García Sánchez
     */
    override fun onPause()
    {
        super.onPause()
        Log.i(TAG, "onPause")
    }
    /**
     * override del método onDestroy
     * Muestra un logo cuando se destruye el fragment
     * @author Fran García Sánchez
     */
    override fun onDestroy()
    {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
    }
}