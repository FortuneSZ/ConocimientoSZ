package edu.FranGarcia.Demo01

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import edu.FranGarcia.Demo01.databinding.ActivityMainBinding

/**
 * Clase MainActivity
 * Clase que gestiona la vista del programa principal
 * @author Fran García Sánchez
 */
class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    /**
     *override del método onCreate
     * Método que dicta que ocurre al crearse el programa
     * @author Fran García Sánchez
     */
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    /**
     * override del método onStart
     * Método que dicta que ocurre cuando empieza el programa
     * @author Fran García Sánchez
     */
    override fun onStart()
    {
        super.onStart()
        // Listener for the button.
        binding.Button.setOnClickListener()
        {
            comprobaciones()
            binding.tvResult.text = null

            val carnet = if (binding.rbNo.isChecked) getString(R.string.txt_no) else
                getString(R.string.txt_yes)
            val turn = if (binding.swTurn.isChecked) getString(R.string.txt_turn) else
                getString(R.string.txt_morning)

            val moduleCheck = with(binding)
            {
                cbADA.isChecked || cbDI.isChecked || cbPMDM.isChecked ||
                        cbPSP.isChecked || cbSGE.isChecked
            }
            if (moduleCheck)
            {
                if (binding.tietName.error == null
                    && binding.tietSurName.error == null
                    && binding.tietBirthDate.error == null)
                {
                    binding.tvResult.text = getString(
                        R.string.txt_result1,
                        binding.tietName.text.toString().trim(),
                        binding.tietSurName.text.toString().trim(),
                        binding.tietBirthDate.text.toString().trim()
                    )

                    binding.tvResult.append(
                        getString(
                            R.string.txt_result2,
                            carnet, turn
                        )
                    )

                    val modules = mutableListOf<String>()
                    if (binding.cbADA.isChecked) modules.add(getString(R.string.txt_ada))
                    if (binding.cbDI.isChecked) modules.add(getString(R.string.txt_di))
                    if (binding.cbPMDM.isChecked) modules.add(getString(R.string.txt_pmdm))
                    if (binding.cbPSP.isChecked) modules.add(getString(R.string.txt_psp))
                    if (binding.cbSGE.isChecked) modules.add(getString(R.string.txt_sge))
                    binding.tvResult.append(
                        getString(
                            R.string.txt_result3,
                            modules.joinToString("\n")
                        )
                    )

                    binding.Button.isEnabled = false
                    binding.button2.isEnabled = true
                }
            }
            else
            {
                Snackbar.make(binding.root, R.string.txt_warning_modules, Snackbar.LENGTH_LONG).show()
            }
        }

        binding.tietName.setOnClickListener()
        {
            comprobaciones()
        }

        binding.tietSurName.setOnClickListener()
        {
            comprobaciones()
        }

        binding.tietBirthDate.setOnClickListener()
        {
            comprobaciones()
        }


        binding.button2.setOnClickListener()
        {
            borrarCampos()
        }
    }

    /**
     * Método comprobaciones
     * Mñetodo que comprueba si los campos especificados están rellenados o no
     */
    fun comprobaciones()
    {
        if (binding.tietName.text.toString().trim().isBlank())
        {
            binding.tietName.error = getString(R.string.txt_warning)
            Snackbar.make(binding.root, R.string.txt_warning, Snackbar.LENGTH_LONG).show()
        }

        if (binding.tietSurName.text.toString().trim().isBlank())
        {
            binding.tietSurName.error = getString(R.string.txt_warning)
            Snackbar.make(binding.root, R.string.txt_warning, Snackbar.LENGTH_LONG).show()
        }

        if (binding.tietBirthDate.text.toString().trim().isBlank())
        {
            binding.tietBirthDate.error = getString(R.string.txt_warning)
            Snackbar.make(binding.root, R.string.txt_warning, Snackbar.LENGTH_LONG).show()
        }
    }

    /**
     * override del método onSaveInstanceState
     * Método que especifica que pasa cuando se guarda una instancia
     */
    @SuppressLint("SuspiciousIndentation")
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            if (!binding.tvResult.text.isNullOrEmpty())
                putString("RESULTADO", binding.tvResult.text.toString())
                putString("Validar",binding.Button.isEnabled.toString())
                putString("Reiniciar",binding.button2.isEnabled.toString())
        }
        super.onSaveInstanceState(outState)
    }

    /**
     * override del método onRestoreInstanceState
     * Método que especifica que pasa cuando se restaura una instancia
     */
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.run {
            if (containsKey("RESULTADO"))
                binding.tvResult.setText(getString("RESULTADO"))
            if (containsKey("Validar"))
                binding.Button.isEnabled = (getString("Validar").toBoolean())
            if (containsKey("Reiniciar"))
                binding.button2.isEnabled =(getString("Reiniciar").toBoolean())
        }
    }

    /**
     * Método borrarCampos
     * Método que se encarga de borrar el contenido de los campos una vez se rellena el formulario
     */
    private fun borrarCampos()
    {
        binding.tietName.text = null
        binding.tietSurName.text = null
        binding.tietBirthDate.text = null
        binding.rbNo.isChecked = true
        binding.swTurn.isChecked = false
        binding.cbADA.isChecked = false
        binding.cbDI.isChecked = false
        binding.cbPMDM.isChecked = false
        binding.cbPSP.isChecked = false
        binding.cbSGE.isChecked = false
        binding.tvResult.text = null
        binding.Button.isEnabled = true
        binding.button2.isEnabled = false
    }
}