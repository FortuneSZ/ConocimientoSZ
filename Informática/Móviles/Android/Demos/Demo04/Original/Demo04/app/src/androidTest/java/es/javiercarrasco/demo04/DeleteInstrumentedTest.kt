package es.javiercarrasco.demo04

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import es.javiercarrasco.demo04.data.SupersDao
import es.javiercarrasco.demo04.data.SupersDataSource
import es.javiercarrasco.demo04.data.SupersDatabase
import es.javiercarrasco.demo04.data.SupersRepository
import es.javiercarrasco.demo04.model.SuperHero
import es.javiercarrasco.demo04.ui.main.MainViewModel
import junit.framework.TestCase.assertNull
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.jvm.java

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class DeleteInstrumentedTest {
    private val TAG = DeleteInstrumentedTest::class.java.simpleName
    private lateinit var dao: SupersDao
    private lateinit var db: SupersDatabase

    private val supersList = kotlin.collections.mutableListOf<SuperHero>()

    // Antes de ejecutar los tests, se crea la base de datos en memoria.
    @Before
    fun onCreateDB() = kotlinx.coroutines.runBlocking {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, SupersDatabase::class.java)
            .allowMainThreadQueries().build()
        dao = db.supersDao()

        for (i in 1..10) {
            supersList.add(
                SuperHero(
                    idSuper = i,
                    superName = "SuperName $i",
                    realName = "RealName $i",
                    favorite = 0,
                    idEditorial = 0
                )
            )
        }

        // Se insertan los datos de prueba en la BD.
        supersList.forEach {
            dao.insertSuperHero(it)
        }
    }

    // Se cierra la BD después de ejecutar los tests.
    @After
    fun closeDb() {
        db.close()
    }

    // Test para borrar un superhéroe de la BD.
    @Test
    fun deleteSuperheroFromMainViewModel() = runTest {
        val ds = SupersDataSource(dao)
        val repository = SupersRepository(ds)
        val vm = MainViewModel(repository)

        vm.deleteSuperHero(supersList[0])
        val result = dao.getSuperById(1)

        assertNull(result)
    }
}