package es.javiercarrasco.demo04.data

import es.javiercarrasco.demo04.model.Editorial
import es.javiercarrasco.demo04.model.SuperHero
import es.javiercarrasco.demo04.model.SupersWithEditorial
import kotlinx.coroutines.flow.Flow

class SupersDataSource(val db: SupersDao) {
    val allSuperHeros: Flow<List<SuperHero>> = db.getAllSuperHeros()
    val allSupersWithEditorials: Flow<List<SupersWithEditorial>> = db.getSuperHerosWithEditorials()
    val allEditorials: Flow<List<Editorial>> = db.getAllEditorials()
    val numEditorials: Flow<Int> = db.getNumEditorials()

    suspend fun insertSuperHero(superHero: SuperHero) {
        db.insertSuperHero(superHero)
    }

    suspend fun getSuperById(idSuper: Int): SupersWithEditorial? {
        return db.getSuperById(idSuper)
    }

    suspend fun getEditorialById(editorialId: Int) = db.getEditorialById(editorialId)

    suspend fun insertEditorial(editorial: Editorial) {
        db.insertEditorial(editorial)
    }
}