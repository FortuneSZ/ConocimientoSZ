package es.javiercarrasco.demo04.ui.main

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import es.javiercarrasco.demo04.MyRoomApplication
import es.javiercarrasco.demo04.R
import es.javiercarrasco.demo04.adapters.SupersAdapter
import es.javiercarrasco.demo04.data.SupersDataSource
import es.javiercarrasco.demo04.data.SupersRepository
import es.javiercarrasco.demo04.databinding.ActivityMainBinding
import es.javiercarrasco.demo04.databinding.LayoutEditorialBinding
import es.javiercarrasco.demo04.ui.superhero.SupersActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val vm: MainViewModel by viewModels {
        val db = (application as MyRoomApplication).supersDatabase
        val dataSource = SupersDataSource(db.supersDao())
        val repository = SupersRepository(dataSource)
        MainViewModelFactory(repository)
    }

    private val adapter = SupersAdapter(
        onClickSuperHero = { supersWithEditorial ->
            SupersActivity.navigate(this, supersWithEditorial.superHero.idSuper)
        },
        favListener = { supersWithEditorial ->
            vm.updateFavorite(supersWithEditorial)
        }
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.mToolbar.inflateMenu(R.menu.menu)

        binding.recyclerView.adapter = adapter

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                vm.currentSuperHero.collect {
                    adapter.submitList(it)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        // Set the listener for the menu item.
        binding.mToolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.opt_add_editorial -> {
                    addEditorial()
                    true
                }

                R.id.opt_add_superhero -> {
                    lifecycleScope.launch {
                        vm.numEditorials.collect {
                            Log.i("MainActivity", "Number of Editorials: $it")
                            if (it > 0)
                                SupersActivity.navigate(this@MainActivity)
                            else Toast.makeText(
                                this@MainActivity,
                                "No hay editoriales creadas",
                                Toast.LENGTH_SHORT
                            ).show()

                            this.cancel()
                        }
                    }

                    true
                }

                else -> false
            }
        }
    }

    // Method to add a new Editorial.
    private fun addEditorial() {
        val bindDialog = LayoutEditorialBinding.inflate(layoutInflater)
        val dialog = MaterialAlertDialogBuilder(this).apply {
            setTitle(R.string.txt_opt_add_editorial)
            setView(bindDialog.root)
            setPositiveButton(android.R.string.ok, null)
            setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
        }.create()

        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                val name = bindDialog.tietEditorialName.text.toString()

                // Check if name is empty.
                if (name.isEmpty()) {
                    bindDialog.tilEditorialName.error = getString(R.string.txt_empty_field)
                } else {
                    Log.i("Dialog Editorial", "Name: $name")
                    vm.saveEditorial(name.trim()) // <-- Save Editorial
                    dialog.dismiss()
                }
            }
        }

        dialog.show()
    }
}