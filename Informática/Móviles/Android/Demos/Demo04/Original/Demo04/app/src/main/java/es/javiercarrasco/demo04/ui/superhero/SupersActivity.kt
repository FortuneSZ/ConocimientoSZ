package es.javiercarrasco.demo04.ui.superhero

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import es.javiercarrasco.demo04.MyRoomApplication
import es.javiercarrasco.demo04.R
import es.javiercarrasco.demo04.data.SupersDataSource
import es.javiercarrasco.demo04.data.SupersRepository
import es.javiercarrasco.demo04.databinding.ActivitySupersBinding
import es.javiercarrasco.demo04.model.SuperHero
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

class SupersActivity : AppCompatActivity() {
    private val TAG = SupersActivity::class.java.simpleName
    private lateinit var binding: ActivitySupersBinding

    private var editorialIdAux = 0

    private val vm: SupersViewModel by viewModels {
        val db = (application as MyRoomApplication).supersDatabase
        val dataSource = SupersDataSource(db.supersDao())
        val repository = SupersRepository(dataSource)
        val superIdAux = intent.getIntExtra(SUPER_ID, 0)
        SupersViewModelFactory(repository, superIdAux)
    }

    companion object {
        const val SUPER_ID = "super_id"

        fun navigate(activity: AppCompatActivity, superId: Int = 0) {
            val intent = Intent(activity, SupersActivity::class.java).apply {
                putExtra(SUPER_ID, superId)
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            }
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySupersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEditorial.setOnClickListener {
            showEditorials()
        }

        binding.btnSave.setOnClickListener {
            val superHero = SuperHero(
                superName = binding.tiedSuperName.text.toString().trim(),
                realName = binding.tiedRealName.text.toString().trim(),
                favorite = if (binding.cbFavorite.isChecked) 1 else 0,
                idEditorial = editorialIdAux
            )
            vm.saveSuper(superHero)
            finish()
        }

        lifecycleScope.launch {
            // IMPORTANTE: El repeatOnLifecycle + collect, suspende al finalizar,
            // por lo que aquello que quede por debajo no se ejecutará.
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                combine(vm.stateSupers, vm.stateEditorial) { superHero, editorial ->
                    Log.d(TAG, "superHero: $superHero")
                    Log.d(TAG, "editorial: $editorial")
                    binding.tiedSuperName.setText(superHero.superName)
                    binding.tiedRealName.setText(superHero.realName)
                    binding.cbFavorite.isChecked = superHero.favorite == 1

                    editorialIdAux = editorial.idEd
                    binding.tvEditorial.text = editorial.name
                }.collect()

//                launch {
//                    vm.stateSupers.collect {
//                        binding.tiedSuperName.setText(it.superName)
//                        binding.tiedRealName.setText(it.realName)
//                        binding.cbFavorite.isChecked = it.favorite == 1
//                    }
//                }
//                launch {
//                    vm.stateEditorial.collect {
//                        editorialIdAux = it.idEd
//                        binding.tvEditorial.text = it.name
//                    }
//                }
            }
        }
    }

    private fun showEditorials() {
        lifecycleScope.launch {
            vm.allEditorials.collect {
                MaterialAlertDialogBuilder(this@SupersActivity).apply {
                    setTitle(getString(R.string.txt_editorial))
                    setItems(it.map { it.name }.toTypedArray()) { dialog, which ->
                        editorialIdAux = it[which].idEd
                        binding.tvEditorial.text = it[which].name
                        dialog.dismiss()
                    }
                }.show()
            }
        }
    }
}