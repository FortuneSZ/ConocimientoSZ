package es.javiercarrasco.demo04.data

import es.javiercarrasco.demo04.model.Editorial
import es.javiercarrasco.demo04.model.SuperHero
import es.javiercarrasco.demo04.model.SupersWithEditorial

class SupersRepository(val dataSource: SupersDataSource) {
    val allSuperHeros = dataSource.allSuperHeros
    val allSupersWithEditorials = dataSource.allSupersWithEditorials
    val allEditorials = dataSource.allEditorials
    val numEditorials = dataSource.numEditorials

    suspend fun insertSuperHero(superHero: SuperHero) {
        dataSource.insertSuperHero(superHero)
    }

    suspend fun getSuperById(idSuper: Int): SupersWithEditorial? {
        return dataSource.getSuperById(idSuper)
    }

    suspend fun getEditorialById(editorialId: Int) = dataSource.getEditorialById(editorialId)

    suspend fun insertEditorial(editorial: Editorial) {
        dataSource.insertEditorial(editorial)
    }
}