package es.javiercarrasco.demo04.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import es.javiercarrasco.demo04.R
import es.javiercarrasco.demo04.databinding.ItemSupersBinding
import es.javiercarrasco.demo04.model.SupersWithEditorial

class SupersAdapter(
    private val onClickSuperHero: (SupersWithEditorial) -> Unit,
    private val favListener: (SupersWithEditorial) -> Unit
) : ListAdapter<SupersWithEditorial, SupersAdapter.SupersWithEditorialViewHolder>(
    ModulesDiffCallback()
) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SupersWithEditorialViewHolder {
        return SupersWithEditorialViewHolder(
            ItemSupersBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )
    }

    override fun onBindViewHolder(holder: SupersWithEditorialViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class SupersWithEditorialViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemSupersBinding.bind(view)
        fun bind(supersWithEditorial: SupersWithEditorial) {
            binding.tvSuperName.text = supersWithEditorial.superHero.superName
            binding.tvEditorial.text = supersWithEditorial.editorial.name

            binding.ivFav.setImageState(
                intArrayOf(R.attr.state_fav_on),
                supersWithEditorial.superHero.favorite == 1
            )

            binding.ivFav.setOnClickListener {
                favListener(supersWithEditorial)
            }

            itemView.setOnClickListener {
                onClickSuperHero(supersWithEditorial)
            }
        }
    }
}

class ModulesDiffCallback : DiffUtil.ItemCallback<SupersWithEditorial>() {

    override fun areItemsTheSame(
        oldItem: SupersWithEditorial,
        newItem: SupersWithEditorial
    ): Boolean {
        return oldItem.superHero.idSuper == newItem.superHero.idSuper
    }

    override fun areContentsTheSame(
        oldItem: SupersWithEditorial,
        newItem: SupersWithEditorial
    ): Boolean {
        return oldItem == newItem
    }
}
