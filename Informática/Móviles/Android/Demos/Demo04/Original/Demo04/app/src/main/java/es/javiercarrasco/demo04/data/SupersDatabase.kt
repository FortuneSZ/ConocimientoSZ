package es.javiercarrasco.demo04.data

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.Transaction
import es.javiercarrasco.demo04.model.Editorial
import es.javiercarrasco.demo04.model.SuperHero
import es.javiercarrasco.demo04.model.SupersWithEditorial
import kotlinx.coroutines.flow.Flow

@Database(entities = [SuperHero::class, Editorial::class], version = 1)
abstract class SupersDatabase : RoomDatabase() {
    abstract fun supersDao(): SupersDao
}

@Dao
interface SupersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEditorial(editorial: Editorial)

    @Transaction
    @Query("SELECT * FROM SuperHero ORDER BY superName")
    fun getSuperHerosWithEditorials(): Flow<List<SupersWithEditorial>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSuperHero(superHero: SuperHero)

    @Query("SELECT * FROM SuperHero WHERE idSuper = :idSuper")
    suspend fun getSuperById(idSuper: Int): SupersWithEditorial?

    @Query("SELECT * FROM SuperHero")
    fun getAllSuperHeros(): Flow<List<SuperHero>>

    @Query("SELECT * FROM Editorial")
    fun getAllEditorials(): Flow<List<Editorial>>

    @Query("SELECT count(idEd) FROM Editorial")
    fun getNumEditorials(): Flow<Int>

    @Query("SELECT * FROM Editorial WHERE idEd = :editorialId")
    suspend fun getEditorialById(editorialId: Int): Editorial?
}
