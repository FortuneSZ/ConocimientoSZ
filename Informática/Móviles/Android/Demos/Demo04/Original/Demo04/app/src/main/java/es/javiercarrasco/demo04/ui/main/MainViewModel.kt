package es.javiercarrasco.demo04.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import es.javiercarrasco.demo04.data.SupersRepository
import es.javiercarrasco.demo04.model.Editorial
import es.javiercarrasco.demo04.model.SupersWithEditorial
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class MainViewModel(val supersRepository: SupersRepository) : ViewModel() {
    val numEditorials: Flow<Int> = supersRepository.numEditorials
    val currentSuperHero: Flow<List<SupersWithEditorial>> = supersRepository.allSupersWithEditorials

    fun saveEditorial(name: String) {
        val editorial = Editorial(0, name)
        viewModelScope.launch {
            supersRepository.insertEditorial(editorial)
        }
    }

    fun updateFavorite(supersWithEditorial: SupersWithEditorial) {
        viewModelScope.launch {
            val superAux = supersWithEditorial.superHero.copy(
                favorite = if (supersWithEditorial.superHero.favorite == 1) 0 else 1
            )
            supersRepository.insertSuperHero(superAux)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(val supersRepository: SupersRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(supersRepository) as T
    }
}