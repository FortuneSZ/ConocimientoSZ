package es.javiercarrasco.demo04.ui.superhero

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import es.javiercarrasco.demo04.data.SupersRepository
import es.javiercarrasco.demo04.model.Editorial
import es.javiercarrasco.demo04.model.SuperHero
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SupersViewModel(
    val supersRepository: SupersRepository, val superId: Int
) : ViewModel() {
    private val _stateSupers = MutableStateFlow(SuperHero())
    val stateSupers: StateFlow<SuperHero> = _stateSupers.asStateFlow()

    private val _stateEditorial = MutableStateFlow(Editorial())
    val stateEditorial: StateFlow<Editorial> = _stateEditorial.asStateFlow()

    val allEditorials = supersRepository.allEditorials

    init {
        viewModelScope.launch {
            val superAux = supersRepository.getSuperById(superId)
            if (superAux != null) {
                _stateSupers.value = superAux.superHero
                _stateEditorial.value = superAux.editorial
            }
        }
    }

    fun saveSuper(superHero: SuperHero) {
        viewModelScope.launch {
            val superAux = _stateSupers.value.copy(
                superName = superHero.superName,
                realName = superHero.realName,
                favorite = superHero.favorite,
                idEditorial = superHero.idEditorial
            )
            supersRepository.insertSuperHero(superAux)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class SupersViewModelFactory(
    val supersRepository: SupersRepository, val superId: Int
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SupersViewModel(supersRepository, superId) as T
    }
}