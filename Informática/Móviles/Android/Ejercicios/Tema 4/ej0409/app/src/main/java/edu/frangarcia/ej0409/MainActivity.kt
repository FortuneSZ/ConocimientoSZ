package edu.frangarcia.ej0409

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import edu.frangarcia.ej0409.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.enviar.setOnClickListener()
        {
            Snackbar.make(
                binding.root,
                binding.ratingBar.rating.toString(),
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }
}