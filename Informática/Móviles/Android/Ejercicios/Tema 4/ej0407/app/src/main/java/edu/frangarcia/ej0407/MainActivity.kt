package edu.frangarcia.ej0407

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import edu.frangarcia.ej0407.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()
{

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Glide.with(this)
        .load("http://www.hci.uniovi.es/Products/DSTool/images/busqueda/estructura2.gif") // Recurso.
        .override(300, 300) // Ajusta el tamaño.
        .into(binding.imageView) // Contenedor.
    }
}