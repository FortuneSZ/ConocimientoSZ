package edu.frangarcia.ej0408

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import edu.frangarcia.ej0408.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.introducir.setOnClickListener()
        {
            if (binding.num.text.toString() == "")
            {
                Snackbar.make(
                    binding.root, // Se muestra sobre una vista.
                    "Error",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
            else
            {
                var numero:Int = binding.num.text.toString().toInt()
                if (numero %2 == 0)
                {
                    binding.primo.text = "no es un número primo"
                }
                else
                {
                    binding.primo.text = "Es un número primo"
                }
            }

        }
    }
}