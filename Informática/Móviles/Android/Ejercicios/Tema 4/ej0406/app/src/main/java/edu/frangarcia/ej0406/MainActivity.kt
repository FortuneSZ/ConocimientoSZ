package edu.frangarcia.ej0406

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import edu.frangarcia.ej0406.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var turno = ""

        binding.manyana.setOnClickListener()
        {
            turno = binding.manyana.text.toString()
        }

        binding.tarde.setOnClickListener()
        {
            turno = binding.tarde.text.toString()
        }

        binding.matricularse.setOnClickListener()
        {
            if (turno == "")
            {
                binding.manyana.error = "debes elegir un turno"
                binding.texto.visibility = View.INVISIBLE
            }
            else
            {
                binding.manyana.error = null
                var numAsignaturas = 0
                var asignaturas = arrayOf(binding.interfaces,binding.moviles,binding.empresas)
                //var asig = arrayOf("","","")
                var asig = mutableListOf("");

                for (i in 0..2)
                {
                    if (asignaturas[i].isChecked)
                    {
                        if (asig.get(0) == "")
                        {
                            asig = mutableListOf(asignaturas[i].text.toString());
                        }
                        else
                        {
                            asig.add(asignaturas[i].text.toString())
                        }
                        numAsignaturas++
                    }
                }

                if (numAsignaturas < 1)
                {
                    binding.interfaces.error = "debes elegir al menos una asignatura"
                    binding.texto.visibility = View.INVISIBLE
                }
                else
                {
                    binding.interfaces.error = null
                    binding.texto.visibility = View.VISIBLE
                    var texto = "Te has matriculado del turno de $turno, matriculándote de" +
                            " $numAsignaturas asignaturas, siendo estas "
                    for (i in 0..< asig.size)
                    {
                        texto = texto + asig.get(i)
                        if (i < asig.size-1 && asig[i] != "")
                        {
                            texto = texto + ","
                        }
                    }
                    binding.texto.text = texto
                }
            }
        }
    }
}