package edu.frangarcia.formregistro.utils

import android.content.Context
import edu.frangarcia.formregistro.FormRegistro


fun writeFile(context: Context, content: FormRegistro): Boolean
{
    var resultado = false
    try
    {
        val file = context.openFileOutput("formRegistro.csv", Context.MODE_APPEND)
        file.write(content.toString().toByteArray())
        file.flush()
        file.close()
        resultado = true
    }
    catch (e: Exception)
    {
        e.printStackTrace()
    }

    return resultado
}
