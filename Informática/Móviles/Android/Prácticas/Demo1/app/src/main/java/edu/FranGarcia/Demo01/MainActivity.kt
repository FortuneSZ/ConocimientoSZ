package edu.FranGarcia.Demo01

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.google.android.material.snackbar.Snackbar
import edu.FranGarcia.Demo01.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


    }

    override fun onStart()
    {
        super.onStart()
        // Listener for the button.
        binding.Button.setOnClickListener()
        {
            binding.tvResult.text = null
            val tName = binding.tietName.text.toString().trim()
            val tsurname = binding.tietSurName.text.toString().trim();
            val tbirthdate = binding.tietBirthDate.text.toString().trim();

            if (tName.isBlank())
            {
                binding.tietName.error = getString(R.string.txt_warning)
                //Toast.makeText(this, R.string.txt_warning, Toast.LENGTH_SHORT).show()
                Snackbar.make(binding.root, R.string.txt_warning, Snackbar.LENGTH_LONG).show()
            }
            else if (tsurname.isBlank())
            {
                binding.tietSurName.error = getString(R.string.txt_warning)
                //Toast.makeText(this, R.string.txt_warning, Toast.LENGTH_SHORT).show()
                Snackbar.make(binding.root, R.string.txt_warning, Snackbar.LENGTH_LONG).show()
            }
            else if (tbirthdate.isBlank())
            {
                binding.tietBirthDate.error = getString(R.string.txt_warning)
                //Toast.makeText(this, R.string.txt_warning, Toast.LENGTH_SHORT).show()
                Snackbar.make(binding.root, R.string.txt_warning, Snackbar.LENGTH_LONG).show()
            }
            else
            {
                binding.tietName.error = null
                val moduleCheck = with(binding)
                {
                    cbADA.isChecked || cbDI.isChecked || cbPMDM.isChecked ||
                            cbPSP.isChecked || cbSGE.isChecked
                }
                if (moduleCheck)
                {
                    binding.tvResult.text = getString(
                        R.string.txt_result1,
                        tName,
                        binding.tietSurName.text.toString().trim(),
                        binding.tietBirthDate.text.toString().trim()
                    )
                    val carnet = if (binding.rbNo.isChecked) getString(R.string.txt_no) else
                        getString(R.string.txt_yes)
                    val turn = if (binding.swTurn.isChecked) getString(R.string.txt_turn) else
                        getString(R.string.txt_morning)
                    binding.tvResult.append(
                        getString(
                            R.string.txt_result2,
                            carnet, turn
                        )
                    )
                    binding.Button.isEnabled = false;
                    binding.button2.isEnabled = true;
                }
            }

            val modules = mutableListOf<String>()
            if (binding.cbADA.isChecked) modules.add(getString(R.string.txt_ada))
            if (binding.cbDI.isChecked) modules.add(getString(R.string.txt_di))
            if (binding.cbPMDM.isChecked) modules.add(getString(R.string.txt_pmdm))
            if (binding.cbPSP.isChecked) modules.add(getString(R.string.txt_psp))
            if (binding.cbSGE.isChecked) modules.add(getString(R.string.txt_sge))
            binding.tvResult.append(
                getString(
                    R.string.txt_result3,
                    modules.joinToString("\n")
                )
            )
        }

        binding.button2.setOnClickListener()
        {
            borrarCampos();
        }
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            if (!binding.tvResult.text.isNullOrEmpty())
                putString("RESULTADO", binding.tvResult.text.toString())
                putString("Validar",binding.Button.isEnabled.toString())
                putString("Reiniciar",binding.button2.isEnabled.toString())
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.run {
            if (containsKey("RESULTADO"))
                binding.tvResult.setText(getString("RESULTADO"))
            if (containsKey("Validar"))
                binding.Button.isEnabled = (getString("Validar").toBoolean())
            if (containsKey("Reiniciar"))
                binding.button2.isEnabled =(getString("Reiniciar").toBoolean())
        }
    }

    private fun borrarCampos()
    {
        binding.tietName.text = null;
        binding.tietSurName.text = null;
        binding.tietBirthDate.text = null;
        binding.rbNo.isChecked = true;
        binding.swTurn.isChecked = false;
        binding.cbADA.isChecked = false;
        binding.cbDI.isChecked = false;
        binding.cbPMDM.isChecked = false;
        binding.cbPSP.isChecked = false;
        binding.cbSGE.isChecked = false;
        binding.tvResult.text = null;
        binding.Button.isEnabled = true;
        binding.button2.isEnabled = false;
    }
}