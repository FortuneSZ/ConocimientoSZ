package edu.frangarcia.formregistro

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import edu.frangarcia.formregistro.databinding.ActivityMainBinding
import edu.frangarcia.formregistro.utils.writeFile

class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.privacidad.setOnClickListener()
        {
            binding.button.isEnabled = binding.privacidad.isChecked
        }
        //Acción al hacer click en aceptar
        binding.button.setOnClickListener()
        {
            if (binding.nombreText.text!!.isNotEmpty())
                {
                    binding.nombre.error = null
                    if (binding.apellidosText.text!!.isNotEmpty())
                    {
                        binding.apellidos.error = null
                        if (binding.emailText.text!!.isNotEmpty())
                        {
                            binding.email.error = null
                            if (binding.telText.text!!.isNotEmpty() && binding.telText.text!!.length == 9)
                            {
                                binding.tel.error = null
                                val registro = FormRegistro(binding.nombreText.text.toString(),
                                    binding.apellidosText.text.toString(),
                                    binding.emailText.text.toString(),
                                    binding.telText.text.toString())

                                if (writeFile(this, registro))
                                {
                                    Snackbar.make(
                                        binding.root,
                                        resources.getString(R.string.txt_registro_ok),
                                        Snackbar.LENGTH_SHORT
                                    ).show()

                                    borrarCampos()
                                }
                                else
                                {
                                    Snackbar.make(
                                        binding.root,
                                        resources.getString(R.string.txt_registro_error),
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                            }
                            else
                            {
                                binding.tel.error = resources.getString(R.string.txt_error_phone)
                            }
                        }
                        else
                        {
                            binding.email.error = resources.getString(R.string.txt_error_correo)
                        }
                    }
                    else
                    {
                        binding.apellidos.error = resources.getString(R.string.txt_error_apellidos)
                    }
                }
            else
            {
                binding.nombre.error = resources.getString(R.string.txt_error_nombre)
            }
        }
    }
    //Función para borrar los campos una vez son correctos
    private fun borrarCampos()
    {
        binding.nombreText.text = null
        binding.apellidosText.text = null
        binding.emailText.text = null
        binding.telText.text = null
        binding.privacidad.isChecked = false
        binding.button.isEnabled = false;
    }
}