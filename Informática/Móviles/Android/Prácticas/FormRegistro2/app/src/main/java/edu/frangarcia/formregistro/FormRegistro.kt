package edu.frangarcia.formregistro

//Data class FormRegistro
data class FormRegistro(
    var nombre :String,
    var apellidos :String,
    var correo :String,
    var phone :String,
    var vip :Boolean = false)
{
    override fun toString(): String
    {
        return "$nombre;$apellidos;$correo;$phone;$vip\n"
    }

    //Función para devolver todos los campos menos vip
    fun toString2(): String
    {
        return "$nombre;$apellidos;$correo;$phone\n"
    }
}

