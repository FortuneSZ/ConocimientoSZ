package edu.frangarcia.filmlist.model

/**
 * Data class Film.kt
 * Clase que crea las películas a mostrar en la lista
 * @author Fran García
 * @param id id de la película
 * @param title título de la película
 * @param year año de la película
 * @param duration duración de la película
 * @param genre género de la película
 * @param director director de la película
 * @param cover carátula de la película
 */
data class Film(val id:Int,val title:String,val year:Int,val duration:Int
,val genre:String,val director:String,val cover:String)

