package edu.frangarcia.filmlist.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import edu.frangarcia.filmlist.model.Film
import edu.frangarcia.filmlist.databinding.FilmBinding

/**
 * Clase FilmsAdapater.kt
 * Adaptador que muestra las películas en la lista del programa principal
 * @author Fran García
 * @param filmList lista de películas
 */
class FilmsAdapter(val filmList:MutableList<Film>) : RecyclerView.Adapter<FilmsAdapter.FilmsViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : FilmsViewHolder {
        return FilmsViewHolder(
            FilmBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )
    }

    /**
     * override del Método getItemCount
     * Obtiene el número de películas de la lista
     */
    override fun getItemCount(): Int = filmList.size

    /**
     * override del Método onBindViewHolder
     */
    override fun onBindViewHolder(holder: FilmsViewHolder, position: Int) {
        holder.bind(filmList[position])
    }

    /**
     * Clase interna FilmsViewHolder
     * Clase que se encarga de crear los elementos de la lista con las películas obtenidas del fichero
     */
    inner class FilmsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = FilmBinding.bind(view)

        /**
         * Método bind
         * Añade la información de las películas a cada elemento de la lista
         * @param film película a la que añadirle los datos
         */
        fun bind(film: Film) {
            binding.FilmName.text = film.title
            binding.FilmDirector.text = film.director
            binding.FilmYear.text = film.year.toString()
            binding.FilmGenres.text = film.genre
            binding.FilmDuration.text = buildString{
                append(film.duration.toString())
                append(" min")
            }

            Glide.with(binding.root)
                .load(film.cover)
                .centerCrop()
                .into(binding.FilmCover)
        }
    }
}