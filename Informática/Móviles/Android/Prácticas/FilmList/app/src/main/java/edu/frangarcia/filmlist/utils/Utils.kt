package edu.frangarcia.filmlist.utils

/**
 * Fichero kotlin Utils que contiene funciones necesarias para el funcionamiento del programa
 * @author Fran García
 */
import android.content.Context
import android.util.Log
import edu.frangarcia.filmlist.R
import edu.frangarcia.filmlist.model.Film
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

/**
 * Método para leer un fichero RAW
 * @author Fran García
 * @param context contexto de la aplicación principal
 * @return MutableList de películas
 */
fun readRawFile(context: Context): MutableList<Film> {
    val films: MutableList<Film> = mutableListOf();
    try {
        val entrada = InputStreamReader(
            context.resources.openRawResource(R.raw.films)
        )

        val br = BufferedReader(entrada)
        var linea = br.readLine()

        while (!linea.isNullOrEmpty()) {
            var parts: List<String>;
            parts = linea.split(';');
            films.add(
                Film(
                    parts.get(0).toInt(),
                    parts.get(1),
                    parts.get(2).toInt(),
                    parts.get(3).toInt(),
                    parts.get(4),
                    parts.get(5),
                    parts.get(6)
                )
            )
            linea = br.readLine()
        }
        br.close()
        entrada.close()
    } catch (e: IOException) {
        Log.e("ERROR IO", e.message.toString())
    }
    return films;
}