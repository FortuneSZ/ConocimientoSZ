package edu.frangarcia.filmlist

import android.os.Bundle
import android.view.View
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import edu.frangarcia.filmlist.adapters.FilmsAdapter
import edu.frangarcia.filmlist.databinding.ActivityMainBinding
import edu.frangarcia.filmlist.model.Film
import edu.frangarcia.filmlist.utils.readRawFile

/**
 * Clase MainActivity.kt
 * Actividad principal que muestra una lista de películas, las cuales pdoemos borrar con una pulsación prolongada
 * @author Fran García
 */
class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding
    private lateinit var filmsAdapter: FilmsAdapter
    private var filmList: MutableList<Film> = mutableListOf();
    private var recentlyDeletedFilm: Film? = null
    private var recentlyDeletedPosition: Int = -1
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        filmList = readRawFile(this);

        filmsAdapter = FilmsAdapter(
            filmList)


        binding.ReciclerFilmView.adapter = filmsAdapter

        setupItemTouchHelper();

        if (filmList.isEmpty()) {
            Snackbar.make(binding.ReciclerFilmView, getString(R.string.warning_no_films), Snackbar.LENGTH_LONG).show()
        }

    }

    /**
     * Método setupItemTouchHelper
     * Método que se encarga de la gestión del borrado de películas de la lista
     */
    private fun setupItemTouchHelper()
    {
        val callback = object : androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback(0, androidx.recyclerview.widget.ItemTouchHelper.LEFT)
        {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean = false

            /**
             * override del Método onSwiped
             * Método que se encarga de gestionar que ocurre al deslizar un elemento de la lista
             */
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int)
            {
                val position = viewHolder.adapterPosition
                deleteFilm(position)
            }
        }

        val itemTouchHelper = androidx.recyclerview.widget.ItemTouchHelper(callback)
        itemTouchHelper.attachToRecyclerView(binding.ReciclerFilmView)
    }

    /**
     * Método deleteFilm
     * Método que se encarga de eliminar el elemento de la lista cuando es desplazado
     */
    private fun deleteFilm(position: Int)
    {
        recentlyDeletedFilm = filmList[position]
        recentlyDeletedPosition = position
        filmList.removeAt(position)
        filmsAdapter.notifyItemRemoved(position)
        showUndoSnackbar()
        checkIfEmpty()
    }

    /**
     * Método showUndoSnackbar
     * Método que se encarga de mostrar un snackbar informando de la eliminación del elemento
     */
    private fun showUndoSnackbar()
    {
        Snackbar.make(binding.root, getString(R.string.txt_film_deleted, recentlyDeletedFilm?.title), Snackbar.LENGTH_LONG)
            .setAction(getString(R.string.txt_undo)) {
                undoDelete()
            }
            .show()
    }

    /**
     * Método undoDelete
     * Método que se encarga de deshacer la eliminación del elemento borrado si se selecciona dicha opción en la snackbar
     */
    private fun undoDelete()
    {
        recentlyDeletedFilm?.let {
            filmList.add(recentlyDeletedPosition, it)
            filmsAdapter.notifyItemInserted(recentlyDeletedPosition)
            checkIfEmpty()
        }
    }

    /**
     * Método checkIfEmpty
     * Método que comprueba si la lista de películas está vacía
     */
    private fun checkIfEmpty()
    {
        if (filmList.isEmpty())
        {
            binding.ReciclerFilmView.visibility = View.GONE
            binding.txtEmpty.visibility = View.VISIBLE
        } else
        {
            binding.ReciclerFilmView.visibility = View.VISIBLE
            binding.txtEmpty.visibility = View.GONE
        }
    }

}