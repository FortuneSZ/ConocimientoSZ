/*This program will ask the user to introduce a number
until he introduces a 0, then it will show the number
of total inputs (not counting the 0 input)*/
import java.util.Scanner;
public class InputCount
{
    public static int InputCounter()
    {
        int InputNum = 0, counter = 0;
        Scanner sc = new Scanner(System.in);

        do
        {
            System.out.print("Introduce a number:");
            InputNum = sc.nextInt();


            if (InputNum != 0)
            {
                counter++;
            }
        }
        while (InputNum != 0);

        return counter;
    }
    public static void main(String[] args)
    {
        int InputCount;

        InputCount = InputCounter();
        System.out.printf("The total number of inputs is: %d",InputCount);
    }
}
