Create a program that asks the user double numbers until the word “end” is input and then it has to print the minimum, the maximum, 
the addition and the average in each step.

For example:
Number? 5
min= 5 max=5 add=5 avg= 5
Number? 4
Min = 4 max=5 add=9 avg=4,5
Number? end
