/*This program will ask for numbers until the user inserts
"end", on every step it will show the minimum, the maximum,
the addition and the average*/
import java.util.Scanner;
public class MinMaxAddAvg
{
    public static double Min(double min, double number)
    {
        if (min == 0)
        {
            min = number;
        }
        if (number < min)
        {
            min = number;
        }
        return min;
    }

    public static double Max(double max, double number)
    {
        if (max == 0)
        {
            max = number;
        }
        if (number > max)
        {
            max = number;
        }
        return max;
    }

    public static double Add(double add, double number)
    {
        add += number;

        return add;
    }

    public static double Avg(double avg, double add, int insertednums)
    {
        avg = add/insertednums;
        return avg;
    }
    public static void main(String[] args)
    {
        String insert;
        int insertednums = 0;
        double number, min = 0, max = 0, add = 0, avg = 0;
        Scanner sc = new Scanner(System.in);

        do
        {
            System.out.println("number?");
            insert = sc.nextLine();
            if (!insert.equals("end"))
            {
                number = Double.parseDouble(insert);
                insertednums++;

                min = Min(min,number);
                max = Max(max,number);
                add = Add(add,number);
                avg = Avg (avg,add,insertednums);

                System.out.printf("Min = %1.0f Max = %1.0f Add = %1.0f" +
                                " Avg = %1.1f %n",min,max,add,avg);
            }
        }
        while (!insert.equals("end"));

    }
}
