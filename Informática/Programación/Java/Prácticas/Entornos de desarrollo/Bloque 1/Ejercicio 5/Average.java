/*This program will ask the user how many marks is going to
introduce, then it will ask the user for these marks and fill
and array with them.

Later will calculate the average of these marks and will show
the marks that are greater than the average*/
import java.util.Scanner;
public class Average
{
    public static double[] FillArray(double marks,Scanner sc)
    {

        double[] array = new double[(int)marks];

        System.out.println("Introduce the marks on one line");
        for (int i = 0; i < marks; i++)
        {
            array[i] = sc.nextDouble();
        }
        return array;
    }
    public static double Average(double[] marks)
    {
        double avg = 0;
        for (int i = 0; i < marks.length; i++)
        {
            avg += marks[i];
        }
        avg /= marks.length;
        return avg;
    }
    public static void main(String[] args)
    {
        int marks;
        double avg;
        double[] markArray;
        Scanner sc = new Scanner(System.in);


        System.out.println("How many marks are you going to introduce");
        marks = sc.nextInt();
        markArray = FillArray(marks,sc);
        avg = Average(markArray);

        System.out.printf("The average is: %1.0f, the marks greater than the" +
                " average are: ",avg);
        for (int i = 0; i < markArray.length; i++)
        {
            if (markArray[i] > avg)
            {
                System.out.printf("%1.0f ",markArray[i]);
            }
        }
    }
}
