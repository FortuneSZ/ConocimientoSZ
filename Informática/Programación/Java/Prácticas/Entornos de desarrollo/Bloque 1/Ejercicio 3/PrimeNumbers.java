/*This program will ask the user to insert a number, then the program
will show every prime number between 1 and the specified number*/
import java.util.Scanner;
public class PrimeNumbers
{
    public static void Prime(int num)
    {
        int counter = 0;
        boolean found = false;
        System.out.printf("Prime numbers until %d are: ",num);
        for (int i = 2; i < num; i++)
        {
            counter = 0;
            for (int x = 1; x < num; x++)
            {
                if (i % x == 0)
                {
                    counter++;
                }
            }
            if (counter == 2)
            {
                if (found == true)
                {
                    System.out.print(",");
                }
                found = true;
                System.out.print(i);
            }
        }
    }
    public static void main(String[] args)
    {
        int num;
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert a number");
        num = sc.nextInt();
        Prime(num);
    }
}
