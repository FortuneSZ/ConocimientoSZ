/*This program will ask the user how many marks is going to
introduce, then it will ask the user for these marks and the
student that have them, with the format "student mark"and fill
two arrays with them, students array and marks array.

Later will calculate the average of these marks and will show
the students with marks that are greater than the average*/
import java.util.Scanner;
public class StudentAverage
{

    public static void FillArray(double[] markArray, String[] stuArray,
                                 int marks,Scanner sc)
    {
        System.out.println("Introduce the marks using this format:");
        System.out.println("student mark");
        for (int i = 0; i < marks; i++)
        {
            stuArray[i] = sc.next();
            markArray[i] = sc.nextDouble();
        }

    }
    public static double Average(double[] marks)
    {
        double avg = 0;
        for (int i = 0; i < marks.length; i++)
        {
            avg += marks[i];
        }
        avg /= marks.length;
        return avg;
    }
    public static void main(String[] args)
    {
        int marks;
        String[] studentArray;
        double avg;
        double[] markArray;
        Scanner sc = new Scanner(System.in);

        System.out.println("How many marks are you going to introduce");
        marks = sc.nextInt();
        studentArray = new String[marks];
        markArray = new double[marks];
        FillArray(markArray,studentArray,marks,sc);
        avg = Average(markArray);

        System.out.printf("The average is: %1.1f %nThe student with marks" +
                " greater than the average are: %n",avg);
        for (int i = 0; i < markArray.length; i++)
        {
            if (markArray[i] > avg)
            {
                System.out.printf("-%s %n",studentArray[i]);
            }
        }
    }
}
