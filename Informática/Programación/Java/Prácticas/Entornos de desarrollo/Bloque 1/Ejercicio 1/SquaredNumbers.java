/*This programa will ask the user to insert the number
of squared number they want to see, then the program will
show that number of squared numbers, starting from 1*/
import java.util.Scanner;
public class SquaredNumbers
{
    public static void ShowSquaredNumbers(int SquaredNum)
    {
        System.out.printf("The fist %d squared numbers are: ",SquaredNum);

        for (int i = 1; i <= SquaredNum; i++)
        {
            System.out.printf("%d",i * i);
            if (i != SquaredNum)
            {
                System.out.print(", ");
            }
        }
    }
    public static void main(String[] args)
    {
        int SquaredNum;
        Scanner sc = new Scanner(System.in);

        System.out.println("Write the number of squared " +
                "numbers you want to see");
        SquaredNum = sc.nextInt();

        ShowSquaredNumbers(SquaredNum);
    }
}
