import java.util.Random;

public class Main
{
    public static void main(String[] args)
    {

        Window[] windows = new Window[10];
        Random r = new Random();

        for (int i = 0; i < windows.length; i++)
        {
            String name = "Window ";
            name += i+1;
            windows[i] = new Window(name,90 + r.nextInt(31),
                    40 + r.nextInt(61));
        }

        for (int i = 0; i < windows.length; i++)
        {
            System.out.println(windows[i]);
        }
    }
}