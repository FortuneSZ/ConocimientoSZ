public class Tanker extends Ship
{
    private Boolean doubleHelmet;
    private int capacity;

    public Boolean getDoubleHelmet()
    {
        return doubleHelmet;
    }

    public void setDoubleHelmet(Boolean doubleHelmet)
    {
        this.doubleHelmet = doubleHelmet;
    }

    public int getCapacity()
    {
        return capacity;
    }

    public void setCapacity(int capacity)
    {
        this.capacity = capacity;
    }

    public Tanker(String name, int anchors, String material, int length, Boolean doubleHelmet, int capacity)
    {
        super(name, anchors, material, length);
        this.doubleHelmet = doubleHelmet;
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Tanker{" +
                ", name='" + name + '\'' +
                ", anchors=" + anchors +
                ", material='" + material + '\'' +
                ", length=" + length +
                "doubleHelmet=" + doubleHelmet +
                ", capacity=" + capacity +
                '}';
    }
}
