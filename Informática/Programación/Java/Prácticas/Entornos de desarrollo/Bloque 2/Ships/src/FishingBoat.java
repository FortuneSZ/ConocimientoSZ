public class FishingBoat extends Ship
{
    private String type;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public FishingBoat(String name, int anchors, String material, int length, String type)
    {
        super(name, anchors, material, length);
        this.type = type;
    }

    @Override
    public String toString() {
        return "FishingBoat{" +
                ", name='" + name + '\'' +
                ", anchors=" + anchors +
                ", material='" + material + '\'' +
                ", length=" + length +
                "type='" + type + '\'' +
                '}';
    }
}
