public class BulkCarrier extends Ship
{
    private int loadHatches;

    public int getLoadHatches()
    {
        return loadHatches;
    }

    public void setLoadHatches(int loadHatches)
    {
        this.loadHatches = loadHatches;
    }

    public BulkCarrier(String name, int anchors, String material, int length, int loadHatches)
    {
        super(name, anchors, material, length);
        this.loadHatches = loadHatches;
    }

    @Override
    public String toString() {
        return "BulkCarrier{" +
                ", name='" + name + '\'' +
                ", anchors=" + anchors +
                ", material='" + material + '\'' +
                ", length=" + length +
                "loadHatches=" + loadHatches +
                '}';
    }
}
