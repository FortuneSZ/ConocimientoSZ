public class Ship
{
    protected String name;
    protected int anchors;
    protected String material;
    protected int length;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAnchors()
    {
        return anchors;
    }

    public void setAnchors(int anchors)
    {
        this.anchors = anchors;
    }

    public String getMaterial()
    {
        return material;
    }

    public void setMaterial(String material)
    {
        this.material = material;
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int length)
    {
        this.length = length;
    }

    public Ship(String name, int anchors, String material, int length)
    {
        this.name = name;
        this.anchors = anchors;
        this.material = material;
        this.length = length;
    }

    @Override
    public String toString()
    {
        return "Ship{" +
                "name='" + name + '\'' +
                ", anchors=" + anchors +
                ", material='" + material + '\'' +
                ", length=" + length +
                '}';
    }
}
