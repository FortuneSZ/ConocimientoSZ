public class Container extends Ship
{
    private int capacityTEU;

    public int getCapacityTEU()
    {
        return capacityTEU;
    }

    public void setCapacityTEU(int capacityTEU)
    {
        this.capacityTEU = capacityTEU;
    }

    public Container(String name, int anchors, String material, int length, int capacityTEU)
    {
        super(name, anchors, material, length);
        this.capacityTEU = capacityTEU;
    }

    @Override
    public String toString() {
        return "Container{" +

                ", name='" + name + '\'' +
                ", anchors=" + anchors +
                ", material='" + material + '\'' +
                ", length=" + length +
                "capacityTEU=" + capacityTEU +
                '}';
    }
}
