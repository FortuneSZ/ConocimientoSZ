public class Main
{
    public static void main(String[] args)
    {
        Ship[] ships = new Ship[5];
        ships[0] = new Ship("My ship", 2, "Wood", 200);
        ships[1] = new Container("My container",3, "Metal",300,10);
        ships[2] = new BulkCarrier("My carrier", 1, "Metal", 150,50);
        ships[3] = new Tanker("My tanker", 2, "Fiberglass", 250,true, 100);
        ships[4] = new FishingBoat("My boat", 1, "Wood", 100,"Artisan");

        for (int i = 0; i < ships.length; i++)
        {
            System.out.println(ships[i]);
        }
    }
}