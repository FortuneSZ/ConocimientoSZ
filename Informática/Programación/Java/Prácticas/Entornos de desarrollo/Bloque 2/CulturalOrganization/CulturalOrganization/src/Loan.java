public class Loan
{
    private String startDate;
    private String endDate;
    private CulturalObject object;
    private User user;

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public CulturalObject getObject()
    {
        return object;
    }

    public void setObject(CulturalObject object)
    {
        this.object = object;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Loan(String startDate, String endDate, CulturalObject object, User user)
    {
        this.startDate = startDate;
        this.endDate = endDate;
        this.object = object;
        this.user = user;
    }
}
