import java.util.Arrays;

public class User
{
    private String id;
    private String name;
    private Loan[] loans = new Loan[MAX_LOANS];
    private int numLoans = 0;
    public static final int MAX_LOANS = 5;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Loan[] getLoans()
    {
        return loans;
    }

    public void setLoans(Loan[] loans)
    {
        this.loans = loans;
    }

    public int getNumLoans()
    {
        return numLoans;
    }

    public void setNumLoans(int numLoans)
    {
        this.numLoans = numLoans;
    }

    public User(String id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public void addLoan(Loan loan)
    {
        loans[numLoans] = loan;
        numLoans++;
    }
    public void removeLoan(Loan loan)
    {
        numLoans--;
    }

    @Override
    public String toString()
    {
        return "Id: " + id + ", name: " + name +", NumLoans: " + numLoans;
    }
}
