public class Main
{
    public static void main(String[] args)
    {
        CulturalObject[] objects = new CulturalObject[3];
        objects[0] = new Book(1,"Ender´s game","Orson Scott Card",200);
        objects[1] = new MusicDisc(2,"Crossroad","Bon Jovi","AAA");
        objects[2] = new MusicDisc(2,"Zooropa","U2","BBB");

        User u = new User("1111111A","Nacho");
        Loan l1 = new Loan("07/03/2020","21/03/2020",objects[0],u);
        Loan l2 = new Loan("10/03/2020","24/03/2020",objects[0],u);
        u.addLoan(l1);
        u.addLoan(l2);

        System.out.println(u);
        System.out.println(l2.getUser());
    }
}