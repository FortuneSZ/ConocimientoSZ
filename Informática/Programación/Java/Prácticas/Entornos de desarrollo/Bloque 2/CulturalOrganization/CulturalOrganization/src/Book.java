public class Book extends CulturalObject
{
    private  int numPages;

    public int getNumPages()
    {
        return numPages;
    }

    public void setNumPages(int numPages)
    {
        this.numPages = numPages;
    }

    public Book(int id, String title, String author, int numPages)
    {
        super(id, title, author);
        this.numPages = numPages;
    }

    @Override
    public String toString() {
        return "NumPages: " + numPages + ", " + super.toString();
    }
}
