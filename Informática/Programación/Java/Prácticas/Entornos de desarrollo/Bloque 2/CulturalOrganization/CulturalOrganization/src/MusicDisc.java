public class MusicDisc extends CulturalObject
{
    private String company;

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public MusicDisc(int id, String title, String author, String company)
    {
        super(id, title, author);
        this.company = company;
    }

    @Override
    public String toString() {
        return "company: " + company + ", " + super.toString();
    }
}
