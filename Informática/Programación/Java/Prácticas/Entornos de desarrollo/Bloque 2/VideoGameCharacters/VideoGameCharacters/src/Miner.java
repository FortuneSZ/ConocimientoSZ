public class Miner extends Citizen
{
    private int fatigueLevel;

    public int getFatigueLevel()
    {
        return fatigueLevel;
    }

    public void setFatigueLevel(int fatigueLevel)
    {
        this.fatigueLevel = fatigueLevel;
    }

    public Miner(int age, int height, String genre,
                 int lifeLevel, String movemenType, String tool, String job, int fatigueLevel)
    {
        super(age, height, genre, lifeLevel, movemenType, tool, job);
        this.fatigueLevel = fatigueLevel;
    }

    @Override
    public String toString() {
        return "Miner{" +
                super.toString() +
                "fatigueLevel=" + fatigueLevel +
                "} ";
    }
}
