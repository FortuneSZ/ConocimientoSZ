public class Citizen extends  Character
{
    protected String tool;
    protected String job;

    public String getTool()
    {
        return tool;
    }

    public void setTool(String tool)
    {
        this.tool = tool;
    }

    public String getJob()
    {
        return job;
    }

    public void setJob(String job)
    {
        this.job = job;
    }

    public Citizen(int age, int height, String genre,
                   int lifeLevel, String movemenType, String tool, String job)
    {
        super(age, height, genre, lifeLevel, movemenType);
        this.tool = tool;
        this.job = job;
    }

    @Override
    public String toString() {
        return "Citizen{" +
                super.toString() +
                "tool='" + tool + '\'' +
                ", job='" + job + '\'' +
                "} ";
    }
}
