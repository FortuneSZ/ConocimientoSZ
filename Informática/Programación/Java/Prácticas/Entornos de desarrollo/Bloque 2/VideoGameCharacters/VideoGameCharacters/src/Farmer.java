public class Farmer extends Citizen
{

    private int workLevel;

    public int getWorkLevel()
    {
        return workLevel;
    }

    public void setWorkLevel(int workLevel)
    {
        this.workLevel = workLevel;
    }

    public Farmer(int age, int height, String genre,
                  int lifeLevel, String movemenType, String tool, String job, int workLevel)
    {
        super(age, height, genre, lifeLevel, movemenType, tool, job);
        this.workLevel = workLevel;
    }

    @Override
    public String toString() {
        return "Farmer{" +
                super.toString() +
                "workLevel=" + workLevel +
                "} ";
    }
}
