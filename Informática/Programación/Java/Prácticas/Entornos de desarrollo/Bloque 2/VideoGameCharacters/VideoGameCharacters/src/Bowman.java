public class Bowman extends Soldier
{

    public Bowman(int age, int height, String genre, int lifeLevel, String movemenType, String weapon, int damage)
    {
        super(age, height, genre, lifeLevel, movemenType, weapon, damage);
    }

    @Override
    public String toString() {
        return "Bowman{" +
                super.toString() + "} " ;
    }
}
