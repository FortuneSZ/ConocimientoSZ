public class Soldier extends Character
{
    protected String weapon;
    protected int damage;
    public String getWeapon()
    {
        return weapon;
    }

    public void setWeapon(String weapon)
    {
        this.weapon = weapon;
    }

    public int getDamage()
    {
        return damage;
    }

    public void setDamage(int damage)
    {
        this.damage = damage;
    }

    public Soldier(int age, int height, String genre, int lifeLevel, String movemenType, String weapon, int damage)
    {
        super(age, height, genre, lifeLevel, movemenType);
        this.weapon = weapon;
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "Soldier{" +

                super.toString()
                + "weapon='" + weapon + '\'' +
                ", damage=" + damage + "} " ;
    }
}
