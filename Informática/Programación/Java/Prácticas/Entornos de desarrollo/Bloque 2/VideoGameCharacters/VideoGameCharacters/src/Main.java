public class Main {
    public static void main(String[] args)
    {
        Character[] c = new Character[5];
        c[0] = new Character(40, 175, "Male", 100,"normal");
        c[1] = new Bowman(33, 156, "Male", 80, "Silent","Bow", 4);
        c[2] = new Miner(44, 173, "Female", 70,  "Slow","Axe",
                "Unknown", 12);
        c[3] = new Soldier(35, 164, "Female", 120, "fast","Sword",5);
        c[4] = new Citizen(60, 160, "Male", 20, "common"
                ,"Scissors","gardener");

        for (int i = 0; i < c.length; i++)
        {
            System.out.println((c[i]));
        }
    }
}