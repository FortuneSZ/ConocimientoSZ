public class Knight extends Soldier
{
    private int horseLifeLevel;

    public int getHorseLifeLevel()
    {
        return horseLifeLevel;
    }

    public void setHorseLifeLevel(int horseLifeLevel)
    {
        this.horseLifeLevel = horseLifeLevel;
    }

    public Knight(int age, int height, String genre,
                  int lifeLevel, String movemenType, String weapon, int damage, int horseLifeLevel)
    {
        super(age, height, genre, lifeLevel, movemenType, weapon, damage);
        this.horseLifeLevel = horseLifeLevel;
    }

    @Override
    public String toString() {
        return "Knight{" +
                super.toString()
                + "horseLifeLevel=" + horseLifeLevel +
                "} ";
    }
}
