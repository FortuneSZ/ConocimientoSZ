import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        ThreeDPoint[] array = new ThreeDPoint[5];
        Scanner sc = new Scanner(System.in);
        int x, y, z;

        for (int i = 0; i < array.length; i++)
        {
            System.out.println("Enter the coordinates (x y z");
            x = sc.nextInt();
            y = sc.nextInt();
            z = sc.nextInt();
            array[i] = new ThreeDPoint(x,y,z);
        }

        for (int i = 1; i < array.length; i++)
        {
            System.out.println("The distance between "
                    + array[0] + " to "
                    + array[i] + " is "
                    + array[0].distanceTo(array[i]));
        }
    }
}