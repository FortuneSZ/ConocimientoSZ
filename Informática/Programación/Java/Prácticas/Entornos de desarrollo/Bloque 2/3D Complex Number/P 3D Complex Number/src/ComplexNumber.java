public class ComplexNumber
{
    double a;
    double b;

    public double getA()
    {
        return a;
    }

    public void setA(int a)
    {
        this.a = a;
    }

    public double getB()
    {
        return b;
    }

    public void setB(int b)
    {
        this.b = b;
    }

    public ComplexNumber(double a, double b)
    {
        this.a = a;
        this.b = b;
    }

    public ComplexNumber()
    {
        super();
    }

    public double getMagnitude()
    {
        return Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
    }

    static void Sum(ComplexNumber num1,ComplexNumber num2)
    {
        ComplexNumber a = new ComplexNumber((num1.a + num2.a),(num1.b + num2.a));
        System.out.println(a);
    }

    @Override
    public String toString()
    {
        if (b > 0)
        {
            return a + "+" + b + "i";
        }
        else
        {
            return a + b + "i";
        }
    }
}
