public class ThreeDPoint
{
    int x;
    int y;
    int z;

    public int getX()
    {
        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public int getZ()
    {
        return z;
    }

    public void setZ(int z)
    {
        this.z = z;
    }

    public ThreeDPoint(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void moveTo(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double distanceTo(ThreeDPoint p2)
    {
        double X = this.x - p2.x;
        double Y = this.y - p2.y;
        double Z = this.z - p2.z;
        return Math.sqrt(Math.pow(X,2) + Math.pow(Y,2) + Math.pow(Z,2));
    }

    @Override
    public String toString()
    {
        return "(" + x + "," + y + "," + z + ")";
    }
}
