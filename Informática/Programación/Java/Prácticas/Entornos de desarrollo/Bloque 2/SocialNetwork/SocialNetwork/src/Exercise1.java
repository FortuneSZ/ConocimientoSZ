public class Exercise1
{
    static boolean isGroup(int[][] users, int[]group)
    {
        boolean groupp = true;

        for (int i = 0; i < group.length; i++)
        {
            for (int x = 1; x < group.length; x++)
            {
                if (users[i][x] != 1)
                {
                    groupp = false;
                }
            }
        }
        return  groupp;
    }
    static int lonely(int[][] users)
    {
        int unPopular = 0, cantUnPopular = 0, cant = 0;

        for (int i = 0; i < users.length; i++)
        {
            for (int x = 0; x < users.length; x++)
            {
                if (users[x][i] == 0)
                {
                    cant++;
                }
            }
            if (cant > cantUnPopular)
            {
                cantUnPopular = cant;
                unPopular = i;
            }
            cant = 0;
        }
        return unPopular;
    }
    static int mostPopular(int[][] users)
    {
        int popular = 0, cantPopular = 0, cant = 0;

        for (int i = 0; i < users.length; i++)
        {
            for (int x = 0; x < users.length; x++)
            {
                if (users[x][i] == 1)
                {
                    cant++;
                }
            }
            if (cant > cantPopular)
            {
                cantPopular = cant;
                popular = i;
            }
            cant = 0;
        }
        return popular;
    }
    static boolean isFollower(int[][]users, int u1, int u2)
    {
        boolean result;

        if (users[u1][u2] == 1)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        return result;
    }

    public static void main(String[] args)
    {
        int[][] users =
        {
            {0, 1, 1, 0, 0, 0, 1, 0, 0, 1},
            {1, 0, 1, 0, 0, 0, 1, 0, 0, 0},
            {1, 1, 0, 0, 0, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 1, 1, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {1, 1, 0, 0, 0, 0, 1, 0, 0, 1},
            {0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
            {1, 0, 0, 0, 1, 0, 1, 0, 0, 0}
        };
    int [] group = {0,1,2};
     System.out.println(isFollower(users,0,1));
     
     System.out.println(isGroup(users,group));
    }
}