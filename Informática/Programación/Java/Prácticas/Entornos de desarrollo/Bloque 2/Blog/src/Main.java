public class Main
{
    public static void main(String[] args)
    {
        Editor e = new Editor("editor1", "password123");
        Visitor v = new Visitor("visitor1", "pass1111");
        Post p = new Post("Post title", "Post contents", "2020-03-07");
        e.PublishPost(p);
        Comment c = new Comment("Comment text", "2020-03-21", v);
        v.CommentPost(p, c);

        for (int i = 0; i < 1;i++)
        {
            System.out.println(e);
            System.out.println(v);
            System.out.println(p);
            System.out.println(c);
        }
        System.out.println();
    }
}