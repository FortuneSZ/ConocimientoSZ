public class Comment
{
    private String text;
    private String date;
    private Visitor visitor;

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public Visitor getVisitor()
    {
        return visitor;
    }

    public void setVisitor(Visitor visitor)
    {
        this.visitor = visitor;
    }

    public Comment(String text, String date, Visitor visitor)
    {
        this.text = text;
        this.date = date;
        this.visitor = visitor;
    }

    @Override
    public String toString() {
        return "text: " + text +
                ",date: " + date +
                ",visitor: " + visitor.login;
    }
}
