import java.util.Arrays;

public class Post
{
    private String title;
    private String content;
    private String date;
    private Comment[] comments = new Comment[MAX_COMMENTS];
    private int numcomments;
    private static final int MAX_COMMENTS = 100;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public Comment[] getComments()
    {
        return comments;
    }

    public void setComments(Comment[] comments)
    {
        this.comments = comments;
    }

    public int getNumcomments()
    {
        return numcomments;
    }

    public void setNumcomments(int numcomments)
    {
        this.numcomments = numcomments;
    }

    public Post(String title, String content, String date)
    {
        this.title = title;
        this.content = content;
        this.date = date;
    }

    @Override
    public String toString() {
        return "title: " + title +
                ",content: " + content +
                ",date: " + date +
                ",numcomments: " + numcomments;
    }
}
