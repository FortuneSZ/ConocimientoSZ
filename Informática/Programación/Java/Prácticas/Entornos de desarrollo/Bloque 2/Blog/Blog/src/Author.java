public class Author extends Editor
{
    protected Editor supervisor;

    public Editor getSupervisor()
    {
        return supervisor;
    }

    public void setSupervisor(Editor supervisor)
    {
        this.supervisor = supervisor;
    }

    public Author(String login, String password, Editor supervisor)
    {
        super(login, password);
        this.supervisor = supervisor;
    }

    @Override
    public String toString() {
        return super.toString() +
                ",supervisor:" + supervisor;
    }
}
