import java.util.Arrays;

public class Main
{
    public static void main(String[] args)
    {


        DiceDouble[] dice = new DiceDouble[3];
        for (int i = 0; i < dice.length; i++)
        {
            Dice die1 = new Dice(), die2 = new Dice();
            dice[i] = new DiceDouble(die1,die2);
            dice[i].rollDouble();
        }
        System.out.println("---------------------------------------------------");
        System.out.println("Dice rolls in ascending order: (comparator)");
        Arrays.sort(dice,new DiceDoubleComparator());
        for (int i = 0; i < dice.length; i++)
        {
            System.out.printf("%d + %d = %d \n",dice[i].die1.getValue(),dice[i].die2.getValue(),dice[i].getSum());
        }
        System.out.println("Max roll dice is: (comparator)");
        System.out.printf("%d + %d = %d \n",dice[0].die1.getValue(),dice[0].die2.getValue(),dice[0].getSum());
        System.out.println("---------------------------------------------------");
        System.out.println("Dice rolls in descending order: (comparable)");
        Arrays.sort(dice);
        for (int i = 0; i < dice.length; i++)
        {
           System.out.printf("%d + %d = %d \n",dice[i].die1.getValue(),dice[i].die2.getValue(),dice[i].getSum());
        }
        System.out.println("Max roll dice is: (comparable)");
        System.out.printf("%d + %d = %d \n",dice[2].die1.getValue(),dice[2].die2.getValue(),dice[2].getSum());
        System.out.println("---------------------------------------------------");
        System.out.println("Dice rolls in descending order: (abstract comparator)");
        Arrays.sort(dice,new DiceDoubleComparator2());
        for (int i = 0; i < dice.length; i++)
        {
            System.out.printf("%d + %d = %d \n",dice[i].die1.getValue(),dice[i].die2.getValue(),dice[i].getSum());
        }
        System.out.println("Max roll dice is: (abstract comparator)");
        System.out.printf("%d + %d = %d \n",dice[2].die1.getValue(),dice[2].die2.getValue(),dice[2].getSum());
        System.out.println("---------------------------------------------------");

    }
}