import java.util.Comparator;

public class DiceDouble implements Comparable<DiceDouble>
{
    Dice die1;
    Dice die2;


    public Dice getDie1()
    {
        return die1;
    }

    public Dice getDie2()
    {
        return die2;
    }

    public void setDie1(Dice die1)
    {
        this.die1 = die1;
    }

    public void setDie2(Dice die2)
    {
        this.die2 = die2;
    }

    public DiceDouble(Dice die1, Dice die2)
    {
        this.die1 = die1;
        this.die2 = die2;
    }

    public void rollDouble()
    {
        die1.Roll();
        die2.Roll();
    }

    public int getSum()
    {
        return die1.getValue() + die2.getValue();
    }

    public int compareTo(DiceDouble p)
    {
        if (this.getSum() < p.getSum())
            return -1;
        else if (p.getSum() < this.getSum())
            return 1;
        else
            return 0;
    }

    @Override
    public String toString() {
        return "DiceDouble{" +
                "die1=" + die1.getValue() +
                ", die2=" + die2.getValue() +
                '}';
    }
}
