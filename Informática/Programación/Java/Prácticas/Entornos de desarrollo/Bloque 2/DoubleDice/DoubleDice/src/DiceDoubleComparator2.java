import java.util.Comparator;

public class DiceDoubleComparator2 extends AbDiceDoubleComparator
{
    @Override
    public int compare(DiceDouble d1,DiceDouble d2)
    {
        return Integer.compare(d1.getSum(),d2.getSum());
    }
}
