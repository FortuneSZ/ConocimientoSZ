import java.util.Comparator;

public abstract class AbDiceDoubleComparator implements Comparator<DiceDouble>
{
    @Override
    public int compare(DiceDouble d1,DiceDouble d2)
    {
        return Integer.compare(d1.getSum(),d2.getSum());
    }
}
