import java.util.Random;

public class Dice implements Roll
{
    private int value;
    private Random r = new Random();

    public int getValue()
    {
        return value;
    }

    public void Roll()
    {
        value = r.nextInt(6)+1;
    }
}
