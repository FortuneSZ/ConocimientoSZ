import java.util.Comparator;

public class DiceDoubleComparator implements Comparator<DiceDouble>
{
    @Override
    public int compare(DiceDouble d1,DiceDouble d2)
    {
        return Integer.compare(d2.getSum(),d1.getSum());
    }
}
