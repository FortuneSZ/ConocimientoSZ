public class PhotoAlbum
{
    int pageNumber;

    public int getPageNumber()
    {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber)
    {
        this.pageNumber = pageNumber;
    }

    public PhotoAlbum(int pageNumber)
    {
        this.pageNumber = pageNumber;
    }

    public PhotoAlbum()
    {
        this(16);
    }

    @Override
    public String toString()
    {
        return "I am an album with " + pageNumber + " pages";
    }
}
