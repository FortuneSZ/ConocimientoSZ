package shapes.types;
import shapes.Shape;

public class Circle implements Shape
{
    double radius;

    public Circle(double radius)
    {
        this.radius = radius;
    }

    @Override
    public double calculateArea()
    {
        return Math.PI * radius * radius;
    }


    @Override
    public void draw()
    {
        System.out.println("Drawing a circle!");
    }
}
