package shapes.types;

import shapes.Shape;

public class Rectangle implements Shape
{
    double b, h;

    public Rectangle(double b, double h)
    {
        this.b = b;
        this.h = h;
    }

    @Override
    public double calculateArea()
    {
        return b * h;
    }
    @Override
    public void draw()
    {
        System.out.println("Drawing a rectangle!");
    }
}
