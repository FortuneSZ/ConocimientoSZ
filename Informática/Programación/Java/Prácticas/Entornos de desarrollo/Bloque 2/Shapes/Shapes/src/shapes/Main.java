package shapes;

import shapes.types.Circle;
import shapes.types.Rectangle;
import shapes.types.Square;

public class Main {
    public static void main(String[] args)
    {
        Shape[] shapes = new Shape[5];
        shapes[0] = new Circle(10);
        shapes[1] = new Circle(5);
        shapes[2] = new Rectangle(5,4);
        shapes[3] = new Square(7);
        shapes[4] = new Rectangle(2,8);

        for (int i = 0; i < shapes.length;i++)
        {
            System.out.println(shapes[i].calculateArea());
        }
    }
}