package shapes;

public interface Shape
{
    public double calculateArea();
    public void draw();
}
