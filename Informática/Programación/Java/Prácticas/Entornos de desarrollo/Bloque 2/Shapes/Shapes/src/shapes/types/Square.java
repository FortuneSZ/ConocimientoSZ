package shapes.types;

import shapes.Shape;

public class Square implements Shape
{
    double l;

    public Square(double l)
    {
        this.l = l;
    }

    @Override
    public double calculateArea()
    {
        return l * l;
    }
    @Override
    public void draw()
    {
        System.out.println("Drawing a square!");
    }
}
