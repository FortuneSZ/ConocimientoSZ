package model;

import java.util.Arrays;
//Class planet
public class Planet extends CelestialBodies
{
    private int numMoons;
    private Moon[] moons = new Moon[10];
    private int numExplorers;
    //Getetrs && Setters
    public int getNumMoons()
    {
        return numMoons;
    }

    public void setNumMoons(int numMoons)
    {
        this.numMoons = numMoons;
    }

    public int getNumExplorers()
    {
        return numExplorers;
    }

    public void setNumExplorers(int numExplorers)
    {
        this.numExplorers = numExplorers;
    }
    //Constructor
    public Planet(String name, float orbitalDistance)
    {
        super(name, orbitalDistance);
    }
    //Methods
    public void sendExplorers()
    {
        numExplorers++;
    }

    public void addCelestialBodies(CelestialBodies c)
    {
        Moon d = new Moon(c.name,c.orbitalDistance);
        addMoon(d);
    }

    public void addMoon(Moon d)
    {
        moons[numMoons] = d;
        numMoons += 1;
    }

    public void Show()
    {
        if (numMoons == 0)
        {
            System.out.println("Any moons");
        }
        else
        {
            System.out.println("Have " + numMoons + " moons");
            for(int i = 0; i < numMoons; i++)
            {
                System.out.println(moons[i]);
            }
        }
    }

    @Override
    public String toString()
    {
        return "Planet: "+ super.toString() + " Moons(" + numMoons + ") Explorers(" + numExplorers + ")";
    }
}
