package model;
//Abstract class celestial body
public abstract class CelestialBodies
{
    protected String name;
    protected float orbitalDistance;

    public String getName()
    {
        return name;
    }
    //Getters && Setters
    public void setName(String name)
    {
        this.name = name;
    }

    public float getOrbitalDistance()
    {
        return orbitalDistance;
    }

    public void setOrbitalDistance(float orbitalDistance)
    {
        this.orbitalDistance = orbitalDistance;
    }
    //Constructors
    public CelestialBodies(String name, float orbitalDistance)
    {
        this.name = name;
        this.orbitalDistance = orbitalDistance;
    }

    public CelestialBodies(String name)
    {
        super();
    }
    //Methods
    public float distanceCelestialBodies (CelestialBodies e)
    {
       return Math.abs(this.orbitalDistance) - Math.abs(e.orbitalDistance);
    }

    public abstract void addCelestialBodies(CelestialBodies c);


    @Override
    public String toString()
    {
        return name + " DIstance = " + orbitalDistance + " UA";
    }
}
