package model;

import java.util.Arrays;
//Class sun
public class Sun extends CelestialBodies implements info
{
    private int numPlanets;
    private Planet[] planets = new Planet[20];

    //Getters && Setters
    public int getNumPlanets()
    {
        return numPlanets;
    }

    public void setNumPlanets(int numPlanets)
    {
        this.numPlanets = numPlanets;
    }
    //Constructor
    public Sun(String name, float orbitalDistance)
    {
        super(name, orbitalDistance = 0);
    }
    //Methods
    public void addCelestialBodies(CelestialBodies c)
    {
        Planet d = new Planet(c.name,c.orbitalDistance);
        addPlanet(d);
    }

    public void addPlanet(Planet d)
    {
        planets[numPlanets] = d;
        numPlanets += 1;
    }

    public void Show()
    {
        if (numPlanets == 0)
        {
            System.out.println("No planets");
        }
        else
        {
            System.out.println("Have " + numPlanets + " planets");
            for (int i = 0; i < numPlanets; i++)
            {
                System.out.println(planets[i]);
            }
        }
    }

    @Override
    public String toString()
    {
        return "Sun: " + super.toString() + " planets (" + numPlanets + ")";
    }
}
