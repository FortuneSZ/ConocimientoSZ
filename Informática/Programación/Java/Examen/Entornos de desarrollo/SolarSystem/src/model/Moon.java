package model;
//Class moon
public class Moon extends CelestialBodies implements info
{
    private int numSondas;
    //Getters && Setters
    public int getNumSondas()
    {
        return numSondas;
    }

    public void setNumSondas(int numSondas)
    {
        this.numSondas = numSondas;
    }
    //Constructor
    public Moon(String name, float orbitalDistance)
    {
        super(name, orbitalDistance = 0);
    }
    //Methods
    public void addCelestialBodies(CelestialBodies c)
    {

    }

    public void sendSondas()
    {
        numSondas++;
    }

    public void Show()
    {

    }

    @Override
    public String toString()
    {
        return "Moon: " + name + " Sondas (" + numSondas + ")";
    }
}
