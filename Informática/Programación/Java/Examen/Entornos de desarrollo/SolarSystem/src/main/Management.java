package main;

import model.CelestialBodies;
import model.Moon;
import model.Planet;
import model.Sun;

import java.util.Scanner;
//Class management
public class Management
{
    private CelestialBodies[] bodies;

    public CelestialBodies[] getBodies()
    {
        return bodies;
    }
    //Method that show the detailed infor of a celestial body
    public void ShowDetailedInfo(String celes2)
    {
        for (int i = 0; i < bodies.length; i++)
        {
            if (celes2.equals(bodies[i].getName()))
            {
                if (bodies[i] instanceof Planet)
                {
                    ((Planet)bodies[i]).Show();
                }
                if (bodies[i] instanceof Moon)
                {
                    ((Moon)bodies[i]).Show();
                }
                if (bodies[i] instanceof Sun)
                {
                    ((Sun)bodies[i]).Show();
                }

            }
        }
    }
    //Method that sends an explorer to a planet
    public void SendExplorer()
    {
        for (int i = 0; i < bodies.length; i++)
        {
            if (bodies[i] instanceof Planet)
            {
                ((Planet)bodies[i]).sendExplorers();
            }
        }
    }
    //Method that send a prove to a moon
    public void SendToMoon()
    {
        for (int i = 0; i < bodies.length; i++)
        {
            if (bodies[i] instanceof Moon)
            {
                ((Moon)bodies[i]).sendSondas();
            }
        }
    }
    //Method that orders by distance
    public void ordenar2(CelestialBodies b)
    {
        for (int i = 0; i < bodies.length - 1; i++)
        {
            for (int j = i + 1; j < bodies.length; j++)
            {
                if (bodies[i].distanceCelestialBodies(b) > bodies[j].distanceCelestialBodies(b))
                {
                    CelestialBodies aux = bodies[i];
                    bodies[i] = bodies[j];
                    bodies[j] = aux;
                }
            }
        }

        for (int i = 0; i < bodies.length; i++)
        {
            if (bodies[i] instanceof Planet || bodies[i] instanceof Sun)
            {
                System.out.println(bodies[i]);
            }
        }

    }
    //Method that orders by name
    public CelestialBodies[] ordenar(CelestialBodies[] c)
    {
        for (int i = 0; i < c.length - 1; i++)
        {
            for (int j = i + 1; j < c.length; j++)
            {
                if (c[i].getName().compareTo(c[j].getName()) > 0)
                {
                    CelestialBodies aux = c[i];
                    c[i] = c[j];
                    c[j] = aux;
                }
            }
        }
        showBodies();

        return c;
    }
    //Setter
    public void setBodies(CelestialBodies[] bodies)
    {
        this.bodies = bodies;
    }
    //Method that shows all the celestial bodies
    public void showBodies()
    {
        for (int i = 0; i < bodies.length; i++)
        {
            System.out.println(bodies[i]);
        }
    }
    //Method that shows a cpncrete celestial body
    public void ShowBody(String celes)
    {
        for (int i = 0; i < bodies.length; i++)
        {
            if (celes.equals(bodies[i].getName()))
            {
                System.out.println(bodies[i]);
            }
        }
    }
    //Menu
    public void Menu()
    {
        System.out.println();
        System.out.println("1. Show All Celestial Bodies");
        System.out.println("2. Info Celestial Bodies");
        System.out.println("3. Sort Celestial Bodies sorted by Name");
        System.out.println("4. Send Sondas to Moons");
        System.out.println("5. Send Explorer to Planets");
        System.out.println("6. Extend info Celestial Bodies");
        System.out.println("7. Sort Distance Celestial Bodies from planet");
        System.out.println("8. Exit");
    }
}
