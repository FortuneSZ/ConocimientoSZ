package main;

import model.CelestialBodies;
import model.Moon;
import model.Planet;
import model.Sun;

import java.awt.*;
import java.util.Scanner;
/*This program manages a solar system, it has a menu with the following options:

1. Show All Celestial Bodies
2. Info Celestial Bodies
3. Sort Celestial Bodies sorted by Name
4. Send Sondas to Moons
5. Send Explorer to Planets
6. Extend info Celestial Bodies
7. Sort Distance Celestial Bodies from planet
8. Exit
*/
public class Main
{
    public enum BodyType{planet,moon,sun};
    public static CelestialBodies bodyCreate(BodyType type,String name,float distance)
    {
        if (type == BodyType.moon)
        {
            return new Moon(name,distance);
        }
        else if (type == BodyType.planet)
        {
            return new Planet(name,distance);
        }
        else if (type == BodyType.sun)
        {
            return new Sun(name,distance);
        }

        return null;
    }
    public static void main(String[] args)
    {
        int opcion = 0;
        Management m = new Management();
        CelestialBodies[] bodies = new CelestialBodies[14];
        CelestialBodies T = new Planet("",0f);
        bodies[0] = bodyCreate(BodyType.sun,"Sol",0.00f);
        bodies[1] = bodyCreate(BodyType.planet,"Mercurio",0.39f);
        bodies[2] = bodyCreate(BodyType.planet,"Venus",0.72f);
        bodies[3] = bodyCreate(BodyType.planet,"Tierra",1.00f);
        bodies[4] = bodyCreate(BodyType.planet,"Marte",1.52f);
        bodies[5] = bodyCreate(BodyType.planet,"Jupiter",5.20f);
        bodies[6] = bodyCreate(BodyType.planet,"Saturno",9.58f);
        bodies[7] = bodyCreate(BodyType.planet,"Urano",19.18f);
        bodies[8] = bodyCreate(BodyType.planet,"Neptuno",30.07f);
        bodies[9] = bodyCreate(BodyType.moon,"Luna",0f);
        ((Planet)bodies[3]).addMoon((Moon)bodies[9]);
        bodies[10] = bodyCreate(BodyType.moon,"Io",0f);
        ((Planet)bodies[5]).addMoon((Moon)bodies[10]);
        bodies[11] = bodyCreate(BodyType.moon,"Europa",0f);
        ((Planet)bodies[5]).addMoon((Moon)bodies[11]);
        bodies[12] = bodyCreate(BodyType.moon,"Gaminedes",0f);
        ((Planet)bodies[5]).addMoon((Moon)bodies[12]);
        bodies[13] = bodyCreate(BodyType.moon,"Calisto",0);
        ((Planet)bodies[5]).addMoon((Moon)bodies[13]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[1]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[2]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[3]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[4]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[5]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[6]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[7]);
        ((Sun)bodies[0]).addPlanet((Planet)bodies[8]);

        m.setBodies(bodies);

        do
        {
            m.Menu();

            Scanner sc = new Scanner(System.in);
            System.out.println();
            opcion = sc.nextInt();

            switch (opcion)
            {
                case 1:
                    m.showBodies();
                    break;

                case 2:
                    System.out.println("Name of Celestial Bodie");
                    String celes = sc.next();
                    m.ShowBody(celes);
                    break;

                case 3:
                    m.ordenar(bodies);
                    break;

                case 4:
                    m.SendToMoon();
                    break;

                case 5:
                    m.SendExplorer();
                    break;

                case 6:
                    System.out.println("Name of Celestial Bodie");
                    String celes2 = sc.next();
                    m.ShowDetailedInfo(celes2);
                    break;

                case 7:
                    System.out.println("Name of Celestial Bodie");
                    String celes3 = sc.next();
                    for (int i = 0; i < bodies.length; i++)
                    {
                        if (celes3.equals(bodies[i].getName()))
                        {
                            T = bodies[i];
                        }
                    }
                    m.ordenar2(T);
                    break;
            }
        }
        while (opcion != 8);
    }
}