public class Main
{
    public static void main(String[] args)
    {
        AnimalConversation<Cat,Dog> conv1 = new AnimalConversation<>(new Cat("Miau"),new Dog("Guau"));
        conv1.chat();
    }
}