public class Ingredient implements GetNutritionalValues
{
    double quantity;
    Aliment aliment;

    /*Constructor*/

    public Ingredient(double quantity, Aliment aliment)
    {
        this.quantity = quantity;
        this.aliment = aliment;
    }

    /*Getters and setters*/

    public double getQuantity()
    {
        return quantity;
    }

    public void setQuantity(double quantity)
    {
        this.quantity = quantity;
    }

    public Aliment getAliment()
    {
        return aliment;
    }

    public void setAliment(Aliment aliment)
    {
        this.aliment = aliment;
    }

    /*Methods*/

    public double getCalories()
    {
        double Calories = 0;
        return Calories;
    }

    public  double getCarbohydrates()
    {
        double carbohydrates = 0;
        return carbohydrates;
    }

    public double getFat()
    {
        double fat = 0;
        return fat;
    }

    public boolean hasMilk()
    {
        return true;
    }

    public boolean hasNuts()
    {
        return true;
    }

    public boolean hasEggs()
    {
        return true;
    }

    public boolean hasGluten()
    {
        return true;
    }
}
