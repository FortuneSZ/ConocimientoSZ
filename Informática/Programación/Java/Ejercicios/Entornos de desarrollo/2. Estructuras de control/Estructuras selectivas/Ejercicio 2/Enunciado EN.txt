Exercise 2:

Create a program called GramOunceConverter2 that will be an improved version of a previous exercise. 

In this case, the user will type a weight (float), and a unit (g for grams, o for ounces). 

Then, depending on the unit chosen, the program will convert the weight to the opposite unit. 

For instance, if the user types a weight of 33 and chooses o as unit, then the program must convert 33 ounces to grams. 

You must solve this program using a switch structure. If the unit is other than g or o, then the program must output an error message: 

“Unexpected unit”, with no final result.