import java.util.Scanner;
public class GramOunceConverter2
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        float weight,convertedweight;
        char unit;

        System.out.println("Write the weight to convert");
        weight = sc.nextFloat();
        System.out.println("Write the unit to convert the weight");
        unit = sc.next().charAt(0);

        switch (unit)
        {
            case 'g':
                convertedweight = (float) (weight * 28.3495);
                System.out.printf("%1.0f ounces equals to %1.0f grams",weight,convertedweight);
                break;
            case 'o':
                convertedweight = (float) (weight / 28.3495);
                System.out.printf("%1.0f grams equals to %1.3f ounces",weight,convertedweight);
                break;
            default:
                System.out.println("Unespected unit");
                break;
        }
    }
}
