import java.util.Scanner;
public class SumDigits
{
    public static void main(String[] args)
    {
        int num,result = 0,digits = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter numbers, if you enter 0 it will end");
        do
        {
            num = sc.nextInt();
            result += num;
        }
        while (num != 0);

        for (int i = result;i >= 1;i /= 10 )
        {
            digits++;
        }

        System.out.printf("The result is %d, and has %d digits",result,digits);
    }
}
