Crea un programa llamado sumaDígitos que pida al usuario que introduzca números (valores enteros) hasta que introduzca un 0. 

El programa debe sumar todos los números introducidos por el usuario y entonces mostrar el resultado final, y cuantos dígitos tiene.

Por ejemplo, si el usuario introduce 12, 20, 60, 99 y 0, entonces el programa debe mostrar: "El resultado es 224, y tiene 3 dígitos.