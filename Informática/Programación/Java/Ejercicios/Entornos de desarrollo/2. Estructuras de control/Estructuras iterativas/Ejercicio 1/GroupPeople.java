import java.util.Scanner;
public class GroupPeople
{
    public static void main(String[] args)
    {
        int people,group50=0,group10=0,group1=0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of people that will asist to the conference");
        people = sc.nextInt();
        int altPeople = people;

        while(altPeople >= 50)
        {
            group50++;
            altPeople -= 50;
        }

        while (altPeople >= 10)
        {
            group10++;
            altPeople -= 10;
        }

        while (altPeople >= 1)
        {
            group1++;
            altPeople -= 1;
        }

        System.out.printf("%d people can be grouped in %d groups of 50 people, %d groups of 10 people and %d groups" +
                " of 1 people",people,group50,group10,group1);
    }
}
