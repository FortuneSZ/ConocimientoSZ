import java.util.Scanner;
public class Triangle
{
    public static void main(String[] args)
    {
        int height,counter = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the height of the triangle");

        height = sc.nextInt();
        System.out.println();

        for (int i = height; i > 0; i--)
        {
            if (counter > 0)
            {
                for(int x = 0; x < counter; x++)
                {
                    System.out.print(" ");
                }
            }

            for (int z = 0; z < height - counter; z++)
            {
                System.out.print("*");
            }

            System.out.println();
            counter++;
        }
    }
}
