import java.util.Scanner;
public class Counter
{
    public static void main(String[] args)
    {
        int num;
        Scanner sc = new Scanner(System.in);

        do
        {
            System.out.println("Insert a number between");
            num = sc.nextInt();
        }
        while (num < 1 || num > 100);

        for (int i = num; i >= 1; i--)
        {
            System.out.println(i);
        }

    }
}
