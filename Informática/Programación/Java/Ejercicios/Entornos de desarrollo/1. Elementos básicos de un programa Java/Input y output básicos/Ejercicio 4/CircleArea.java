import java.util.Scanner;
public class CircleArea
{
    static final double pi = 3.14159;

    public static void main(String[] args)
    {
        double radius,area;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the radius of the circle");
        radius = sc.nextDouble();

        area = pi * radius * radius;

        System.out.printf("The area of a circle of radius %1.0f is %3.2f",radius,area);
    }
}
