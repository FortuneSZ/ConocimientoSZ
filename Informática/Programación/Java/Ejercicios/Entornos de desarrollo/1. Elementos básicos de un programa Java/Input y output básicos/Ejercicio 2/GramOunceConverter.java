import java.util.Scanner;
public class GramOunceConverter
{
    public static void main(String[] args)
    {
        int gram;
        float ounce;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a weight in grams");
        gram = sc.nextInt();

        ounce = (float) (gram / 28.3495);

        System.out.printf("%d grams equals to %f ounces",gram,ounce);
    }
}
