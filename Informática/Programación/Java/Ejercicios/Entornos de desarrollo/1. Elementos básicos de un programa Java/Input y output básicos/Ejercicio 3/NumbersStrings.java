import java.util.Scanner;
public class NumbersStrings
{
    public static void main(String[] args)
    {
        String num1,num2,num3,num4,num12,num34;
        int number1,number2,result;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter 4 numbers");
        num1 = sc.nextLine();
        num2 = sc.nextLine();
        num3 = sc.nextLine();
        num4 = sc.nextLine();

        num12 = num1 + num2;
        num34 = num3 + num4;

        number1 = Integer.parseInt(num12);
        number2 = Integer.parseInt(num34);
        result = number1 + number2;

        System.out.printf("The result is %d",result);

    }
}
