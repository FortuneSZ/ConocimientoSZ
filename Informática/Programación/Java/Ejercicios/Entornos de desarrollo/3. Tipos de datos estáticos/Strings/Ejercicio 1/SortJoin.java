import java.util.Scanner;
import java.util.Arrays;
public class SortJoin
{
    public static void main(String[] args)
    {
        String names;
        String[] parts;
        Scanner sc = new Scanner(System.in);

        System.out.println("Type names separated with spaces");
        names = sc.nextLine();
        parts = names.split(" ");
        Arrays.sort(parts);
        names = String.join(",",parts);
        System.out.println(names);
    }
}
