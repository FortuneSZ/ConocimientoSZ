import java.util.Scanner;
public class LispChecker
{
    public static void main(String[] args)
    {
        int leftpar = 0, rightpar = 0;
        String lisp;
        Scanner sc = new Scanner(System.in);

        System.out.println("Insert a LISP instruction");
        lisp = sc.nextLine();

        for (int i = 0; i < lisp.length(); i++)
        {
            if (lisp.charAt(i) == '(')
            {
                leftpar++;
            }
            if (lisp.charAt(i) == ')')
            {
                rightpar++;
            }
        }

        if (leftpar == rightpar)
        {
            System.out.println("Correct");
        }
        else
        {
            System.out.println("Error");
        }
    }
}
