Create a program called LispChecker. LISP is a programming languages where every instruction is enclosed in parentheses. 
This could be a set of instructions in LISP:

(let ((new (x-point a y))))

You must implement a program that takes a string with LISP instructions (just one string) 
and then check if the parentheses are correct (this is, the number of opening parentheses and closing parentheses are the same).