import java.util.Scanner;
public class CheckMessages
{
    public static void ChangeArray(String[] array)
    {
        for (int i = 0; i < 10; i++)
        {
            if (array[i].contains("Eclipse"))
            {
                array[i] = array[i].replace("Eclipse","IntelliJ");
            }
        }
    }
    public static void FillArray(String[] array)
    {
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 10; i++)
        {
            System.out.println("Enter an string");
            array[i] = sc.nextLine();
        }
    }
    public static void ShowChanges(String[] array)
    {
        for (int i = 0; i < 10; i++)
        {
            System.out.println(array[i]);
        }
    }
    public static void main(String[] args)
    {
        String[] array = new String[10];

        FillArray(array);
        ChangeArray(array);
        ShowChanges(array);


    }
}
