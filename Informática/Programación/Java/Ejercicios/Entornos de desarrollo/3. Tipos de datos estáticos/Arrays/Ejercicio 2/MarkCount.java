import java.util.Scanner;
public class MarkCount
{
    public static void main(String[] args)
    {
        int marks;
        int[] markCount = new int[10];
        Scanner sc = new Scanner(System.in);

        for (int i= 0; i<10; i++)
        {
            do
            {
                System.out.printf("Insert the %d mark",i+1);
                System.out.println();
                marks = sc.nextInt();

                if (i <= 10 && i >= 0)
                {
                    markCount[marks-1]++;
                }
            }
            while (i > 10 || i < 0);
        }
        System.out.println();

        System.out.println("Marks per category");
        System.out.println();
        for (int i= 0; i<10; i++)
        {
            System.out.printf("%d: %d marks",i+1,markCount[i]);
            System.out.println();
        }
    }
}
