Crea un programa llamado SumaDeMatrices que pida al usuario que introduzca dos matrices o tablas bidimensionales de 3 filas y columnas,
y que entonces imprima el resultado de sumarlas. Para sumar dos matrices, debes hacerlo cerlda por celda:

resultado[i][j] = matrizA[i][j] + matrizB[i][j]