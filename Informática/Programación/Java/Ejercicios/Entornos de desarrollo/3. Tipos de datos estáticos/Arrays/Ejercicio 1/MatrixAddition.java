import java.util.Scanner;
public class MatrixAddition
{
    public static void main(String[] args)
    {
        int[][] table1 = new int[3][3];
        int[][] table2 = new int[3][3];
        int[][] table3 = new int[3][3];
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the values of the first table");

        for (int i =0; i < 3; i++)
        {
            for (int x = 0; x < 3; x++)
            {
                table1[i][x] = sc.nextInt();
            }
        }
        System.out.println();

        System.out.println("Table 1 values:");

        for (int i =0; i < 3; i++)
        {
            for (int x =0; x < 3; x++)
            {
                System.out.print(table1[i][x]);
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("Enter the values of the second table");

        for (int i =0; i < 3; i++)
        {
            for (int x =0; x < 3; x++)
            {
                table2[i][x] = sc.nextInt();
            }
        }
        System.out.println();

        System.out.println("Table 2 values:");

        for (int i =0; i < 3; i++)
        {
            for (int x =0; x < 3; x++)
            {
                System.out.print(table2[i][x]);
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("Addition of the 2 tables");
        for (int i =0; i < 3; i++)
        {
            for (int x =0; x < 3; x++)
            {
                table3[i][x] = table1[i][x] + table2[i][x];
            }
        }
        System.out.println();

        System.out.println("Table 3 values:");

        for (int i =0; i < 3; i++)
        {
            for (int x =0; x < 3; x++)
            {
                System.out.print(table3[i][x]);
            }
            System.out.println();
        }

    }
}
