import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CarIDCheck
{
    public static void main(String[] args)
    {
        String carID;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a car ID");
        carID = sc.nextLine();

        Pattern p = Pattern.compile("^\\d\\d\\d\\d[A-Z][A-Z][A-Z]$");
        Matcher m = p.matcher(carID);

        if (m.find())
        {
            System.out.println("Correct car id");
        }
        else
        {
            System.out.println("Incorrect car id");
        }
    }
}
