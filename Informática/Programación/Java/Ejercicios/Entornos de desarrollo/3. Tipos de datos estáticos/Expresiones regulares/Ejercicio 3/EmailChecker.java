import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class EmailChecker
{
    public static void main(String[] args)
    {
        String email;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter an email");
        email = sc.nextLine();

        Pattern p = Pattern.compile("^\\w+@\\w+.\\w+$");
        Matcher m = p.matcher(email);

        if (m.find())
        {
            System.out.println("Correct email");
        }
        else
        {
            System.out.println("Incorrect email");
        }
    }

}
