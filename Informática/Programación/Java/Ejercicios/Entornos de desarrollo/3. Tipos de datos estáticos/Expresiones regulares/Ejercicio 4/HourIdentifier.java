import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class HourIdentifier
{
    public static void ShowMatch(Matcher m2, String[] hours)
    {
        int counter = 0;
        if (m2.find())
        {
            do
            {
                hours[counter] = m2.group();
                counter++;
            }
            while (m2.find());
            Arrays.sort(hours);
            System.out.println("Hour list:");
            for (int i =0; i < hours.length; i++)
            {
                System.out.println(hours[i]);
            }
        }
    }
    public static void DetermineArray(Matcher m,Matcher m2)
    {
        int counter = 0;
        do
        {
            counter++;
        }
        while (m.find());
        String[] hours = new String[counter];

        ShowMatch(m2,hours);
    }
    public static void main(String[] args)
    {
        String text, text2;
        Scanner sc = new Scanner(System.in);

        System.out.println("Insert a text");
        text = sc.nextLine();
        text2 = text;

        Pattern p = Pattern.compile("\\d{2}:\\d{2}");
        Matcher m = p.matcher(text);
        Matcher m2 = p.matcher(text2);

        if (!m.find())
        {
            System.out.println("The text doesn't contains hours");
        }
        else
        {
            DetermineArray(m,m2);
        }
    }
}
