import java.util.Scanner;
enum Months { JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY,
    AUGUST, SEPTEMBER, OCTOBER, NOVERMBER, DECEMBER};
public class MonthEnum
{
    public static void main(String[] args)
    {
        Months month;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a month");
        month = Months.valueOf(sc.nextLine());

        switch (month)
        {
            case JANUARY:
                System.out.printf("%s has 31 days",month);
                break;
            case FEBRUARY:
                System.out.printf("%s has 28 days",month);
                break;
            case MARCH:
                System.out.printf("%s has 31 days",month);
                break;
            case APRIL:
                System.out.printf("%s has 30 days",month);
                break;
            case MAY:
                System.out.printf("%s has 31 days",month);
                break;
            case JUNE:
                System.out.printf("%s has 30 days",month);
                break;
            case JULY:
                System.out.printf("%s has 31 days",month);
                break;
            case AUGUST:
                System.out.printf("%s has 31 days",month);
                break;
            case SEPTEMBER:
                System.out.printf("%s has 30 days",month);
                break;
            case OCTOBER:
                System.out.printf("%s has 31 days",month);
                break;
            case NOVERMBER:
                System.out.printf("%s has 30 days",month);
                break;
            case DECEMBER:
                System.out.printf("%s has 31 days",month);
                break;
        }


    }
}
