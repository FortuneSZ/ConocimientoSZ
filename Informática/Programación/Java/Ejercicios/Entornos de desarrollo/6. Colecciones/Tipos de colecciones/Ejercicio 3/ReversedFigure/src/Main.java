import java.util.Scanner;
import java.util.Stack;

public class Main
{
    public static void main(String[] args)
    {
        Stack<String> figure = new Stack<>();
        Scanner sc = new Scanner(System.in);

        System.out.println("White the height of the figure");
        int height = sc.nextInt();

        for (int i = 0; i <= height; i++)
        {
            String line = sc.nextLine();
            figure.push(line);
        }

        System.out.println();
        for (int i = 0; i <= height ; i++)
        {
            System.out.println(figure.pop());
        }
    }
}