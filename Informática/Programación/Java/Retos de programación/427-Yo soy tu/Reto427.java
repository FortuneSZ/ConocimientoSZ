import java.util.Scanner;

public class Reto427
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String nombre, parentesco;
        int casos;
        
        casos = sc.nextInt();
        sc.nextLine();
        
        for(int i = 1; i <= casos; i++)
        {
            nombre = sc.nextLine();
            parentesco = sc.nextLine();
            
            if(nombre.equals("Luke") && parentesco.equals("padre"))
            {
                System.out.println("TOP SECRET");
            }
            else
            {
                System.out.println(nombre + ", yo soy tu " + parentesco);
            }
        }
    }
}
