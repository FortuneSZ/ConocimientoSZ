import java.util.Scanner;

public class Reto413
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int casos, ancho, alto, totalBaldosas, claras, oscuras;
        
        casos = sc.nextInt();
        
        for(int i = 1; i <= casos; i++)
        {
            ancho = sc.nextInt();
            alto = sc.nextInt();
            totalBaldosas = ancho * alto;
            claras = totalBaldosas / 2;
            oscuras = claras;
            if(totalBaldosas % 2 != 0)
            {
                claras++;
            }
            
            System.out.println(claras + " " + oscuras);
        }
    }
}
