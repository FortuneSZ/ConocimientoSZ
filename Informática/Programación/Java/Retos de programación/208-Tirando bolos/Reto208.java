import java.util.Scanner;

public class Reto208
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
		long f, b, bolosTotales, bolosDerribados;
		
		do
		{
			f = sc.nextLong();
			b = sc.nextLong();
			
			if (f > 0)
			{
				bolosTotales = f * (f+1) / 2;				
				bolosDerribados = (f - b + 1) * (f - b + 2) / 2;
				
				System.out.println(bolosTotales - bolosDerribados);
			}
		}
		while(f > 0);
    }
}
