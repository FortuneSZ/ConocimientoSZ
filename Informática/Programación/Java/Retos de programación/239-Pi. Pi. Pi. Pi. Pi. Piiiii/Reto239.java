import java.util.Scanner;

public class Reto239
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int n1, n2, totalSegundos;
        int dias, horas, minutos, segundos;

        do
        {
            n1 = sc.nextInt();
            n2 = sc.nextInt();
            if (n1 != 0 || n2 != 0)
            {
                // El número total de segundos se calcula como: 
                // número total de días (n1) * número total de estaciones de
                // radio (n2) * número de horas por día (24) * duración de la
                // señal (6 segundos)
                totalSegundos = n1 * n2 * 24 * 6;

                dias = totalSegundos / 86400;
                totalSegundos = totalSegundos % 86400;
                horas = totalSegundos / 3600;
                totalSegundos = totalSegundos % 3600;
                minutos = totalSegundos / 60;
                totalSegundos = totalSegundos % 60;
                segundos = totalSegundos;
                
                System.out.printf("%d:%02d:%02d:%02d%n", dias, horas, minutos, segundos);
            }
        } while (n1 != 0 || n2 != 0);
    }
}
