import java.util.Scanner;

public class Reto621
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int casos, pagina;
        
        casos = sc.nextInt();
        
        for(int i = 1; i <= casos; i++)
        {
            pagina = sc.nextInt();
            
            if(pagina % 2 == 0)
            {
                System.out.println(pagina+1);
            }
            else
            {
                System.out.println(pagina-1);
            }
        }
    }
}
