import java.util.Scanner;

public class Reto362
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int fechas, dia, mes;
        
        fechas = sc.nextInt();
        
        for(int i = 1; i <= fechas; i++)
        {
            dia = sc.nextInt();
            mes = sc.nextInt();
            
            if(dia == 25 && mes == 12)
            {
                System.out.println("SI");
            }
            else
            {
                System.out.println("NO");
            }
        }
    }
}
