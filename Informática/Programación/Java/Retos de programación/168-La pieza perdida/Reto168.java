import java.util.Scanner;

public class Reto168
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int totalPiezas, sumaTotal, sumaParcial, pieza;
        
        do
        {
            totalPiezas = sc.nextInt();
            
            if(totalPiezas != 0)
            {
                sumaTotal = 0;
                sumaParcial = 0;
                
                // Sumar 1+2+3+...+totalPiezas
                /*
                for(int i = 1; i <= totalPiezas; i++)
                {
                    sumaTotal = sumaTotal + i;
                }
                */
                sumaTotal = (totalPiezas + 1) * totalPiezas / 2;
                
                // Leer totalPiezas-1 y sumarlas
                for(int i = 1; i <= totalPiezas - 1; i++)
                {
                    pieza = sc.nextInt();
                    sumaParcial = sumaParcial + pieza;
                }
                
                // Calcular resta
                System.out.println(sumaTotal - sumaParcial);
            }            
        }
        while(totalPiezas != 0);
    }
}
