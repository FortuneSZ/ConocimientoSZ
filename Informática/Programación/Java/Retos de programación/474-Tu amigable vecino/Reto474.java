import java.util.Scanner;

public class Reto474
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int casos, s, a, b, distSAB, distSBA;
        
        casos = sc.nextInt();
        
        for(int i = 1; i <= casos; i++)
        {
            s = sc.nextInt();
            a = sc.nextInt();
            b = sc.nextInt();
            
            distSAB = Math.abs(s - a) + Math.abs(a - b);
            distSBA = Math.abs(s - b) + Math.abs(b - a);
            
            if(distSAB < distSBA)
            {
                System.out.println(distSAB);
            }
            else
            {
                System.out.println(distSBA);
            }
        }
    }
}
