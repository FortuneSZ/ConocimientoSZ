p { ... }

En todos los párrafos

p.miClase { ... }

En todos los párrafos con clase miclase

p .miClase { ... }

En todas las etiquetas con la clase miclase dentro de un párrafo

p, #miID { ... }

En todos los párrafos y en la etiqueta con id miID

p span em { ... }

En todas las etiquetas em dentro de un span dentro de párrafo

.miClase .miClase2 { ... } 

En todas las etiquetas con clase miclase2 dentro de miclase1

<p>blablablabla <span>blablablabla</span> blablablabla</p>
<p class="miClase">blabla <span>blablabla<em>blabla</em></span> </p>
<h1 class="miID">blablablabla <span class="miClase">aaaa</span> </h1>
<p>blablablabla <span class="miClase">blablabla</span> blabla</p>
<p class="miClase">blabla <span class="miClase2">blabla</span> </p>