﻿/*Este programa empleará una función para mostrar la incial de una palabra
introducida por el suario*/
class Ejercicio_05a_05
{
    static char Inicial(string palabra)
    {
        char initial = palabra[0];
        return initial;
    }
    static void Main()
    {
        string palabra;

        Console.WriteLine("Escribe una palabra");
        palabra = Console.ReadLine();
        Console.Clear();

        Console.WriteLine("la inicial de {0} es {1}",palabra,Inicial(palabra));
    }
}
