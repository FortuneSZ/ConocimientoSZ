﻿/*Este programa emplea una función para decir si el número introducido por el
usuario es o no primo*/
class Ejercicio_05a_07
{
    static bool EsPrimo(int num)
    {
        bool primo = true;
        for (int i = num; i > 1; i--)
        {
            if (i != num && num % i == 0)
            {
                primo = false;
            }
        }
        return primo;
    }
    static void Main()
    {
        int num;

        Console.WriteLine("Escribe un número");
        num = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (EsPrimo(num) == true)
        {
            Console.WriteLine("{0} Es primo",num);
        }
        else
        {
            Console.WriteLine("{0} No es primo",num);
        }
    }
}
