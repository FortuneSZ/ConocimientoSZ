﻿/*Este programa empleará una función que dependiendo del número introducido
por el usuario mostrará 1 en caso de ser positivo,0¡ 0 en caso de ser 0 y
-1 en caso de ser negativo*/
class Ejercicio_05a_04
{
    static int Signo(int num)
    {
        int signoNum;

        if (num > 0)
        {
            signoNum = 1;
        }
        else if (num == 0)
        {
            signoNum = 0;
        }
        else
        {
            signoNum = -1;
        }
        return signoNum;
    }
    static void Main()
    {
        int numero;

        Console.WriteLine("Escribe un número");
        numero = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(Signo(numero));
    }
}
