﻿/*Este programa emplea una función para dibujar un cuadrado de lado 3*/
class Ejercicio_05a_01
{
    static void DibujarCuadrado()
    {
        for(int i = 0; i < 3;i++)
        {
            for (int j = 0; j < 3;j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
    static void Main()
    {
        DibujarCuadrado();
    }
}
