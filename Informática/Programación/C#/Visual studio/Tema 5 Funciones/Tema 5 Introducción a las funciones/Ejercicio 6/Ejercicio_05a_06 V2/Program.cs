﻿/*Este programa emplea una función que pedirá repetidamente un número siempre
y cuando no esté entre los dos números pasados como parámetros,y luego lo 
mostyrará*/
class Ejercicio_05a_06
{
    static int PedirEntero(string texto,int num1,int num2)
    {
        int num;
        Console.WriteLine(texto);
        do
        {
            Console.Clear();
            Console.WriteLine("Introduce un número entre {0} y {1}",num1,num2);
            num = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
        }
        while (num < num1 || num > num2);
        return num;
    }
    static void Main()
    {
        int num1 = 1800,num2 = 2000;
        string texto;

        Console.WriteLine("Escribe un texto");
        texto = Console.ReadLine();
        Console.Clear();

        Console.WriteLine(PedirEntero(texto,num1,num2));
    }
}
