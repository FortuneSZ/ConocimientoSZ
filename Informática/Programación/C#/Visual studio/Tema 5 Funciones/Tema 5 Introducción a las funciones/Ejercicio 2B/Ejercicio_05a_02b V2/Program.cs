﻿/*Este programa emplea una función que dibujará un rectángulo en base a los
parámetros introducidos por el usuario*/
class Ejercicio_05a_02
{
    static void DibujarRectangulo(int ancho, int altura)
    {
        for (int i = 0; i < altura; i++)
        {
            for (int j = 0; j < ancho; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
    static void Main()
    {
        int ancho, altura;

        Console.WriteLine("Escribe el ancho del rectángulo");
        ancho = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escribe el alto del rectángulo");
        altura = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        DibujarRectangulo(ancho, altura);
    }
}
