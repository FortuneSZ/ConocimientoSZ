﻿/*Este programa emplea una función para contar la cantidad de veces que aparece
una letra especificada por el usuario en un texto también especificado*/
class Ejercicio_05a_08
{
    static int ContarLetra(char letra,string texto)
    {
        int letras = 0;

        foreach (char c in texto)
        {
            if (c == letra)
            {
                letras++;
            }
            
        }
        return letras;
    }
    static void Main()
    {
        string texto;
        char letra;

        Console.WriteLine("Introduce un texto");
        texto = Console.ReadLine();
        Console.Clear();

        Console.WriteLine("Introduce una letra");
        letra = Convert.ToChar(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("La letra {0} aparece {1} veces en la frase",letra,
            ContarLetra(letra,texto));
    }
}
