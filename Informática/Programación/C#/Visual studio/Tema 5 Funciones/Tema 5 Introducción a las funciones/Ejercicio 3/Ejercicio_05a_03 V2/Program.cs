﻿/*Este programa emplea una función para repetir un texto determinado n veces,
siendo el texto y las veces determinados por parámetros introducidos por el
usuario*/
class Ejercicio_05a_03
{
    static void RepetirTexto(string texto,int veces)
    {
        for (int i = 0; i < veces; i++)
        {
            Console.WriteLine(texto);
        }
    }
    static void Main()
    {
        string texto;
        int veces;

        Console.WriteLine("Escribe un texto");
        texto = Console.ReadLine();
        Console.Clear();

        Console.WriteLine("Escribe el número de veces que quieres repetirlo");
        veces = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        RepetirTexto(texto,veces);

    }
}
