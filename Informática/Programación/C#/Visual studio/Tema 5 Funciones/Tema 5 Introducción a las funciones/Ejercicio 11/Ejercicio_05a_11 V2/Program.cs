﻿/*Este programa calcula el máximo y el mínimo de un array en una función y
posteriormente los muestra*/
class Ejercicio_05a_11
{
    static void MaxMinArray(int[] num, out int min, out int max)
    {
        min = num[0];
        max = num[0];
        for (int i = 0; i < num.Length; i++)
        {
            if (num[i] < min)
            {
                min = num[i];
            }
            if (num[i] > max)
            {
                max = num[i];
            }
        }
    }
    static void Main()
    {
        int[] num = { 1, 2, 3, 4, 5 };
        int min, max;

        MaxMinArray(num,out min,out max);

        Console.WriteLine("Minimo: {0}", min);
        Console.WriteLine("Máximo: {0}", max);

    }
}
