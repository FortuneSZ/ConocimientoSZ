﻿/*Este programa emplea una función para calcular el área y el perímetro de un
círculo en base a su radio,introducido previamente por el usuario*/
class Ejercicio_05a_10
{
    static void DatosCirculo(double radio,out double área,out double perímetro)
    {
        perímetro = 2 * Math.PI * radio;
        área = Math.PI * (radio * radio);
    }
    static void Main()
    {
        double radio, área, perímetro;

        Console.WriteLine("Escribe el radio del círculo");
        radio = Convert.ToDouble(Console.ReadLine());
        Console.Clear();

        DatosCirculo(radio, out área, out perímetro);

        Console.WriteLine("Radio del círculo: {0}",radio);
        Console.WriteLine("Área del círculo: {0}",área);
        Console.WriteLine("Perímetro del círculo: {0}",perímetro);
    }
}
