﻿/*Este programa emplea una función para intercambiar los valores de dos 
variables numéricas*/
class Ejercicio_05a_09
{
    static void Intercambiar(ref int num1,ref int num2)
    {
        int aux;

        aux = num1;
        num1 = num2;
        num2 = aux;
    }
    static void Main()
    {
        int num1=2,num2=4;

        Console.WriteLine("valor de num1 = {0}, valor de num1 = {1}",num1,
            num2);
        Intercambiar(ref num1,ref num2);
        Console.WriteLine("valor de num1 = {0}, valor de num1 = {1}", num1,
            num2);
    }
}