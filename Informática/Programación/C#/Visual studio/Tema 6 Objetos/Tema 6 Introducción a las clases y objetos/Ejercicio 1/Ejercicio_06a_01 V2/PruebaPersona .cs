﻿/*Este programa crea dos objetos de clase persona y hace que saluden*/
class PruebaPersona
{
    static void Main()
    {
        Persona Fran = new Persona();
        Persona Sara = new Persona();
        Fran.SetNombre("Fran");
        Sara.SetNombre("Sara");
        Fran.Saludar();
        Sara.Saludar();
        
    }
    
}
