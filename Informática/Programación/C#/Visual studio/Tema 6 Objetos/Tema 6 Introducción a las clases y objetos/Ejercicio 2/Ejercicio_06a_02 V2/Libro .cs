﻿class Libro
{
    private string autor;
    private string titulo;
    private string ubicacion;

    public string GetAutor()
    {
        return autor;
    }

    public void SetAutor(string au) 
    {
        autor = au; 
    }

    public string GetTitulo() 
    {
        return titulo;
    }

    public void SetTitulo(string tit)
    {
        titulo = tit;
    }

    public string GetUbicacion()
    {
        return ubicacion;
    }

    public void SetUbicacion(string ub)
    {
        ubicacion = ub;
    }
    
        
}

