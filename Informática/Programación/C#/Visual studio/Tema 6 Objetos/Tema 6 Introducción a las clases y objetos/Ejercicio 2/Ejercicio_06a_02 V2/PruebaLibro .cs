﻿class PruebaLibro
{
    static void Main()
    {
        Libro ellanoteama= new Libro();

        ellanoteama.SetAutor("Ella");
        ellanoteama.SetTitulo("No te ama");
        ellanoteama.SetUbicacion("Su corazón");

        Console.WriteLine("Autor: {0}",ellanoteama.GetAutor());
        Console.WriteLine("Título: {0}",ellanoteama.GetTitulo());
        Console.WriteLine("Ubicación: {0}", ellanoteama.GetUbicacion());
    }
}
