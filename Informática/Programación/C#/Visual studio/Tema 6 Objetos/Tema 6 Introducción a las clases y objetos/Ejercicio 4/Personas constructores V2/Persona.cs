﻿class Persona
{
    private string nombre;

    public string GetNombre()
    {
        return nombre;
    }

    public void SetNombre(string n)
    {
        nombre = n;
    }

    public void Saludar()
    {
        Console.WriteLine("Hola soy {0}", nombre);
    }

    public Persona(string nombre)
    {
        this.nombre = nombre;
    }
}
