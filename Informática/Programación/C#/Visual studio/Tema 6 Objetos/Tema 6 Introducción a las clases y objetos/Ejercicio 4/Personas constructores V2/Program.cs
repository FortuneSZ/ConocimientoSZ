﻿/*Este programa crea dos objetos de clase persona y hace que saluden*/
class PruebaPersona
{
    static void Main()
    {
        Persona Fran = new Persona("Fran");
        Persona Sara = new Persona("Sara");
        Fran.Saludar();
        Sara.Saludar();

    }

}
