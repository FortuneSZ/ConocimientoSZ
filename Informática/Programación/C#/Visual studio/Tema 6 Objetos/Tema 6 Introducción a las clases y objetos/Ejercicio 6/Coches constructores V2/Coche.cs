﻿class Coche
{
    private string marca;
    private string modelo;
    private int cilindrada;
    private float potencia;

    public string GetMarca()
    {
        return marca;
    }

    public void SetMarca(string marca)
    {
        this.marca = marca;
    }

    public string GetModelo()
    {
        return modelo;
    }

    public void SetModelo(string modelo)
    {
        this.modelo = modelo;
    }

    public int GetCilindrada()
    {
        return cilindrada;
    }

    public void SetClindrada(int cilindrada)
    {
        this.cilindrada = cilindrada;
    }

    public float GetPotencia()
    {
        return potencia;
    }

    public void SetPotencia(float potencia)
    {
        this.potencia = potencia;
    }

    public Coche() 
    {
        this.marca = "kirby";
        this.modelo = "dreamland";
        this.potencia=0;
        this.cilindrada=0;
    }

    public Coche(string marca, string modelo, int cilindrada,
        float potencia)
    {
        this.marca = marca;
        this.modelo = modelo;
        this.cilindrada = cilindrada;
        this.potencia = potencia;
    }
}


