﻿class PruebaCoche
{
    static void Main()
    {
        Coche TuChoche = new Coche();

        Console.WriteLine("Dime la marca");
        TuChoche.SetMarca(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Dime el modelo");
        TuChoche.SetModelo(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Dime la clindrada");
        TuChoche.SetClindrada(Convert.ToInt32(Console.ReadLine()));
        Console.Clear();

        Console.WriteLine("Dime la potencia");
        TuChoche.SetPotencia(Convert.ToSingle(Console.ReadLine()));
        Console.Clear();

        Console.WriteLine("Marca: {0}",TuChoche.GetMarca());
        Console.WriteLine("Modelo: {0}",TuChoche.GetModelo());
        Console.WriteLine("Cilindrada: {0}",TuChoche.GetCilindrada());
        Console.WriteLine("Potencia: {0}",TuChoche.GetPotencia());

    }
}
