﻿//Clase usuario
class Usuario
{
    protected string dni;
    protected string nombre;
    protected Poliza poliza;

    public string DNI
    {
        get
        {
            return dni;
        }
        set
        {
            dni = value;
        }
    }

    public string Nombre
    {
        get
        {
            return nombre;
        }
        set 
        {
            nombre = value;
        }
    }

    public Poliza Poliza
    {
        get 
        {
            return poliza;
        }
        set
        {
            poliza = value;
        }
    }

    public Usuario(string dni, string nombre, Poliza poliza)
    {
        this.dni = dni;
        this.nombre = nombre;
        this.poliza = poliza;
    }

    public override string ToString()
    {
        return "dni: " + dni 
            + "\nnombre: "
            + nombre + "\n"
            + poliza.ToString();
    }
}

