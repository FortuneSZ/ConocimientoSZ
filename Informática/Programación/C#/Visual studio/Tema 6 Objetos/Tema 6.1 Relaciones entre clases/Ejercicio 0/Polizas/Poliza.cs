﻿//Clase poliza
class Poliza
{
    protected int id;
    protected double precio;

    public int ID
    {
        get
        {
            return ID;
        }
        set
        {
            precio = value;
        }
    }

    public double Precio
    {
        get
        {
            return precio;
        }
        set
        {
            precio = value;
        }
    }

    public Poliza(int id, double precio)
    {
        this.id = id;
        this.precio = precio;
    }

    public override string ToString()
    {
        return "id: " + id + " precio poliza: " + precio + " euros";
    }
}

