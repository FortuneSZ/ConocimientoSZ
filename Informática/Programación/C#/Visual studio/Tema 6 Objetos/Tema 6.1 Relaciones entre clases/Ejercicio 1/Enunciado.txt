Crea un proyecto llamado CasasPuertas. Crea una clase Puerta que tenga un ancho y un alto como atributos, y una clase Casa que tenga un nombre y 3 puertas. 
Define el/los constructores que consideres convenientes para asignar a una casa su nombre y sus puertas, 
y un método MostrarEstado que muestre por pantalla el nombre de la casa y los datos de sus puertas