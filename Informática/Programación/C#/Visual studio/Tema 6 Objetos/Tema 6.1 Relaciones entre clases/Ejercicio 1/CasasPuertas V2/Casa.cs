﻿class Casa
{
    private string nombre;
    private Puerta p1;
    private Puerta p2;
    private Puerta p3;

    public string Nombre
    {
        get 
        {
            return nombre; 
        }
        set
        {
            nombre = value; 
        }
    }

    public Casa(string nombre, Puerta p1, Puerta p2, Puerta p3)
    {
        Nombre = nombre;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public override string ToString()
    {
        return "Nombre: " + nombre + ", Puerta 1: " + p1 +
            ", Puerta 2: " + p2 + ", Puerta 3: " + p3;
    }
}

