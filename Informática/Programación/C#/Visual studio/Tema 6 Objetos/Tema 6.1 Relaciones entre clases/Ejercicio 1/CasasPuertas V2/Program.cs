﻿class CasasPuertas
{
    static void Main()
    {
        Puerta p1 = new Puerta(5, 4);
        Puerta p2 = new Puerta(10, 2);
        Puerta p3 = new Puerta(1, 2);
        Casa c = new Casa("Casa",p1,p2,p3);

        Console.WriteLine(c);
    }
}
