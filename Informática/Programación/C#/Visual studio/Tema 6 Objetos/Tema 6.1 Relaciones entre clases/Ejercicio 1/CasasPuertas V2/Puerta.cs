﻿class Puerta
{
    private int ancho;
    private int alto;

    public int Ancho
    {
        get {
            return ancho; 
        }
        set 
        {
            ancho = value; 
        }
    }

    public int Alto
    {
        get 
        {
            return alto;
        }
        set 
        {
            alto = value; 
        }
    }

    public Puerta(int ancho, int alto)
    {
        Ancho = ancho;
        Alto = alto;
    }

    public override string ToString()
    {
        return "Ancho: " + ancho + ", Alto: " + alto;
    }
}

