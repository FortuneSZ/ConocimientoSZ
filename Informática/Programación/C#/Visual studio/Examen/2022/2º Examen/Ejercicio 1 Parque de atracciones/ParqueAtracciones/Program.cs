﻿//Programa principal
class ParqueAtracciones
{
    enum menu { nuevo = 1,cliente,habitacion,lista,salir};
    static void Menu()
    {
        Console.WriteLine((int)menu.nuevo +"." 
            + " Nuevo cliente");
        Console.WriteLine((int)menu.cliente + "." 
            + " Buscar por cliente");
        Console.WriteLine((int)menu.habitacion + "." 
            + " Buscar por habitación");
        Console.WriteLine((int)menu.lista + "." 
            + " Listar clientes");
        Console.WriteLine((int)menu.salir + "."
            + " Salir");
        Console.WriteLine();
        Console.WriteLine("Elija una opción");
        Console.WriteLine();

    }

    static void Enter()
    {
        Console.WriteLine();
        Console.WriteLine("Pulsa enter para continuar");
        Console.ReadLine();
        Console.Clear();
    }

    static void MostrarHab(Habitacion[] habitaciones)
    {
        for (int i = 0; i < habitaciones.Length; i++) 
        {
            Console.WriteLine("{0}. " +habitaciones[i],i+1);
        }
    }

    static void Lista(Cliente[] clientes, int numCli)
    {
        if (numCli > 0) 
        {
            Console.Clear();
            for (int i = 0; i < numCli; i++)
            {
                if (clientes[i] is ClienteVIP)
                {
                    Console.ForegroundColor= ConsoleColor.Yellow;
                    
                }
                Console.WriteLine(clientes[i]);
                Console.ResetColor();
            }
        }
        else
        {
            Console.WriteLine("No hay clientes que mostrar");
        }
        Enter();
    }

    static void BuscarHab(Habitacion[] habitaciones,
        Cliente[] clientes, int numCli)
    {
        int opcion;
        bool encontrado = false;

        Console.Clear();
        MostrarHab(habitaciones);
        Console.WriteLine();
        Console.WriteLine("Elija una habitación" +
            " del listado");
        opcion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Clientes alojados en ella:");
        Console.WriteLine();
        for (int i = 0; i < numCli; i++)
        {
            if (clientes[i] is ClienteVIP)
            {
                if (((ClienteVIP)clientes[i]).Hab
                    == habitaciones[opcion - 1])
                {
                    encontrado = true;
                    Console.WriteLine(clientes[i]);
                }
            }
        }
        if (!encontrado)
        {
            Console.Clear();
            Console.WriteLine("No hay clientes" +
                " alojados en esa habitación");
        }
        Enter();
    }
    static void BuscCli(Cliente[] clientes, int numCli)
    {
        bool encontrado = false;
        string dniBusc;

        Console.Clear();
        if (numCli > 0) 
        {
            Console.WriteLine("Introduzca el dni del cliente");
            dniBusc = Console.ReadLine();
            Console.Clear();
            for (int i = 0; i < numCli; i++)
            {
                if (dniBusc == clientes[i].Dni)
                {
                    encontrado= true;
                    Console.WriteLine(clientes[i]);
                }
            }
            if (encontrado = false) 
            {
                Console.WriteLine("Cliente no encontrado");
                Console.WriteLine("Cliente no encontrado");
            }
        }
        else
        {
            Console.WriteLine("No hay clientes");
        }
        Enter();
    }

    static void Cliente(Cliente[] clientes, int numCli
        ,out string dni, out string nombre, out int edad)
    {
        bool existe = false, valido = true;
        edad = 0;

        Console.Clear();
        Console.WriteLine("Introduzca el dni");
        dni = Console.ReadLine();
        if (numCli > 0) 
        {
            for (int i = 0; i < numCli; i++) 
            {
                if (dni == clientes[i].Dni)
                {
                    existe = true;
                }
            }
            if (existe) 
            {
                throw new Exception("El cliente existe");
            }
        }
        Console.Clear();
        Console.WriteLine("Introduzca el nombre del cliente");
        nombre = Console.ReadLine();
        do
        {
            Console.Clear();
            Console.WriteLine("Introduzca la edad del cliente");
            if (Int32.TryParse(Console.ReadLine(), out edad))
            {
                valido = true;
            }
            else
            {
                valido = false;
            }
        }   
        while (valido != true);
    }

    static void Nuevo(Habitacion[] habitaciones, Cliente[] clientes
        ,ref int numCli)
    {
        string eleccion, dni, nombre;
        int opcion, edad;

        try
        {
            Console.Clear();
            Console.WriteLine("¿Es un cliente normal o VIP?");
            eleccion = Console.ReadLine();

            switch (eleccion.ToUpper())
            {
                case "NORMAL":
                    Cliente(clientes, numCli, out dni, out nombre,
                        out edad);
                    clientes[numCli] = new Cliente(dni, nombre,edad);
                    numCli++;
                    break;

                case "VIP":
                    do
                    {
                        Console.Clear();
                        MostrarHab(habitaciones);
                        Console.WriteLine();
                        Console.WriteLine("Elija una habitación" +
                            " del listado");
                        opcion = Convert.ToInt32(Console.ReadLine());
                    }
                    while(opcion < 0 || opcion > habitaciones.Length);
                    Cliente(clientes, numCli, out dni, out nombre,
                        out edad);
                    clientes[numCli] = new ClienteVIP
                        (habitaciones[opcion-1],dni,nombre,edad);
                    numCli++;
                    break;
            }
            Console.Clear();
        }
        catch(Exception e) 
        {
            Console.Clear();
            Console.WriteLine(e.Message);
            Console.WriteLine();
            Console.WriteLine("Pulse enter para continuar");
            Console.ReadLine();
            Console.Clear();
        }
    }
    static void Main()
    {
        Habitacion[] habitaciones = new Habitacion[4];
        habitaciones[0] = new Habitacion(5, "San francisco", 2);
        habitaciones[1] = new Habitacion(2, "Santo domingo", 4);
        habitaciones[2] = new Habitacion(8, "Santa clara", 6);
        habitaciones[3] = new Habitacion(3, "San pedro", 1);

        Cliente[] clientes= new Cliente[20];

        int opcion, numCli = 0;

        do
        {
            Menu();
            opcion = Convert.ToInt32(Console.ReadLine());

            switch ((menu)opcion) 
            {
                case menu.nuevo:
                    Nuevo(habitaciones, clientes,ref numCli);
                    break;

                case menu.cliente:
                    BuscCli(clientes, numCli);
                    break;

                case menu.habitacion:
                    BuscarHab(habitaciones, clientes, numCli);
                    break;

                case menu.lista:
                    Lista(clientes,numCli);
                    break;
            }
        }
        while ((menu)opcion != menu.salir);
    }
}
