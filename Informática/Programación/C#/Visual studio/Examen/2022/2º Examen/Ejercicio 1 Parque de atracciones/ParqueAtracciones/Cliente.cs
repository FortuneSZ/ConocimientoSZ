﻿//Clase cliente
class Cliente
{
    protected string dni;
    protected string nombre;
    protected int edad;

    public string Dni
    {
        get
        {
            return dni;
        }
        set
        {
            dni = value;
        }
    }

    public string Nombre
    {
        get
        {
            return nombre;
        }
        set
        {
            nombre = value;
        }
    }

    public int Edad
    {
        get
        {
            return edad;
        }
        set
        {
            if (value < 0 || value < 100)
            {
                edad = value;
            }
            else
            {
                edad = 0;
            }
            
        }
    }

    public Cliente(string dni, string nombre, int edad)
    {
        Dni = dni;
        Nombre = nombre;
        Edad = edad;
    }

    public override string ToString()
    {
        return dni + " - " + nombre + " " + ", " + edad + " años.";
    }
}

