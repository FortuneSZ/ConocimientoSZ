﻿//Clase habitación
class Habitacion
{
    protected int numero;
    protected string hotel;
    protected int numPlazas;

    public int Numero
    {
        get
        {
            return numero;
        }
        set
        {
            numero = value;
        }
    }

    public string Hotel
    {
        get
        {
            return hotel;
        }
        set
        {
            hotel = value;
        }
    }

    public int NumPlazas
    {
        get
        {
            return numPlazas;
        }
        set
        {
            numPlazas = value;
        }
    }

    public Habitacion(int numero, string hotel, int numPlazas) 
    {
        Numero = numero;
        Hotel = hotel;
        NumPlazas = numPlazas;
    }

    public override string ToString()
    {
        return Hotel+ ", habitación " + Numero + ", " + NumPlazas + " personas";
    }
}

