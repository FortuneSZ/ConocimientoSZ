﻿//Clase cliente vip
class ClienteVIP : Cliente
{
    private Habitacion hab;

    public Habitacion Hab
    {
        get 
        {
            return hab; 
        }
        set
        {
            hab = value; 
        }
    }

    public ClienteVIP(Habitacion hab, string dni,
        string nombre, int edad)
        :base(dni,nombre,edad)
    {
        Hab = hab;
    }

    public override string ToString()
    {
        return base.ToString() + " " + hab.Hotel
            + ", habitación " + hab.Numero + ", " + hab.NumPlazas + " personas";
    }
}

