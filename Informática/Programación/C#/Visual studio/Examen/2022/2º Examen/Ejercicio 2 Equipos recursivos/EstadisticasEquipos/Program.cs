﻿class EstadisticasEquipos
{
    static bool ComprobarNombre(string equipos)
    {
        if (equipos.Length == 1)
        {
            return true;
        }
        else
        {
            if (equipos[0].CompareTo("a") < 0 &&
                equipos[0].CompareTo("Z") > 0 ||
                equipos[0].CompareTo("A") < 0 ||
                equipos[0].CompareTo("z") > 0)
            {
                return false;
            }
            else
            {
                ComprobarNombre(equipos.Substring(1));
            }
        }
        return true;
    }
    static int CalcularPuntos(string partido)
    {
        if (partido.Length == 1) 
        {
            return (int)partido[0];
        }
        else if ((int)partido[0] >
            CalcularPuntos(partido.Substring(2)))
        {
            return 3;
        }
        else if ((int)partido[0] ==
            CalcularPuntos(partido.Substring(2)))
        {
            return 1;
        }
        else
        {
            return 0;
        }

            

        
    }
    static void Main(string[] args)
    {
        string partido;
        string[] partidos, equipos = args;
        int puntos;
        bool valido;

        if (args.Length < 1)
        {
            Console.WriteLine("No se han recibido equipos");
        }

        for (int i = 0; i < equipos.Length; i++)
        {
            valido = ComprobarNombre(equipos[i]);
            Console.WriteLine();
            if (valido)
            {
                puntos = 0;
                Console.Clear();
                Console.WriteLine("Introduce los resultados de los" +
                    "partidos en formato 1-2 separados por" +
                    "espacios");
                partido = Console.ReadLine();
                partidos = partido.Split(" ");

                for (int x = 0; x < partidos.Length; x++)
                {
                    puntos += CalcularPuntos(partidos[x]);
                }
                Console.WriteLine();
                Console.WriteLine("{0} tiene {1] puntos", equipos[i],puntos);
            }
            else
            {
                Console.WriteLine("{0} no es valido", equipos[i]);
            }
        }
    }
}