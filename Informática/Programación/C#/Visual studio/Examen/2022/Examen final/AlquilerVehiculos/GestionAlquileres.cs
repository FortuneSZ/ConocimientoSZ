﻿using System;

class GestionAlquileres
{
    private Vehiculo[] vehiculos;
    private List<Usuario> usuarios;
    private List<Alquiler> alquileres;

    public GestionAlquileres()
    {
        RellenarVehiculos();
        CargarUsuarios();
        alquileres = new List<Alquiler>();
    }

    public void RellenarVehiculos()
    {
        vehiculos = new Vehiculo[5];
        vehiculos[0] = new Coche("AHSGXNDJ45", "Peugeot", "Si", 4, 5);
        vehiculos[1] = new Furgoneta("DSUNHFEVKJ58", "Seat", "Ibiza", 6, 10, 700);
        vehiculos[2] = new Coche("QURJFKG87", "Mercedes", "niIdea", 2, 25);
        vehiculos[3] = new Furgoneta("KFHYRGTRBVCVFJH25", "Ferrari", "Whynot", 7, 50, 800);
        vehiculos[4] = new Coche("KFHERUYRJFTJ91", "Murcia", "Sevilla", 4, 25);
    }

    public void CargarUsuarios()
    {
        usuarios = new List<Usuario>();
        if (File.Exists("usuarios.txt"))
        {
            string linea;
            using (StreamReader users = new StreamReader("usuarios.txt"))
            {
                while ((linea = users.ReadLine()) != null)
                {
                    string[] partes = linea.Split(';');
                    if (partes.Length == 2)
                    {
                        usuarios.Add(new Comercial(partes[0], partes[1]));
                    }
                    if (partes.Length == 3)
                    {
                        usuarios.Add(new Cliente(partes[0], partes[1], partes[2]));
                    }
                }
                users.Close();
            }
        }
    }

    public void AlquilarVehiculo(Cliente c, Vehiculo v, DateTime d)
    {
        Alquiler al = new Alquiler(c, v, d);
        al.FechaFin = al.FechaInicio;
        alquileres.Add(al);
    }

    public void DevolverVehiculo(Cliente c, Vehiculo v, DateTime d)
    {
        for (int i = 0; i < alquileres.Count; i++)
        {
            if (alquileres[i].Cli == c && alquileres[i].Vehi == v) 
            {
                if (d < alquileres[i].FechaInicio)
                {
                    alquileres[i].FechaFin = alquileres[i].FechaInicio.AddDays(1);
                }
                else
                {
                    alquileres[i].FechaFin = d;
                }
                CerrarAlquiler(alquileres[i]);
                alquileres.Remove(alquileres[i]);
            }
        }
    }

    public void CerrarAlquiler(Alquiler al)
    {
        using (StreamWriter alquilerFin = new StreamWriter("alquileres_cerrados.txt", true))
        {
            alquilerFin.WriteLine(al);
        }
    }

    public void VerAlquileres()
    {
        alquileres.Sort();
        foreach(Alquiler al in alquileres)
        {
            Console.WriteLine(al);
            Console.ReadLine();
        }
    }

    public Usuario Login(String user, String password)
    {
        Usuario us = null;

        us = usuarios.Where(u => u.Login == user && u.Password == password).FirstOrDefault();
        return us;
    }

    public Cliente ElegirCliente()
    {
        for (int i =0; i < usuarios.Count; i++)
        {
            if (usuarios[i] is Cliente)
            {
                Console.WriteLine(usuarios[i].Login);
            }
        }
        Console.WriteLine();
        Console.WriteLine("Introduzca el login del cliente");
        string cli = Console.ReadLine();
        Cliente C = null;
        C = (Cliente)usuarios.Where(u => u.Login == cli).FirstOrDefault();
        return C;
    }

    public Vehiculo ElegirVehiculo()
    {
        foreach (Vehiculo v in vehiculos)
        {
            Console.WriteLine(v.Matricula);
        }
        Console.WriteLine();
        Console.WriteLine("Introduzca la matrícula del vehículo");
        string ve = Console.ReadLine();
        Vehiculo V = null;
        V = vehiculos.Where(u => u.Matricula == ve).FirstOrDefault();
        return V;
    }




}

