﻿class Alquiler : IComparable<Alquiler>
{
    private Cliente cli;
    private Vehiculo vehi;
    private DateTime fechaInicio;
    private DateTime fechaFin;

    public Cliente Cli
    {
        get 
        {
            return this.cli; 
        }
        set
        {
            this.cli = value; 
        }
    }
    public Vehiculo Vehi
    {
        get
        {
            return this.vehi;
        }
        set 
        {
            this.vehi = value;
        }
    }

    public DateTime FechaInicio
    {
        get
        {
            return this.fechaInicio;
        }
        set
        {
            this.fechaInicio = value;
        }
    }

    public DateTime FechaFin
    {
        get
        {
            return this.fechaFin;
        }
        set
        {
            this.fechaFin = value;
        }
    }

    public Alquiler(Cliente cli, Vehiculo vehi, DateTime fechaInicio)
    {
        Cli = cli;
        this.vehi = vehi;
        FechaInicio = fechaInicio;
        FechaFin = fechaInicio;
    }

    public double calcularPrecio()
    {
        TimeSpan diferencia = fechaFin.Subtract(fechaInicio);
        return diferencia.Hours * Vehi.PrecioHora;
    }

    public int CompareTo(Alquiler otra)
    {
        return otra.fechaInicio.CompareTo(this.fechaInicio);
    }


    public override string ToString()
    {
        return cli.Dni + ", " + Cli.Login + ", " + Vehi.Matricula + ", " + fechaInicio.ToString("F") + ", " + fechaFin.ToString("F") + calcularPrecio().ToString("0.##");
    }
}

