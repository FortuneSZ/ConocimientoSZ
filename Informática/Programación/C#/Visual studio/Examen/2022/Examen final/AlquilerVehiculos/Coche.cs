﻿class Coche : Vehiculo
{
    public Coche(string matricula,string marca,string modelo, int numPuertas,double precioHora)
        :base(matricula,marca,modelo,numPuertas,precioHora)
    {
        Matricula= matricula;
        Marca= marca;
        Modelo= modelo;
        NumPuertas= numPuertas;
        PrecioHora= precioHora;
    }

    public override string ToString()
    {
        return base.ToString();
    }
}

