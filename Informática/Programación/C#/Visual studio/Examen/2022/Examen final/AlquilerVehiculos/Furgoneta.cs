﻿class Furgoneta : Vehiculo
{
    private int pesoMax;

    public int PesoMax
    {
        get
        {
            return pesoMax;
        }
        set
        {
            if (value > 0) 
            {
                pesoMax = value;
            }
            else
            {
                pesoMax = 750;
            }
            
        }
    }
    public Furgoneta(string matricula,string marca, string modelo, int numPuertas, double precioHora, int pesoMax)
        : base(matricula, marca, modelo, numPuertas, precioHora)
    {
        Matricula = matricula;
        Marca = marca;
        Modelo = modelo;
        NumPuertas = numPuertas;
        PrecioHora = precioHora;
        PesoMax = pesoMax;
    }

    public override string ToString()
    {
        return base.ToString() + "-" + PesoMax;
    }
}

