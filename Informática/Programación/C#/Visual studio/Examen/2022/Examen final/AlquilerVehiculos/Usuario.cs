﻿abstract class Usuario
{
    protected string login;
    protected string password;

    public string Login
    {
        get 
        { 
            return login; 
        } 
        set
        {
            login = value;
        }
    }

    public string Password
    {
        get 
        {
            return password;
        }
        set
        {
            password = value; 
        }
    }

    public Usuario(string login, string password)
    {
        Login = login;
        Password = password;
    }

    public override string ToString()
    {
        return login;
    }
}

