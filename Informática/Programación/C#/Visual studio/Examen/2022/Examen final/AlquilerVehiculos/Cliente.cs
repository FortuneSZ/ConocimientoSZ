﻿class Cliente : Usuario
{
    private string dni;

    public string Dni
    {
        get 
        {
            return dni; 
        }
        set
        {
            dni = value; 
        }
    }
    public Cliente(string login, string password, string dni) : base(login, password)
    {
        Login = login;
        Password = password;
        Dni = dni;
    }

    public override string ToString()
    {
        return base.ToString() + " " + dni;
    }
}

