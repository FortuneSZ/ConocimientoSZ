﻿using System.ComponentModel.DataAnnotations;

class AlquierVehiculos
{
    static void Nuevo(GestionAlquileres ges)
    {
        Cliente cli = ges.ElegirCliente();
        if (cli != null)
        {
            Console.Clear();
            Vehiculo v = ges.ElegirVehiculo();
            if (v != null)
            {      
                bool valido;
                do
                {
                    Console.Clear();
                    valido = true;
                    try
                    {
                        Console.WriteLine("Escribe la fecha de inicio" +
                        " en formato DD/MM/YYYY");
                        DateTime fecha = DateTime.ParseExact(Console.ReadLine()
                            ,"dd/MM/yyyy", null);
                        ges.AlquilarVehiculo(cli, v, fecha);
                    }
                    catch (Exception ex)
                    {
                        valido = false;
                    }
                }
                while (valido != true);
                
            }
        }
    }

    static void Eliminar(GestionAlquileres ges)
    {
        Cliente cli2 = ges.ElegirCliente();
        if (cli2 != null)
        {
            Console.Clear();
            Vehiculo v = ges.ElegirVehiculo();
            if (v != null)
            {
                bool valido;
                do
                {
                    Console.Clear();
                    valido = true;
                    try
                    {
                        Console.WriteLine("Escribe la fecha de fin" +
                        " en formato DD/MM/YYYY");
                        DateTime fecha = DateTime.ParseExact(Console.ReadLine(),
                        "dd/MM/yyyy", null);
                        ges.DevolverVehiculo(cli2, v, fecha);
                    }
                    catch (Exception ex) 
                    {
                        valido = false;
                    }
                }
                while (valido != true);
            }
        }
    }
    static void Main()
    {
        GestionAlquileres ges = new GestionAlquileres();
        Usuario log = null;
        do
        {
            Console.WriteLine("Logueese");
            string login = Console.ReadLine();
            string pass = Console.ReadLine();
            log = ges.Login(login, pass);
        }
        while(log == null);
        int opcion = 0;
        do
        {
            Console.Clear();
            Console.WriteLine("1. Nuevo alquiler");
            Console.WriteLine("2. Devolver vehículo");
            Console.WriteLine("3. Ver alquileres");
            Console.WriteLine("4. Salir");
            Console.WriteLine("");

            opcion = Convert.ToInt32(Console.ReadLine());

            switch (opcion)
            {
                case 1:
                    Console.Clear();
                    Nuevo(ges);
                    break;

                case 2:
                    Console.Clear();
                    Eliminar(ges);
                    break;

                case 3:
                    Console.Clear();
                    ges.VerAlquileres();
                    break;

            }
        }
        while(opcion != 4);
        
    }
}
