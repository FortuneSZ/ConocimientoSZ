﻿abstract class Vehiculo
{
    protected string matricula;
    protected string marca;
    protected string modelo;
    protected int numPuertas;
    protected double precioHora;

    public string Matricula
    {
        get 
        { 
            return matricula; 
        }
        set
        { 
            matricula = value; 
        }
    }

    public string Marca
    {
        get
        {
            return marca;
        }
        set
        {
            marca = value;
        }
    }
    public string Modelo
    {
        get
        {
            return modelo;
        }
        set
        {
            modelo = value;
        }
    }
    public int NumPuertas
    {
        get
        {
            return numPuertas;
        }
        set
        {
            if (value >= 2 && value <= 7)
            {
                numPuertas = value;
            }
            else
            {
                numPuertas = 2;
            }
        }
    }
    public double PrecioHora
    {
        get
        {
            return precioHora;
        }
        set 
        {
            if (value > 0)
            {
                precioHora = value;
            }           
        }
    }

    public Vehiculo(string matricula, string marca, string modelo, int numPuertas, double precioHora)
    {
        Matricula = matricula;
        Marca = marca;
        Modelo = modelo;
        NumPuertas = numPuertas;
        PrecioHora = precioHora;
    }

    public override string ToString()
    {
        return Matricula + "-" + Marca + "-" + Modelo + "-" + NumPuertas + "-" + PrecioHora;
    }
}

