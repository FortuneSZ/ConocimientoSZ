﻿/*Este programa consiste en un menú modularizado de recetas, con las opciones:
nueva receta,que añade una receta al final del array,ordenar, que ordena las
recetas alfabéticamente de forma ascendente, buscar, que permite buscar una
cantidad máxima de calorías y que muestre solo las inferiores a este y la
última opción es salir*/
class Ejercicio1Examen
{
    struct Receta
    {
        public string nombre;
        public int calorias;
        public InfoAdicional infor;
    }
    struct InfoAdicional
    {
        public int comensales;
        public char dificultad;
    }
    enum Menu
    {
        salir,nuevo,ordenar,buscar
    }
    static int MostrarMenu()
    {

        int opcion;

        Console.WriteLine((int)Menu.nuevo + ". Nueva receta");
        Console.WriteLine();
        Console.WriteLine((int)Menu.ordenar + ". Ordenar recetas por nombre");
        Console.WriteLine();
        Console.WriteLine((int)Menu.buscar + ". Buscar recetas por calorías");
        Console.WriteLine();
        Console.WriteLine((int)Menu.salir + ". Salir");

        Console.WriteLine();
        Console.WriteLine("Elija una opción");
        opcion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        return opcion;
    }

    static void NuevaReceta(Receta[] recetas, ref int cantidad)
    {
        Receta Receta;
        bool error = false;
        if (cantidad != recetas.Length)
        {
            Console.WriteLine("Introduzca el nombre de la receta");
            Receta.nombre = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Introduzca las calorías de la receta");
            Receta.calorias = Convert.ToInt32(Console.ReadLine());
            do
            {
                Console.Clear();
                error = false;
                Console.WriteLine("Introduzca el número de comensales");
                Receta.infor.comensales = 1;
                try
                {
                    Receta.infor.comensales = 
                        Convert.ToInt32(Console.ReadLine());

                    if (Receta.infor.comensales < 0 ||
                        Receta.infor.comensales > 6)
                    {
                        error = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Indica la dificultad de la receta");
                        Receta.infor.dificultad = Convert.ToChar
                            (Console.ReadLine());
                        recetas[cantidad] = Receta;
                        cantidad++;
                        Console.Clear();
                        ListaRecetasCompleta(recetas, cantidad);
                    }
                }

                catch (Exception e)
                {
                    error = true;
                }
            }
            while (error == true || Receta.infor.comensales < 0 ||
                        Receta.infor.comensales > 6); 
        }
        else
        {
            Console.WriteLine("No queda espacio disponible");
        }
        enter();
    }

    static void Ordenar(Receta[] recetas, int cantidad)
    {
        if (cantidad > 0)
        {
            for (int i = 0; i < cantidad; i++)
            {
                for (int j = 0; j < cantidad - i - 1; j++)
                {
                    if (recetas[j].nombre.CompareTo(recetas[j + 1].nombre) > 0)
                    {
                        Receta auxiliar = recetas[j];
                        recetas[j] = recetas[j + 1];
                        recetas[j + 1] = auxiliar;
                    }
                }
            }
            ListaRecetasCompleta(recetas, cantidad);
        }
        else
        {
            Console.WriteLine("No hay recetas almacenadas");
        }
        enter();
    } 
    static void enter()
    {
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
        Console.Clear();
    }

    static void ListaRecetas(Receta[] recetas, int cantidad)
    {
        Console.Write("{0} ({1} cal): {2} comensales, dificultad {3}",
            recetas[cantidad].nombre, recetas[cantidad].calorias,
            recetas[cantidad].infor.comensales, 
            recetas[cantidad].infor.dificultad);
        Console.WriteLine();
    }
    static void ListaRecetasCompleta(Receta[] recetas, int cantidad)
    {
        Console.WriteLine("Listado de recetas:");
        Console.WriteLine();
        for (int i = 0; i < cantidad; i++)
        {
            ListaRecetas(recetas, i);
        }
    }

    static void BuscarPorCalorias(Receta[] recetas, int cantidad)
    {
        bool encontrado = false;
        int calorias;

        if (cantidad > 0)
        {
            Console.WriteLine("Introduzca el número máximo de calorias");
            calorias = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            for (int i = 0; i < cantidad; i++)
            {
                if (recetas[i].calorias <= calorias)
                {
                    encontrado = true;
                    ListaRecetas(recetas, i);
                }
            }
            if (encontrado == false)
            {
                Console.WriteLine("No hay recetas que cumplan el criterio");
            }
        }
        else
        {
            Console.WriteLine("No hay recetas almacenadas");
        }
        enter();
    }
    static void Main()
    {
        Receta[] recetas = new Receta[30];
        int cantidad = 0;
        int opcion;

        do
        {
            opcion = MostrarMenu();

            switch ((Menu)opcion)
            {
                case Menu.nuevo:
                    NuevaReceta(recetas, ref cantidad);
                    break;
                    
                case Menu.ordenar:
                    Ordenar(recetas, cantidad);
                    break;

                case Menu.buscar:
                    BuscarPorCalorias(recetas, cantidad);
                    break;
            }
        }
        while ((Menu)opcion != Menu.salir);

        
        
    }
}
