﻿/**/
class Ejercicio2Examen
{
    static bool ValidarEntrada(string linea)
    {
        bool valido= true;

        string[] partes = linea.Split(' ');

        for (int i = 0; i < partes.Length; i++)
        {
            if (partes[0] == "A" ||
                partes[0] == "V" ||
                partes[0] == "C")
            {
                if (i != 0 && Convert.ToInt32(partes[1]) > 10 ||
                    Convert.ToInt32(partes[1]) < 0)
                {
                    valido = false;
                }
            }
            else
            {
                valido= false;
            }
        }

        return valido;

    }

    static int[] ProcesarEntrada(string linea, out char provincia)
    {
        string[] partes = linea.Split(' ');

        provincia = Convert.ToChar(partes[0]);

        int[] numeros = new int[partes.Length -1]; 

        for (int i = 0; i < numeros.Length; i++)
        {
            numeros[i] = Convert.ToInt32(partes[i+1]);
        }

        return numeros;
    }

    static float Media(int[] notas)
    {
        float media = 0f;

        for (int i = 0; i < notas.Length; i++)
        {
            media += i;
        }
        media /= notas.Length;

        return media;
    }

    static int Mediana(int[] notas)
    {
        Array.Sort(notas);
        int mediana = notas[notas.Length / 2];

        return mediana;
    }

    static int Moda(int[] notas)
    {
        int moda = 0;
        for (int i = 0; i < notas.Length; i++)
        {
            for (int j = 0; j < notas.Length; j++)
            {
                if (notas[i] == notas[j])
                {
                    notas[i]++;
                }
                for (int x = 0; x < notas.Length; x++)
                {
                    if (moda == 0)
                    {
                        moda = notas[0];
                    }
                    if (notas[i] > moda)
                    {
                        moda = notas[i];
                    }
                }
            }
        }
        return moda;
    }

    static void Main()
    {
        string linea;
        bool valido;
        char provincia;
        float media;
        int mediana, moda;
        char[] provincias = new char[3];
        int[] notas1, notas2, notas3;
        float[,] Datos = new float[3,3];

        do
        {
            valido = true;
            Console.WriteLine("Introduce las notas de Alicante separadas por " 
                + "espacios (pon una A al principio)");
            linea = Console.ReadLine();
            Console.Clear();
            valido = ValidarEntrada(linea);
        }
        while (!valido);

        notas1 = ProcesarEntrada(linea, out provincia);
        provincias[0] = provincia;
        do
        {
            valido = true;
            Console.WriteLine("Introduce las notas de Valencia separadas por " 
                + "espacios (pon una V al principio)");
            linea = Console.ReadLine();
            Console.Clear();
            valido = ValidarEntrada(linea);
        }
        while (!valido);

        notas2 = ProcesarEntrada(linea, out provincia);
        provincias[1] = provincia;

        do
        {
            valido = true;
            Console.WriteLine("Introduce las notas de Castellón separadas por " 
                + "espacios (pon una C al principio)");
            linea = Console.ReadLine();
            Console.Clear();
            valido = ValidarEntrada(linea);
        }
        while (!valido);

        notas3 = ProcesarEntrada(linea, out provincia);
        provincias[2] = provincia;

        Datos[0, 0] =  Convert.ToSingle(media = Media(notas1));
        Datos[1, 0] = Convert.ToSingle(media = Mediana(notas1));
        Datos[2, 0] = Convert.ToSingle(media = Moda(notas1));

        Datos[0, 1] = Convert.ToSingle(media = Media(notas2));
        Datos[1, 1] = Convert.ToSingle(media = Mediana(notas2));
        Datos[2, 1] = Convert.ToSingle(media = Moda(notas2));

        Datos[0, 2] = Convert.ToSingle(media = Media(notas3));
        Datos[1, 2] = Convert.ToSingle(media = Mediana(notas3));
        Datos[2, 2] = Convert.ToSingle(media = Moda(notas3));

        Console.WriteLine(" \t media \t mediana \t moda");
        for (int i = 0; i < 3; i++)
        {
            
            Console.Write(provincias[i]);
            for (int j = 0; j < 3; j++)
            {   
                Console.Write(Datos[j, i] + " \t ");
            }
            Console.WriteLine();
            
           
        }

    }
}
