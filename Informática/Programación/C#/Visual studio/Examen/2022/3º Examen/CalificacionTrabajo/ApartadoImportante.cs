﻿class ApartadoImportante : Apartado
{
    private double califMin;

    public double CalifMin
    {
        get
        {
            return califMin; 
        }
        set
        {
            if (value >= 0 && value <= calificacion) 
            {
                califMin = value;
            } 
        }
    }
    
    public ApartadoImportante(string titulo) :base(titulo)
    {
        Titulo = titulo;
        CalifMin = 0;
        Calificacion = 0;
        CalifAlum = 0;
    }

    public override void Mostrar()
    {
        Console.Write(titulo + ": ");
        if (califAlum < califMin)
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }
        Console.WriteLine(califAlum);
        Console.ResetColor();
    }
}

