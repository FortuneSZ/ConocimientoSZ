﻿class CalificacionTrabajo
{
    static void RellenarDatos(out string titulo,out double calif,out double califAlum)
    {
        Console.WriteLine("Introduzca el título del apartado");
        titulo = Console.ReadLine();
        Console.Clear();

        Console.WriteLine("Introduzca la calificación máxima" +
            " del apartado");
        calif = Convert.ToDouble(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Introduzca la nota del alumno en" +
            " dicho apartado");
        califAlum = Convert.ToDouble(Console.ReadLine());
        Console.Clear();
    }

    static bool Validar(double calif,double califAlum)
    {
        bool valido = true;

        if (calif < 0 || calif > 10)
        {
            valido = false;
        }

        else if (califAlum < 0 || califAlum > calif)
        {
            valido = false;
        }

        return valido;
    }
    static void CrearApartado(int opcion, Alumno[] alumnos, int i)
    {
        bool valido = true;
        string titulo;
        double calif;
        double califAlum;
        double califMin = 0;

        do
        {
            Console.Clear();
            valido = true;

            RellenarDatos(out titulo,out calif,out califAlum);

            valido = Validar(calif,califAlum);

            if (opcion == 2)
            {
                Console.WriteLine("Introduzca la nota mínima del" +
                    " apartado");
                califMin = Convert.ToDouble(Console.ReadLine());

                if (califMin < 0 || califMin > calif)
                {
                    valido = false;
                }
            }
        }
        while(valido != true);
        
        if (opcion == 1)
        {
            ApartadoNormal a = new ApartadoNormal(titulo);
            a.Calificacion = calif;
            a.CalifAlum = califAlum;
            alumnos[i].Trabajo.NuevoApartado(a);
        }
        if (opcion == 2) 
        {
            ApartadoImportante a = new ApartadoImportante(titulo);
            a.Calificacion = calif;
            a.CalifAlum = califAlum;
            a.CalifMin = califMin;
            alumnos[i].Trabajo.NuevoApartado(a);
        }
    }

    static void MostrarAlumnos(Alumno[] alumnos)
    {
        Console.Clear();
        for (int x = 0; x < alumnos.Length; x++)
        {
            alumnos[x].Mostrar();
        }
    }

    static void CrearAlumnos(Alumno[] alumnos)
    {
        Trabajo t = new Trabajo("Programación 1.3");
        Trabajo t2 = new Trabajo("Lenguaje de marcas: javascript");
        Trabajo t3 = new Trabajo("Bases de datos : SQL");

        alumnos[0] = new Alumno("Fran", t);
        alumnos[1] = new Alumno("Sara", t2);
        alumnos[2] = new Alumno("Effy", t3);
    }
    static void Main()
    {
        int opcion = 0;

        Alumno[] alumnos = new Alumno[3];
        CrearAlumnos(alumnos);

        for (int i = 0; i < alumnos.Length; i++) 
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Alumno " + (i + 1));
                Console.WriteLine("Elija el tipo de apartado :");
                Console.WriteLine("1:Normal, 2:Importante, 0:Terminar");
                opcion = Convert.ToInt32(Console.ReadLine());

                switch (opcion)
                {
                    case 0:
                        break;
                    case 1:
                        CrearApartado(opcion, alumnos, i);
                        break;
                    case 2:
                        CrearApartado(opcion, alumnos, i);
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }
            }
            while (opcion != 0);

            MostrarAlumnos(alumnos);


        }
    }
}
