﻿abstract class Apartado
{
    protected string titulo;
    protected double calificacion;
    protected double califAlum;

    public string Titulo
    {
        get
        {
            return titulo;
        }
        set
        {
            titulo = value;
        }
    }
    public double Calificacion
    {
        get
        {
            return calificacion;
        }
        set 
        {
            if (value >= 0 || value <= 10) 
            {
                calificacion = value;
            }   
        }
    }
    public double CalifAlum
    {
        get
        {
            return califAlum;
        }
        set 
        {
            if (value >= 0 && value <= calificacion)
            {
                califAlum = value;
            }
        }
    }

    public Apartado(string titulo) 
    {
        Titulo = titulo;
        Calificacion = 0;
        CalifAlum = 0;
    }

    public virtual void Mostrar()
    {
         Console.WriteLine(titulo +": " + califAlum);
    }
}

