﻿class Trabajo
{
    private string titulo;
    private List<Apartado> apartados;

    public string Titulo
    {
        get 
        {
            return titulo; 
        }
        set
        {
            titulo = value; 
        }
    }

    public Trabajo(string titulo)
    {
        Titulo = titulo;
        apartados = new List<Apartado>();
    }

    public void NuevoApartado(Apartado a)
    {
        apartados.Add(a);
    }

    public double NotaFinal()
    {
        double notas = 0;

        for (int i = 0; i < apartados.Count; i++) 
        {
            notas += apartados[i].CalifAlum;
        }
        return notas;
    }

    public bool Aprobado()
    {
        bool aprovado = true;

        if (NotaFinal() < 5)
        {
            aprovado = false;
        }

        for (int i = 0; i < apartados.Count; i++)
        {
            if (apartados[i] is ApartadoImportante)
            {
                if (apartados[i].CalifAlum < ((ApartadoImportante)apartados[i]).CalifMin)
                {
                    aprovado = false;
                }
            }
        }
        return aprovado;
    }

    public void Mostrar()
    {
        Console.WriteLine(titulo);
        for (int i = 0; i < apartados.Count; i++ ) 
        {
            Console.Write("- ");
            if (apartados[i] is ApartadoImportante)
            {
                ((ApartadoImportante)apartados[i]).Mostrar();
            }
            else
            {
                apartados[i].Mostrar();
            }  
        }
        Console.Write("NOTA FINAL: {0} + (",NotaFinal().ToString("N2"));
        if (Aprobado() == true) 
        {
            Console.WriteLine("APROBADO)");
        }
        else
        {
            Console.WriteLine("SUSPENSO)");
        }
        
    }
}

