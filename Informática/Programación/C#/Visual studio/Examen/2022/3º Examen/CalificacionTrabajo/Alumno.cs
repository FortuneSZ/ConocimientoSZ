﻿class Alumno
{
    private string nombre;
    private Trabajo trabajo;

    public string Nombre
    {
        get
        {
            return nombre;
        }
        set
        {
            nombre = value;
        }
    }

    public Trabajo Trabajo
    {
        get
        {
            return trabajo;
        }
        set
        {
            trabajo = value;
        }
    }

    public Alumno(string nombre, Trabajo trabajo)
    {
        Nombre = nombre;
        Trabajo = trabajo;
    }

    public void Mostrar()
    {
        Console.WriteLine("Alumno/a {0}", nombre);
        Console.Write("Trabajo:");
        trabajo.Mostrar();
        Console.WriteLine();
    }
}

