﻿/*Este programa muestra los múltiplos de 3 y 7 entre 100 y 200*/
class Ejercicio_02b_07
{
    static void Main()
    {
        Console.WriteLine("Multiplo de 3 y 7 entre 100 y 200");
        for(int i = 100; i < 200;i++)
        {
            if (i % 3 ==0 && i % 7 == 0)
            {
                Console.WriteLine(i);
            }
        }
    }
}