﻿/*Este programa crea la tgabla de multiplicar del número introducido por el
 usuario (del 1 al 10)*/
class Ejercicio_02b_08
{
    static void Main()
    {
        int numero,multi;
        Console.WriteLine("Escriba un número");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (numero > 0 && numero <=10)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("^*^*^*^*^*^*^*^*^*");
            for (int i = 0; i <= 10; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write("^ ");
                }
                
                multi = numero * i;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("{0} x {1} = {2}",numero,i,multi);
                Console.ForegroundColor = ConsoleColor.Red;
                if (multi < 10)
                {
                    Console.Write("      ");
                }
                else if (multi < 100 && i!=10)
                { 
                    Console.Write("     ");
                }
                else if (multi >= 100)
                {
                    Console.Write("   ");
                }
                else
                {
                    Console.Write("    ");
                }

                if (i % 2 == 0)
                {
                    Console.WriteLine("^");
                }
                else
                {
                    Console.WriteLine("*");
                }
            }
            Console.WriteLine("^*^*^*^*^*^*^*^*^*");
            Console.ResetColor();

        }
        else
        {
            Console.WriteLine("Número no válido");
        }
        
    }
}
