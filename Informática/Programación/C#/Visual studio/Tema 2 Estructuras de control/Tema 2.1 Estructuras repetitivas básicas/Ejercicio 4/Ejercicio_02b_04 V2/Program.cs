﻿/*Este programa irá sumando entre si los números que vaya introduciendo el 
 usuario,hasta que el usuario introduzca un 0*/
class Ejercicio_02b_04
{
    static void Main()
    {
        int Sum=0, Num;
        
        do
        {
            Console.WriteLine("Introduce un número");
            Num = Convert.ToInt32(Console.ReadLine());
            Sum = Sum + Num;
            Console.Clear();
            Console.WriteLine(Sum);
        }
        while (Num != 0);
    }
}