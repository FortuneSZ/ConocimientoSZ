﻿/*Este programa contará del 1 al 10*/
class Ejercicio_02b_05
{
    static void Main()
    {
        int Num = 1;

        do
        {
            Console.WriteLine(Num);
            Num++;
        }
        while (Num <= 10);
    }
}
