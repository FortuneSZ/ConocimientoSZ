﻿/*Este programa contará y mostrará por pantalla del 20 al 10 en orden 
 * descendente*/
class Ejercicio_02b_06
{
    static void Main()
    {
        for(int i = 20; i >= 10; i--)
        {
            Console.WriteLine(i);
        }
    }
}