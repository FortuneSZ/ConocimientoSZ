﻿/*Este programa pide al usuario su edad y año de nacimiento,mostrará un
 mensaje de error si alguno no es correcto,si el incorrecto es la edad
el programa debe finalizar*/
class Ejercicio_02d_01
{
    static void Main()
    {
        int edad, anyonacimiento;

        try
        {
            Console.WriteLine("Escribe tu edad");
            edad = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Escribe tu año de nacimiento");
            anyonacimiento = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
        }
        catch(Exception e)
        {
            Console.Clear();
            Console.WriteLine("Error al introducir los números");
            Console.WriteLine();
            Console.WriteLine("Pulse enter para más información");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine(e.Message);
        }
    }
}
