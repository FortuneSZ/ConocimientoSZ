﻿/*Este programa creará un triángulo invertido con asteriscos del lado que 
indique el usuario*/
class Ejercicio_02c_02
{
    static void Main()
    {
        int lado;
        Console.WriteLine("Escriba el lado del triángulo invertido");
        lado = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        for (int i = 1; i <= lado; i++)
        {
            for(int x = 1; x <= lado-i; x++)
            {
                Console.Write(" ");
            }
            for (int j = 1; j <= i; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}