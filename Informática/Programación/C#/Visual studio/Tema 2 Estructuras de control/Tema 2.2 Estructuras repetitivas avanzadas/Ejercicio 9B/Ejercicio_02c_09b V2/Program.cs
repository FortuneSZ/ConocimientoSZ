﻿/*Este programa le pedirá al usuario que adivine un número del 1 al 100,
 previamente fijado,y le dirá si se ha pasado o sio se ha quedado corto
tras cada intento,con un número limitado de intentos*/
class Ejercicio_02c_09
{
    static void Main()
    {
        int clave = 11, intento,intentos = 6;

        do
        {
            Console.WriteLine("Escriba la clave (entre 1 y 100)");
            Console.WriteLine("Intentos restantes:{0}", intentos);
            intento = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            if (intento < 1 || intento > 100)
            {
                Console.WriteLine("Error,fuera de rango");
            }

            if (intento > clave)
            {
                Console.WriteLine("Te has pasado");
                intentos--;
                Console.ReadLine();
                Console.Clear();
            }
            else if (intento < clave)
            {
                Console.WriteLine("Te has quedado corto");
                intentos--;
                Console.ReadLine();
                Console.Clear();
            }
            else
            {
                Console.WriteLine("Has acertado");
            }
        }
        while (intento != clave && intentos != 0);

        if (intentos == 0)
        {
            Console.WriteLine("Te has quedado sin intentos");
            Console.ReadLine();
        }
    }
}
