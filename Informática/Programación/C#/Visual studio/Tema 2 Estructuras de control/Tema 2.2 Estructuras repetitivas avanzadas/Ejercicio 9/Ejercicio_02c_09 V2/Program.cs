﻿/*Este programa le pedirá al usuario que adivine un número del 1 al 100,
 previamente fijado,y le dirá si se ha pasado o sio se ha quedado corto
tras cada intento*/
class Ejercicio_02c_09
{
    static void Main()
    {
        int clave=11, intento;

        do
        {
            Console.WriteLine("Escriba la clave (entre 1 y 100)");
            intento = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            if ( intento < 1 || intento > 100)
            {
                Console.WriteLine("Error,fuera de rango");
            }

            if (intento > clave)
            {
                Console.WriteLine("Te has pasado");
                Console.ReadLine();
                Console.Clear();
            }
            else if (intento < clave)
            {
                Console.WriteLine("Te has quedado corto");
                Console.ReadLine();
                Console.Clear();
            }
            else
            {
                Console.WriteLine("Has acertado");
            }
        }
        while (intento != clave);
    }
}