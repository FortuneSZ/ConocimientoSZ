﻿/*Este programa creará un cuadrado con asteriscos del lado que indique el
 usuario*/
class Ejercicio_02c_02
{
    static void Main()
    {
        int lado;
        Console.WriteLine("Escriba el lado del cuadrado");
        lado = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        for (int i = 0; i < lado; i++)
        {
            for (int j = 0; j < lado; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}