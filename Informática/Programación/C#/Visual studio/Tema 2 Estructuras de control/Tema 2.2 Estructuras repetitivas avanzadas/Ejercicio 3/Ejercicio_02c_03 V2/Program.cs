﻿/*Este programa creará un triángulo con asteriscos del lado que indique el
 usuario*/
class Ejercicio_02c_02
{
    static void Main()
    {
        int lado;
        Console.WriteLine("Escriba el lado del triángulo");
        lado = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        for (int i = 0; i < lado; i++)
        {
            for (int j = 0; j < i; j++)
            {
                for (int k = 0; k < lado-j; k++)
                {
                    Console.Write(" ");
                }
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}