﻿/*Este programa conmtará el número de letras "a" minúsculas que hay en la
 palabra introducida por el usuario*/
class Ejercicio_02c_07
{
    static void Main()
    {
        string texto;
        int contador=0;
        Console.WriteLine("Escriba un texto");
        texto = Console.ReadLine();
        Console.Clear();

        foreach (char a in texto)
        {
            if (a == 'a')
            {
                contador++;
            }
        }
        Console.WriteLine("hay un total de {0} aes en el texto introducido",
            contador);
    }
}
