﻿/*Este programa calculará el máximo común divisor de 2 números introducidos
 por el usuario*/
class Ejercicio_02c_05
{
    static void Main()
    {
        int num1, num2, contador;
        Console.WriteLine("Escriba el primer número");
        num1 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escriba el segundo número");
        num2 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (num1 < num2)
        {
            contador = num1;
        }
        else
        {
            contador = num2;
        }

        do
        {
            if (contador == 1)
            {
                Console.WriteLine("No hay máximo común divisor");
                break;
            }

            if  (num1 % contador == 0 && num2 % contador == 0)
            {
                Console.WriteLine("El máximo común divisor es {0}", contador);
                break;
            }
            else
            {
                contador--;
            }
        }
        while (contador != 0);

    }
}
