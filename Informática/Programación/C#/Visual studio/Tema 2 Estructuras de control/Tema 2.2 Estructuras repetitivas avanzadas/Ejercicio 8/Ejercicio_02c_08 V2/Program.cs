﻿/*Este programa realizará una cuenta atrás desde el número especificado por el
 usuario hasta 0*/
class Ejercicio_02c_08
{
    static void Main()
    {
        int numero;
        Console.WriteLine("Escriba un número");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (numero > 0)
        {
            for (int i = numero; i >= 0; i--)
            {
                Console.WriteLine(i);
            }
        }
        else
        {
            Console.WriteLine("No puede realizarse la cuenta atrás");
        }
    }
}
