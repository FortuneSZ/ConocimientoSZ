﻿/*Este programa dirá si el número introducido por el usuario es primo o no*/
class Ejercicio_02c_10
{
    static void Main()
    {
        int numero,divisores = 0;
        Console.WriteLine("Introduzca un número");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        for (int i = numero; i > 0; i--)
        {
            if (numero % i == 0)
            {
                divisores++;
            }
        }
        if (divisores == 2 && numero % numero == 0 && numero % 1 == 0)
        {
            Console.WriteLine("Es un número primo");
        }
        else
        {
            Console.WriteLine("No es un número primo");
        }
    }
}
