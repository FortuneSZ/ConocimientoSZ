﻿/*Este programa mostrará por panmtalla los números del 2 al 106,excepto los
 múltiplos de 10*/
class Ejercicio_02c_06
{
    static void Main()
    {
        {
            for (int i = 2; i <= 106;i++)
            {
                if (i% 10 != 0)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
