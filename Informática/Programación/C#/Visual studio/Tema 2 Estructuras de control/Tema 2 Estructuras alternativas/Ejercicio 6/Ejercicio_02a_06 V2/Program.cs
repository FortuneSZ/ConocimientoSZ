﻿/*Este programa determina si el número introducido por el usuario es múltiplo
 de 2 y de 3*/
class Ejercicio_02a_06
{
    static void Main()
    {
        int Numero;
        Console.WriteLine("Escriba un número entero");
        Numero = Convert.ToInt32(Console.ReadLine());

        if (Numero % 2 == 0 && Numero % 3 == 0)
        {
            Console.WriteLine("El número es múltiplo de 2 y de 3");
        }
    }
}
