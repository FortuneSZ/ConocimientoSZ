﻿/*Este programa determina si el número introducido por el usuario es múltiplo
 de 2 pero no de 3*/
class Ejercicio_02a_07
{
    static void Main()
    {
        int Numero;
        Console.WriteLine("Escriba un número entero");
        Numero = Convert.ToInt32(Console.ReadLine());

        if (Numero % 2 == 0 && Numero % 3 != 0)
        {
            Console.WriteLine("El número es múltiplo de 2 y no es múltiplo" +
                " de 3");
        }
    }
}