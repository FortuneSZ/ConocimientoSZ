﻿/*Este programa pide al usuario 3 números y dice cual es mayor*/
class Ejercicio_02a_08
{
    static void Main()
    {
        int Num1, Num2, Num3;
        Console.WriteLine("Escribe 3 números");
        Num1 = Convert.ToInt32(Console.ReadLine());
        Num2 = Convert.ToInt32(Console.ReadLine());
        Num3 = Convert.ToInt32(Console.ReadLine());

        if (Num1 > Num2 && Num1 > Num3)
        {
            Console.WriteLine("El mayor es {0}", Num1);
        }
        else 
        {
            if (Num2 > Num1 && Num2 > Num3)
            {
                Console.WriteLine("El mayor es {0}", Num2);
            }
            else
            {
                Console.WriteLine("El mayor es {0}", Num3);
            }
        } 
    }
}
