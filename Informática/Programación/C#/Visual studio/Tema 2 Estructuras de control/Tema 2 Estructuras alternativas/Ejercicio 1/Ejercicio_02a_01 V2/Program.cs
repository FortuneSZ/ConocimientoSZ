﻿/*Este programa dice si un número es par*/
using System;

class Ejercicio_02a_01
{
    static void Main()
    {
        int Num;

        Console.WriteLine("Escriba un número");
        Num = Convert.ToInt32(Console.ReadLine());

        if (Num % 2 == 0)
        {
            Console.Clear();
            Console.WriteLine("Es par");
        }
    }
}