﻿/*Este programa pide al usuario 2 números enteros y dice si uno,ninguno
 o todos los números son positivos*/
class Ejercicio_02a_10
{
    static void Main()
    {
        int Num1, Num2;
        Console.WriteLine("Escribe 2 números enteros");
        Num1 = Convert.ToInt32(Console.ReadLine());
        Num2 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        if (Num1 >= 0 && Num2 >= 0)
        {
            Console.WriteLine("Ambos números son positivos");
        }
        else if ((Num1 >= 0 && Num2 < 0) || Num2 >= 0 && Num1 < 0)
        {
            Console.WriteLine("Solo uno de los números es positivo");
        }
        else
        {
            Console.WriteLine("Ninguno de los números es positivo");
        }

    }
}
