﻿/*Este programa dice cual de los dos números enteros es mayor*/
using System;

class Ejercicio_02a_02
{
    static void Main()
    {
        int Num1, Num2;

        Console.WriteLine("Escriba el primer número");
        Num1 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Escriba el segundo número");
        Num2 = Convert.ToInt32(Console.ReadLine());

        if (Num1 >= Num2)
        {
            Console.WriteLine("El mayor es {0}", Num1);
        }

        if (Num2 > Num1)
        {
            Console.WriteLine("El mayor es {0}",Num2);
        }
    }
}
