﻿/*Este programa determina cual de los dos números introducidos por el usuario
 es menor empleando el operador condicional*/
class Ejercicio_02a_11
{
    static void Main()
    {
        int Num1, Num2,NumMenor;
        Console.WriteLine("Escriba 2 números enteros");
        Num1 = Convert.ToInt32(Console.ReadLine());
        Num2 = Convert.ToInt32(Console.ReadLine());

        NumMenor = Num1 < Num2 ? Num1 : Num2;
        Console.WriteLine("El menor de los dos números es {0}",NumMenor);
    }
}