﻿/*Este programa determina si la letra introducida por el usuario es una vocal
 * o no*/
class Ejercicio_02a_12
{
    static void Main()
    {
        char Letra;
        Console.WriteLine("Escribe una letra");
        Letra = Convert.ToChar(Console.ReadLine());

        switch(Letra)
        {
            case 'a':
            case 'A':
            case 'e':
            case 'E':
            case 'i':
            case 'I':
            case 'o':
            case 'O':
            case 'u':
            case 'U':
                Console.WriteLine("La letra introducida es una vocal");
                break;

            default:
                Console.WriteLine("La letra introducida no es una vocal");
                break ;

        }
    }
}
