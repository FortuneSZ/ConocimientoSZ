﻿/*Este programa determinará si el caracter introducido por el usuario es una
letra mayúscula,un número u otra cosa*/
class Ejercicio_02a_14
{
    static void Main()
    {
        char Caracter;
        Console.WriteLine("Escribe un caracter");
        Caracter = Convert.ToChar(Console.ReadLine());
        Console.Clear();

        switch(Caracter)
        {
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'Ñ':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':
                Console.WriteLine("Es una letra mayúscula");
                break;

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                Console.WriteLine("Es un número");
                break;

            default:
                Console.WriteLine("Es otra cosa");
                break;
        }
    }
}