﻿/*Este programa pide al usuario 2 números y realiza la división entre el 
primer número y el segundo,excepto si el segundo es 0,en cuyo caso mostrará
un mensaje de error*/
using System;
class Ejercicio_02a_04
{
    static void Main()
    {
        int Num1,Num2;

        Console.WriteLine("Escribe el primer número");
        Num1 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escribe el segundo número");
        Num2 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (Num2 == 0)
        {
            Console.WriteLine("No se puede dividir por cero");
        }
        else
        {
            Console.WriteLine("La división entre {0} y {1} es {2}", Num1, Num2,
                Num1 / Num2);
        }
    }
}