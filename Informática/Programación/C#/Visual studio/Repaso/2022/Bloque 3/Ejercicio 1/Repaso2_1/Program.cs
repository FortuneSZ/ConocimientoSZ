﻿/*
 * Programa que le pide al usuario una expresión matemática en una línea,
 * por ejemplo "12 + 3 - 4 * 5" y la calcula de izquierda a derecha
 */
using System;

class Repaso2_1
{
    static void Main()
    {
        string expresion, operacion = "+";
        string[] partes;
        int resultado = 0, numero;

        Console.WriteLine("Escribe la expresión matemática:");
        expresion = Console.ReadLine();
        partes = expresion.Split();

        for (int i = 0; i < partes.Length; i++)
        {
            if (i % 2 != 0)
            {
                operacion = partes[i];
            }
            else
            {
                numero = Convert.ToInt32(partes[i]);
                if (i == 0)
                {
                    resultado = numero;
                }
                else
                {
                    switch (operacion)
                    {
                        case "+":
                            resultado = resultado + numero;
                            break;
                        case "-":
                            resultado = resultado - numero;
                            break;
                        case "*":
                            resultado = resultado * numero;
                            break;
                        case "/":
                            resultado = resultado / numero;
                            break;
                    }
                }
            }
        }

        Console.WriteLine(resultado);

    }
}


