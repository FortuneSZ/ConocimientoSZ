﻿/*
 * Programa que lee las distancias acumuladas de un conjunto de N tramos
 * y le dice al usuario cuál es el tramo más largo y qué día se recorrió
 */
using System;

class Repaso2_2
{
    static void Main()
    {
        int[] tramos;
        int numDias, maximo, posicion;

        Console.WriteLine("Dime el número de días:");
        numDias = Convert.ToInt32(Console.ReadLine());
        tramos = new int[numDias];

        Console.WriteLine("Escribe los valores de los {0} tramos:", numDias);
        for (int i = 0; i < tramos.Length; i++)
        {
            tramos[i] = Convert.ToInt32(Console.ReadLine());
        }

        maximo = tramos[0];
        posicion = 0;

        for (int i = 1; i < tramos.Length; i++)
        {
            if (tramos[i] - tramos[i - 1] > maximo)
            {
                maximo = tramos[i] - tramos[i - 1];
                posicion = i;
            }
        }

        Console.WriteLine("El tramo más largo es de {0}km", maximo);
        Console.WriteLine("Lo recorriste el día {0}", posicion + 1);
    }
}


