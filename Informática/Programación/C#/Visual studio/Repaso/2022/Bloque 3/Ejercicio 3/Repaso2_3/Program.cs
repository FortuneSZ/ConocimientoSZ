﻿// See https:/*
*Programa que gestiona un array sobredimensionado de rutinas de running
 */

using System;

class Ejercicio
{
    enum opciones { SALIR, NUEVA, MEDIA, CORTO }

    struct rutina
    {
        public string mes;
        public float distancia;
        public int tiempo;
    }

    static void Main()
    {
        string[] meses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo",
            "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
            "Diciembre" };
        rutina[] rutinas = new rutina[50];
        int cantidad = 0, tiempoMinimo;
        opciones opcion;
        bool correcto;

        do
        {
            Console.Clear();
            Console.WriteLine((int)opciones.NUEVA + ". Nueva rutina");
            Console.WriteLine((int)opciones.MEDIA + ". Media de km");
            Console.WriteLine((int)opciones.CORTO + ". Tiempo más corto");
            Console.WriteLine((int)opciones.SALIR + ". Salir");
            Console.WriteLine("Elige una opción del menú:");
            opcion = (opciones)Convert.ToInt32(Console.ReadLine());

            switch (opcion)
            {
                case opciones.NUEVA:
                    if (cantidad < rutinas.Length)
                    {
                        rutina nueva;
                        Console.WriteLine("Mes de la rutina:");
                        nueva.mes = Console.ReadLine();

                        // Comprobar mes
                        correcto = false;
                        for (int i = 0; i < meses.Length; i++)
                        {
                            if (meses[i].ToUpper() == nueva.mes.ToUpper())
                            {
                                correcto = true;
                            }
                        }

                        if (correcto)
                        {
                            try
                            {
                                Console.WriteLine("Distancia en km:");
                                nueva.distancia =
                                    Convert.ToSingle(Console.ReadLine());
                                Console.WriteLine("Tiempo en minutos:");
                                nueva.tiempo =
                                    Convert.ToInt32(Console.ReadLine());

                                if (nueva.distancia > 0 && nueva.tiempo > 0)
                                {
                                    rutinas[cantidad] = nueva;
                                    cantidad++;
                                }
                                else
                                {
                                    Console.WriteLine("El valor debe ser positivo");
                                }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Dato incorrecto");
                            }

                            for (int i = 0; i < cantidad; i++)
                            {
                                Console.WriteLine("{0} - {1} - {2}",
                                    rutinas[i].mes, rutinas[i].distancia,
                                    rutinas[i].tiempo);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Mes incorrecto");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No caben más rutinas");
                    }
                    break;
                case opciones.MEDIA:
                    if (cantidad > 0)
                    {
                        float media = 0;
                        for (int i = 0; i < cantidad; i++)
                        {
                            media += rutinas[i].distancia;
                        }
                        media /= cantidad;
                        Console.WriteLine("Media de distancias: " +
                            media.ToString("N2"));
                    }
                    else
                    {
                        Console.WriteLine("No hay rutinas");
                    }
                    break;
                case opciones.CORTO:
                    if (cantidad > 0)
                    {
                        tiempoMinimo = rutinas[0].tiempo;
                        for (int i = 1; i < cantidad; i++)
                        {
                            if (rutinas[i].tiempo < tiempoMinimo)
                            {
                                tiempoMinimo = rutinas[i].tiempo;
                            }
                        }

                        Console.WriteLine("Tiempo mínimo: {0} min.",
                            tiempoMinimo);
                    }
                    else
                    {
                        Console.WriteLine("No hay rutinas");
                    }
                    break;
            }

            if (opcion != opciones.SALIR)
            {
                Console.WriteLine("Pulsa Intro para continuar...");
                Console.ReadLine();
            }
        }
        while (opcion != opciones.SALIR);
    }
}

