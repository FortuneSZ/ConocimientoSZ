﻿/*
 * Programa que le pide una frase al usuario y la muestra codificada 
 * incrementando en uno las letras alfabéticas (la 'z' pasa a 'a')
 */
using System;

class Repaso2_4
{
    static void Main()
    {
        string frase;
        char simbolo;

        Console.WriteLine("Escribe una frase:");
        frase = Console.ReadLine();

        for (int i = 0; i < frase.Length; i++)
        {
            simbolo = frase[i];
            if ((simbolo >= 'a' && simbolo < 'z') ||
                (simbolo >= 'A' && simbolo < 'Z'))
            {
                simbolo++;
            }
            else if (simbolo == 'z')
            {
                simbolo = 'a';
            }
            else if (simbolo == 'Z')
            {
                simbolo = 'A';
            }

            Console.Write(simbolo);
        }
    }
}


