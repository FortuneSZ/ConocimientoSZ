﻿/*
 * Programa que le pide al usuario un número y lo redondea a las unidades
 * de millar
 */

using System;

class Ejercicio
{
    static void Main()
    {
        long numero, resto;

        Console.WriteLine("Escribe un entero largo (>= 1000):");
        numero = Convert.ToInt64(Console.ReadLine());

        resto = numero % 1000;

        numero = numero - resto;
        if (resto >= 500)
        {
            numero = numero + 1000;
        }

        Console.WriteLine(numero);
    }
}
