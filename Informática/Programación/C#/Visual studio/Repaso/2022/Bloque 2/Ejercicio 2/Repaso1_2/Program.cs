﻿/*
 * Programa que dibuja un trapecio descendente con el ancho de base y altura
 * que diga el usuario, y especificando también carácter de relleno
 */

using System;

class Ejercicio
{
    static void Main()
    {
        byte ancho, alto;
        char caracter;
        int contadorEspacios, contadorSimbolos;

        Console.WriteLine("Introduce anchura base del trapecio:");
        ancho = Convert.ToByte(Console.ReadLine());
        Console.WriteLine("Introduce altura del trapecio:");
        alto = Convert.ToByte(Console.ReadLine());
        Console.WriteLine("Introduce el carácter de relleno:");
        caracter = Convert.ToChar(Console.ReadLine());

        contadorEspacios = 0;
        contadorSimbolos = ancho + 2 * (alto - 1);

        for (int i = 1; i <= alto; i++)
        {
            for (int j = 1; j <= contadorEspacios; j++)
            {
                Console.Write(" ");
            }
            for (int j = 1; j <= contadorSimbolos; j++)
            {
                Console.Write(caracter);
            }
            Console.WriteLine();
            contadorEspacios++;
            contadorSimbolos = contadorSimbolos - 2;
        }

    }
}

