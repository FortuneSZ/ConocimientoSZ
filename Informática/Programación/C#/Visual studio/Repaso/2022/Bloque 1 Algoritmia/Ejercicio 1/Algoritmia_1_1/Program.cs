﻿/*Este programa emplea la algoritmia para calcular el resto de una división
 mediante restas sucesivas*/
class Algoritmia_1_1
{
    static void Main()
    {
        int dividendo, divisor, calculo;
        Console.WriteLine("Escribe el dividendo");
        dividendo = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        Console.WriteLine("Escribe el divisor");
        divisor = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        calculo = dividendo - divisor;
        do
        {
            calculo -= divisor;
        }
        while (calculo - divisor >= 0);

        Console.WriteLine("El resto es {0}",calculo);
    }
}
