﻿/*Este programa le pide al usuario un número y le dice cuantas cifras tiene*/
class Algoritmia_1_3
{
    static void Main()
    {
        int numero,auxiliar,cifras=1;
        Console.WriteLine("Escriba un número");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.ReadLine();
        auxiliar = numero;
        while(auxiliar / 10 >= 1)
        {
            auxiliar = auxiliar / 10;
            cifras++;
        }
        Console.WriteLine("El numero {0} tiene {1} cifras",numero,cifras);
    }
}