﻿/*Este programa realiza la suma de los dígitos de un número*/
class Algoritmia_1_4
{
    static void Main()
    {
        int numero,aux,suma=0,resto;
        Console.WriteLine("Introduzca un número");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        aux = numero;
        while(numero % 10 >= 1)
        {
            resto = numero % 10;
            suma += resto;
            numero /= 10;
        }

        Console.WriteLine("La suma de cada cifra de {0} es {1}",aux,suma);

    }
}
