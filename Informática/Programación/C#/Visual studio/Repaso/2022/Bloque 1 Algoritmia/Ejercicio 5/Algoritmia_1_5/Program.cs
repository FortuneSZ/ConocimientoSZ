﻿/*Este programa calcula el número inverso del número introducido por el
 usuario*/
class Algoritmia_1_5
{
    static void Main()
    {
        int numero,aux,reversenumero=0;
        Console.WriteLine("Escriba un número");
        numero = Convert.ToInt32(Console.ReadLine());
        aux = numero;
        Console.Clear();
        while (numero % 10 >=1)
        {
            reversenumero *= 10;
             reversenumero += (numero % 10);
            numero /= 10;
        }
        Console.WriteLine("El número inverso de {0} es {1}",
            aux, reversenumero);
    }
}
