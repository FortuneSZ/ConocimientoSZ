﻿/*Este programa calcula el máximo común divisor de dos númeroas mediante
 el algoritmo de Euclides*/
class Algoritmia_1_2
{
    static void Main()
    {
        int n1, n2, mcd=0,aux;
        Console.WriteLine("Escribe el primer número");
        n1 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Escribe el segundo número");
        n2 = Convert.ToInt32(Console.ReadLine());

        if(n1 < n2)
        {
            aux = n1;
            n1 = n2;
            n2 = aux;
        }

        while (n1 % n2 != 0)
        {
            mcd = n1 % n2;

            if (mcd != 0)
            {
                n1 = n2;
                n2 = mcd;
            }
        }
        mcd = n2;
        Console.WriteLine("El máximo común divisor es {0}",mcd);
    }
}
