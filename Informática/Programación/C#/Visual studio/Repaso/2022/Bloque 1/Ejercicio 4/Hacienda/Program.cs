﻿/*Este programa te dice que porcentaje tienes que pagar de impuestos según
 su salario anual*/
class Repaso4
{
    static void Main()
    {
        int SalarioAnual;
        Console.WriteLine("Escriba su salario anual");
        SalarioAnual = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (SalarioAnual >= 35000)
        {
            Console.WriteLine("Debes pagar el 30%");
            Console.WriteLine("Has de pagar {0} euros", SalarioAnual*30/100);
        }
        else if (SalarioAnual >= 20000 && SalarioAnual < 35000)
        {
            Console.WriteLine("Debes pagar el 20%");
            Console.WriteLine("Has de pagar {0} euros", SalarioAnual * 20 / 100);
        }
        else if (SalarioAnual >= 10000 && SalarioAnual < 20000)
        {
            Console.WriteLine("Debes pagar el 15%");
            Console.WriteLine("Has de pagar {0} euros", SalarioAnual * 15 / 100);
        }
        else
        {
            Console.WriteLine("Debes pagar el 5%");
            Console.WriteLine("Has de pagar {0} euros", SalarioAnual * 5 / 100);
        }

    }
}
