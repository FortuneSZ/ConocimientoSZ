﻿/*Este programa dirá en base a los datos introducidos por el usuario,si tiene
 que pagar impuestos o no*/
class Repaso3
{
    static void Main()
    {
        int Edad, SalarioMes;
        Console.WriteLine("Escriba su edad");
        Edad = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escriba su salario mensual");
        SalarioMes = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (Edad >= 16 && SalarioMes >= 1000)
        {
            Console.WriteLine("Tienes que pagar impuestos");
        }
        else
        {
            Console.WriteLine("No tienes que pagar impuestos");
        }
    }
}
