﻿/*Este programa te dice que porcentaje tienes que pagar de impuestos según
 su salario anual*/
class Repaso5
{
    static void Main()
    {
        int SalarioAnual,Porcentaje,CantidadAPagar;
        Console.WriteLine("Escriba su salario anual");
        SalarioAnual = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (SalarioAnual >= 35000)
        {
            Porcentaje = 30;
            CantidadAPagar = SalarioAnual * 30 / 100;
        }
        else if (SalarioAnual >= 20000 && SalarioAnual < 35000)
        {
            Porcentaje = 20;
            CantidadAPagar = SalarioAnual * 20 / 100;
        }
        else if (SalarioAnual >= 10000 && SalarioAnual < 20000)
        {
            Porcentaje = 15;
            CantidadAPagar = SalarioAnual * 15 / 100;
        }
        else
        {
            Porcentaje = 5;
            CantidadAPagar = SalarioAnual * 5 / 100;
        }
        Console.WriteLine("Debes pagar el {0}%",Porcentaje);
        Console.WriteLine("Has de pagar {0} euros",CantidadAPagar);
    }
}
