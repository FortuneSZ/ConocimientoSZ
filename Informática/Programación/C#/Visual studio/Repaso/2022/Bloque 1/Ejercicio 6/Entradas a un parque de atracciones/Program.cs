﻿/*Este programa determina el coste de las entradas de un grupo de 3 personas
 a un parque de atracciones en base a su edad*/
class Repaso7
{
    static void Main()
    {
        int Persona1, Persona2, Persona3,Coste=0;
        Console.WriteLine("Escriba la edad de las 3 personas que acuden al " +
            "parque de atracciones");
        Persona1 = Convert.ToInt32(Console.ReadLine());
        Persona2 = Convert.ToInt32(Console.ReadLine());
        Persona3 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (Persona1 < 18 && Persona2 < 18 && Persona3 < 18)
        {
            Console.WriteLine("No pueden entrar al parque");
        }
        else
        {
            if(Persona1 <=3 ||Persona2 <=3 || Persona3 <=3)
            {
                Coste = Coste;
            }

            if (Persona1 > 3 && Persona1 < 18)
            {
                Coste += 10;
            }
            else if (Persona1 > 18)
            {
                Coste += 25;
            }

            if (Persona2 > 3 && Persona2 < 18)
            {
                Coste += 10;
            }
            else if (Persona2 > 18)
            {
                Coste += 25;
            }

            if (Persona3 > 3 && Persona3 < 18)
            {
                Coste += 10;
            }
            else if (Persona3 > 18)
            {
                Coste += 25;
            }
            Console.WriteLine("Pueden entrar al parque");
            Console.WriteLine("El coste total son {0} euros",Coste);
        }
    }
}
