﻿/*Este programa dice si el usuario es adulto o no en base a la edad 
 * introducida por el mismo*/
class Repaso1
{
    static void Main()
    {
        int Edad;
        Console.WriteLine("Introduzca su edad");
        Edad = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (Edad >= 18)
        {
            Console.WriteLine("Eres adulto");
        }
        else
        {
            Console.WriteLine("No eres adulto");
        }
    }
}
