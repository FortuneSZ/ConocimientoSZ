﻿/*Este programa resuelve una ecuación de primer grado en base a 2 números
 introducidos por el usuario*/
class Repaso6
{
    static void Main()
    {
        int A, B,X;
        Console.WriteLine("Escriba el valor de A");
        A = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escriba el valor de B");
        B = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (A == 0)
        {
            Console.WriteLine("La ecuación no tiene solución");
        }
        else
        {
            X = -B / A;

            Console.WriteLine("La solución a la ecuación {0}x + {1} = 0 es {2}",
                A, B, X);
        }
        
    }
}
