﻿/* Este programa pide al usuario dos números,los divide y calcula la división
y el resto entre el primer número y el segundo*/

class Ejercicio_01c_07
{
    static void Main()
    {
        int num1, num2,resto,div;
        Console.WriteLine("Escribe un número entero");
        num1 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Escribe otro número entero");
        num2 = Convert.ToInt32(Console.ReadLine());
        div = num1 / num2;
        resto = num1 % num2;
        Console.WriteLine("la división entre {0} y {1} es {2},cuyo resto es {3}", num1, num2, div, resto);
    }
}
