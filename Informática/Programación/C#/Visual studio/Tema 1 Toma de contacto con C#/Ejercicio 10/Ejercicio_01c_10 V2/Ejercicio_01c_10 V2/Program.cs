﻿/*Este programa pide al usuario 2 números enteros,y posteriormente
realiza varias operaciones copn ellos*/
class Ejercicio_01c_10
{
    static void Main()
    {
        int num1, num2,res1,res2;
        Console.WriteLine("Escriba el primer número");
        num1 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Escriba el segundo número");
        num2 = Convert.ToInt32(Console.ReadLine());
        res1 = (num1 + num2) * (num1 - num2);
        res2 = num1 * num1 - num2 * num2;
        Console.Write("resultado 1 = {0} ", res1);
        Console.Write("resultado 2 = {0} ", res2);
    }
}