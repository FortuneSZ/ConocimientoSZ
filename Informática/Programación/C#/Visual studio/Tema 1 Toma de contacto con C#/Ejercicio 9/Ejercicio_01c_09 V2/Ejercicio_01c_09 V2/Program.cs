﻿/*Este programa convierte de metros a millas*/
using System;
class Ejercicio_01c_08
{
    static void Main()
    {
        float millas, metros;

        Console.WriteLine("Escrime el número de metros a convertir");
        metros = Convert.ToInt32(Console.ReadLine());
        millas = metros / 1609;
        Console.WriteLine("{0} metros es igual a {1} millas", metros, millas);
    }
}