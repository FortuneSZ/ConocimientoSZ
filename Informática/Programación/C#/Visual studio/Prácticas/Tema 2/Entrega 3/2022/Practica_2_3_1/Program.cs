﻿/*Este programa creara una piramide en base a la altura suministrada por el
usuario */
using System;
class Practica_2_3_1
{

    static void Main()
    {
        int altura=0;
        try
        {
            Console.WriteLine("dime un lado de la pirámide");
            altura = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            if (altura >= 0)
            {
                for (int i = 1; i <= altura; i++)
                {
                    for (int j = 1; j <= altura - i; j++)
                    {
                        Console.Write(" ");
                    }
                    for (int x = 1; x <= i; x++)
                    {
                        Console.Write("*");
                    }
                    for (int x = 1; x <= i - 1; x++)
                    {
                        Console.Write("*");
                    }

                    Console.WriteLine();
                }
            }

            else
            {
                Console.Clear();
                Console.WriteLine("Altura no valida");
                Console.WriteLine();
            }
        }

        catch (Exception ex)
        {
            Console.Clear();
            Console.WriteLine("Altura no valida");
            Console.WriteLine();
            Console.WriteLine("Pulse enter para ver más detalles");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine(ex.Message);
            Console.ReadLine();
        }
    }
}

