﻿/*Este programa le pide al usuario un número,el cual es la cantidad que desea 
ahorrar,y posteriormente le irá pidiendo números hasta llegar a ese objetivo*/
using System;
class Practica_2_3_2
{
    static void Main()
    {
        int total, ingresos = 0, ingresar,longitud=0,longitud2=1;
        Console.WriteLine("Escriba la cantidad que desea ahorrar");
        total = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        for (int i = total; i % 10 >=0; i/=10)
        {
            longitud++;
        }

        do
        {
            Console.WriteLine("*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*");
            Console.Write("^ "+"Objetivo a lograr: {0}",total);

            for (int i = 0;i<=17-longitud;i++)
            {
                Console.Write(" ");
            }
            Console.WriteLine(" ^");

            Console.Write("* "+"Ingresos actuales: {0}",ingresos);

            for (int i = ingresos; i % 10 >= 1; i /= 10)
            {
                longitud2++;
            }

            for (int i = 0; i <= 17 - longitud2; i++)
            {
                Console.Write(" ");
            }

            Console.WriteLine(" *");
            Console.WriteLine("^ "+"Indique la cantidad que desea ahorrar"+
                " ^");
            Console.WriteLine("*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*");
            ingresar = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            ingresos += ingresar;
            longitud2 = 0;
        }
        while (ingresos < total);
        Console.WriteLine("*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^");
        Console.WriteLine("^ " + "Felicidades,lograste tu objetivo" + " *");
        Console.WriteLine("*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^");
    }
}
