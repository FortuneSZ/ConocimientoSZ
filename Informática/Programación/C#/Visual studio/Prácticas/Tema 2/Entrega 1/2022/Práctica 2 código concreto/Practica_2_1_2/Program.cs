﻿/*Este programa pedirá al usuario un código numérico entero,si coincide con
 ciertos códigos mostrará un mensaje concreto,si no mostrará código no
identificado*/
class Practica_2_1_2
{
    static void Main()
    {
        int codigo;
        Console.WriteLine("Escriba un codigo numerico");
        codigo = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        switch(codigo)
        {
            case 5678:
            case 8765:
                Console.WriteLine("Hola, señor");
                break;

            case 1234:
            case 4321:
                Console.WriteLine("Hola, señora");
                break;

            default:
                Console.WriteLine("Código no identificado");
                break;
        }
    }
}
