﻿/*Este programa pedirá al usuario 3 temperaturas,y en base a ellas mostrará un
 mensaje diciendo la estación en la que estamos*/
class Practica_2_1_1
{
    static void Main()
    {
        int temp1, temp2, temp3;
        Console.WriteLine("Escriba 3 temperaturas en grados centígrados(ºC)");
        temp1 = Convert.ToInt32(Console.ReadLine());
        temp2 = Convert.ToInt32(Console.ReadLine());
        temp3 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        if (temp1 >= 25 && temp2 >= 25 && temp3 >= 25)
        {
            Console.WriteLine("Es verano");
        }
        else if (temp1 < 15 && temp2 < 15 && temp3 < 15)
        {
            Console.WriteLine("Es invierno");
        }
        else
        {
            Console.WriteLine("Es primavera u otoño");
        }
    }
}
