﻿/*Este programa le pedirá al usuario un número y mostrará por pantalla sus
 10 números siguientes y sus 5 números anteriores*/
class Practica_2_2_2
{
    static void Main()
    {
        int numero;
        Console.WriteLine("Escriba un número");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.Write("10 número siguientes: ");

        for (int i = numero+1; i <= numero+10; i++)
        {
            Console.Write(i);
            if (i != numero+10)
            {
                Console.Write(",");
            }
        }
        Console.WriteLine();
        Console.Write("5 número anteriores: ");

        for (int z = numero - 1; z >= numero - 5; z--)
        {
            Console.Write(z);
            if (z != numero - 5)
            {
                Console.Write(",");
            }
        }
    }
    
}
