﻿/*Este programa pedirá al usuario su año de nacimiento y el año actual,y en 
 base a ello calculará su edad*/
class Practica_2_2_1
{
    static void Main()
    {
        int anyonacimiento, anyoactual;
        Console.WriteLine("Escriba el año actual");
        anyoactual = Convert.ToInt32(Console.ReadLine());

        do
        {
            Console.Clear();
            Console.WriteLine("Escriba su año de nacimiento");
            anyonacimiento = Convert.ToInt32(Console.ReadLine());
        }
        while (anyonacimiento > anyoactual);

        Console.Clear();
        Console.WriteLine("Tienes {0} años",anyoactual-anyonacimiento);
    }
}
