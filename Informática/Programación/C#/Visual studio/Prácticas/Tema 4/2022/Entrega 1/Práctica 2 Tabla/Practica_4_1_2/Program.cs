﻿/*Este programa pedirá al usuario el número de filas y columnas que quiere,y
creará una tabla de dicho tamaño,la cual le pedirá rellenar,una vez rellena
le pedirá un valor a buscar, y si se encuentra en la tabla le dirá en que 
posiciones se encuentra,también le mostrará la multiplicación de los números 
de la diagonal principal*/
using System;
class Practica_4_1_2
{
    static void Main()
    {
        int filasColumnas, filas, columnas, multiDiagonal = 0, valorBuscar;
        int[,] tabla;
        bool encontrado = false;

        Console.WriteLine("Escribe el número de filas y columnas de la tabla");
        filasColumnas = Convert.ToInt32(Console.ReadLine());
        filas = filasColumnas;
        columnas = filasColumnas;
        tabla = new int[filas,columnas];
        Console.Clear();

        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                Console.WriteLine("Introduce un número entero en la casilla" +
                    " ({0},{1})",i+1,j+1);
                tabla[i,j] = Convert.ToInt32(Console.ReadLine());
                Console.Clear();

                if (i == 0 && j == 0)
                {
                    multiDiagonal = tabla[i,j];
                }
                else if (i == j)
                {
                    multiDiagonal *= tabla[i,j];
                }
            }
        }
        Console.WriteLine("Introduce un valor a buscar");
        valorBuscar = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                if (tabla[i,j] == valorBuscar)
                {
                    Console.WriteLine("Valor encontrado en la posición " +
                        "({0},{1})",i+1,j+1);
                    encontrado = true;
                }
            }
        }
        Console.WriteLine();
        if (encontrado == false)
        {
            Console.WriteLine("El valor especificado no se encuentra en la " +
                "tabla");
        }
        Console.WriteLine("La multiplicación de la diagonal principal es " +
                " {0}", multiDiagonal);
    }
}
