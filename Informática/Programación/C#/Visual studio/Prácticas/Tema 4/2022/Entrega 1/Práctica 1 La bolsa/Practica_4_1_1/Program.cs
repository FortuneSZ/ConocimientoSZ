﻿/*Este programa creará un array del tamaño determinado por el usuario,luego
le pedirá que lo rellene,posteriormente calculará el rango de los valores,
el beneficio máximo y la cantidad de mínimos locales y los mostrará por 
pantalla*/
using System;
class Practica_4_1_1
{
    static void Main()
    {
        float[] valores;
        float rango, max = 0, min = 0, maxBenef, valorInicial;
        int cantidad, numMinLoc = 0;

        Console.WriteLine("Diga la cantidad de valores a introducir");
        cantidad = Convert.ToInt32(Console.ReadLine());
        valores = new float[cantidad];

        for (int i = 0; i < valores.Length; i++)
        {
            Console.Clear();
            Console.WriteLine("Introduzca un valor");
            valores[i] = Convert.ToSingle(Console.ReadLine());
        }

        valorInicial = valores[0];
        
        for (int i = 0; i < valores.Length; i++)
        {
            if (min == 0)
            {
                min = valores[i];
            }

            if (valores[i] < min)
            {
                min = valores[i];
            }

            if (max == 0)
            {
                max = valores[i];
            }

            if (valores[i] > max)
            {
                max = valores[i];
            }

            if (i != 0 && i != valores.Length - 1 && valores[i-1] > valores[i] 
                && valores[i + 1] > valores[i])
            {
                numMinLoc++;
            }
        }

        maxBenef = max * 100 / valorInicial;
        maxBenef -= 100;
        rango = max - min;

        Console.Clear();
        Console.WriteLine("Rango: {0}", rango);
        Console.WriteLine("Beneficio máximo: {0}%", maxBenef.ToString("N2"));
        Console.WriteLine("Cantidad de mínimos locales: {0}", numMinLoc);
    }
}
