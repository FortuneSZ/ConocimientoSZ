﻿/*Este programa mostrará un menú en el cual se pueden añadir canciones,mostrar
todas las canciones,buscar canciones de un artista especificado por el 
usuario, ordenar alfabéticamente o por peso en kb hasta que este selecccione 
la opción de salir*/
using System;
class Ejercicio_04b_01
{
    enum menu
    {
        anyadir=1,mostrar,buscar,ordenalf,ordenkb, salir
    }
    struct Cancion
    {
        public string artista;
        public string titulo;
        public duracion duracion;
        public int tamaño;
    }

    struct duracion
    {
        public int minutos;
        public int segundos;
    }
    static void Main()
    {
        Cancion[] c = new Cancion[100];
        int opcion, cantidad = 0;
        string ArtBuscar;
        bool encontrado = false;

        do
        {
            Console.WriteLine((int)menu.anyadir +
                ". Añadir una canción al final");
            Console.WriteLine((int)menu.mostrar + 
                ". Mostrar los títulos de todas las canciones");
            Console.WriteLine((int)menu.buscar +
                ". Buscar canciones de un artista determinado");
            Console.WriteLine((int)menu.ordenalf + 
                ". Ordenar las canciones alfabéticamente");
            Console.WriteLine((int)menu.ordenkb +
                ". Ordenar las canciones en función de su peso en kb");
            Console.WriteLine((int)menu.salir + ". Salir");
            opcion = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            switch ((menu)opcion)
            {
                case menu.anyadir:
                    Cancion can;

                    Console.WriteLine("Escribe el nombre del artista");
                    can.artista = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Escribe el título de la canción");
                    can.titulo = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Escribe los minutos de la canción");
                    can.duracion.minutos = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine("Escribe los segundos de la canción");
                    can.duracion.segundos = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine("Escribe el peso de la canción en KB");
                    can.tamaño = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();

                    c[cantidad] = can;
                    cantidad++;
                    break;
                case menu.mostrar:
                    if (cantidad > 0)
                    {
                        Console.WriteLine("Artista - Titulo - Duración - " +
                           "Peso");
                        Console.WriteLine();
                        for (int i = 0; i < cantidad; i++)
                        {
                            Console.Write("{0} - ", c[i].artista);
                            Console.Write("{0} - ", c[i].titulo);
                            Console.Write("{0} min {1} sec - ",
                                c[i].duracion.minutos, c[i].duracion.segundos);
                            Console.WriteLine("{0} KB", c[i].tamaño);

                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No hay canciones que mostrar");
                    }
                   
                    break;
                case menu.buscar:
                    if (cantidad > 0)
                    {
                        Console.WriteLine("Introduce el nombre del artista");
                        ArtBuscar = Console.ReadLine();
                        Console.Clear();
                        for (int i = 0; i <= cantidad; i++)
                        {
                            if (c[i].artista == ArtBuscar)
                            {
                                encontrado = true;
                            }
                        }
                        if (encontrado = true)
                        {
                            Console.WriteLine("Canciones de {0}", ArtBuscar);
                            Console.WriteLine();
                            for (int i = 0; i <= cantidad; i++)
                            {
                                if (c[i].artista == ArtBuscar)
                                {
                                    Console.WriteLine(c[i].titulo);
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("No hay canciones de ese artista");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No hay canciones que buscar");
                    }
                    break;

                case menu.ordenalf:

                    if (cantidad > 0)
                    {
                        for (int i = 0; i < cantidad - 1; i++)
                        {
                            for (int j = i + 1; j < cantidad; j++)
                            {
                                if (c[i].titulo.CompareTo(c[j].
                                titulo) > 0)
                                {
                                    Cancion auxiliar = c[i];
                                    c[i] = c[j];
                                    c[j] = auxiliar;
                                }
                            }

                        }

                        Console.WriteLine("Artista - Titulo - Duración - " +
                            "Peso");
                        Console.WriteLine();
                        for (int i = 0; i < cantidad; i++)
                        {
                            Console.Write("{0} - ", c[i].artista);
                            Console.Write("{0} - ", c[i].titulo);
                            Console.Write("{0} min {1} sec - ",
                                c[i].duracion.minutos, c[i].duracion.segundos);
                            Console.WriteLine("{0} KB", c[i].tamaño);

                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No hay canciones que mostrar");
                    }
                    break;

                case menu.ordenkb:
                    if (cantidad > 0)
                    {
                        for (int i = 0; i < cantidad - 1; i++)
                        {
                            for (int j = i + 1; j < cantidad; j++)
                            {
                                if (c[i].tamaño < c[j].tamaño)
                                {
                                    Cancion auxiliar = c[i];
                                    c[i] = c[j];
                                    c[j] = auxiliar;
                                }
                            }

                        }
                        

                        Console.WriteLine("Artista - Titulo - Duración - " +
                            "Peso");
                        Console.WriteLine();
                        for (int i = 0; i < cantidad; i++)
                        {
                            Console.Write("{0} - ", c[i].artista);
                            Console.Write("{0} - ", c[i].titulo);
                            Console.Write("{0} min {1} sec - ",
                                c[i].duracion.minutos, c[i].duracion.segundos);
                            Console.WriteLine("{0} KB", c[i].tamaño);

                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No hay canciones que mostrar");
                    }
                    break;
            }
            if (opcion != (int)menu.salir)
            {
                Console.WriteLine();
                Console.WriteLine("Pulsa enter para continuar");
                Console.ReadLine();
                Console.Clear();
            }
           
        }
        while ((menu)opcion != menu.salir);

    }
}
