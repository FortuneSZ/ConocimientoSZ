﻿/*Este programa pide al usuario que introduzca una frase, este cogerá los 
nombres propios, es decir palabras empezadas por letra mayúscula 
(no contando la primera palabra que siempre va en mayúscula), las ordenará
alfabéticamente y las mostrará separadas por comas*/
using System;
class Practica_4_3_2
{
    static void Main()
    {
        int tamanyo = 0,contador = 0;
        string frase, palabra, inicial;
        string[] nombresPropios = new string[tamanyo];
        char[] delimitadores = { ',', ' ' };

        Console.WriteLine("Escribe un frase");
        frase = Console.ReadLine();

        string[] partes = frase.Split(delimitadores);
        for (int i = 0; i < partes.Length; i++)
        {
            if (i != 0)
            {
                
                palabra = partes[i];
                inicial = "" + palabra[0];
                if (inicial.CompareTo(inicial.ToUpper()) == 0)
                {
                    tamanyo++;
                    nombresPropios = new string[tamanyo];
                }
            }  
        }

        if (tamanyo != 0)
        {
            for (int i = 0; i < partes.Length; i++)
            {
                if (i != 0)
                {

                    palabra = partes[i];
                    inicial = "" + palabra[0];
                    if (inicial.CompareTo(inicial.ToUpper()) == 0)
                    {
                        nombresPropios[contador] = palabra;
                        contador++;
                    }
                }
            }

            for (int i = 0; i < tamanyo - 1; i++)
            {
                for (int j = i + 1; j < tamanyo; j++)
                {
                    if (nombresPropios[i].CompareTo(nombresPropios[j]) > 0)
                    {
                        string auxiliar = nombresPropios[i];
                        nombresPropios[i] = nombresPropios[j];
                        nombresPropios[j] = auxiliar;
                    }
                }
            }
            Console.Clear();
            Console.Write("Los nombres propios son: ");
            for (int i = 0; i < nombresPropios.Length; i++)
            {
                Console.Write("{0}", nombresPropios[i]);
                if (i < tamanyo - 1)
                {
                    Console.Write(",");
                }
            }
        }
        else
        {
            Console.Clear();
            Console.WriteLine("No se encontraron nombres propios");
        }

        
    }
}
