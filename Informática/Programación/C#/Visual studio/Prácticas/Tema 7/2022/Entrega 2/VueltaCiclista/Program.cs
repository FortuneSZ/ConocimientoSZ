﻿/*Este programa gestiona las etapas de una vuelta ciclista, en primer
lugar pide añadir etapas hasta que el usuario introduce una fecha de
etapa vacía, de cada una de esas etapas se especifica la fecha y el
podio.

Posteriormente una vez añadidas todas las etapas se muestra la
información de las mismas y, seguidamente la lista de los ciclistas
que han ganado alguna etapa*/
using System.Text.RegularExpressions;

class ciclismo
{
    static void ErrorPodio()
    {
        Console.WriteLine();
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Ciclista ya añadido al podio");
        Console.ResetColor();
        Console.WriteLine();
        Console.ReadLine();
        Console.Clear();
    }
    static void Enter()
    {
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.WriteLine();
        Console.ReadLine();
        Console.Clear();
    }

    static void Ganadores(SortedDictionary<string, Etapa> etapas)
    {
        List<Ciclista> ganadores = new List<Ciclista>();
        Ciclista Ganador;

        foreach (KeyValuePair<string, Etapa> etapa in etapas)
        {
            Ganador = etapa.Value.MostrarGanador();

            if (!ganadores.Contains(Ganador)) 
            {
                ganadores.Add(Ganador);
            }
        }

        Console.Clear();
        Console.WriteLine("Lista de ganadores");
        Console.WriteLine();
        Console.ForegroundColor = ConsoleColor.Green;
        for (int i = 0; i < ganadores.Count; i++)
        {
            Console.WriteLine(ganadores[i]);
        }
        Console.ResetColor();
    }

    static void MostrarEtapas(SortedDictionary<string, Etapa> etapas)
    {
        foreach (KeyValuePair<string, Etapa> etapa in etapas)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("*************************");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(etapa.Value);
            Console.ResetColor();
        }
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("*************************");
        Console.ResetColor();
        Enter();
    }
    static void Podio(List<Ciclista> ciclistas, SortedDictionary<string,
        Etapa> etapas,Etapa e)
    {
        int contador = 0, posCiclista;
        Ciclista c;
        do
        {
            Console.Clear();
            Console.WriteLine("¿Que ciclista ha quedado {0}?",contador+1);
            Console.WriteLine();

            for (int i = 0; i < ciclistas.Count; i++)
            {
                Console.WriteLine(i+1 +". " + ciclistas[i]);
            }

            try
            {
                Console.WriteLine();
                posCiclista = Convert.ToInt32(Console.ReadLine());
                c = ciclistas[posCiclista - 1];

                if (e.ComprobarPodio(c) == true)
                {
                    ErrorPodio();
                }
                else
                {
                    e.AnyadirPodio(contador, c);
                    contador++;
                }
            }
            catch(Exception ex) 
            {
                Console.WriteLine();
                Console.WriteLine("Posición incorrecta");
                Enter();
            } 
        }
        while (contador != 3);
        etapas.Add(e.Fecha,e);
    }
    static Etapa NuevaEtapa(SortedDictionary<string, Etapa> etapas,string fecha)
    {
        Regex reg = new Regex("^[0-9]{4}-[0-9]{2}-[0-9]{2}$");
        Etapa e = null;
        try
        {
            

            if (reg.IsMatch(fecha)) 
            {
                if (etapas.ContainsKey(fecha)) 
                {
                    throw new Exception("Fecha ya existente");
                }
                else
                {
                    e = new Etapa(fecha);
                    return e;
                }
            }
            else
            {
                throw new Exception("Fecha incorrecta");
            }
        }
        catch (Exception ex)
        {
            Console.ForegroundColor= ConsoleColor.Red;
            Console.WriteLine();
            Console.WriteLine(ex.Message);
            Console.ResetColor();
            Enter();
            
        }
        return e;
    }
    static void Main()
    {
        List<Ciclista> ciclistas = new List<Ciclista>();
        ciclistas.Add(new Ciclista(15,"Paco Ramirez","España"));
        ciclistas.Add(new Ciclista(2, "Marcos", "Francia"));
        ciclistas.Add(new Ciclista(10, "María", "Alemania"));
        ciclistas.Add(new Ciclista(22, "Sergio", "Suecia"));
        ciclistas.Add(new Ciclista(1, "Sara", "Polonia"));
        ciclistas.Add(new Ciclista(11, "Fran", "Italia"));

        SortedDictionary<string,Etapa> etapas = new SortedDictionary<string,Etapa>();
        String fecha;

        do
        {
            Console.Clear();
            Console.WriteLine("Introduzca la fecha de la etapa");
            fecha = Console.ReadLine();

            if (fecha != "")
            {
                Etapa et = NuevaEtapa(etapas, fecha);

                if (et != null)
                {
                    Podio(ciclistas, etapas, et);
                }
            }
        }
        while(fecha != "");

        MostrarEtapas(etapas);
        Ganadores(etapas);
    }
}
