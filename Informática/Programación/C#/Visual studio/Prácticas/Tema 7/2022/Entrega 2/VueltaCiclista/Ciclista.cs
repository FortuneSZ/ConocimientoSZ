﻿//Clase ciclista
class Ciclista
{
    private int dorsal;
    private string nombre;
    private string equipo;

    public int Dorsal
    {
        get 
        {
            return dorsal; 
        }
        set
        {
            dorsal = value; 
        }
    }

    public string Nombre
    {
        get
        {
            return nombre;
        }
        set 
        {
            nombre = value; 
        }
    }

    public string Equipo
    {
        get
        {
            return equipo;
        }
        set
        {
            equipo = value;
        }
    }

    public Ciclista(int dorsal,string nombre, string equipo)
    {
        Dorsal = dorsal;
        Nombre = nombre;
        Equipo = equipo;
    }

    public override string ToString()
    {
        return nombre + ", perteneciente al equipo " 
            + equipo + " con dorsal " + dorsal;
    }
}

