﻿//Clase etapa
using System.Text.RegularExpressions;

class Etapa
{
    private string fecha;
    private Ciclista[] podio;

    public string Fecha
    {
        get
        {
            return fecha;
        }
        set
        {
            fecha = value;
        }
    }

    public Etapa(string fecha)
    {
        Fecha = fecha;
        podio= new Ciclista[3];
    }

    public void AnyadirPodio(int posicion,Ciclista c)
    {
        podio[posicion] = c;
    }

    public bool ComprobarPodio(Ciclista c)
    {
        bool encontrado = false;

        for (int i = 0; i < podio.Length; i++) 
        {
            if (podio[i] == c)
            {
                encontrado= true;
            }
        }
        return encontrado;
    }

    public Ciclista MostrarGanador()
    {
        return podio[0];
    }

    public override string ToString()
    {
        return fecha +"\n\t" + podio[0].Nombre + "\n\t" + podio[1].Nombre 
            + "\n\t" + podio[2].Nombre;
    }
}

