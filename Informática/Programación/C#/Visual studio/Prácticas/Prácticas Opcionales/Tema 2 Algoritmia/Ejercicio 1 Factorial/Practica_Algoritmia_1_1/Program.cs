﻿/*Este programa Calcula el último dígito del factorial de un número
 introducido por el usuario*/
using System;
class Practica_Algoritmia_1_1
{
    static void Main()
    {
        int num, factorial, auxiliar=1, veces;
        Console.WriteLine("Introduzca el número de casos de prueba");
        veces = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        do
        {
            Console.WriteLine("Introduzca un número");
            num = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            for (int i = num; i > 0; i--)
            {
                auxiliar *= i;
            }
            factorial = auxiliar % 10;
            Console.WriteLine("Factorial");
            Console.WriteLine(factorial);
            auxiliar = 1;
            veces--;
        }
        while(veces > 0);
        
    }
}
