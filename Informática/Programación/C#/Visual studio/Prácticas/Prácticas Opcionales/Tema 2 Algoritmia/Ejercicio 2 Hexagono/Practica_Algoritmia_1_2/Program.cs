﻿/*Este porgrama crea un hexagono del tamaño y con el icono introducido
 por el usuario,hasta que estos sean 0 y 0 respectivamente*/
using System;
class Practica_Algoritmia_1_2
{
    static void Main()
    {
        int lado;
        char simbolo;

        do
        {
            Console.WriteLine("Diga el lado del hexagono");
            lado = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Diga el símbolo con el que crear el hexagono");
            simbolo = Convert.ToChar(Console.ReadLine());

            for (int x = 0; x < lado - 1; x++)
            {
                Console.Write(" ");
            }
            for (int x = 0; x < lado; x++)
            {
                Console.Write(simbolo);
            }

            for (int y = 1; y < lado - 1; y++)
            {
                Console.WriteLine(" ");
                for (int x = 0; x < lado - y - 1; x++)
                {
                    Console.Write(" ");
                }
                Console.Write(simbolo);
                for (int x = 0; x < lado + 2 * y - 2; x++)
                {
                    Console.Write(simbolo);
                }
                Console.Write(simbolo);
            }

            for (int y = lado; y > 1; y--)
            {
                Console.WriteLine(" ");
                for (int x = 0; x < lado - y; x++)
                {
                    Console.Write(" ");
                }
                Console.Write(simbolo);
                for (int x = 0; x < lado + 2 * y - 4; x++)
                {
                    Console.Write(simbolo);
                }
                Console.Write(simbolo);
            }

            Console.WriteLine(" ");
            for (int x = 0; x < lado - 1; x++)
            {
                Console.Write(" ");
            }
            for (int x = 0; x < lado; x++)
            {
                Console.Write(simbolo);
            }
            Console.WriteLine(" ");

            Console.WriteLine(" ");
            Console.WriteLine("Pulse enter para continuar");
            Console.ReadLine();
            Console.Clear();
        }
        while (lado != 0 && simbolo != '0');
        
    }
}
