﻿/*Este programa le pedirá al usuario el número de años quiere analizar las
 finanzas,e introducirá tantos números como años haya decidido,posteriormente
el programa le dirá las ventas máximas,mínimas y las ventas medias*/
using System;
class Practica_3_1_1
{
    static void Main()
    {
        int tiempo;
        double euros, media=0, minimo = 0, maximo = 0;

        Console.WriteLine("Escribe el periodo en años");
        tiempo = Convert.ToInt32(Console.ReadLine());
        for(int i = 0; i < tiempo;i++)
        {
            Console.Clear();
            Console.WriteLine("Escriba las ganancias en euros de este " +
                "año");
            euros = Convert.ToDouble(Console.ReadLine());

            if (minimo == 0)
            {
                minimo = euros;
            }

            if (euros < minimo)
            {
                minimo = euros;
            }

            if (maximo == 0)
            {
                maximo = euros;
            }

            if (euros > maximo)
            {
                maximo = euros;
            }

            media += euros;
        }
        Console.Clear();

        media /= tiempo;

        Console.WriteLine("Las ventas mínimas han sido {0} euros", minimo);
        Console.WriteLine("Las ventas máximas han sido {0} euros", maximo);
        Console.WriteLine("Las ventas medias han sido {0} euros", 
            media.ToString("N2"));
    }
}
