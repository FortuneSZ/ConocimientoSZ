﻿/*Este programa pide al usuario una longitud y la unidad en la que se encuentra
 ,si la unidad no es válida la pedirá repetidamente hasta que así sea,una vez
lo sea,si la unidad no es 'C' o 'P' dirá que no es válida y acabará el programa
,si es alguno de los dos,convertirá la distancia a la otra unidad y mostrará
la conversión por pantalla*/
using System;
class Practica_3_1_2
{
    static void Main()
    {
        double longitud,aux;
        char unidad;
        bool correcto=true;

        Console.WriteLine("Escriba la longitud");
        longitud = Convert.ToDouble(Console.ReadLine());
        Console.Clear();

        do
        {
            try
            {
                
                Console.WriteLine("Escriba la unidad de la longitud");
                unidad = Convert.ToChar(Console.ReadLine());
                Console.Clear();

                if (unidad != 'C' && unidad != 'P')
                {
                    Console.WriteLine("Unidad no válida");
                }
                else
                {
                    aux = longitud;
                    if (unidad == 'C')
                    {
                        longitud /= 2.54;
                        Console.WriteLine("{0} cm en pulgadas son {1}",
                            aux,longitud.ToString("N3"));
                    }
                    else
                    {
                        longitud *= 2.54;
                        Console.WriteLine("{0} pulgadas en cm son {1}",
                            aux, longitud.ToString("N3"));
                    }
                }



            }
            catch (Exception e)
            {
                correcto = false;
            }
        }
        while (correcto == false);
        
    }
}
