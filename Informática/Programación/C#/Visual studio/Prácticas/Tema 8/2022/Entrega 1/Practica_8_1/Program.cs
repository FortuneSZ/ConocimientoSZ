﻿/*
 * Programa principal de gestión de solicitudes
 */

using System;
using System.Net;

enum opciones { SALIR, NUEVA, ADMINISTRATIVO, LISTADO, TOTAL, ORDENAR, BORRAR }

class Program
{
    // Comprueba si el id ya existe en el array o no. Lanza excepción si existe
    static void ComprobarId(List<Solicitud> solicitudes, string id)
    {
        for (int i = 0; i < solicitudes.Count; i++)
        {
            if (solicitudes[i].Id == id)
                throw new Exception("El Id de solicitud ya existe");
        }
    }

    // Devuelve el administrativo con el DNI indicado,
    // o lanza excepción si no está
    static Administrativo ComprobarAdmin(List<Administrativo> administrativos,
        string dni)
    {
        Administrativo resultado = null;

        foreach(Administrativo a in administrativos)
        {
            if (a.Dni == dni)
                resultado = a;
        }

        if (resultado == null)
            throw new Exception("No se ha encontrado un administrativo con " +
                "el DNI indicado");
        return resultado;
    }

    // Comprueba si el número de cuenta es correcto o no (lanza excepción)
    static void ComprobarNumCuenta(string cuenta)
    {
        if (cuenta.Length != 20)
            throw new Exception("Número de cuenta incorrecto");
        else
        {
            foreach (char digito in cuenta)
            {
                if (digito < '0' || digito > '9')
                    throw new Exception("Número de cuenta incorrecto");
            }
        }
    }

    // Resto de funciones para las opciones del programa principal

    static void ListarAdministrativos(List<Administrativo> administrativos)
    {
        Console.WriteLine("Listado de administrativos:");
        foreach (Administrativo a in administrativos)
            Console.WriteLine(a);
    }

    static opciones Menu()
    {
        opciones opcion;
        Console.Clear();
        Console.WriteLine("{0}. Nueva solicitud", (int)opciones.NUEVA);
        Console.WriteLine("{0}. Solicitudes por administrativo", 
            (int)opciones.ADMINISTRATIVO);
        Console.WriteLine("{0}. Listado de solicitudes", (int)opciones.LISTADO);
        Console.WriteLine("{0}. Total pendiente de pago", (int)opciones.TOTAL);
        Console.WriteLine("{0}. Ordenar solicitudes por ID", 
            (int)opciones.ORDENAR);
        Console.WriteLine("{0}. Eliminar solicitud", (int)opciones.BORRAR);
        Console.WriteLine("{0}. Salir", (int)opciones.SALIR);
        Console.Write("Elige una opción: ");
        opcion = (opciones)Convert.ToInt32(Console.ReadLine());
        return opcion;
    }

    static void NuevaSolicitud(List<Solicitud> solicitudes,
        List<Administrativo> administrativos)
    {
        Administrativo admin;
        int tipo, horas;
        float importe;
        string id, fecha, dni, cuenta, concepto, espacio, fechaReserva, 
            horaInicio;

        Console.WriteLine("Elige tipo de solicitud:");
        Console.WriteLine("1. Domiciliación / 2. Tasas / 3. Reserva");
        tipo = Convert.ToInt32(Console.ReadLine());

        // Pedimos datos comunes

        Console.Write("Id. de solicitud: ");
        id = Console.ReadLine();
        ComprobarId(solicitudes, id);

        Console.Write("Fecha solicitud: ");
        fecha = Console.ReadLine();

        ListarAdministrativos(administrativos);
        Console.Write("DNI administrativo: ");
        dni = Console.ReadLine();
        admin = ComprobarAdmin(administrativos, dni);

        // Distinguimos tipo de solicitud
        if (tipo == 1)
        {
            Console.Write("Núm. cuenta: ");
            cuenta = Console.ReadLine();
            ComprobarNumCuenta(cuenta);

            solicitudes.Add(new SolicitudDomiciliacion(id, fecha, admin, 
                cuenta));
        }
        else if (tipo == 2)
        {
            Console.Write("Concepto: ");
            concepto = Console.ReadLine();

            Console.Write("Importe a pagar: ");
            if (!Single.TryParse(Console.ReadLine(), out importe) || 
                importe <= 0)
                throw new Exception("Importe incorrecto");
            solicitudes.Add(new SolicitudTasas(id, fecha, admin, concepto, 
                importe));
        }
        else
        {
            Console.Write("Espacio: ");
            espacio = Console.ReadLine();
            Console.Write("Fecha reserva: ");
            fechaReserva = Console.ReadLine();
            Console.Write("Hora inicio reserva: ");
            horaInicio = Console.ReadLine();
            Console.Write("Duración reserva (horas): ");
            horas = Convert.ToInt32(Console.ReadLine());
            solicitudes.Add(new SolicitudReserva(id, fecha, admin, espacio, 
                fechaReserva, horaInicio, horas));
        }
    }

    static void SolicitudesPorAdmin(List<Solicitud> solicitudes,
        List<Administrativo> administrativos)
    {
        string dni;
        bool resultados = false;

        ListarAdministrativos(administrativos);
        Console.Write("DNI administrativo: ");
        dni = Console.ReadLine();
        ComprobarAdmin(administrativos, dni);

        for(int i = 0; i < solicitudes.Count; i++)
        {
            if (solicitudes[i].Administrativo.Dni == dni)
            {
                resultados = true;
                Console.WriteLine(solicitudes[i]);
            }
        }

        if (!resultados)
            Console.WriteLine("El administrativo seleccionado no tiene " +
                "solicitudes a su cargo");
    }

    static void ListadoSolicitudes(List<Solicitud> solicitudes)
    {
        for(int i = 0; i < solicitudes.Count; i++)
        {
            if (solicitudes[i] is SolicitudDomiciliacion)
                Console.ForegroundColor = ConsoleColor.Blue;
            else if (solicitudes[i] is SolicitudTasas)
                Console.ForegroundColor = ConsoleColor.Red;
            else
                Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine(solicitudes[i]);
        }

        Console.ResetColor();
    }

    static void TotalPagar(List<Solicitud> solicitudes)
    {
        float total = 0;

        for(int i = 0; i < solicitudes.Count; i++)
        {
            if (solicitudes[i] is SolicitudTasas)
            {
                total += ((SolicitudTasas)solicitudes[i]).Importe;
            }
        }

        Console.WriteLine("Total a pagar: {0} eur.", total.ToString("N2"));
    }

    static void OrdenarSolicitudes(List<Solicitud> solicitudes)
    {
        solicitudes.Sort((s1, s2) => s1.Id.CompareTo(s2.Id));
        ListadoSolicitudes(solicitudes);
    }

    static void BorrarSolicitud(List<Solicitud> solicitudes)
    {
        string id;
        bool existe = false;
        int i = 0;
        Console.WriteLine("Introduce el ID de la solicitud a borrar:");
        id = Console.ReadLine();
        while(i < solicitudes.Count && !existe)
        {
            if (solicitudes[i].Id == id)
            {
                existe = true;
                solicitudes.RemoveAt(i);
            }
            else
            {
                i++;
            }
        }

        if(!existe)
        {
            Console.WriteLine("No se ha encontrado una solicitud con el ID " + 
                "indicado");
        }
    }

    public static List<Administrativo> CargarAdministrativos()
    {
        string linea;
        List<Administrativo> lista = new List<Administrativo>();
        StreamReader admin = File.OpenText("administrativos.txt");
        do
        {
            linea = admin.ReadLine();
            if (linea != null)
            {
                string[] partes = linea.Split(";");
                Administrativo a = new Administrativo(partes[0], partes[1], partes[2]);
                lista.Add(a);
            }
        }
        while (linea != null);
        admin.Close();
        return lista;
    }

    public static List<Solicitud> CargarSolicitudes(List<Administrativo> administrativos)
    {
        string linea;
        Administrativo a = new Administrativo("","","");
        List<Solicitud> lista = new List<Solicitud>();
        StreamReader admin = File.OpenText("administrativos.txt");
        do
        {
            linea = admin.ReadLine();
            if (linea != null)
            {
                string[] partes = linea.Split(";");

                for (int i = 0; i < administrativos.Count; i++)
                {
                    if (administrativos[i].Dni == partes[2])
                    {
                        a = administrativos[i];
                    }
                }

                if (partes[0] == "D")
                {
                    SolicitudDomiciliacion b = new SolicitudDomiciliacion(partes[1], partes[2],a,partes[4]);
                    lista.Add(b);
                }
                if (partes[0] == "T")
                {
                    float num = Convert.ToSingle(partes[5]);
                    SolicitudTasas b = new SolicitudTasas(partes[1], partes[2], a, partes[4],num);
                    lista.Add(b);
                }
                if (partes[0] == "R")
                {
                    int num = Convert.ToInt32(partes[7]);
                    SolicitudReserva b = new SolicitudReserva(partes[1], partes[2], a, partes[4], partes[5], partes[6], num);
                    lista.Add(b);
                }
            }
        }
        while (linea != null);
        admin.Close();
        return lista;
    }

    public static void GuardarSolicitudes(int numSoli,List<Solicitud> solicitudes)
    {
        using (StreamWriter fichero = new StreamWriter("solicitudes.txt", true))
        {
            for (int i = numSoli-1; i < solicitudes.Count; i++) 
            {
                fichero.WriteLine(solicitudes[i].AFichero());
            }
        }
    }

    static void Main()
    {
        List<Administrativo> administrativos = CargarAdministrativos();
        List<Solicitud> solicitudes = CargarSolicitudes(administrativos);
        int numSoli = solicitudes.Count;
        opciones opcion;

        do
        {
            opcion = Menu();
            switch (opcion)
            {
                case opciones.NUEVA:
                    try
                    {
                        NuevaSolicitud(solicitudes, administrativos);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: " + e.Message);
                    }
                    break;
                case opciones.ADMINISTRATIVO:
                    try
                    {
                        SolicitudesPorAdmin(solicitudes, administrativos);
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case opciones.LISTADO:
                    ListadoSolicitudes(solicitudes);
                    break;
                case opciones.TOTAL:
                    TotalPagar(solicitudes);
                    break;
                case opciones.ORDENAR:
                    OrdenarSolicitudes(solicitudes);
                    break;
                case opciones.BORRAR:
                    BorrarSolicitud(solicitudes);
                    break;
            }

            Console.WriteLine("Pulsa cualquier tecla para continuar...");
            Console.ReadKey(true);
        }
        while (opcion != opciones.SALIR);
        GuardarSolicitudes(numSoli,solicitudes);
    }
 
}