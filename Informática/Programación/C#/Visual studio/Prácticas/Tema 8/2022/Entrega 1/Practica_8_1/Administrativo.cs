﻿/*
 * Personal administrativo que atiende las solicitudes
 */
class Administrativo
{
    private string dni;
    private string nombre;
    private string telefono;
    public string Dni
    {
        get { return dni; }
        set { dni = value; }
    }

    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }

    public string Telefono
    {
        get { return telefono; }
        set { telefono = value; }
    }

    public Administrativo(string dni, string nombre, string telefono)
    {
        this.Dni = dni;
        this.Nombre = nombre;
        this.Telefono = telefono;
    }

    public override string ToString()
    {
        return dni  + ": " + nombre + ", " + telefono;
    }

    public static List<Administrativo> CargarAdministrativos()
    {
        string linea;
        List<Administrativo> lista = new List<Administrativo>();
        StreamReader admin = File.OpenText("administrativos.txt");
        do
        {
            linea = admin.ReadLine();
            if (linea != null)
            {
                string[] partes = linea.Split(";");
                Administrativo a = new(partes[0],partes[1],partes[2]);
                lista.Add(a);
            }
        }
        while (linea != null);
        admin.Close();
        return lista;

    }
}