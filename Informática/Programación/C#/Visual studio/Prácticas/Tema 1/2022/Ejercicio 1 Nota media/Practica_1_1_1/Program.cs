﻿/*Este programa pide al usuario 3 notas de examenes,y muestra la media de las 
3 ,sin decimales*/
using System;
class Practica_1_1_1
{
    static void Main()
    {
        int Nota1, Nota2, Nota3, Media;

        Console.WriteLine("Escribe la nota del primer examen");
        Nota1 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escribe la nota del Segundo examen");
        Nota2 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escribe la nota del tercer examen");
        Nota3 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Media = (Nota1 + Nota2 + Nota3)/3;

        Console.WriteLine("La nota media de los 3 examenes es {0}",Media);
    }
}
                                                                               