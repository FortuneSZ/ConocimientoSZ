﻿/*Este programa pedirá al usuario la velocidad que lleva un coche y la 
 distancia en Km a la ciudad a la que quiere llegar,y le dirá cuantos minutos
tardará en llegar*/
using System;
class Practica_1_1_2
{
    static void Main()
    {
        int Velocidad, Distancia, Tiempo;

        Console.WriteLine("Diga la velocidad a la que va el coche (Km/h)");
        Velocidad = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Diga la distancia a la ciudad a la que va (Km)");
        Distancia = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Tiempo = (Distancia / Velocidad) * 60;

        Console.WriteLine("Si el coche va a {0} Km/h y la ciudad está a" +
            " {1} km tardará {2} minutos en llegar",Velocidad,Distancia,
            Tiempo);
    }
}
