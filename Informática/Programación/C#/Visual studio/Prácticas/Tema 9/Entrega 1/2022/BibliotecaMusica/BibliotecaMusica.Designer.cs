﻿namespace BibliotecaMusica
{
    partial class BibliotecaMusica
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Canciones = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Titulo = new System.Windows.Forms.TextBox();
            this.Minutos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Segundos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Añadir = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Borrar = new System.Windows.Forms.Button();
            this.Aplicar = new System.Windows.Forms.Button();
            this.Lista1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.MaxDur = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Lista2 = new System.Windows.Forms.ComboBox();
            this.Error = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Canciones
            // 
            this.Canciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Canciones.FormattingEnabled = true;
            this.Canciones.ItemHeight = 29;
            this.Canciones.Location = new System.Drawing.Point(0, 59);
            this.Canciones.Name = "Canciones";
            this.Canciones.Size = new System.Drawing.Size(465, 613);
            this.Canciones.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(791, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nuevas canciones";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(478, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 37);
            this.label2.TabIndex = 2;
            this.label2.Text = "Título: ";
            // 
            // Titulo
            // 
            this.Titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Titulo.Location = new System.Drawing.Point(650, 92);
            this.Titulo.Name = "Titulo";
            this.Titulo.Size = new System.Drawing.Size(681, 44);
            this.Titulo.TabIndex = 3;
            // 
            // Minutos
            // 
            this.Minutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Minutos.Location = new System.Drawing.Point(650, 178);
            this.Minutos.Name = "Minutos";
            this.Minutos.Size = new System.Drawing.Size(236, 44);
            this.Minutos.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(478, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 37);
            this.label3.TabIndex = 6;
            this.label3.Text = "Minutos: ";
            // 
            // Segundos
            // 
            this.Segundos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Segundos.Location = new System.Drawing.Point(1110, 174);
            this.Segundos.Name = "Segundos";
            this.Segundos.Size = new System.Drawing.Size(221, 44);
            this.Segundos.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(919, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 37);
            this.label4.TabIndex = 8;
            this.label4.Text = "Segundos:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(478, 269);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 37);
            this.label5.TabIndex = 10;
            this.label5.Text = "Autor: ";
            // 
            // Añadir
            // 
            this.Añadir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Añadir.Location = new System.Drawing.Point(798, 340);
            this.Añadir.Name = "Añadir";
            this.Añadir.Size = new System.Drawing.Size(245, 56);
            this.Añadir.TabIndex = 12;
            this.Añadir.Text = "Añadir";
            this.Añadir.UseVisualStyleBackColor = true;
            this.Añadir.Click += new System.EventHandler(this.Añadir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Borrar);
            this.groupBox1.Controls.Add(this.Aplicar);
            this.groupBox1.Controls.Add(this.Lista1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.MaxDur);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(485, 413);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(846, 277);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtrado de canciones";
            // 
            // Borrar
            // 
            this.Borrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Borrar.Location = new System.Drawing.Point(471, 206);
            this.Borrar.Name = "Borrar";
            this.Borrar.Size = new System.Drawing.Size(245, 56);
            this.Borrar.TabIndex = 15;
            this.Borrar.Text = "Borrar filtros";
            this.Borrar.UseVisualStyleBackColor = true;
            this.Borrar.Click += new System.EventHandler(this.Borrar_Click);
            // 
            // Aplicar
            // 
            this.Aplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Aplicar.Location = new System.Drawing.Point(110, 206);
            this.Aplicar.Name = "Aplicar";
            this.Aplicar.Size = new System.Drawing.Size(245, 56);
            this.Aplicar.TabIndex = 14;
            this.Aplicar.Text = "Aplicar filtros";
            this.Aplicar.UseVisualStyleBackColor = true;
            this.Aplicar.Click += new System.EventHandler(this.Aplicar_Click);
            // 
            // Lista1
            // 
            this.Lista1.FormattingEnabled = true;
            this.Lista1.Location = new System.Drawing.Point(165, 142);
            this.Lista1.Name = "Lista1";
            this.Lista1.Size = new System.Drawing.Size(644, 45);
            this.Lista1.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 37);
            this.label7.TabIndex = 12;
            this.label7.Text = "Autor: ";
            // 
            // MaxDur
            // 
            this.MaxDur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaxDur.Location = new System.Drawing.Point(471, 73);
            this.MaxDur.Name = "MaxDur";
            this.MaxDur.Size = new System.Drawing.Size(338, 44);
            this.MaxDur.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(429, 37);
            this.label6.TabIndex = 3;
            this.label6.Text = "Duración máxima (minutos): ";
            // 
            // Lista2
            // 
            this.Lista2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lista2.FormattingEnabled = true;
            this.Lista2.Location = new System.Drawing.Point(650, 269);
            this.Lista2.Name = "Lista2";
            this.Lista2.Size = new System.Drawing.Size(681, 45);
            this.Lista2.TabIndex = 16;
            // 
            // Error
            // 
            this.Error.AutoSize = true;
            this.Error.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Error.Location = new System.Drawing.Point(46, 638);
            this.Error.Name = "Error";
            this.Error.Size = new System.Drawing.Size(0, 37);
            this.Error.TabIndex = 17;
            // 
            // BibliotecaMusica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1404, 718);
            this.Controls.Add(this.Error);
            this.Controls.Add(this.Lista2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Añadir);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Segundos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Minutos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Titulo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Canciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "BibliotecaMusica";
            this.Text = "Biblioteca Musica";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GuardarCanciones);
            this.Load += new System.EventHandler(this.Autores_OnLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Canciones;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Titulo;
        private System.Windows.Forms.TextBox Minutos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Segundos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Añadir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox MaxDur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Lista1;
        private System.Windows.Forms.Button Borrar;
        private System.Windows.Forms.Button Aplicar;
        private System.Windows.Forms.ComboBox Lista2;
        private System.Windows.Forms.Label Error;
    }
}

