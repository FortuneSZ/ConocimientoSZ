﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Clase del formulario principal

namespace BibliotecaMusica
{
    public partial class BibliotecaMusica : Form
    {
        private List<Autor> autores= new List<Autor>();
        private List<Cancion> canciones = new List<Cancion>();
        public BibliotecaMusica()
        {
            InitializeComponent();
        }
        private void Autores_OnLoad(object sender, EventArgs e)
        {
            autores = Autor.CargarAutores();
            canciones = Cancion.CargarCanciones();
            for (int i = 0; i < autores.Count; i++)
            {
                Lista1.Items.Add(autores[i].Nombre);
                Lista2.Items.Add(autores[i].Nombre);
            }

            for (int i = 0; i < canciones.Count; i++)
            {
                Canciones.Items.Add(canciones[i]);
            }
        }

        private void Añadir_Click(object sender, EventArgs e)
        {
            if (Titulo.Text != "" && Minutos.Text != "" &&
                Segundos.Text != "" && Lista2.Text != "")
            {
                Error.Text = "";
                string titulo = Titulo.Text;
                int minutos = Convert.ToInt32(Minutos.Text);
                int segundos = Convert.ToInt32(Segundos.Text);
                Autor autor = new Autor("",false);
                for (int i = 0; i < autores.Count; i++) 
                {
                    if (autores[i].Nombre == Lista2.Text)
                    {
                        autor = autores[i];
                    }
                }
                Cancion c = new Cancion(titulo, minutos, segundos, autor);
                canciones.Add(c);
                Canciones.Items.Add(c);
            }
            else
            {
                Error.ForeColor= Color.Red;
                Error.Text = "Los campos no pueden estar vacíos";
            }
        }

        private void GuardarCanciones(object sender, FormClosedEventArgs e)
        {
            Cancion.GuardarCanciones(canciones);
        }

        private void Aplicar_Click(object sender, EventArgs e)
        {
            int Dur = 0;
            if (MaxDur.Text != "")
            {
                Dur = Convert.ToInt32(MaxDur.Text);
            }
            string grupo = Lista1.Text;
            Canciones.Items.Clear();
            if (MaxDur.Text != "" && Lista1.Text != "")
            {
                for (int i = 0; i < canciones.Count; i++) 
                {
                    if (canciones[i].Autor.Nombre == grupo &&
                        canciones[i].Minutos <= Dur) 
                    {
                        Canciones.Items.Add(canciones[i]);
                    }
                }
            }
            else if (MaxDur.Text != "" && Lista1.Text == "")
            {
                for (int i = 0; i < canciones.Count; i++)
                {
                    if (canciones[i].Minutos <= Dur)
                    {
                        Canciones.Items.Add(canciones[i]);
                    }
                }
            }
            else if (MaxDur.Text == "" && Lista1.Text != "")
            {
                for (int i = 0; i < canciones.Count; i++)
                {
                    if (canciones[i].Autor.Nombre == grupo)
                    {
                        Canciones.Items.Add(canciones[i]);
                    }
                }
            }
            else
            {
                for (int i = 0; i < canciones.Count; i++)
                {
                    Canciones.Items.Add(canciones[i]);  
                }
            }
            MaxDur.Text = "";
            Lista1.Text = "";
        }

        private void Borrar_Click(object sender, EventArgs e)
        {
            Canciones.Items.Clear();
            MaxDur.Text = "";
            Lista1.Text = "";
            for (int i = 0; i < canciones.Count; i++)
            {
                Canciones.Items.Add(canciones[i]);
            }
        }
    }
}
