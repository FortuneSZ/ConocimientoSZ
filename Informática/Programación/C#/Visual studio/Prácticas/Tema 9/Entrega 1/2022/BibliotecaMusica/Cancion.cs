﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

//Clase canción

namespace BibliotecaMusica
{
    [Serializable]
    class Cancion
    {
        private string titulo;
        private int minutos;
        private int segundos;
        private Autor autor;

        public string Titulo
        {
            get 
            { 
                return titulo; 
            }
            set 
            { 
                titulo = value; 
            }
        }

        public int Minutos
        {
            get
            {
                return minutos;
            }
            set
            {
                if (value < 0)
                {
                    minutos = 0;
                }
                if (value >= 60)
                {
                    minutos = 59;
                }
                else
                {
                    minutos = value;
                }
            }
        }

        public int Segundos
        {
            get
            {
                return segundos;
            }
            set
            {
                if (value < 0) 
                {
                    segundos = 0;
                }
                if (value >= 60) 
                {
                    segundos = 59;
                }
                else
                {
                    segundos = value;
                }
            }
        }

        public Autor Autor
        {
            get
            {
                return autor;
            }
            set
            {
                autor = value;
            }
        }

        public Cancion(string titulo, int minutos, int segundos, Autor autor)
        {
            Titulo = titulo;
            Minutos = minutos;
            Segundos = segundos;
            Autor = autor;
        }

        public static List<Cancion> CargarCanciones()
        {
            List<Cancion> canciones = new List<Cancion>();
            if (File.Exists("canciones.dat"))
            {
                IFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream("canciones.dat",
                 FileMode.Open, FileAccess.Read, FileShare.Read);
                canciones = (List<Cancion>)formatter.Deserialize(stream);
                stream.Close();
            }
            return canciones;
        }

        public static void GuardarCanciones(List<Cancion> canciones)
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream("canciones.dat",
             FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, canciones);
            stream.Close();
        }

        public override string ToString()
        {
            return "[" + autor.Nombre + "] " + titulo 
                + " (" + minutos +":" + segundos + ")";
        }
    }
}
