﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Clase Autor

namespace BibliotecaMusica
{
    [Serializable]
    class Autor
    {
        private string nombre;
        private bool grupo;

        public string Nombre
        {
            get 
            {
                return nombre; 
            }
            set 
            { 
                nombre = value; 
            }
        }

        public bool Grupo
        {
            get
            {
                return grupo;
            }
            set
            {
                grupo = value;
            }
        }

        public Autor(string nombre, bool grupo) 
        {
            Nombre = nombre;
            Grupo = grupo;
        }

        public static List<Autor> CargarAutores()
        {
            List<Autor> autores= new List<Autor>();
            string linea;
            using (StreamReader fichero = new StreamReader("autores.txt"))
            {
                do
                {
                    linea = fichero.ReadLine();
                    if (linea != null)
                    {
                        string[] partes = linea.Split(';');
                        if (partes[1] == "S")
                        {
                            Autor a = new Autor(partes[0],true);
                            autores.Add(a);
                        }
                        else
                        {
                            Autor a = new Autor(partes[0], false);
                            autores.Add(a);
                        }
                    }
                }
                while (linea != null);
            }
            return autores;
        }

        public override bool Equals(object obj)
        {
            return obj is Autor autor &&
            nombre == autor.nombre;
        }

        public override int GetHashCode()
        {
            return -1597784800 + 
            EqualityComparer<string>.Default.GetHashCode(nombre);
        }
    }
}
