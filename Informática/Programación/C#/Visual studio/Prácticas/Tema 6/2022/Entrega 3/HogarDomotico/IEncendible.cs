﻿// Interfaz Iencendible, para los objetos que se pueden encender
interface IEncendible
{
    void Encender();

    void Apagar();

    bool Consultar();
}

