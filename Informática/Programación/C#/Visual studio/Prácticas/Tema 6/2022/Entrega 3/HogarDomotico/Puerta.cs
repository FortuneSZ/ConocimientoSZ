﻿// Clase puerta
class Puerta : ElementoDomotico
{
    private bool abierto;

    public bool Abierto
    {
        get 
        {
            return abierto; 
        } 
    }

    public Puerta(string nombre):base(nombre)
    {
        abierto = false;
        Nombre = nombre;
    }

    public void Abrir()
    {
        abierto= true;
    }

    public void Cerrar()
    {
        abierto= false;
    }

    public override void Mostrar()
    {
        if (abierto)
        {
            Console.BackgroundColor = ConsoleColor.Green;
        }
        else
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }
        Console.Write(" ");
        Console.ResetColor();
        Console.WriteLine(" {0}", nombre);
    }
}

