﻿// Clase luz
using System.Drawing;

class Luz : ElementoDomotico,IEncendible
{
    private bool encendido;

    public bool Encendido
    {
        get
        {
            return encendido;
        }
    }

    public Luz(string nombre):base(nombre) 
    {
        encendido= false;
        Nombre = nombre;
    }

    public void Encender()
    {
        encendido= true;
    }

    public void Apagar()
    {
        encendido= false;
    }

    public bool Consultar()
    {
        return encendido;
    }

    public override void Mostrar()
    {
        if (encendido) 
        {
            Console.BackgroundColor= ConsoleColor.Green;
        }
        else 
        {
            Console.BackgroundColor= ConsoleColor.Red;
        }
        Console.Write(" ");
        Console.ResetColor();
        Console.WriteLine(" {0}", nombre);
    }
}

