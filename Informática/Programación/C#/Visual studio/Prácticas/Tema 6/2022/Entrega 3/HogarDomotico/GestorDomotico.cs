﻿/* Clase GestorDomotico, que gestiona
los elementos de la casa domotica*/
class GestorDomotico
{
    ElementoDomotico[] elementos = new ElementoDomotico[6];

    public GestorDomotico()
    {
        elementos[0] = new Puerta("Puerta principal");
        elementos[1] = new Puerta("Puerta garaje");
        elementos[2] = new Luz("Luz salón");
        elementos[3] = new Luz("Luz cocina");
        elementos[4] = new Horno(0,"Horno cocina");
        elementos[5] = new Calefaccion(20, "Calefacción central");
    }

    public ElementoDomotico GetElemento(int posicion)
    {
        return elementos[posicion];
    }

    public void MostrarEstado()
    {
        for(int i = 0; i < elementos.Length; i++) 
        {
            elementos[i].Mostrar();
        }
    }

    public void ApagarTodo()
    {
        ((Luz)elementos[2]).Apagar();
        ((Luz)elementos[3]).Apagar();
        ((Horno)elementos[4]).Apagar();
        ((Calefaccion)elementos[5]).Apagar();
    }
}

