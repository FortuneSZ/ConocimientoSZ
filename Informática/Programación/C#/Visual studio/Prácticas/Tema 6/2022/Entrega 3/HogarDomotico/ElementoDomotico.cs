﻿// Clase abstracta elemento domótico
abstract class ElementoDomotico
{
    protected string nombre;

    public string Nombre
    {
        get 
        {
            return nombre;
        }
        set 
        {
            nombre = value; 
        } 
    }

    public ElementoDomotico(string nombre) 
    {
        Nombre = nombre;
    }

    abstract public void Mostrar();
}

