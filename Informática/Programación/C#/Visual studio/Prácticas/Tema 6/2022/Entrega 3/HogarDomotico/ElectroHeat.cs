﻿// Clase abstracta común de las clases horno y calefaccion
abstract class ElectroHeat : ElementoDomotico
{
    protected bool encendido;
    protected int temperatura;

    public bool Encendido
    {
        get
        {
            return encendido;
        }
    }

    public ElectroHeat(int temperatura, string nombre) :base(nombre)
    {
        encendido= false;
        this.temperatura = temperatura;
        Nombre = nombre;
    }

    public override void Mostrar()
    {
        if (encendido)
        {
            Console.BackgroundColor = ConsoleColor.Green;
        }
        else
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }
        Console.Write(" ");
        Console.ResetColor();
        Console.WriteLine(" {0} ({1}ºC)", nombre,temperatura);
    }

}
