﻿/*Este programa nos permite gestionar una casa domótica mediante
un menú de opciones, el cual es el siguiente:

 A/B abrir/cerrar puerta principal | C/D abrir/cerrar puerta garaje
 E/F on/off luz del salón | G/H luz de la cocina
 I/J on/off horno | K/L subir/bajar horno
 M/N on/off calefacción | O/P subir/bajar calefacción
 Q apagar todos | S salir

 Cada opción del menú realiza la acción correspondiente, más el
 horno no puede tener una temperatura menor a 0 o mayor a 250,
 ni la calefacción una temperatura inferior a 15 o superior a 30*/
class HogarDomotico
{
    static void Main()
    {
        GestorDomotico g = new GestorDomotico();
        ConsoleKeyInfo opcion;    
        do
        {
            g.MostrarEstado();
            for (int i = 0; i < 14; i++)
            {
                Console.WriteLine();
            }
            Console.WriteLine("A/B abrir/cerrar puerta principal " +
                "| C/D abrir/cerrar puerta garaje");
            Console.WriteLine("E/F on/off luz del salón " +
                "| G/H luz de la cocina");
            Console.WriteLine("I/J on/off horno " +
                "| K/L subir/bajar horno");
            Console.WriteLine("M/N on/off calefacción " +
                "| O/P subir/bajar calefacción");
            Console.WriteLine("Q apagar todos | S salir");

            opcion = Console.ReadKey();

            try
            {
                switch (opcion.Key)
                {
                    case ConsoleKey.A:
                        ((Puerta)g.GetElemento(0)).Abrir();
                        break;

                    case ConsoleKey.B:
                        ((Puerta)g.GetElemento(0)).Cerrar();
                        break;

                    case ConsoleKey.C:
                        ((Puerta)g.GetElemento(1)).Abrir();
                        break;

                    case ConsoleKey.D:
                        ((Puerta)g.GetElemento(1)).Cerrar();
                        break;

                    case ConsoleKey.E:
                        ((Luz)g.GetElemento(2)).Encender();
                        break;

                    case ConsoleKey.F:
                        ((Luz)g.GetElemento(2)).Apagar();
                        break;

                    case ConsoleKey.G:
                        ((Luz)g.GetElemento(3)).Encender();
                        break;

                    case ConsoleKey.H:
                        ((Luz)g.GetElemento(3)).Apagar();
                        break;

                    case ConsoleKey.I:
                        ((Horno)g.GetElemento(4)).Encender();
                        break;

                    case ConsoleKey.J:
                        ((Horno)g.GetElemento(4)).Apagar();
                        break;

                    case ConsoleKey.K:
                        ((Horno)g.GetElemento(4)).Temperatura =
                            ((Horno)g.GetElemento(4)).Temperatura+1;
                        break;

                    case ConsoleKey.L:
                        ((Horno)g.GetElemento(4)).Temperatura =
                            ((Horno)g.GetElemento(4)).Temperatura - 1;
                        break;

                    case ConsoleKey.M:
                        ((Calefaccion)g.GetElemento(5)).Encender();
                        break;

                    case ConsoleKey.N:
                        ((Calefaccion)g.GetElemento(5)).Apagar();
                        break;

                    case ConsoleKey.O:
                        ((Calefaccion)g.GetElemento(5)).Temperatura =
                            ((Calefaccion)g.GetElemento(5)).Temperatura + 1;
                        break;

                    case ConsoleKey.P:
                        ((Calefaccion)g.GetElemento(5)).Temperatura =
                            ((Calefaccion)g.GetElemento(5)).Temperatura - 1;
                        break;

                    case ConsoleKey.Q:
                        g.ApagarTodo();
                        break;
                }
                Console.Clear();
            }
            catch (Exception ex) 
            {
                Console.Clear();
                Console.WriteLine(ex.Message);
                Console.WriteLine();
                Console.WriteLine("Pulse enter para continuar");
                Console.ReadLine();
                Console.Clear();
            }
            
        }
        while(opcion.Key != ConsoleKey.S);
        Console.WriteLine("Has elegido salir");

    }
}
