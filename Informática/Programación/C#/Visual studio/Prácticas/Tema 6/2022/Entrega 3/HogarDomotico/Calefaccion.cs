﻿// Clase calefaccion
class Calefaccion : ElectroHeat, IEncendible
{
    public int Temperatura
    {
        get
        {
            return temperatura;
        }
        set
        {
            if (value >= 15 && value <= 30)
            {
                this.temperatura = value;
            }
            else if (value < 15)
            {
                throw new Exception
                    ("ERROR: No se puede bajar más la temperatura");
            }
            else
            {
                throw new Exception
                    ("ERROR: No se puede subir más la temperatura");
            }
        }
    }

    public  Calefaccion(int temperatura, string nombre) : base(temperatura, nombre)
    {
        encendido = false;
        Temperatura = temperatura;
        Nombre = nombre;
    }

    public void Encender()
    {
        encendido = true;
    }

    public void Apagar()
    {
        encendido = false;
    }

    public bool Consultar()
    {
        return encendido;
    }

    public override void Mostrar()
    {
        base.Mostrar();
    }
}

