﻿/*Este programa consiste en un menú de opciones que gestiona un array
juegos de mesa.

 Las opciones son las siguientes:

 1.Nuevo juego: añade un nuevo juego al final del array, en caso de
 que no esté lleno o el precio sea negativo.

 2.Borrar juego: Borra un juego de una posición específica,el usuario
 debe confirmar la operación, en caso de no confirmarse se cancelará.

 3.Juego más caro: Muestra el juego más caro registrado en el array.

 4.Juegos por título: Pide al usuario que introduzca un título y 
 si algúno del array contiene ese título lo mostrará por pantalla,
 mostrará que no hay ninguno de ser el caso.

 0. Salir: Finaliza el programa.*/
using System;
class Practica_6_1
{
    enum menu { salir, nuevo, borrar, caro, titulo }

    static void NuevoJuego(JuegoMesa[] juegos, ref int numJuegos)
    {
        JuegoMesa nuevoJuego;
        string nombre;
        int edadmin;
        int jugmin;
        int jugmax;
        float precio;

        if (numJuegos != juegos.Length)
        {
            Console.WriteLine("Introduce el nombre del juego");
            nombre = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Introduce la edad mínima del juego");
            edadmin = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Introduce la cantidad mínima de jugadores");
            jugmin = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Introduce la cantidad máxima de jugadores");
            jugmax = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            try
            {
                Console.WriteLine("Introduce el precio del juego");
                precio = Convert.ToSingle(Console.ReadLine());
                Console.Clear();

                if (precio > 0)
                {
                    nuevoJuego = new JuegoMesa(nombre,precio,edadmin,jugmin,jugmax);
                    juegos[numJuegos] = nuevoJuego;
                    numJuegos++;
                    Console.WriteLine("Registro realizado correctamente");
                }
                else
                {
                    Console.WriteLine("El precio no es válido");
                }
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("Debes introducir un número");
            }
        }
        else
        {
            Console.WriteLine("No hay espacio para más juegos");
        }
    }

    static void BorrarJuego(JuegoMesa[] juegos, ref int numJuegos)
    {
        int juegoBorrar;
        string confirmar;

        if (numJuegos > 0)
        {
            for (int i = 0; i < numJuegos; i++)
            {
                Console.WriteLine("{0}. {1}", i + 1,
                    juegos[i].Nombre);
            }
            Console.WriteLine();
            Console.WriteLine("Escriba el número del juego que desea borrar");
            juegoBorrar = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            if (juegoBorrar >= 1 && juegoBorrar <= numJuegos)
            {
                Console.WriteLine("Confirme la operación escribiendo s");
                confirmar = Console.ReadLine();
                Console.Clear();

                if (confirmar.ToUpper() == "S")
                {
                    for (int i = juegoBorrar; i < numJuegos; i++)
                    {
                        juegos[i - 1] = juegos[i];
                    }
                    numJuegos--;
                    Console.WriteLine("Borrado realizado correctamente");
                }
                else
                {
                    Console.WriteLine("Operación cancelada");
                }
            }
            else
            {
                Console.WriteLine("Número no válido");
            }
        }
        else
        {
            Console.WriteLine("No hay juegos que borrar");
        }
    }

    static int JuegoMasCaro(JuegoMesa[] juegos, ref int numJuegos)
    {
        int posCaro = 0;
        float juegoCaro;

        if (numJuegos > 0)
        {
            juegoCaro = juegos[0].Precio;
            for (int i = 0; i < numJuegos; i++)
            {
                if (juegos[i].Precio > juegoCaro)
                {
                    juegoCaro = juegos[i].Precio;
                    posCaro = i;
                }
            }
        }
        else
        {
            posCaro = -1;
        }
        return posCaro;
    }

    static void JuegosPorTitulo(JuegoMesa[] juegos, ref int numJuegos)
    {
        string tituloBuscar;
        bool encontrado;

        if (numJuegos > 0)
        {
            encontrado = false;
            Console.WriteLine("Introduzca el nombre del juego que busca");
            tituloBuscar = Console.ReadLine();
            Console.Clear();
            for (int i = 0; i < numJuegos; i++)
            {
                if (juegos[i].Nombre.ToUpper().Contains(tituloBuscar.ToUpper()
                    .ToString()))
                {
                    encontrado = true;
                    juegos[i].Mostrar();
                    Console.WriteLine();
                }
            }
            if (encontrado == false)
            {
                Console.WriteLine("No se encuentran resultados");
            }
        }
        else
        {
            Console.WriteLine("No hay juegos que mostrar");
        }
    }

    static int MostrarMenu()
    {
        int opcion;

        Console.WriteLine((int)menu.nuevo + ". Nuevo juego");
        Console.WriteLine((int)menu.borrar + ". Borrar juego");
        Console.WriteLine((int)menu.caro + ". Juego más caro");
        Console.WriteLine((int)menu.titulo + ". Juegos por título");
        Console.WriteLine((int)menu.salir + ". Salir");

        Console.WriteLine();
        Console.WriteLine("Elija una opción");
        opcion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        return opcion;
    }

    static void enter()
    {
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
        Console.Clear();
    }

    static void Main()
    {
        JuegoMesa[] juegos = new JuegoMesa[30];
        int opcion, numJuegos = 0, posCaro = 0;
        do
        {
            opcion = MostrarMenu();

            switch ((menu)opcion)
            {
                case menu.nuevo:
                    NuevoJuego(juegos, ref numJuegos);
                    enter();
                    break;

                case menu.borrar:
                    BorrarJuego(juegos, ref numJuegos);
                    enter();
                    break;

                case menu.caro:
                    posCaro = JuegoMasCaro(juegos, ref numJuegos);
                    if (posCaro != -1)
                    {
                        Console.WriteLine("Juego más caro");
                        Console.WriteLine();
                        juegos[posCaro].Mostrar();
                    }
                    else
                    {
                        Console.WriteLine("No hay juegos");

                    }
                    enter();
                    break;

                case menu.titulo:
                    JuegosPorTitulo(juegos, ref numJuegos);
                    enter();
                    break;

                case menu.salir:
                    Console.WriteLine("Has seleccionado salir");
                    enter();
                    break;

                default:
                    Console.WriteLine("Valor incorrecto");
                    enter();
                    break;
            }
        }
        while (opcion != 0);
    }
}
