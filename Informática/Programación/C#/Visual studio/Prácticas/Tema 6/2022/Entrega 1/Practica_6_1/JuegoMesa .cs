﻿//Clase juego de mesa
class JuegoMesa
{
    private string nombre;
    private float precio;
    private int edadMin;
    private int jugMin;
    private int jugMax;

    //Constructor parametrizado
    public JuegoMesa(string nombre, float precio, int edadMin,
        int jugMin, int jugMax)
    {
        Nombre = nombre;

        if (precio >= 0) 
        {
            Precio = precio;
        }
        else
        {
            Precio = 0;
        }

        if (edadMin >= 0)
        {
            EdadMin = edadMin;
        }
        else
        {
            EdadMin = 0;
        }

        if (jugMin >= 0)
        {
            JugMin = jugMin;
        }
        else
        {
            JugMin = 0;
        }

        if (jugMax >= 0)
        {
            JugMax = jugMax;
        }
        else
        {
            JugMax = 0;
        }
    }



    //Getter y setters
    public string Nombre
    {
        get 
        {
            return nombre;
        }
        set
        {
            nombre = value;
        }
    }
    public float Precio
    {
        get
        {
            return precio;
        }
        set
        {
            precio = value;
        }
    }
    public int EdadMin
    {
        get
        {
            return edadMin;
        }
        set
        {
            edadMin = value;
        }
    }
    public int JugMin
    {
        get
        {
            return jugMin;
        }
        set
        {
            jugMin = value;
        }
    }
    public int JugMax
    {
        get
        {
            return jugMax;
        }
        set
        {
            jugMax = value;
        }
    }

    //métodos

    public void Mostrar()
    {
        Console.WriteLine("{0} ({1} años, {2}-{3} jugadores):" +
            " {4} euros",nombre,edadMin,jugMin,jugMax
            ,precio.ToString("N2"));
    }
}

