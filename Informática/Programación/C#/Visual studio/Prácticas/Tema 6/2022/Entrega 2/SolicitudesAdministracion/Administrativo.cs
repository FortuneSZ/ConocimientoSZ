﻿class Administrativo
{
    private string nombre;
    private string DNI;
    private int tlf;

    public string Nombre
    {
        get
        {
            return nombre; 
        }
        set
        {
            nombre = value; 
        }
    }
    public string dni
    {
        get
        {
            return DNI;
        }
        set
        {
            DNI = value;
        }
    }
    public int Tlf
    {
        get
        {
            return tlf;
        }
        set
        {
            tlf = value;
        }
    }
    public Administrativo(string nombre, string dni, int tlf)
    {
        Nombre = nombre;
        DNI = dni;
        Tlf = tlf;
    }
    public override string ToString()
    {
        return "Administrativo de nombre " + nombre + ". con DNI "
            + DNI + " y teléfono " + tlf;
    }
}

