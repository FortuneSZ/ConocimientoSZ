﻿//Clase solicitud
class Solicitud
{
    protected string id;
    protected string fecha;
    protected Administrativo administrativo;

    public string ID
    {
        get 
        {
            return id; 
        }
        set 
        {
            id = value; 
        }
    }
    public string Fecha
    {
        get
        {
            return fecha;
        }
        set
        {
            fecha = value;
        }
    }
    public Administrativo ADministrativo
    {
        get
        {
            return administrativo;
        }
        set
        {
            administrativo = value; 
        }
    }
    public Solicitud(string id, string fecha, Administrativo administrativo)
    {
        ID = id;
        Fecha = fecha;
        ADministrativo = administrativo;
    }
    public override string ToString()
    {
        return "ID " + id + " Realizada el " + fecha + ". " + administrativo.ToString();
    }
}

