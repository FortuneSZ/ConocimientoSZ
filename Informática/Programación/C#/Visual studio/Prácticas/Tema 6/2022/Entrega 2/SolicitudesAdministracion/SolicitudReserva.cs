﻿//Clase de solicitud de reserva
class SolicitudReserva:Solicitud
{
    private string espacio;
    private string fechaRes;
    private string hora;
    private int horas;

    public string Espacio
    {
        get 
        {
            return espacio; 
        }
        set
        {
            espacio = value; 
        }
    }
    public string FechaRes
    {
        get
        {
            return fechaRes;
        }
        set 
        {
            fechaRes = value;
        }
    }
    public string Hora
    {
        get
        {
            return hora;
        }
        set
        {
            hora = value;
        }
    }
    public int Horas
    {
        get
        {
            return horas;
        }
        set
        {
            horas = value;
        }
    }
    public SolicitudReserva(string id, string fecha, string espacio,
        string fechaRes, string hora, int horas,
        Administrativo administrativo):base(id,fecha,administrativo)
    {
        Espacio= espacio;
        FechaRes = fechaRes;
        Hora = hora;
        Horas= horas;
    }
    public override string ToString()
    {
        return "Solicitud reserva. " + base.ToString() + 
            "\nReservando el espacio " + espacio +
            " el día " + fecha + ".\na las " + hora +
            " horas. durante " + horas + " horas"
            + ". Atendido por " + administrativo.dni; 
    }
}

