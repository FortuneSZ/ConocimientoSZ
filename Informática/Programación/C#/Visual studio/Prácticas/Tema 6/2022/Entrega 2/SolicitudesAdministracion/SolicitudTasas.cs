﻿//Clase de solicitud de tasas
class SolicitudTasas:Solicitud
{
    private string concepto;
    private float importe;

    public string Concepto
    {
        get
        {
            return concepto;
        }
        set
        {
            concepto = value;
        }
    }
    public float Importe
    {
        get
        {
            return importe;
        }
        set 
        {
            if (value > 0)
            {
                importe = value;
            }
            else
            {
                throw new Exception("El importe debe ser positivo");
            }
             
        }
    }
    public SolicitudTasas(string id, string fecha ,
        string concepto, float importe,Administrativo administrativo)
        :base(id,fecha,administrativo)
    {
        Concepto = concepto;
        Importe = importe;
        
    }
    public override string ToString()
    {
        return "Pago tasas. " +base.ToString() + "Con concepto " 
            + concepto + ". con un importe a pagar de " + importe
            + " euros" + ". Atendido por " + administrativo.dni;
    }
}

