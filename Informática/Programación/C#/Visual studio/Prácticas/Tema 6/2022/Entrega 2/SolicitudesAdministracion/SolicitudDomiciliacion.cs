﻿//Clase solicitud de domiciliación
class SolicitudDomiciliacion:Solicitud
{
    private string numCuenta;

    public string NumCuenta
    {
        get 
        {
            return numCuenta; 
        }
        set 
        {
            bool correcto = true;
            for (int i = 0;i < value.Length;i++)
            {
                if (value.Length < 20)
                {
                    correcto = false;
                }
                if (value[i] > '9' || value[i] < '0')
                {
                    correcto = false;
                }
            }
            if (correcto) 
            {
                numCuenta = value;
            }
            else
            {
                throw new Exception("Número de cuenta con formato" +
                    " incorrecto");
            }
            
        }
    }
    public SolicitudDomiciliacion(string id, string fecha,
        string numCuenta, Administrativo administrativo)
        :base(id,fecha,administrativo) 
    {
        NumCuenta = numCuenta;
    }

    public override string ToString()
    {
        return "Cambio Domiciliación. " + base.ToString() +
            "Núm cuenta " + numCuenta + ". Atendido por "
            + administrativo.dni;
    }

}

