﻿/*Este programa gestiona un programa bancario que permite:
 
 Crear una nueva solicitud, que puede ser una domiciliación,
 tasas o una reserva.

 Mostrar todas las solicitudes de un administrativo concreto.

 Mostrar el listado de solicitudes, diferenciado por colores.

 Mostrar total de tasas pendientes a pagar.

 Salir.*/
class SolicitudesAdministracion
{
    enum menu{salir,nuevo,administrativo,lista,pendientes };

    enum solicitud {Domiciliación = 1, Tasas, Reserva };

    static bool ComprobarId(Solicitud[] solicitudes, string id,
        int contador)
    {
        bool valido = true;

        if (contador > 0)
        {
            for (int i = 0; i < contador; i++)
            {
                if (id == solicitudes[i].ID)
                {
                    throw new Exception("ID ya registrado");
                }
            }
        } 
        return valido;
    }

    static Administrativo ComprobarDni(Administrativo[]
        administrativos,string dni)
    {
        bool encontrado = false;
        Administrativo a = new Administrativo("","",0);

        for (int i = 0; i < administrativos.Length; i++) 
        {
            if (dni == administrativos[i].dni)
            {
                encontrado= true;
                a = administrativos[i];
            }
        }
        if (encontrado == false) 
        {
            throw new Exception("No se ha encontrado " +
                "un administrativo con el DNI indicado");
        }
        return a;
    }

    static Administrativo Administrativo(Administrativo[]
        administrativos)
    {
        string dni;
        Administrativo a;

        Console.Clear();
        Console.WriteLine("Introduzca el dni del" +
            " administrativo que le atiende");
        dni = Console.ReadLine();
        a = ComprobarDni(administrativos, dni);

        return a;
    }
    static void CompletarSolicitud()
    {
        Console.WriteLine("Solicitud completada exitósamente");
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
        Console.Clear();
    }

    static void Tasas(Solicitud[] solicitudes, string id,
        string fecha, Administrativo administrativo, ref int contador)
    {
        string concepto;
        int importe;

        Console.Clear();
        Console.WriteLine("Introduzca el concepto");
        concepto = Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Introduzca el importe");
        importe = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        SolicitudTasas t = new SolicitudTasas
            (id, fecha, concepto, importe, administrativo);
        solicitudes[contador] = t;
        contador++;
        CompletarSolicitud();
    }

    static void Reserva(Solicitud[] solicitudes, string id,
       string fecha, Administrativo administrativo, ref int contador)
    {
        string espacio, fechares, hora;
        int horas;

        Console.Clear();
        Console.WriteLine("Introduzca el espacio que" +
            " desea reservar");
        espacio= Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Introduzca la fecha de la reserva");
        fechares= Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Introduzca la hora de la reserva");
        hora= Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Introduzca las horas de la reserva");
        horas = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        SolicitudReserva r = new SolicitudReserva
            (id,fecha,espacio,fechares,hora,horas,administrativo);
        solicitudes[contador] = r;
        contador++;
        CompletarSolicitud();
    }

    static void Domiciliacion(Solicitud[] solicitudes,string id,
        string fecha,Administrativo administrativo,ref int contador)
    {
        string numcuenta;

        Console.Clear();
        Console.WriteLine("Escriba su número de cuenta");
        numcuenta= Console.ReadLine();
        Console.Clear();
        SolicitudDomiciliacion s = new SolicitudDomiciliacion
            (id,fecha,numcuenta,administrativo);
        solicitudes[contador] = s;
        contador++;
        CompletarSolicitud();
    }

    static void Solicitudes(Solicitud[] solicitudes,
        Administrativo[] administrativos, int opcion,
        ref int contador)
    {
        string id, fecha;
        Administrativo administrativo;

        Console.Clear();
        try
        {
            Console.WriteLine("Introduzca la id de la solicitud");
            id = Console.ReadLine();
            if (solicitudes.Length >= 1)
            {
                ComprobarId(solicitudes, id, contador);
            }
            Console.Clear();
            Console.WriteLine("Introduzca la fecha de la solicitud");
            fecha= Console.ReadLine();
            administrativo = Administrativo(administrativos);

            switch (opcion) 
            {
                case 1:
                    Domiciliacion(solicitudes,id,
                        fecha, administrativo,ref contador);
                    break;

                case 2:
                    Tasas(solicitudes, id,
                        fecha, administrativo, ref contador);
                    break;

                case 3:
                    Reserva(solicitudes, id,
                        fecha, administrativo, ref contador);
                    break;
            }
        }
        catch (Exception ex)
        {
            Console.Clear();
            Console.WriteLine("Datos incorrectos para la solicitud");
            Console.WriteLine();
            Console.WriteLine(ex.Message);
            Console.WriteLine();
            Console.WriteLine("Pulse enter para salir");
            Console.ReadLine();
            Console.Clear();
        }
        

    }
    static void Nuevo(Solicitud[] solicitudes,
        Administrativo[] administrativos,ref int contador)
    {
        int opcion;

        Console.Clear();
        Console.WriteLine("¿Que tipo de solicitud quiere realizar?");
        Console.WriteLine();
        Console.WriteLine((int)solicitud.Domiciliación + ". "
            + solicitud.Domiciliación);
        Console.WriteLine();
        Console.WriteLine((int)solicitud.Tasas + ". "
            + solicitud.Tasas);
        Console.WriteLine();
        Console.WriteLine((int)solicitud.Reserva + ". "
            + solicitud.Reserva);
        Console.WriteLine();
        opcion = Convert.ToInt32(Console.ReadLine());

        switch((solicitud)opcion)
        {
            case solicitud.Domiciliación:
                Solicitudes(solicitudes,administrativos, opcion,
                   ref contador);
                break;

            case solicitud.Tasas:
                Solicitudes(solicitudes, administrativos, opcion,
                  ref  contador);
                break;

            case solicitud.Reserva:
                Solicitudes(solicitudes, administrativos, opcion,
                   ref contador);
                break;
        }
    }

    static void Pendientes(Solicitud[] solicitudes, int contador)
    {
        float sumatorio = 0;
        bool encontrado = false;

        for (int i = 0; i < contador; i++) 
        {
            if (solicitudes[i] is SolicitudTasas) 
            { 
                encontrado = true;
                sumatorio += ((SolicitudTasas)solicitudes[i]).Importe;
            }
        }
        if (encontrado == true) 
        {
            Console.WriteLine("La suma total de las tasas es {0}",
                sumatorio);
        }
        else
        {
            Console.WriteLine("No hay tasas disponibles");
        }
    }

    static void Lista(Solicitud[] solicitudes, int contador)
    {
        if (contador > 0) 
        {
            for (int i = 0; i < contador; i++)
            {
                if (solicitudes[i] is SolicitudDomiciliacion)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(solicitudes[i]);
                }
                if (solicitudes[i] is SolicitudTasas)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(solicitudes[i]);
                }
                if (solicitudes[i] is SolicitudReserva)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(solicitudes[i]);
                }
            }
        }
        else
        {
            Console.Clear();
            Console.WriteLine("No hay solicitudes");
        }
        
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
    }

    static void PorAdministrativo(Administrativo[] administrativos,
        Solicitud[] solicitudes, int contador)
    {
        bool encontrado = false;
        string administrativo;

        Console.Clear();
        Console.WriteLine("Introduzca el administrativo del cual quiere saber" +
            " sus solicitudes");
        administrativo= Console.ReadLine();

        for (int i = 0; i < administrativos.Length; i++) 
        {
            if (administrativo == administrativos[i].dni)
            {
                encontrado = true;  
            }
        }
        if (encontrado == true) 
        {
            encontrado = false;
            for (int i = 0; i < contador; i++)
            {
                if (administrativo.Equals(solicitudes[i].ADministrativo.dni))
                {
                    encontrado= true;
                    Console.WriteLine(solicitudes[i]);
                }
            }

            if (encontrado == false) 
            {
                Console.Clear();
                Console.WriteLine("El administrativo" +
                    " seleccionado no tiene solicitudes a su cargo");
            }
        }
        else
        {
            Console.Clear();
            Console.WriteLine("No se ha encontrado un" +
                " administrativo con el DNI indicado");
        }
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
    }

    static void Menu()
    {

        Console.WriteLine((int)menu.nuevo + ". Nueva solicitud");
        Console.WriteLine();
        Console.WriteLine((int)menu.administrativo
            + ". Solicitudes por administrativo");
        Console.WriteLine();
        Console.WriteLine((int)menu.lista
            + ". Listado de solicitudes");
        Console.WriteLine();
        Console.WriteLine((int)menu.pendientes
            + ". Total pendiente de pago");
        Console.WriteLine();
        Console.WriteLine((int)menu.salir
            + ". Salir");
        Console.WriteLine();
        Console.WriteLine("Seleccione la opción que desea");

    }

    static void Main()
    {
        Administrativo[] administrativos = new Administrativo[5];
        administrativos[0] = new Administrativo
            ("Fran","48773966M",654145568);
        administrativos[1] = new Administrativo
            ("Sandra","48751296L",612478562);
        administrativos[2] = new Administrativo
            ("Paco","12435176A",697431856);
        administrativos[3] = new Administrativo
            ("Pedro","75645124T",649871365);
        administrativos[4] = new Administrativo
            ("Andrea","27941638A",645132876);
        Solicitud[] solicitudes = new Solicitud[20];
        try
        {
            int opcion, contador = 0;

            do
            {
                Console.Clear();
                Menu();

                opcion = Convert.ToInt32(Console.ReadLine());

                switch ((menu)opcion)
                {
                    case menu.nuevo:
                        Nuevo(solicitudes, administrativos, ref contador);
                        break;

                    case menu.administrativo:
                        PorAdministrativo(administrativos,solicitudes,contador);
                        break;

                    case menu.lista:
                        Lista(solicitudes,contador);
                        break;

                    case menu.pendientes:
                        Pendientes(solicitudes, contador);
                        break;
                }
            }
            while ((menu)opcion != menu.salir);
            
        }
        catch (Exception ex) 
        {
            Console.WriteLine(ex.Message);
        }
        
    }
}
