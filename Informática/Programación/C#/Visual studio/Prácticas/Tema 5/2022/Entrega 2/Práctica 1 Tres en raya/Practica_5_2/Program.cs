﻿/*Este programa consiste en el juego del 3 en raya, donde se irán turnando el
jugador y el ordenador hasta que bien alguno de ellos alinee 3 o, si se llena
el tablero y nadie gana, se dará por empate.

El jugador que empieza es aleatorio.*/
class Practica_5_2
{
    static string[,] CrearTablero()
    {
        int filas =3, columnas = 3;
        string[,] tablero = new string[filas,columnas];

        for(int i = 0; i < filas;i++)
        {
            for(int x = 0; x < columnas;x++)
            {
                tablero[i, x] = " ";
            }
        }
        return tablero;
    }

    static void DibujarTablero(string[,] tablero)
    {
        int contador = 0;
        for (int y = 0; y< 3; y++)
        {
            for (int i = 0; i < 13; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
            for (int i = 0; i < 3; i++)
            {
                Console.Write("| ");
                if (tablero[contador, i] == "O")
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                if (tablero[contador, i] == "X")
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.Write(tablero[contador, i] + " ");
                Console.ResetColor();
            }
            contador++;
            Console.Write("|");
            Console.WriteLine();
        }
        for (int i = 0; i < 13; i++)
        {
            Console.Write("-");
        }
    }

    static void TurnoUsuario(string[,] tablero)
    {
        bool valido;
        int fila, columna;

        do
        {
            Console.Clear();
            DibujarTablero(tablero);
            Console.WriteLine();
            Console.WriteLine();

            valido = true;
            Console.WriteLine("Elija la posición");
            Console.WriteLine();

            Console.Write("Fila:");
            fila = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Columna:");
            columna = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            if (tablero[fila - 1,columna - 1] != " ")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Posición invalida");
                Console.ResetColor();
                Console.WriteLine();
                Console.ReadLine();
                valido = false;
                
            }
            else
            {
                tablero[fila-1, columna-1] = "O";
            }
        }
        while (!valido);
    }

    static void TurnoOrdenador(string[,] tablero) 
    {
        int fila, columna;
        bool valido = true;
        Random r= new Random();

        do
        {
            valido = true;
            fila = r.Next(0, 3);
            columna = r.Next(0, 3);

            if (tablero[fila, columna] != " ")
            {
                valido = false;
            }
            else
            {
                tablero[fila, columna] = "X";
            }
        }
        while (!valido);
        
    }

    static int ComprobarTablero(string[,] tablero,int turno)                             
    {
        int resultado = 0;
        string simbolo = "";
        bool filas = false, columnas = false, oblicuas = false;

        filas = ComprobarFilas(tablero,out simbolo);
        if (!filas)
        {
            columnas = ComprobarColumnas(tablero, out simbolo);
            if (!columnas)
            {
                oblicuas = ComprobarOblicua(tablero, out simbolo);
            }
        }
        if (filas == true || columnas == true || oblicuas == true)
        {
            if (simbolo == "O")
            {
                resultado = 1;
            }
            if (simbolo == "X")
            {
                resultado = 2;
            }
        }
        if (turno == 8 && filas != true && columnas != true 
            && oblicuas != true)
        {
            resultado = -1;
        }
        return resultado;
    }
    static bool ComprobarFilas(string[,] tablero,out string simbolo)
    {
        simbolo= "";
        int contadorO = 0, contadorX = 0;
        bool filavic = false;
        for (int i = 0; i < tablero.GetLength(0);i++)
        {
            contadorO = 0;
            contadorX = 0;
            for (int x = 0; x < tablero.GetLength(1); x++)
            {
                if (tablero[i,x] == "O")
                {
                    contadorO++;
                }
                if (tablero[i,x] == "X")
                {
                    contadorX++;
                }
            }
            if (contadorO == 3)
            {
                filavic = true;
                simbolo = "O";
            }
            if (contadorX == 3)
            {
                filavic = true;
                simbolo = "X";
            }
        }
        return filavic;
    }

    static bool ComprobarColumnas(string[,] tablero, out string simbolo)
    {
        simbolo = "";
        int contadorO = 0, contadorX = 0;
        bool coluvic = false;
        for (int i = 0; i < tablero.GetLength(0); i++)
        {
            contadorO = 0;
            contadorX = 0;
            for (int x = 0; x < tablero.GetLength(1); x++)
            {
                if (tablero[x, i] == "O")
                {
                    contadorO++;
                }
                if (tablero[x, i] == "X")
                {
                    contadorX++;
                }
            }
            if (contadorO == 3)
            {
                coluvic = true;
                simbolo = "O";
            }
            if (contadorX == 3)
            {
                coluvic = true;
                simbolo = "X";
            }
        }
        return coluvic;
    }

    static bool ComprobarOblicua(string[,] tablero, out string simbolo)
    {
        simbolo = "";
        int contadorO = 0, contadorX = 0, contador = 2;
        bool oblivic = false;

        for (int i = 0; i < tablero.GetLength(0); i++)
        {

            if (tablero[i, i] == "O")
            {
                contadorO++;
            }
            if (tablero[i, i] == "X")
            {
                contadorX++;
            }

            if (contadorO == 3)
            {
                oblivic = true;
                simbolo = "O";
            }
            if (contadorX == 3)
            {
                oblivic = true;
                simbolo = "X";
            }
        }

        if (oblivic != true)
        {
            contadorO = 0;
            contadorX = 0;

            for (int i = 0; i < tablero.GetLength(0); i++)
            { 
                if (tablero[i, contador] == "O")
                {
                    contadorO++;
                }
                if (tablero[i, contador] == "X")
                {
                    contadorX++;
                }

                contador--;

                if (contadorO == 3)
                {
                    oblivic = true;
                    simbolo = "O";
                }
                if (contadorX == 3)
                {
                    oblivic = true;
                    simbolo = "X";
                }
            }
        }

        return oblivic;
    }

    static bool Validar(int validacion)
    {
        bool victoria = false;
        if (validacion == 1)
        {
            Console.Clear();
            Console.WriteLine("Gana usuario");
            victoria = true;
        }
        else if (validacion == 2)
        {
            Console.Clear();
            Console.WriteLine("Gana ordenador");
            victoria = true;
        }
        else if (validacion == -1)
        {
            Console.Clear();
            Console.WriteLine("Empate");
        }
        return victoria; 
    }

    static void realizarTurnoUsuario(string[,] tablero,ref int turnos,
        ref int validacion, ref bool victoria)
    {
        TurnoUsuario(tablero);
        validacion = ComprobarTablero(tablero, turnos);
        victoria = Validar(validacion);
        turnos++;
    }

    static void realizarTurnoOrdenador(string[,] tablero, ref int turnos,
        ref int validacion, ref bool victoria)
    {
        TurnoOrdenador(tablero);
        DibujarTablero(tablero);
        validacion = ComprobarTablero(tablero, turnos);
        victoria = Validar(validacion);
        turnos++;
    }

    static void IACompleja(string[,] tablero)
    {

    }
    static void Main()
    {
        int turnos = 0, validacion = 0 ,primero;
        bool victoria = false, dificil = false;
        string confirmar;
        string[,] tablero = new string[3,3];
        Random r = new Random();

        tablero = CrearTablero();
        primero = r.Next(0,2);

        if (primero == 0) 
        {
            realizarTurnoOrdenador(tablero, ref turnos, ref validacion,
                ref victoria);
        }
        
        do
        {
            if (victoria != true && turnos != 9)
            {
                
                realizarTurnoUsuario(tablero, ref turnos, ref validacion, 
                    ref victoria);
            }
            
            Console.Clear();

            if (victoria != true && turnos != 9)
            {
                realizarTurnoOrdenador(tablero, ref turnos, ref validacion,
                ref victoria);
            }
        }
        while (!victoria && turnos != 9);
        Console.Clear();
        Validar(validacion);
        DibujarTablero(tablero);
    }
}
