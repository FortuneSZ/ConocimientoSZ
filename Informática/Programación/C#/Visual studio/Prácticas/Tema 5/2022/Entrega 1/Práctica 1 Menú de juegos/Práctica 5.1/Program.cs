﻿/*Este programa consiste en un menú de opciones que gestiona un array
sobredimensionado de juegos de mesa empleando funciones.

 Las opciones son las siguientes:

 1.Nuevo juego: añade un nuevo juego al final del array, en caso de que no esté
 lleno o el precio sea negativo.

 2. Borrar juego: Borra un juego de una posición específica,el usuario debe
 confirmar la operación, en caso de no confirmarse se cancelará.

 3. Juego más caro: Muestra el juego más caro registrado en el array.

 4.Juegos por tipo: Pide al usuario que introduzca el tipo de juego que desea
 buscar, si existe mostrará los juegos de ese tipo si hay, o mostrará que no
 hay títulos de ese tipo de ser el caso.

 5. Juegos por título: Pide al usuario que introduzca un título y si algúno del
 array contiene ese título lo mostrará por pantalla, mostrará que no hay
 ninguno de ser el caso.

 0. Salir: Finaliza el programa.*/
using System;
class Practica_4_2
{
    enum tipo { ROL, INFANTIL, PUZZLE, OTROS }
    enum menu { salir, nuevo, borrar, caro, tipo, titulo }
    struct infoBasica
    {
        public byte edadMin;
        public byte jugMin;
        public byte jugMax;
    }

    struct juego
    {
        public string nombre;
        public infoBasica infor;
        public float precio;
        public tipo tipoJuego;
    }

    static void NuevoJuego(juego[] juegos,ref int numJuegos)
    {
        juego nuevoJuego;
        if (numJuegos != juegos.Length)
        {
            Console.WriteLine("Introduce el nombre del juego");
            nuevoJuego.nombre = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Introduce la edad mínima del juego");
            nuevoJuego.infor.edadMin = Convert.ToByte
                (Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Introduce la cantidad mínima de jugadores");
            nuevoJuego.infor.jugMin = Convert.ToByte
                (Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Introduce la cantidad máxima de jugadores");
            nuevoJuego.infor.jugMax = Convert.ToByte(Console.ReadLine());
            Console.Clear();
            try
            {
                Console.WriteLine("Introduce el precio del juego");
                nuevoJuego.precio = Convert.ToSingle(Console.ReadLine());
                Console.Clear();

                Console.WriteLine("Introduce de que tipo es el juego");
                Console.WriteLine("0=ROL,1=INFANTIL,2=PUZZLE,3=OTROS");
                nuevoJuego.tipoJuego = (tipo)Convert.ToInt32
                    (Console.ReadLine());
                Console.Clear();

                if (nuevoJuego.precio > 0)
                {
                    juegos[numJuegos] = nuevoJuego;
                    numJuegos++;
                    Console.WriteLine("Registro realizado correctamente");
                }
                else 
                {
                    Console.WriteLine("El precio no es válido");
                }    
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("Debes introducir un número");
            }
        }
        else
        {
            Console.WriteLine("No hay espacio para más juegos");
        }
    }

    static void BorrarJuego(juego[] juegos,ref int numJuegos)
    {
        int juegoBorrar;
        char confirmar;

        if (numJuegos > 0)
        {
            for (int i = 0; i < numJuegos; i++)
            {
                Console.WriteLine("{0}. {1}", i + 1,
                    juegos[i].nombre);
            }
            Console.WriteLine();
            Console.WriteLine("Escriba el número del juego que desea borrar");
            juegoBorrar = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            if (juegoBorrar >= 1 && juegoBorrar <= numJuegos)
            {
                Console.WriteLine("Confirme la operación escribiendo s");
                confirmar = Convert.ToChar(Console.ReadLine());
                Console.Clear();

                if (confirmar == 's' || confirmar == 'S')
                {
                    for (int i = juegoBorrar; i < numJuegos; i++)
                    {
                        juegos[i - 1] = juegos[i];
                    }
                    numJuegos--;
                    Console.WriteLine("Borrado realizado correctamente");
                }
                else
                {
                    Console.WriteLine("Operación cancelada");
                }
            }
            else
            {
                Console.WriteLine("Número no válido");
            }
        }
        else
        {
            Console.WriteLine("No hay juegos que borrar");
        }
    }

    static int JuegoMasCaro(juego[] juegos, ref int numJuegos)
    {
        int posCaro = 0;
        float juegoCaro;

        if (numJuegos > 0)
        {
            juegoCaro = juegos[0].precio;
            for (int i = 0; i < numJuegos; i++)
            {
                if (juegos[i].precio > juegoCaro)
                {
                    juegoCaro = juegos[i].precio;
                    posCaro = i;
                }
            }          
        }
        else
        {
            posCaro = -1;
        }
        return posCaro;
    }

    static void JuegosPorTipo(juego[] juegos, ref int numJuegos)
    {
        int tipoBuscar;
        bool encontrado = false;

        if (numJuegos > 0)
        {
            encontrado = false;
            Console.WriteLine("Introduce el tipo de juego a" +
                " buscar");
            Console.WriteLine("0=ROL,1=INFANTIL,2=PUZZLE,3=OTROS");
            tipoBuscar = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            foreach (tipo d in Enum.GetValues(typeof(tipo)))
            {
                if (tipoBuscar == (int)d)
                {
                    encontrado = true;
                }
            }
            if (encontrado == true)
            {
                encontrado = false;
                for (int i = 0; i < numJuegos; i++)
                {
                    if ((tipo)tipoBuscar == juegos[i].tipoJuego)
                    {
                        encontrado = true;
                        MostrarJuegos(juegos, i);
                    }
                }
                if (encontrado != true)
                {
                    Console.WriteLine("No se encuentran juegos del tipo " +
                        "indicado");
                }
            }
            else
            {
                Console.WriteLine("Tipo inválido");
            }
        }
        else
        {
            Console.WriteLine("No hay juegos que mostrar");
        }
    }

    static void JuegosPorTitulo(juego[] juegos, ref int numJuegos)
    {
        string tituloBuscar;
        bool encontrado;

        if (numJuegos > 0)
        {
            encontrado = false;
            Console.WriteLine("Introduzca el nombre del juego que busca");
            tituloBuscar = Console.ReadLine();
            Console.Clear();
            for (int i = 0; i < numJuegos; i++)
            {
                if (juegos[i].nombre.ToUpper().Contains(tituloBuscar.ToUpper()
                    .ToString()))
                {
                    encontrado = true;
                    MostrarJuegos(juegos, i);
                    Console.WriteLine();
                }
            }
            if (encontrado == false)
            {
                Console.WriteLine("No se encuentran resultados");
            }
        }
        else
        {
            Console.WriteLine("No hay juegos que mostrar");
        }
    }

    static void MostrarJuegos(juego[] juegos, int pos)
    {
        Console.WriteLine("Nombre: {0}",juegos[pos].nombre);
        Console.WriteLine("Precio: {0} euros",juegos[pos].precio);
        Console.WriteLine("Tipo de juego: {0}",juegos[pos].tipoJuego);
        Console.WriteLine("Edad mínima: {0} años",juegos[pos].infor.edadMin);
        Console.WriteLine("Jugadores mínimos: {0}",juegos[pos].infor.jugMin);
        Console.WriteLine("Jugadores máximos: {0}",juegos[pos].infor.jugMax);
    }

    static int MostrarMenu()
    {
        int opcion;

        Console.WriteLine((int)menu.nuevo + ". Nuevo juego");
        Console.WriteLine((int)menu.borrar + ". Borrar juego");
        Console.WriteLine((int)menu.caro + ". Juego más caro");
        Console.WriteLine((int)menu.tipo + ". Juegos por tipo");
        Console.WriteLine((int)menu.titulo + ". Juegos por título");
        Console.WriteLine((int)menu.salir + ". Salir");

        Console.WriteLine();
        Console.WriteLine("Elija una opción");
        opcion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        return opcion;
    }

    static void enter()
    {
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
        Console.Clear();
    }

    static void NuevaReceta()
    {

    }

    static void Main()
    {
        juego[] juegos = new juego[30];
        int opcion, numJuegos = 0, posCaro = 0;
        do
        {
            opcion = MostrarMenu();

            switch ((menu)opcion)
            {
                case menu.nuevo:
                    NuevoJuego(juegos,ref numJuegos);
                    enter();
                    break;

                case menu.borrar:
                    BorrarJuego(juegos,ref numJuegos);
                    enter();
                    break;

                case menu.caro:
                    posCaro = JuegoMasCaro(juegos, ref numJuegos);
                    if (posCaro != -1)
                    {
                        Console.WriteLine("Juego más caro");
                        Console.WriteLine();
                        MostrarJuegos(juegos, posCaro);
                    }
                    else
                    {
                        Console.WriteLine("No hay juegos");
                        
                    }
                    enter();
                    break;

                case menu.tipo:
                    JuegosPorTipo(juegos, ref numJuegos);
                    enter();
                    break;

                case menu.titulo:
                    JuegosPorTitulo(juegos, ref numJuegos);
                    enter();
                    break;

                case menu.salir:
                    Console.WriteLine("Has seleccionado salir");
                    enter();
                    break;

                default:
                    Console.WriteLine("Valor incorrecto");
                    enter();
                    break;
            }
        }
        while (opcion != 0);
    }
}
