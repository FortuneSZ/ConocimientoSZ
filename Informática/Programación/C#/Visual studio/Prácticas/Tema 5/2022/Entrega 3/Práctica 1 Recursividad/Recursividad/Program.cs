﻿/*Este programa se compone de dos funciones recursvias, una suma 
desde 1 hasta el número especificado por el usuario, y la otra 
dice si todas las letras de la palabra especificada por el usuario
están ordenadas ascendentemente*/
class Recursividad
{
    static int SumaNumeros(int numero)
    {
        if (numero == 1)
        {
            Console.Write("{0} = ", numero);
            return 1;
        }
        else
        {
            Console.Write("{0} + ", numero);
            return numero + SumaNumeros(numero - 1);
        }
    }

    static bool PalabraOrdenada(string palabra)
    {
        if (palabra.Length > 1)
        {
            if (palabra[0] < palabra[1])
            {
                return PalabraOrdenada(palabra.Substring(1));
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    static void Main()
    {
        int numero; 
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Función SumaNumeros");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(SumaNumeros(5));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(SumaNumeros(10));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(SumaNumeros(2));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Función PalabraOrdenada");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("abocado: "+ PalabraOrdenada("abocado"));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("hilo: " + PalabraOrdenada("hilo"));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("falta: " + PalabraOrdenada("falta"));
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("----------------------------------------"+
            "----");
        Console.ResetColor();
    }
}