﻿/*Este programa recibe ciertos números como argumentos que recibe 
el main y empleando expresiones lambda mostrará por pantalla lineas
de asteriscos del tamaño especificado por cada número recibido*/
class Grafico
{
    static void Asteriscos(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Array.ForEach(args, n => Console.WriteLine(n));
        Console.ResetColor();
    }
    static void CrearCadena(int[] numeros, string[] args)
    {
        int contador = 0;
        foreach (int n in numeros)
        {
            args[contador] = new String('*', n);
            contador++;
        }
        Asteriscos(args);
    }
    static void Main(string[] args)
    {
        if (args.Length != 0)
        {
            try
            {
                int[] numeros = Array.ConvertAll(args, n =>
                Convert.ToInt32(n));

                CrearCadena(numeros, args);

            }
            catch(Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Datos de entrada inválidos");
                Console.ResetColor();
            }
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Datos de entrada inválidos");
            Console.ResetColor();
        }
        
    }
}
