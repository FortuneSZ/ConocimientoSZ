﻿/*Este programa pide al usuario que rellene los datos de una canción,los cuales
posteriormente mostrará por pantalla*/
class Ejercicio_04b_01
{
    struct Cancion
    {
        public string artista;
        public string titulo;
        public int duracion;
        public int tamaño;
    }
    static void Main()
    {
        Cancion c;

        Console.WriteLine("Escribe el nombre del artista");
        c.artista = Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Escribe el título de la canción");
        c.titulo = Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Escribe la duración de la canción en segundos");
        c.duracion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        Console.WriteLine("Escribe el peso de la canción en KB");
        c.tamaño = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Nombre del artista: {0}",c.artista);
        Console.WriteLine("Título de la canción: {0}",c.titulo);
        Console.WriteLine("Duración: {0}s",c.duracion);
        Console.WriteLine("Peso: {0}KB",c.tamaño);

    }
}
