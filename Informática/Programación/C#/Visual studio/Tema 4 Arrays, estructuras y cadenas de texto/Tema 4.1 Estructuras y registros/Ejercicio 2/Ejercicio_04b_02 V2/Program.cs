﻿/*Este programa mostrará un menú en el cual se pueden añadir canciones,mostrar
todas las canciones, y buscar canciones de un artista especificado por el 
usuario hasta que este selecccione la opción de salir*/
class Ejercicio_04b_01
{
    struct Cancion
    {
        public string artista;
        public string titulo;
        public int duracion;
        public int tamaño;
    }
    static void Main()
    {
        Cancion[] c = new Cancion[100];
        int opcion, cantidad = 0;
        string ArtBuscar;

        do
        {
            Console.WriteLine("1. Añadir una canción al final");
            Console.WriteLine("2. Mostrar los títulos de todas las canciones");
            Console.WriteLine("3. Buscar canciones de un artista determinado");
            Console.WriteLine("4. Salir");
            opcion = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            switch (opcion)
            {
                case 1:
                    Cancion can;

                    Console.WriteLine("Escribe el nombre del artista");
                    can.artista = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Escribe el título de la canción");
                    can.titulo = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Escribe la duración de la canción en " +
                        "segundos");
                    can.duracion = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine("Escribe el peso de la canción en KB");
                    can.tamaño = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();

                    c[cantidad] = can;
                    cantidad++;
                    break;
                case 2:
                    Console.WriteLine("Artista - Titulo - Duración - " +
                            "Peso");
                    Console.WriteLine();
                    for (int i = 0; i < cantidad; i++)
                    {
                        Console.Write("{0} - ", c[i].artista);
                        Console.Write("{0} - ", c[i].titulo);
                        Console.Write("{0} - sec", c[i].duracion);
                        Console.WriteLine("{0} KB", c[i].tamaño);

                        Console.WriteLine();
                    }
                    Console.WriteLine();
                    Console.WriteLine("Pulsa enter para continuar");
                    Console.ReadLine();
                    Console.Clear();
                    break;
                case 3:
                    Console.WriteLine("Introduce el nombre del artista");
                    ArtBuscar = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Canciones de {0}", ArtBuscar);
                    Console.WriteLine();
                    for (int i = 0; i <= cantidad; i++)
                    {
                        if (c[i].artista == ArtBuscar)
                        {
                            Console.WriteLine(c[i].titulo);
                        }
                    }
                    Console.WriteLine();
                    Console.WriteLine("Pulsa enter para continuar");
                    Console.ReadLine();
                    Console.Clear();
                    break;
            }
        }
        while (opcion != 4);
        
    }
}