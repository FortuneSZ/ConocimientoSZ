﻿/*Este programa pide al usuario que rellene un array de 5 nombres,y, una vez
rellenado,le irá pidiendo nombres y le dirá si está o no en el array,hasta que
el usuario ponga "fin"*/
class Ejercicio_04a_06
{
    static void Main()
    {
        string[] nombres = new string[5];
        string nombre;
        bool encontrado;

        for (int i = 0; i < nombres.Length; i++)
        {
            Console.WriteLine("Introduce un nombre");
            nombre = Console.ReadLine();

            nombres[i] = nombre;
            Console.Clear();
        }
        nombre = "";
        do
        {
            Console.Clear();
            encontrado = false;
            Console.WriteLine("Introduce un nombre y te diré si está en el" +
                " array");
            nombre = Console.ReadLine();
            Console.Clear();

            for (int i = 0; i < nombres.Length; i++)
            {
                if (nombre == nombres[i])
                {
                    encontrado = true;
                }
            }
            if (encontrado == true)
            {
                Console.WriteLine("Está en el array");
            }
            else
            {
                Console.WriteLine("No está en el array");
            }
            Console.WriteLine();
            Console.WriteLine("Pulse enter para continuar");
            Console.ReadLine();
        }
        while (nombre != "fin");
    }
}
