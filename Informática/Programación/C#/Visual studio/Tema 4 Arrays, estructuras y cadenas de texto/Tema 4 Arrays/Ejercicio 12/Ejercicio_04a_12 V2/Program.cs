﻿/*Este programa crea un array bidimensionals,el usuario lo rellena y una vez
hecho esto el programa le dirá cual es el número más alto de cada fila*/
using System;

class Ejercicio_04a_11
{
    static void Main()
    {
        int[][] tabla = new int[2][];
        int max, tamFila;

        for (int i = 0; i < tabla.Length; i++)
        {
            Console.WriteLine("Indica el tamaño de la fila {0}", i + 1);
            tamFila = Convert.ToInt32(Console.ReadLine());
            tabla[i] = new int[tamFila];
            Console.Clear();
            for (int j = 0; j < tabla[i].Length; j++)
            {
                Console.WriteLine("escribe el {0} numero de la {1} fila",
                    j + 1, i + 1);
                tabla[i] [j] = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
            }
        }

        for (int i = 0; i < tabla.Length; i++)
        {
            max = tabla[i] [0];
            for (int j = 1; j < tabla[i].Length; j++)
            {
                if (tabla[i] [j] > max)
                {
                    max = tabla[i] [j];
                }
            }
            Console.WriteLine("El mayor de la fila {0} es: {1}", i + 1, max);
            max = 0;
        }
    }
}
