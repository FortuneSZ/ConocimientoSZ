﻿/*Este programa le pide al usuario que rellene un array de 10 nombres,
posteriormente le pedirá un nombre a buscar,y este dirá si se encuentra en
el array y en que posición*/
class Ejercicio_04a_08
{
    static void Main()
    {
        string[] nombres = new string[10];
        string nombreBus;

        for (int i = 0; i < nombres.Length; i++)
        {
            Console.WriteLine("Escribe un nombre");
            nombres[i] = Console.ReadLine();
            Console.Clear();
        }

        Console.WriteLine("Escribe un nombre a buscar");
        nombreBus = Console.ReadLine();
        Console.Clear();

        for (int i = 0; i < nombres.Length; i++)
        {
            if (nombreBus == nombres[i])
            {
                Console.WriteLine("Encontrado en la posición {0}",i+1);
            }
        }
    }
}
