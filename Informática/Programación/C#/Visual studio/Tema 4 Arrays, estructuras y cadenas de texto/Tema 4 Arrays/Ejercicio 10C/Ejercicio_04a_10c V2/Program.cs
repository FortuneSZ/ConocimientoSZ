﻿/*Este programa creará un array de tamaño 100 y rellenará algunas entradas
con juegos,luego le pedirá al usuario que introduzca un juego,este será
puesto al final del arry,luego pedirá otro juego,que será puesto en la posición
3 del array,luego pedirá al usuario una posición de la cual borrar un título
y posteriormente, se mostrarán todos los títulos del array*/
class Ejercicio_04a_10a
{
    static void Main()
    {
        string[] juegos = new string[100];
        juegos[0] = "Mortal Kombat 11";
        juegos[1] = "The binding of isaac";
        juegos[2] = "Dragon ball xenoverse";
        juegos[3] = "God of war ragnarok";
        int cantidad = 4, posicion = 3;


        if (cantidad != 100)
        {
            Console.WriteLine("Introduce el título de un juego");
            juegos[cantidad] = Console.ReadLine();
            cantidad++;
        }
        Console.Clear();

        if (cantidad != 100)
        {
            Console.WriteLine("Introduce otro juego");
            for (int i = cantidad; i > posicion - 1; i--)
            {
                juegos[i] = juegos[i - 1];
            }
            juegos[posicion - 1] = Console.ReadLine();
            cantidad++;
        }
        Console.Clear();

        Console.WriteLine("Introduce la posición de la cual borrar un juego");
        posicion = Convert.ToInt32(Console.ReadLine());
        for (int i = posicion - 1; i < cantidad - 1; i++)
        {
            juegos[i] = juegos[i + 1];
        }
        cantidad--;

        Console.WriteLine("Listado completo de juegos");
        Console.WriteLine();
        for (int i = 0; i < cantidad; i++)
        {
            Console.WriteLine(juegos[i].ToString());
        }
    }
}
