﻿/*Este programa creará un array de tamaño 100 y rellenará algunas entradas
con juegos,mostrará un menú de opciones,que permite añadir juegos al final,
en una posición concreta,borrar un juego de una posición concreta o salir,
el usuario elegirá hasta que seleccione salir*/
class Ejercicio_04a_10a
{
    static void Main()
    {
        string[] juegos = new string[100];
        juegos[0] = "Mortal Kombat 11";
        juegos[1] = "The binding of isaac";
        juegos[2] = "Dragon ball xenoverse";
        juegos[3] = "God of war ragnarok";
        int cantidad = 4, posicion, eleccion;


        do
        {
            Console.WriteLine("1. Nuevo videojuego al final");
            Console.WriteLine("2. Nuevo videojuego en una posición");
            Console.WriteLine("3. Borrar videojuego");
            Console.WriteLine("4. Salir");

            Console.WriteLine();
            Console.WriteLine("Elije una opción del menú");
            eleccion = Convert.ToInt32(Console.ReadLine());

            switch (eleccion)
            {
                case 1:
                    if (cantidad != juegos.Length)
                    {
                        Console.Clear();
                        Console.WriteLine("Introduce el título de un juego");
                        juegos[cantidad] = Console.ReadLine();
                        cantidad++;
                    }
                    break;

                case 2:
                    Console.Clear();
                    Console.WriteLine("Introduce la posición en la que " +
                        "insertar el juego");
                    posicion = Convert.ToInt32(Console.ReadLine());
                    for (int i = cantidad; i > posicion - 1; i--)
                    {
                        juegos[i] = juegos[i - 1];
                    }
                    Console.Clear();
                    Console.WriteLine("Introduce el título del juego");
                    juegos[posicion - 1] = Console.ReadLine();
                    cantidad++;
                    break;

                case 3:
                    Console.Clear();
                    Console.WriteLine("Introduce la posición de la cual " +
                        "borrar un juego");
                    posicion = Convert.ToInt32(Console.ReadLine());
                    for (int i = posicion - 1; i < cantidad - 1; i++)
                    {
                        juegos[i] = juegos[i + 1];
                    }
                    cantidad--;
                    break;

                default:
                    Console.WriteLine("Opción incorrecta");
                    break;
            }
            Console.Clear();
            Console.WriteLine("Listado completo de juegos");
            Console.WriteLine();
            for (int i = 0; i < cantidad; i++)
            {
                Console.WriteLine(juegos[i].ToString());
            }
            Console.WriteLine();
            Console.WriteLine("Pulse enter para continuar");
            Console.ReadLine();
            Console.Clear();

        }
        while (eleccion != 4);
    }
}

