﻿/*Este programa pedirá al usuario que introduzca 10 números reales,los cuales
se guardarán en un array,posteriormente se mostrará cual es el menor de ellos
y se dirá la posición en la que se encuentra*/
class Ejercicio_04a_09
{
    static void Main()
    {
        double[] numeros = new double[10];
        double minimo = 0;
        int posmin = 0;
        for (int i = 0; i < numeros.Length; i++)
        {
            Console.WriteLine("Escribe un número real");
            numeros[i] = Convert.ToDouble(Console.ReadLine());
            Console.Clear();
        }

        for (int i = 0; i < numeros.Length; i++)
        {
            if (minimo == 0)
            {
                minimo = numeros[i];
                posmin = i + 1;
            }

            if (numeros[i] < minimo)
            {
                minimo = numeros[i];
                posmin = i + 1;
            }
        }

        Console.WriteLine("El menor número es {0}, que se encuentra en la " +
            "posición {1}",minimo,posmin);


    }
}
