﻿/*Este programa almacena los días de cada mes en un array,le pide al usuario el
número del mes en el que se encuentra y el día,entonces el programa le dirá
cuantos días faltan hasta fin de año*/
class Ejercicio_04a_05
{
    static void Main()
    {
        int[] meses = new int[12] 
        { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int eleccion,numdia,diasfin;

        Console.WriteLine("Introduzca el número del mes");
        eleccion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        Console.WriteLine("Introduzca el número del día");
        numdia = Convert.ToInt32(Console.ReadLine());
        diasfin = meses[eleccion-1]-numdia;
        for (int i = eleccion; i < meses.Length; i++)
        {
            diasfin += meses[i];
        }
        Console.Clear();
        Console.WriteLine(diasfin + " días hasta fin de año");
    }
}
