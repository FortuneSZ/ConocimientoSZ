﻿/*Este programa pide al usuario 6 números enteros,los almacena en un array y
los muestra en orden inverso*/
class Ejercicio_04a_03
{
    static void Main()
    {
        int[] num = new int[6];
        int numero;

        for (int i = 0; i < num.Length; i++)
        {
            Console.Clear();
            Console.WriteLine("Introduce un número entero");
            numero = Convert.ToInt32(Console.ReadLine());
            num[i] = numero;
        }
        Console.Clear();
        Console.WriteLine("Números en orden inverso");
        for (int i = num.Length-1; i >= 0; i--)
        {
            Console.WriteLine(num[i]);
        }
    }
}
