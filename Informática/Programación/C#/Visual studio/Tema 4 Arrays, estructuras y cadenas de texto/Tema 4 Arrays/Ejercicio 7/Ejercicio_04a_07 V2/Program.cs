﻿/*Este programa creará un array de 100 posiciones,y pedirá al usuario que lo
 rellene con nombres,este lo hara repetidamente hasta que el usuario presione
enter sin escribir nada,una vez esto,mostrará el número de nombres en el array
y también mostrará los mismos*/
class Ejercicio_04a_07
{
    static void Main()
    {
        string[] nombres = new string[100];
        string nombre;
        int i = 0,contador = 0;

        do
        {
            Console.WriteLine("Escribe un nombre");
            nombre = Console.ReadLine();
            if (nombre != "")
            {
                nombres[i] = nombre;
                i++;
                contador++;
            }
            Console.Clear();
        }
        while (nombre != "");

        Console.WriteLine("Hay {0} nombres almacenados",contador);

        Console.WriteLine();
        for (i = 0; i < contador; i++)
        {
            Console.WriteLine(nombres[i]);
        }
    }
}
