﻿/*Este programa pide al usuario 4 números tipo float y los almacena en un 
array*/
class Ejercicio_04a_01
{
    static void Main()
    {
        float[] numeros = new float[4];
        float suma=0, media=0;

        for(int i = 0; i < numeros.Length; i++)
        {
            Console.Clear();
            Console.WriteLine("Escribe un número decimal");
            numeros[i] = Convert.ToSingle(Console.ReadLine());
            suma += numeros[i];
        }
        Console.Clear();
        media = suma / 4;
        Console.WriteLine("La media es {0}",media);
    }
}
