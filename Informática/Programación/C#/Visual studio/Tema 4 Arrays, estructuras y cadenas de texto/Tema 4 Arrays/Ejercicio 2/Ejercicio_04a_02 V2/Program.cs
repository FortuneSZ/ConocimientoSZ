﻿/*Este programa almacena lá cantidad de días de cada mes,le pide al usuario que
introduzca un número del 1 al 12,y le dirá cuantos díoas tiene dicho mes*/
class Ejercicio_04a_02
{
    static void Main()
    {
        int[] meses = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int eleccion;

        Console.WriteLine("Introduzca el número del mes");
        eleccion = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Ese mes tiene " + meses[eleccion-1] + " días");
    }
}
