﻿/*Este programa crea un array del tamaño dicho por el usuario,el usuario lo
 rellena,calcula su media,y dice que número de los introducidos está por encima
de la media*/
class Ejercicio_04a_04
{
    static void Main()
    {
        double[] numeros;
        double numero, media=0;
        int tamanyo;

        Console.WriteLine("Introduce el número de números que deseas " +
            "introducir");
        tamanyo = Convert.ToInt32(Console.ReadLine());
        numeros = new double[tamanyo];

        for (int i = 0; i < tamanyo; i++)
        {
            Console.WriteLine("Escriba un número real");
            numero = Convert.ToDouble(Console.ReadLine());
            numeros[i] = numero;
            media += numero;
        }
        media /= tamanyo;

        Console.WriteLine("la media es {0}", media);
        for (int i = 0; i <= tamanyo - 1; i++)
        {

            if (numeros[i] > media)
            {
                Console.Write(numeros[i] + " ");
            }
        }
    }
}
