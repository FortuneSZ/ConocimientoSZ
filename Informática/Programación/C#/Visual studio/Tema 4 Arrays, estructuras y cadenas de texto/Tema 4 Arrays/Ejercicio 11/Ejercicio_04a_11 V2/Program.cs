﻿/*Este programa crea un array bidimensional de 2 filas y 10 columnas,el usuario
lo rellena y una vez hecho esto el programa le dirá cual es el número más alto
de cada fila*/
using System;

class Ejercicio_04a_11
{
    static void Main()
    {
        int[,] tabla = new int[2, 10];
        int max;

        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                Console.WriteLine("escribe el {0} de la {1} fila", j + 1, i + 1);
                tabla[i, j] = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
            }
        }

        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            max = tabla[i, 0];
            for (int j = 1; j < tabla.GetLength(1); j++)
            {
                if (tabla[i, j] > max)
                {
                    max = tabla[i, j];
                }
            }
            Console.WriteLine("El mayor de la fila {0} es: {1}", i + 1, max);
            max = 0;
        }
    }
}
