﻿/*Este programa pide al usuario 10 palabras y luego los ordena de menor
a mayor y los muestra por pantalla*/
class Ejercicio_04d_03
{
    static void Main()
    {
        string[] textos = new string[10];

        for (int i = 0; i < textos.Length; i++)
        {
            Console.WriteLine("Escribe un número float");
            textos[i] = Console.ReadLine();
            Console.Clear();
        }

        Array.Sort(textos);


        Console.WriteLine("Array ordenado");
        Console.WriteLine();

        for (int i = 0; i < textos.Length; i++)
        {
            Console.WriteLine(textos
                [i]);
        }
    }
}
