﻿/*Este programa pide al usuario 10 números float y luego los ordena de mayor
a menor y los muestra por pantalla*/
class Ejercicio_04d_02
{
    static void Main()
    {
        float[] numeros = new float[10];

        for (int i = 0; i < numeros.Length; i++)
        {
            Console.WriteLine("Escribe un número float");
            numeros[i] = Convert.ToSingle(Console.ReadLine());
            Console.Clear();
        }

        for (int i = 0; i < numeros.Length; i++)
        {
            for (int j = 0; j < numeros.Length - i - 1; j++)
            {
                if (numeros[j] < numeros[j + 1])
                {
                    float auxiliar = numeros[j];
                    numeros[j] = numeros[j + 1];
                    numeros[j + 1] = auxiliar;
                }
            }
        }
        

        Console.WriteLine("Array ordenado");
        Console.WriteLine();

        for (int i = 0; i < numeros.Length; i++)
        {
            Console.WriteLine(numeros[i]);
        }
    }
}
