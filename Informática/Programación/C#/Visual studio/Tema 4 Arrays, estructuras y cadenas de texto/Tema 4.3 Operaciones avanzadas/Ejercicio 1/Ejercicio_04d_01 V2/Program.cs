﻿/*Este programa pide al usuario números separados por espacios y los suma
empleando foreach*/

class Ejercicio_04d_01
{
    static void Main()
    {
        string numeros;
        int suma = 0;

        Console.WriteLine("Introduce números separados por espacios");
        numeros = Console.ReadLine();
        string [] num2 = numeros.Split(" ");

        foreach (string num in num2)
        {
            if (num != " ")
            {
              
                suma += Convert.ToInt32(num);
            }
            
        }

        Console.WriteLine("La suma de todos los números es {0}",suma);
    }
}
