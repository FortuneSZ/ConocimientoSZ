﻿/*Este programa determina la cantidad de combinaciones de asignaturas optativas
posibles en base a un enum*/
class Ejercicio_04d_05
{
    enum asig {Informatica,Frances,Electronica,Psicologia,Ingles }
    static void Main()
    {
        int combinaciones = 0;
        foreach (asig opt1 in Enum.GetValues(typeof(asig)))
        {
            foreach (asig opt2 in Enum.GetValues(typeof(asig)))
            {
                if (opt1 != opt2)
                {
                    Console.WriteLine("{0} - {1}",opt1,opt2);
                    combinaciones++;
                }
            }
        }
        Console.WriteLine();
        Console.WriteLine("Hay {0} combinaciones de asignaturas posibles",
            combinaciones);
    }
}
