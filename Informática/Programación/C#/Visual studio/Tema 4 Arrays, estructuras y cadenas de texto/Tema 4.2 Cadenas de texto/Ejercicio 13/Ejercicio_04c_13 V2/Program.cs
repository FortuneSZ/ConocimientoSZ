﻿/*"este programa pide una frase al usuario y la modifica cambiando todas las
vocales por 'o' minúscula*/
using System.Text;
class Ejercicio_04c_13
{
    static void Main()
    {
        StringBuilder fraseModificable;
        string fraseOriginal;

        Console.WriteLine("Escribe una frase");
        fraseOriginal = Console.ReadLine();
        fraseModificable = new StringBuilder(fraseOriginal);
        Console.Clear();
        

        for (int i = 0; i < fraseModificable.Length; i++)
        {
            if (fraseModificable[i] == 'a' || fraseModificable[i] == 'e' ||
                fraseModificable[i] == 'i' || fraseModificable[i] == 'u')
            {
                fraseModificable[i] = 'o';
            }
        }
        Console.WriteLine("Frase original: {0}",fraseOriginal);
        Console.WriteLine("Frase modificada: {0}", fraseModificable);
    }
}
