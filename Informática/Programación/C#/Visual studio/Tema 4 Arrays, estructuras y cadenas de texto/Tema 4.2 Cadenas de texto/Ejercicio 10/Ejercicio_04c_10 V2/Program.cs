﻿/*Este programa pedirá una frase al usuario y la mostrará en orden iversos*/
class Ejercicio_04c_10
{
    static void Main()
    {
        string frase;

        Console.WriteLine("Escribe una frase");
        frase = Console.ReadLine();

        string[] partes = frase.Split(" ");

        for (int i = partes.Length-1; i >= 0; i--)
        {
            Console.Write(partes[i]);
            if (i - 1 >= 0)
            {
                Console.Write(" ");
            }
        }
        
    }
}
