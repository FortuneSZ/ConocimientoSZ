﻿/*Este programa pedirá al usuario que rellene un array de 10 frases,y luego
reemplazará Java por CSharp en cada frase que se encuentre y mostrará la lista
cambiada por pantalla
*/
class Ejercicio_04c_08
{
    static void Main()
    {
        string[] frases = new string[10];
        string frase, frasebuscar;

        for (int i = 0; i < frases.Length; i++)
        {
            Console.WriteLine("Escribe una frase");
            frase = Console.ReadLine();
            frases[i] = frase;
            Console.Clear();
        }

        for (int i = 0; i < frases.Length; i++)
        {
            frases[i] = frases[i].Replace("Java", "CSharp");
        }

        for (int i = 0; i < frases.Length; i++)
        {
            Console.WriteLine(frases[i]);
        }
    }
}
