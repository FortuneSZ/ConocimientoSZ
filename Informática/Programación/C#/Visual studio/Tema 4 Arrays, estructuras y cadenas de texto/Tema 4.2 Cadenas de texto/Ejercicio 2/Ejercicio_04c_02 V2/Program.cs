﻿/*Este programa le pedirá al usuario su nombre y le dirá cual es su inicial*/
class Ejercicio_04c_02
{
    static void Main()
    {
        string nombre;

        Console.WriteLine("Escribe tu nombre");
        nombre = Console.ReadLine();
        Console.Clear();

        Console.WriteLine("La inicial de tu nombre es {0}", nombre[0]);
    }
}
