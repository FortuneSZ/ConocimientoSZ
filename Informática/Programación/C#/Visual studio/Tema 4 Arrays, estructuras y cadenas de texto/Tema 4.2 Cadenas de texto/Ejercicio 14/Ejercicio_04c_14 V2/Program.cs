﻿/*Este programa pedirá al usuario una palabra,la cual ocultará empleando 
guiones (-) y le pedirá repetidamente letras al usuario,y si estas se
encuentran en la palabra original se destaparán,se le pedirá esto hasta que
adivine la palabra o hasta que los intentos bajen a 0*/
using System.Text;

class Ejercicio_04c_14
{
    static void Main()
    {
        string palabra;
        StringBuilder palabraOculta = new StringBuilder("");
        int intentos = 8;
        char letra;

        Console.WriteLine("Escribe una palabra");
        palabra = Console.ReadLine();
        Console.Clear();

        for (int i = 0;i < palabra.Length;i++)
        {
            if (palabra[i] != ' ')
            {
                palabraOculta.Append('-');
            }
            else
            {
                palabraOculta.Append(' ');
            }
            
        }

        do
        {
            Console.WriteLine(palabraOculta);
            Console.WriteLine();
            Console.WriteLine("Intentos restantes: {0}",intentos);
            Console.WriteLine();
            Console.WriteLine("Introduce una letra");
            letra = Convert.ToChar(Console.ReadLine());
            Console.Clear();

            for (int i = 0;i < palabra.Length;i++)
            {
                if (palabra[i] == letra)
                {
                    palabraOculta[i] = letra;
                }
            }

            if (palabra.CompareTo(palabraOculta.ToString()) > 0)
            {
                intentos--;

                if (intentos == 0)
                {
                    Console.WriteLine("Fallaste,más suerte la próxima vez");
                }
            }
            else
            {
                Console.WriteLine("Acertaste la palabra!");
                Console.WriteLine();
                Console.WriteLine("La palabra era {0}",palabra);
            }
        }
        while(palabra != palabraOculta.ToString() && intentos > 0);
        
    }
}
