﻿/*Este programa le pedirá al usuario su nombre y mostrará cada letra del
mismo separada por guiones*/
class Ejercicio_04c_02
{
    static void Main()
    {
        string nombre;

        Console.WriteLine("Escribe tu nombre");
        nombre = Console.ReadLine();
        Console.Clear();

        for (int i = 0; i < nombre.Length; i++)
        {
            Console.Write(nombre[i]);
            if (i != nombre.Length - 1)
            {
                Console.Write(" - ");
            }
        }
        
    }
}
