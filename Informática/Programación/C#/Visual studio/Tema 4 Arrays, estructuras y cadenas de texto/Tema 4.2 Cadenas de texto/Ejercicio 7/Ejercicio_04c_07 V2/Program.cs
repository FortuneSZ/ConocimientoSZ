﻿/*Este programa pedirá al usuario que rellene un array de 10 frases,y luego
de eso le pedirá repetidamente una palabra a buscar y dirá en que posiciones
la frase contiene dicha palabra,todo esto hasta que el usuario introduzca fin
*/
class Ejercicio_04c_07
{
    static void Main()
    {
        string[] frases = new string[10];
        string frase, frasebuscar;
        bool encontrado = false;

        for (int i = 0; i < frases.Length; i++)
        {
            Console.WriteLine("Escribe una frase");
            frase = Console.ReadLine();
            frases[i] = frase;
            Console.Clear();
        }

        do
        {
            encontrado = false;
            Console.WriteLine("Intorduzca un tecto a buscar");
            frasebuscar = Console.ReadLine();

            for (int i = 0; i < frases.Length; i++)
            {
                if (frases[i].ToUpper().Contains(frasebuscar.ToUpper()))
                {
                    Console.WriteLine("Encontrado en la posición {0}", i + 1);
                    encontrado = true;
                }
            }

            if (!encontrado)
            {
                Console.WriteLine("No hay resultados");
            }

            if (frasebuscar != "fin")
            {
                Console.WriteLine();
                Console.WriteLine("Pulse enter para continuar");
                Console.ReadLine();
                Console.Clear();
            }
        }
        while (frasebuscar != "fin");
    }
}
