﻿/*Este programa pide dos números y un operando al usuario,y realizará y 
mostrará por pantalla la operación marcada por el operando*/
class Ejercicio_04c_01
{
    static void Main()
    {
        int num1, num2, resultado;
        string operando;

        Console.WriteLine("Escribe el primer número");
        num1 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        Console.WriteLine("Escribe el segundo número");
        num2 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        Console.WriteLine("Escribe la operación a realizar");
        operando = Console.ReadLine();
        Console.Clear();

        switch (operando)
        {
            case "+":
            case "suma":
            resultado = num1 + num2;
            Console.WriteLine("La suma de {0} y {1} es {2}",num1,num2,
                resultado);
                break;

            case "-":
            case "resta":
                resultado = num1 - num2;
                Console.WriteLine("La resta de {0} y {1} es {2}", num1, num2,
                    resultado);
                break;
            case "*":
            case "x":
            case "X":
            case "multiplicacion":
                resultado = num1 * num2;
                Console.WriteLine("La multiplicacion de {0} y {1} es {2}",
                    num1, num2, resultado);
                break;

            case "/":
            case "division":
                resultado = num1 / num2;
                Console.WriteLine("La division de {0} y {1} es {2}", num1, num2,
                    resultado);
                break;

        }
    }
}