﻿/*Este programa pide al usuario que introduzca una frase y la mostrará
subrayada en toda parte donde haya algún caracter*/
class Ejercicio_04c_15
{
    static void Main()
    {
        string frase;

        Console.WriteLine("Introduce una frase");
        frase = Console.ReadLine();
        Console.Clear();

        Console.WriteLine(frase);
        for (int i = 0; i < frase.Length; i++)
        {
            if (frase[i] != ' ')
            {
                Console.Write('-');
            }
            else
            {
                Console.Write(' ');
            }
        }
    }
}
