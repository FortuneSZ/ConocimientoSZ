﻿/*Este programa elimina los espacios innecesarios en la frase introducida por
el usuario*/
class Ejercicio_04c_09
{
    static void Main()
    {
        string frase;

        Console.WriteLine("Escribe una frase");
        frase = Console.ReadLine();

        do
        {
            frase = frase.Replace("  ", " ");

        }
        while (frase.Contains("  "));
        Console.Clear();

        Console.WriteLine("Frase corregida");
        Console.WriteLine(frase);


    }
}
