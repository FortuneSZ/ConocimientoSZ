﻿/*Este programa pide al usuario que introduzca en una misma línea números
separados por un espacio,y luego muestra por pantalla su suma*/
class Ejercicio_04c_11
{
    static void Main()
    {
        string texto;
        int suma = 0;

        Console.WriteLine("Escribe números separados por espacios");
        texto = Console.ReadLine();
        Console.Clear();

        string[] numeros = texto.Split(" ");

        for (int i = 0; i < numeros.Length; i++)
        {
            suma += Convert.ToInt32(numeros[i]);
        }

        Console.WriteLine("La suma de los números es {0}",suma);
    }   
}
