﻿/*Este programa pedirá al usuario que introduzca 5 frases y le dirá cual es
la mayor alfabéticamente*/
class Ejercicio_04c_12
{
    static void Main()
    {
        string[] frases = new string[5];
        string mayor="";

        for (int i = 0; i < frases.Length; i++)
        {
            Console.WriteLine("Escribe una frase");
            frases[i] = Console.ReadLine();
            Console.Clear();
        }
        mayor = frases[0];
        for (int i = 0; i < frases.Length; i++)
        {
            if ( string.Compare(frases[i], mayor, true) > 0)
            {
                mayor = frases[i];
            }
        }
        Console.WriteLine("La frase mayor es {0}",mayor);
    }
}
