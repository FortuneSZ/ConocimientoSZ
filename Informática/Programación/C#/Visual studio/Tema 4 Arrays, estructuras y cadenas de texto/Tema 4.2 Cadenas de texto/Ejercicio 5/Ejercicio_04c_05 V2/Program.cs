﻿/*Este programa le pedirá al usuario su nombre y lo mostrará en forma de
pirámide*/
class Ejercicio_04c_02
{
    static void Main()
    {
        string nombre;

        Console.WriteLine("Escribe tu nombre");
        nombre = Console.ReadLine();
        Console.Clear();

        for (int i = 0; i <= nombre.Length; i++)
        {
            Console.WriteLine(nombre.Substring(0,i));
        }
        
    }
}
