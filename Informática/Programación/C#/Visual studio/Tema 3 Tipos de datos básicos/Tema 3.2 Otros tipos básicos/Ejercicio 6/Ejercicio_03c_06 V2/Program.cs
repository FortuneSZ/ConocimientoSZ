﻿/*Este programa almacena en una variable booleana si ambos números
introducidos por el usuario son pares o no*/
using System;

class Ejercicio_03c_06
{
    static void Main()
    {
        int n1, n2;
        bool par;

        Console.WriteLine("Escribe el primer número");
        n1 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Escribe el segundo número");
        n2 = Convert.ToInt32(Console.ReadLine());

        /*
        if (n1 % 2 == 0 && n2 % 2 == 0)
        {
            par = true;
        }
        else
        {
            par = false;
        }
        */

        par = num1 % 2 == 0 && num2 % 2 == 0;

        if (par)
        {
            Console.WriteLine("Son pares los dos");
        }
        else
        {
            Console.WriteLine("No son pares los dos");
        }
    }
}
