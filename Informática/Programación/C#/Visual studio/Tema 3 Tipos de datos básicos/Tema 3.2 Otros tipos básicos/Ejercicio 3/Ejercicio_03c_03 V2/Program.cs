﻿/*Este porograma le pedirá al usuario su nombre,le saludará si se llama Juan,
 y le dirá que no lo conoce si no es así*/
class Ejercicio_03c_03
{
    static void Main()
    {
        string nombre;

        Console.WriteLine("Escribe tu nombre");
        nombre = Console.ReadLine();
        Console.Clear();

        if (nombre == "Juan")
        {
            Console.WriteLine("Hola");
        }
        else
        {
            Console.WriteLine("No te conozco");
        }
    }
}
