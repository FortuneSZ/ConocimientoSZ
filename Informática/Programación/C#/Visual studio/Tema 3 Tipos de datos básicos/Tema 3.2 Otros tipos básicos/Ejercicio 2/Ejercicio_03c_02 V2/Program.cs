﻿/*Este programa crea una palabra invirtiendo el orden de las 4 letras
 introducidas por el usuario*/
class Ejercicio_03c_02
{
    static void Main()
    {
        char letra1, letra2, letra3, letra4;

        Console.WriteLine("Escribe un caracter(4 veces)");
        letra1 = Convert.ToChar(Console.ReadLine());
        letra2 = Convert.ToChar(Console.ReadLine());
        letra3 = Convert.ToChar(Console.ReadLine());
        letra4 = Convert.ToChar(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("\"{0}{1}{2}{3}\"", letra4,letra3,letra2,letra1);
    }
}
