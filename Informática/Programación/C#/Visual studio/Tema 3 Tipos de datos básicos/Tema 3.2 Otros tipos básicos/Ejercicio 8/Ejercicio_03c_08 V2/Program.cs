﻿/*Este programa le pedirá al usuario que diga el número de un mes,y dirá que
 mes es*/
class Ejercicio_03c_08
{
    enum meses
    {
        ENERO = 1, FEBRERO, MARZO, ABRIL,
        MAYO, JUNIO, JULIO, AGOSTO,
        SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE
    };
    static void Main()
    {
        int numero;

        Console.WriteLine("Escribe un número de mes:");
        numero = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine((meses)numero);
    }
}
