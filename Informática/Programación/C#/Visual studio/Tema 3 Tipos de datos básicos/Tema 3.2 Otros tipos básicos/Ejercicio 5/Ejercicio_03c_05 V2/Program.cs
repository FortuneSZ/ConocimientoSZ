﻿/*Este programa le pide al usuario una letra y le dirá si es minúscula
 o no,junto a su código ASCII*/
class Ejercicio_03c_05
{
    static void Main()
    {
        char letra;
        bool minus;

        Console.WriteLine("Introduzca una letra");
        letra = Convert.ToChar(Console.ReadLine());
        Console.Clear();

        if (letra >= 'a' && letra <= 'z')
        {
            minus = true;
        }
        else
        {
            minus = false;
        }

        if (minus == true)
        {
            Console.WriteLine("Es una letra mínúscula");
        }
        else
        {
            Console.WriteLine("No es una letra mínúscula");
        }

        Console.WriteLine();
        Console.WriteLine("Código numérico de la letra {0}: {1}", letra,
            (int)letra);
    }
}
