﻿/*Este programa le pedirá al usuario su nombre de usuario y su contraseña,
 la contraseña la pedirá 2 veces,y si no coinciden las pedirá una y otra
vez hasta que lo haga*/
class Ejercicio_03c_04
{
    static void Main()
    {
        string login, password, intento;

        Console.WriteLine("Introduzca su usuario");
        login = Console.ReadLine();
        Console.Clear();

        do
        {
            Console.WriteLine("Introduzca su contraseña");
            password = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Introduzca de nuevo su contraseña");
            intento = Console.ReadLine();
            Console.Clear();
        }
        while(intento != password);


        Console.WriteLine("Bienvenido {0}",login);
    }
}
