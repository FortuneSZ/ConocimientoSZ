﻿/*Este programa pide al usuario dos números enteros de 5/6 cifras y los 
 multiplica*/
class Ejercicio_03a_03
{
    static void Main()
    {
        int num1, num2, resultado;
        Console.WriteLine("Introduzca dos números de 5/6 cifras");
        num1 = Convert.ToInt32(Console.ReadLine());
        num2 = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        resultado = num1 * num2;

        Console.WriteLine("El resultado de la multiplicación es {0}",resultado);
    }
}
