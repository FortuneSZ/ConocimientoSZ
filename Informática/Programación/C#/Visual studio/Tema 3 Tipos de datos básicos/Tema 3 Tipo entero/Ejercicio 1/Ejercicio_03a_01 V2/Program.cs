﻿/*Este programa pedirá al usuario 2 número de tipo byte,calculará su 
multiplicación en otra variable y la mostrará por pantalla*/
class Ejercicio_03a_01
{
    static void Main()
    {
        byte num1, num2;
        int resultado;
        Console.WriteLine("Escriba el primer número");
        num1 = Convert.ToByte(Console.ReadLine());
        Console.WriteLine("Escriba el segundo número");
        num2 = Convert.ToByte(Console.ReadLine());
        Console.Clear();

        resultado = num1*num2;
        Console.WriteLine(resultado);

    }
}
