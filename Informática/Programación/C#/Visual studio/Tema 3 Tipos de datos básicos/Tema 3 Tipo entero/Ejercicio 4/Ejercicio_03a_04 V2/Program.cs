﻿/*Este programa convierte a binario y a hexadecimal un número introducido por 
el usario*/
class Ejercicio_03a_04
{
    static void Main()
    {
        int numero;

        do
        {
            Console.WriteLine("Escriba un número entero");
            numero = Convert.ToInt32(Console.ReadLine());
            Console.Clear();


            Console.WriteLine("Número en decimal");
            Console.WriteLine(numero);
            Console.WriteLine();

            Console.WriteLine("Número en binario");
            Console.WriteLine(Convert.ToString(numero, 2));
            Console.WriteLine();

            Console.WriteLine("Número en hexadecimal");
            Console.WriteLine(Convert.ToString(numero, 16));
        }
       while (numero != 0);
    }
}
