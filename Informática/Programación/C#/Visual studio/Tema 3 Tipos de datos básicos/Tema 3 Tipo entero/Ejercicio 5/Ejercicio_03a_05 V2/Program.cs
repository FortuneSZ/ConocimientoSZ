﻿/*Este programa pedirá al usuario la cantidad de rojo,verde y azul y 
posteriomente dirá el color hexadecimal que forma dicha combinación*/
class Ejercicio_03a_05
{
    static void Main()
    {
        int R, G, B;
        Console.WriteLine("Introduzca el color rojo(de 0 a 255)");
        R = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduzca el color verde(de 0 a 255)");
        G = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduzca el color azul(de 0 a 255)");
        B = Convert.ToInt32(Console.ReadLine());

        Console.Write("El color formado es : ");
        Console.Write(Convert.ToString(R,16));
        Console.Write(Convert.ToString(G, 16));
        Console.Write(Convert.ToString(B, 16));
    }
}
