﻿/*Este programa calcula el área de un círculo y la muestra por pnatalla*/
class Ejercicio_03b_02
{
    static void Main()
    {
        int radio;
        double area;

        Console.WriteLine("Escriba el radio");
        radio = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        area = (radio * radio) * Math.PI;

        Console.WriteLine("El aréa es {0}",area);
    }
}
