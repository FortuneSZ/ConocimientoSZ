﻿/*Este programa calcula la velocidad en base a la distancia y al tiempo 
introducidos por el usuario*/
class Ejercicio_03b_03
{
    static void Main()
    {
        int km, min, horas, sec;
        float tiempo, velocidad;
        Console.WriteLine("Escriba la distancia que desea recorrer");
        km = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Escriba el tiempo que tarda en recorrerlo");
        Console.WriteLine();
        Console.WriteLine("Pulse enter para continuar");
        Console.ReadLine();
        Console.Clear();

        Console.WriteLine("Horas");
        horas = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Minutos");
        min = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        Console.WriteLine("Segundos");
        sec = Convert.ToInt32(Console.ReadLine());
        Console.Clear();

        tiempo = (horas * 3600) + (min * 60) + sec;
        km *= 1000;

        velocidad = km / tiempo;

        Console.WriteLine("La velocidad es {0} m/s",velocidad);
    }
}
