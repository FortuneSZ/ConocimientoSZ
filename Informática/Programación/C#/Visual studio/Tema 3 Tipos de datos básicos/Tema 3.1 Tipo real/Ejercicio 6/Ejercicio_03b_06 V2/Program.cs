﻿/*Este programa calcula la raiz cuadrada de un número*/
class Ejercicio_03b_06
{
    static void Main()
    {
        float numero,raiz;

        Console.WriteLine("Escriba un número");
        numero = Convert.ToSingle(Console.ReadLine());

        if (numero >= 0)
        {
            raiz = (float)Math.Sqrt(numero);

            Console.WriteLine(raiz);
        }
        else
        {
            Console.WriteLine("No se puede hacer la raiz cuadrada de un" +
                "número negativo");
        }
        


    }
}
