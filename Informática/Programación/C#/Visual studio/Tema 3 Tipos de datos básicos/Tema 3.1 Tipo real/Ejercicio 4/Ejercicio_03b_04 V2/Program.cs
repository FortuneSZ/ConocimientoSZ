﻿/*Este programa calcula una ecuación de segundo grado en base a los valores
 introducidos por el usuario*/
class Ejercicio_03b_04
{
    static void Main()
    {
        double a, b, c, resultadopos, resultadoneg, cuadrado;

        Console.WriteLine("Introduzca el valor de A");
        a = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Introduzca el valor de B");
        b = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Introduzca el valor de C");
        c = Convert.ToDouble(Console.ReadLine());

        cuadrado = (b * b) - (4 * a * c);

        if (a == 0 || cuadrado < 0)
        {
            Console.WriteLine("No hay solución");
        }
        else
        {
            resultadopos = -b + Math.Sqrt(cuadrado);
            resultadopos /= (2 * a);

            resultadoneg = -b - Math.Sqrt(b * b - 4 * a * c);
            resultadoneg /= (2 * a);

            Console.WriteLine("Los resultados de la ecuación son {0} y {1}",
                resultadopos,resultadoneg);
        }
        
    }
}
