<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">.
 <resultado>.
 	<jornada> <xsl:attribute name="numero">1</xsl:attribute>.
 		<partido>.
 			<xsl:for-each select="rusia2018/partidos">
 			<selecciones><xsl:value-of select="partido/@equi1"/>-<xsl:value-of select="partido/@equi2"/></selecciones>.
 			<resultado>5-0</resultado>
 			</xsl:for-each>.
 		</partido>.
 	</jornada>.
 </resultado>.
</xsl:template>
</xsl:stylesheet>