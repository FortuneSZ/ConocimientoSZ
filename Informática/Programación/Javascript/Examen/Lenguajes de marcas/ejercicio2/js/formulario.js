let nombres = document.getElementsByName("nombre");
let nombre = nombres[0];
let emails = document.getElementsByName("email");
let email = emails[0];
let regex = /^[a-zA-Z]{5,}@[a-zA-Z.]{3,}$/i;
let regex2 = /^[0-9]{9}$/;
let edades = document.getElementsByName("edad");
let edad = edades[0];
let tlf = document.getElementsByName("telefono");
let telefono = tlf[0];
let submit = document.getElementById("enviar");
let valores = document.getElementsByName("valoracion");
for (let valoracion of valores)
{
    this.addEventListener("click",function()
    {
        let valor = this.value;
    })
}
let valido = true;
let opiniones = new Array();

email.addEventListener("input",function()
{
    if (email.value == "")
    {
        email.setCustomValidity("El e-mail no puede estar vacío");
        valido = false;
    }
    else if (regex.test(email.value) == false)
    {
        email.setCustomValidity("Asegúrate de escribir un email válido");
        valido = false;
    }     
    else
    {
        email.setCustomValidity("");
        valido = true;
    }
})

edad.addEventListener("invalid",function()
{
    if (edad.value < 18 || edad.value > 100)
    {
        edad.setCustomValidity("Edad fuera de rango");
        valido = false;
    }
    else
    {
        this.setCustomValidity("");
        valido = true;
    }
    
})

telefono.addEventListener("input",function()
{
    if (regex2.test(telefono.value) == false)
    {
        telefono.setCustomValidity("Número de teléfono no válido");
        valido = false;
    }
    else
    {
        this.setCustomValidity("");
        valido = true;
    } 
})

submit.addEventListener("input",function()
{
    if (valido == true)
    {
        submit.onsubmit = "return false";
        opiniones[opiniones.length-1] =
        [
            {
                nombre: nombre,
                email: email,
                edad: edad,
                tlf: telefono,
                opinion: valor
            }
        ]
        let seccion = document.getElementById("opiniones");
        let p = document.createElement("p");
        p.innerHTML(opiniones[opiniones.length-1].nombre + " (" + opiniones[opiniones.length-1].email + "," +
        opiniones[opiniones.length-1].edad +" años" + "," + opiniones[opiniones.length-1].tlf + "):" + opiniones[opiniones.length-1].opinion +"/5");
        seccion.appendChild(p);
    }
    else
    {
        alert("error");
    }
})



