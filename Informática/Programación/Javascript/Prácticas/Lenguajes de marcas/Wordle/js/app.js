//Función que genera la rejilla de juego
function generarRejilla()
{
    let pos = document.getElementById("casillas");
    for (let i = 0; i < 5; i++)
    {
        let div1 = document.createElement("div");
        div1.setAttribute("id","fila"+Number(i + 1));
        for (let x = 0; x < 5; x++)
        {
            let div2 = document.createElement("div");
            div2.setAttribute("class","casilla");
            div2.setAttribute("id","casilla_"+Number(i + 1)+"_"+Number(x + 1))
            div1.appendChild(div2);
            pos.appendChild(div1);          
        }    
    }
}

function focus()
{
    for (let i = 1; i < 6; i++)
    {
        let varfocus = document.getElementById("c"+ i)
        varfocus.addEventListener("keyup",function()
        {
            document.getElementById("c"+ (i+1)).focus();
        })
    }
}
//Función que genera un número aleatório
function random(num)
{
    let numero = Math.floor(Math.random() * num);
    return numero;
}
//Función que obtiene la palabra a adivinar
function palabra()
{
    let palabras = ["PERRO", "BUENO", "CREMA", "ARBOL", "CEBRA", "BRISA"];
    let palabra = palabras[random(palabras.length)];
    return palabra;
}
//Función que colorea las casillas en base a si está en la palabra y/o en la posición correcta
function aciertos(userintro,palabraclave,casilla,i)
{
    if (userintro == palabraclave[i]) 
    {
        casilla.style.background="#538d4e";
    }
    else if (palabraclave.includes(userintro))
    {
        casilla.style.background="#b59f3a";
    }
    else
     {
         casilla.style.background="#3a3a3c";
    }
}
//Función que representa cada intento del jugador
function intento(numfilas,palabraclave)
{
    let userintro, casilla,userword = "";
    for (let i = 0; i < 5; i++)
    {
        userintro = document.getElementById("c"+ (i+1)).value.toUpperCase();
        casilla = document.getElementById("casilla_" + numfilas + "_" + (i+1));
        casilla.innerHTML = userintro;

        aciertos(userintro,palabraclave,casilla,i);
        userword += document.getElementById("c"+ (i+1)).value.toUpperCase();
        document.getElementById("c"+ (i+1)).value = "";
    }
    return userword;
}

//Función que valida la palabra introducida por el jugador
function validar()
{
    let valido = new Boolean;
    let insert;
    valido = true;

    for (let i = 0; i < 5; i++)
    {
        insert = document.getElementById("c"+ (i+1)).value;
        if(/^[a-zA-Z]$/.test(insert) == false || insert == "")
        {
            valido = false;
        }
    }   
    if (valido != true)
        {
            alert("ERROR");
        } 
    return valido;
}
//Generamos la rejilla de juego
window.addEventListener("load",function()
{
    generarRejilla();
    focus();
    let palabraclave = palabra();
    let palabrajugador;
    let numfilas = 1;
    let intentos = 6;
    let button = document.getElementById("enviar");
    let div1 = document.getElementById("c"+ 1);
    div1.focus();
    
    button.addEventListener("click",function()
    {
        if (validar() == true)
        {
            div1.focus();
            intentos--;
            palabrajugador = intento(numfilas,palabraclave);
            if (numfilas < 5) 
            {
                numfilas++;
            }

            if (palabrajugador == palabraclave)
            {
                let element = document.getElementById("formulario");
                element.style.display = "none";
                let mensaje = document.getElementById("mensaje");
                button.style.display = "none";
                mensaje.innerHTML = "Has ganado";
            }
            else if (intentos == 0)
            {
                let element = document.getElementById("formulario");
                element.style.display = "none";
                let mensaje = document.getElementById("mensaje");
                button.style.display = "none";
                mensaje.innerHTML = "Has perdido, la palabra era " + palabraclave;
            }

        }
    })
}
)
//Generamos la palabra a adivinar




