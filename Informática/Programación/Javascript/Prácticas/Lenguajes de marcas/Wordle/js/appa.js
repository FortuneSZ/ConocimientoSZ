window.addEventListener("load", function()
{
    const INTENTOS = 5;
    const LETRAS = 5;
    function getRandomInt(N)
    {
        return Math.floor(Math.random() * N);
    }

    function generarPalabra()
    {

        let palabras = new Array("PERRO", "BUENO", "CREMA", "ARBOL", 
        "CEBRA", "BRISA", "CABRA", "MURCIA");

        let palabraSecreta = palabras[getRandomInt(9)];  

        return "PERRO";
    }

    function generarRejilla()
    {
        let aux = document.createDocumentFragment();
        let aux2 = document.createDocumentFragment();

        for (let i = 0; i < INTENTOS; i++) 
        {
            // Crear elemento FILAS
            let div1 = document.createElement("div");
            // Asignación de ID
            div1.setAttribute("id", "fila" + i);

            for (let j = 0; j < LETRAS; j++) 
            {
                // Crear elemento COLUMNAS
                let div2 = document.createElement("div");
                // Asignación de ID
                div2.setAttribute("id", "casilla_" + i + "_" + j);
                // Asignación de CLASS
                div2.setAttribute("class", "casilla");
                aux.appendChild(div2); 
            }
            aux2.appendChild(div1).appendChild(aux);
        }
        document.getElementById("casillas").appendChild(aux2);
    }

    function subirPalabra(fila, palabraSecreta)
    {
        let entradaUsuario, caja;
        for (let i = 0; i < 5; i++) 
        {
            entradaUsuario = document.getElementById("c"+ (i+1)).value.toUpperCase();
            caja = document.getElementById("casilla_" + fila + "_" + i);
            caja.innerHTML = entradaUsuario;

            if (entradaUsuario == palabraSecreta[i]) 
            {
                caja.style.background="#538d4e";
            }
            else if (palabraSecreta.includes(entradaUsuario))
            {
                caja.style.background="#b59f3a";
            }
            else
            {
                caja.style.background="#3a3a3c";
            }

            document.getElementById("c"+ (i+1)).value = "";
        }
    }

    function ResetRejilla()
    {
        for (let i = 0; i < INTENTOS; i++) 
        {
            document.getElementById("c"+ (i+1)).value = "";
            for (let j = 0; j < LETRAS; j++) 
            {
                document.getElementById("casilla_" + i + "_" + j).value = "";
            }
        }
    }

    // Funcion validar
    function validarLetras()
    {
        let correcto = new Boolean(true);
        let input, contador = 0;
        for (let i = 1; i < 6; i++) 
        {
            input = document.getElementById("c"+ i).value;
            if(/^[a-zA-Z]$/.test(input) == false || input === "")
            {
                contador++;
            }
        }
        if (contador != 0) 
        {
            alert("Incorrecto.");
            correcto = false;
        }
        return correcto;  
    }

    // Generamos la palabra secreta
    let palabraSecreta = generarPalabra();

    // Generamos las casillas
    generarRejilla();

    // Comprobamos la pulsación del boton y enviamos la palabra a validar
    let boton = document.getElementById("enviar");

    let fila = 0;
    boton.addEventListener("click", function()
    {
        if (validarLetras() == true) 
        {
            subirPalabra(fila, palabraSecreta);

            if (fila < 5) 
            {
                fila++;
            }
        }
    });

});