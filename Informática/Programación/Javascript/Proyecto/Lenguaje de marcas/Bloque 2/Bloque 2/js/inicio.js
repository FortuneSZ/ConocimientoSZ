function RecipeFactory(recetasfiltradas)
{
    recetasfiltradas.sort(compare);
    for (let i = 0; i < recetasfiltradas.length; i++)
    {
        GenerarReceta(recetasfiltradas,i)
    }
}


//Función de filtro en caso de la dificultad haya sido escogida
function FiltroD(recetas,eleccion,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {   
        if (recetas[i].dificultad == eleccion)
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}

//Función de filtrado en caso de que el tiempo no sean nulo
function FiltroT(recetas,tiempo,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {
        if (tiempo < (recetas[i].coccion + recetas[i].preparacion))
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}

//Función de filtrado en caso de que el tiempo no sean nulo y la dificultad haya sido escogida
function FiltroDT(recetas,tiempo,eleccion,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {
        if (tiempo < (recetas[i].coccion + recetas[i].preparacion) && eleccion == recetas[i].dificultad)
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}

//Función de filtro en caso de que el texto de búsqueda no sea nulo
function FiltroT(recetas,textobuscar,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {
        if ((recetas[i].titulo).includes(textobuscar))
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}

//Función de filtro en caso de que el texto de búsqueda no sea nulo y la dificultad haya sido escogida
function FiltroTD(recetas,textobuscar,eleccion,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {
        if ((recetas[i].titulo).includes(textobuscar) && eleccion == recetas[i].dificultad)
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}

//Función de filtro en caso de que el texto de búsqueda y el tiempo no sean nulos y la dificultad haya sido escogida
function FiltroTDT(recetas,textobuscar,tiempo,eleccion,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {
        if ((recetas[i].titulo).includes(textobuscar) &&
        tiempo < (recetas[i].coccion + recetas[i].preparacion) && eleccion == recetas[i].dificultad)
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}
//Función de filtro en caso de que el texto de búsqueda y el tiempo no sean nulos y la dificultad no haya sido escogida
function FiltroTT(recetas,textobuscar,tiempo,recetasfiltradas)
{
    let x = 0;
    for (let i = 0; i < recetas.length; i++)
    {
        if (tiempo < (recetas[i].coccion + recetas[i].preparacion) && (recetas[i].titulo).includes(textobuscar))
        {
            recetasfiltradas[x] = recetas[i];
            x++;
        }
    }
    RecipeFactory(recetasfiltradas);
}

//Función que elimina las recetas en pantalla
function BorrarRecetas()
{
    let elem = document.getElementsByClassName("row");
    let div1 = elem[0];
    let recetas1 = document.getElementsByClassName("col-sm-6 p-2")
    let recetas2 = document.getElementsByClassName("col-sm-5 p-2")
    do
    {
        if (recetas1.length != 0)
        {
            let receta = recetas1[0];
            div1.removeChild(receta);
        }
        if (recetas2.length != 0)
        {
            let receta2 = recetas2[0];
            div1.removeChild(receta2);    
        }
        
    }
    while(recetas1.length != 0 || recetas2.length != 0);
}

//Función que genera el array de recetas
function GenerarArray()
{
    let recetas = [
        {
            titulo: "spaghetti carbonara",
            imagen: "img/espaguetis-carbonara.jpg",
            resumen: "Los espaguetis a la carbonara es probablemente la forma más internacional de preparar esta pasta. La auténtica salsa carbonara de italia contiene yema de huevo, queso y bacon. No tiene nata, ingrediente que le solemos añadir en España.",
            preparacion: 5,
            coccion: 12,
            dificultad: "baja",
            ingredientes: ["spaghettis","nata","queso","sal","pimienta","mantequilla"]
        },
        {
            titulo: "tortilla española",
            imagen: "img/tortillaespanola.jpg",
            resumen: "La tradicional receta de tortilla de patatas o tortilla española, un plato básico de la cocina española a base de patatas, huevo y cebolla.",
            preparacion: 10,
            coccion: 30,
            dificultad: "media",
            ingredientes: ["patatas","huevos","cebolla","aceite","sal"]
        },
        {
            titulo: "ensaladilla rusa",
            imagen: "img/ensaladilla-rusa.jpg",
            resumen: "La tradicional receta de ensaladilla rusa con patata, zanahoria, huevo y atún con mayonesa.",
            preparacion: 10,
            coccion: 20,
            dificultad: "baja",
            ingredientes: ["patatas","huevos","mayonesa","sal"]
        },
        {
            titulo: "carne en salsa",
            imagen: "img/maxresdefault.jpg",
            resumen: "Hoy quiero enseñaros a preparar la mejor receta de carne en salsa, para disfrutar de su textura melosa y tierna y de su sabor tradicional.",
            preparacion: 15,
            coccion: 35,
            dificultad: "media",
            ingredientes: ["carne","tomate","sal","aceite"]
        }
    ]

    return recetas;
}
//Comparador de recetas por título
function compare( a, b ) 
{
    if ( a.titulo < b.titulo ){
      return -1;
    }
    if ( a.titulo > b.titulo ){
      return 1;
    }
    return 0;
  }
  //Función que genera las recetas siguiendo el filtro especificado
  function GenerarReceta(recetasfiltradas,i)
  {
    let recetas = GenerarArray();

    let elem = document.getElementsByClassName("row");
    let div1 = elem[0];
    let div2 = document.createElement("div");
    
    if (i%2 == 0)
    {
        div2.setAttribute("class","col-sm-6 p-2");
    }
    else
     {
         div2.setAttribute("class","col-sm-5 p-2");
     }
     
    let h2 = document.createElement("h2");
    h2.innerText = recetasfiltradas[i].titulo;
    let img = document.createElement("img");
    img.setAttribute("src",recetasfiltradas[i].imagen);
    img.setAttribute("class","imgtext");
    img.setAttribute("alt","");
    let p = document.createElement("p");
    p.innerHTML = recetasfiltradas[i].resumen;
    p.setAttribute("class","recipetext");
    div2.appendChild(h2);
    div2.appendChild(img);
    div2.appendChild(p);
    div1.appendChild(div2);
  }

//Función que muestra todas las recetas al cargar la página
function GenerarRecetas()
    {
        //Array de recetas
        let recetas = GenerarArray();
        recetas.sort(compare);
        //Creación de las recetas
        for (let i = 0; i < recetas.length; i++)
        {
            let elem = document.getElementsByClassName("row");
            let div1 = elem[0];
            let div2 = document.createElement("div");
            if (i%2 == 0)
            {
                div2.setAttribute("class","col-sm-6 p-2");
            }
            else
            {
                div2.setAttribute("class","col-sm-5 p-2");
            }
            let h2 = document.createElement("h2");
            h2.innerText = recetas[i].titulo;
            let img = document.createElement("img");
            img.setAttribute("src",recetas[i].imagen);
            img.setAttribute("class","imgtext");
            img.setAttribute("alt","");
            let p = document.createElement("p");
            p.innerHTML = recetas[i].resumen;
            p.setAttribute("class","recipetext");
            div2.appendChild(h2);
            div2.appendChild(img);
            div2.appendChild(p);
            div1.appendChild(div2);
        }
    }

    window.addEventListener("load",GenerarRecetas());

    let buscar = document.getElementById("buscar");
    /*Función que borra todas las recetas de la pantalla y muestra las
    que cumplan con el cliterio de búsqueda*/
    buscar.addEventListener("click",function()
    {
        let buscador = document.getElementById("buscador");
        BorrarRecetas();

        let recetas = GenerarArray();
        let recetasfiltradas = new Array();
        let textobuscar = document.getElementById("buscador").value;
       let display = document.getElementById("dificultad");
       let eleccion = display.options[display.selectedIndex].innerText;
       let tiempo = document.getElementById("tiempo").value;

       if (textobuscar != "")
       {
            if (tiempo != "")
            {
                if (eleccion != "cualquiera")
                {
                    FiltroTDT(recetas,textobuscar,tiempo,eleccion,recetasfiltradas);
                }
                else
                {
                    FiltroTT(recetas,textobuscar,tiempo,recetasfiltradas);
                }
            }
            else
            {
                if (eleccion != "cualquiera")
                {
                    FiltroTD(recetas,textobuscar,eleccion,recetasfiltradas);
                }
                else
                {
                    FiltroT(recetas,textobuscar,recetasfiltradas);
                }        
            }
       }
       else
       {
            if (tiempo != "")
            {
                if (eleccion != "cualquiera")
                {
                    FiltroDT(recetas,tiempo,eleccion,recetasfiltradas);
                }
                else
                {
                    FiltroT(recetas,tiempo,recetasfiltradas);
                }
            }
            else
            {
                if (eleccion != "cualquiera")
                {
                    FiltroD(recetas,eleccion,recetasfiltradas);
                }
                else
                {
                    GenerarRecetas();
                }
            }
       }
    });

    let mostrar = document.getElementById("mostrar");
    let ocultar = document.getElementById("ocultar");
    let filtrado = document.getElementById("filtrado");
    let borrar = document.getElementById("borrar");
    //Función que muestra las opciones de búsqueda
    mostrar.addEventListener("click", function()
    {
        mostrar.style.display = "none";
        ocultar.style.display = "inline";
        filtrado.style.display = "inline";
        
    })
    //Función que oculta las opciones de búsqueda
    ocultar.addEventListener("click",function()
    {
        ocultar.style.display = "none";
        mostrar.style.display = "inline";
        filtrado.style.display = "none";
    })
    borrar.addEventListener("click",function()
    {
        let textobuscar = document.getElementById("buscador");
       let display = document.getElementById("dificultad");
       let eleccion = display.options[display.selectedIndex].innerText;
       let tiempo = document.getElementById("tiempo");

       textobuscar.value = "";
       display.value = "cualquiera";
       tiempo.value = "";

       BorrarRecetas();
       GenerarRecetas();
    })

