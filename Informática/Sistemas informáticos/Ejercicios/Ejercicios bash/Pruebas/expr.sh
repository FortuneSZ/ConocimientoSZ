#!/bin/bash

cadena="Hola mundo"
longitud=$(expr length "$cadena")

echo "La cadena $cadena tiene una longitud de $longitud caracteres."

cadena="123456789"
subcadena=$(expr substr "$cadena" 6 2)
echo $cadena
echo "la subcadena desde el caracter 6 y longitud 2 es : $subcadena"
