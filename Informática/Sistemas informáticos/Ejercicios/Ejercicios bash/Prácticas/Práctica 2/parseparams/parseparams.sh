#!/bin/bash
numero=0
fichero=""
texto=""

listaParam=($@)
num=${#listaParam[@]}

for (( i=1; i<$num; i+=2 ))
do
    if [[ ${listaParam[i]} =~ ^[0-9]+$ &&  ${listaParam[i]} -ge 1  &&  ${listaParam[i]} -le 1000  ]]
    then
        numero=${listaParam[i]}
    elif [[ -f ${listaParam[i]} ]]
    then
        fichero=${listaParam[i]}
    elif [[ [[${listaParam[i]}]] ]]
    then
        texto=${listaParam[i]}
    fi
done

echo "$numero"
echo "$fichero"
echo "$texto"

