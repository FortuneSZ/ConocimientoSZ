#!/bin/bash

# Inicializar el tablero vacío
tablero=( "." "." "." "." "." "." "." "." "." )

# Función para mostrar el tablero
function mostrar_tablero {
  echo " ${tablero[0]} | ${tablero[1]} | ${tablero[2]} "
  echo "---+---+---"
  echo " ${tablero[3]} | ${tablero[4]} | ${tablero[5]} "
  echo "---+---+---"
  echo " ${tablero[6]} | ${tablero[7]} | ${tablero[8]} "
}

# Función para comprobar si hay tres en raya
function hay_tres_en_raya {
  local jugador=$1
  if [[ ${tablero[0]} == $jugador && ${tablero[1]} == $jugador && ${tablero[2]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[3]} == $jugador && ${tablero[4]} == $jugador && ${tablero[5]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[6]} == $jugador && ${tablero[7]} == $jugador && ${tablero[8]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[0]} == $jugador && ${tablero[3]} == $jugador && ${tablero[6]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[1]} == $jugador && ${tablero[4]} == $jugador && ${tablero[7]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[2]} == $jugador && ${tablero[5]} == $jugador && ${tablero[8]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[0]} == $jugador && ${tablero[4]} == $jugador && ${tablero[8]} == $jugador ]]; then
    return 0
  elif [[ ${tablero[2]} == $jugador && ${tablero[4]} == $jugador && ${tablero[6]} == $jugador ]]; then
    return 0
  else
    return 1
  fi
}

# Bucle del juego
while true; do
  # Turno del jugador 1
  echo "Turno del jugador 1 (X)"
  mostrar_tablero
  read -p "Introduce la posición (1-9): " pos
  if [[ ${tablero[$pos-1]} == "." ]]; then
    tablero[$pos-1]="X"
  else
    echo "La posición está ocupada"
    continue
  fi
  if hay_tres_en_raya "X"; then
    mostrar_tablero
    echo "¡Tres en raya! Jugador 1 gana"
    exit 0
  fi

  # Turno del jugador 2
  echo "Turno del jugador 2 (O)"
  mostrar_tablero
  read -p "Introduce la posición (1-9): " pos
  if [[ ${tablero[$pos-1]} == "." ]]; then
    tablero[$pos-1]="O"
  else
    echo "La posición está ocupada"
    continue
  fi
  if ! [[ "${tablero[*]}" =~ "." ]]; then
mostrar_tablero
echo "¡Empate!"
exit 0
fi
done