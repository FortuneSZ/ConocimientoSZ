#!/bin/bash

# requiere instalar el comando bc
# $ sudo apt install bc

# Parámetros
#
# USO: divisas.sh importe mon
#
for fichero in $@
do
  if [[ -f $fichero ]]
  then
    fichero_divisas=$fichero
  else
    fichero_divisas=divisas.csv
  fi
done
importe_divisa=$1
moneda=$2

# comprobar que está instalado el comando bc
if [[ ! $(which bc) ]]
then
  echo "Este script requiere instalar el comando bc."
  echo "Ejecuta: sudo apt install bc"
  echo
  exit 1
fi

# comprobar número de parámetros
if [[ $# -le 2 ]]
then
  echo "Error: El número de argumentos debe ser 2."
  echo "Uso: $0 <importe> <divisa>"
  echo
  exit 2
fi

# comprobar el tipo de los parámetros

# El importe de la divisa debe ser un número entero o real
if [[ ! $importe_divisa =~ ^[+-]?[0-9]*\.?[0-9]+$ ]]
then
  echo "Error: El importe de la divisa debe ser un número real."
  echo
  exit 4
fi

# La moneda debe ser una cadena de 3 caracteres según el estándar ISO-4217
# https://es.wikipedia.org/wiki/ISO_4217
if [[ ! $moneda =~ ^[A-Z][A-Z][A-Z]$ ]]
then
  echo "Error: Formato de moneda incorrecto."
  echo
  exit 5
fi


# comprobar que existe el fichero con los cambios de divisa
if [[ ! -f $fichero_divisas ]] 
then
  echo "Error: No exite el fichero de divisas $fichero_divisas."
  echo
  exit 3
fi


# Obtener la linea que contiene la moneda
LINEA=$(cat $fichero_divisas | grep "$moneda")
#echo $LINEA

# Extraer el tipo de cambio
cambio=$(echo $LINEA | cut -d ";" -f2)
#echo "Linea2 = $cambio"

importe_euros=$(echo $importe_divisa*$cambio | bc -l)

echo "$importe_divisa $moneda = $importe_euros EUR"

exit 0

