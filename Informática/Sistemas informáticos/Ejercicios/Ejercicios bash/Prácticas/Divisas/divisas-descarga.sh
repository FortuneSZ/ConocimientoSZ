if [[ ! -f eurofxref-daily.xml ]]
then
    echo $(wget https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml)
fi

fichero=eurofxref-daily.xml
while read linea
do
if [[ $(grep currency $linea) -eq 0 ]]
then
    divisa=$((cut 9-23 $linea))
    echo $divisa
fi
done