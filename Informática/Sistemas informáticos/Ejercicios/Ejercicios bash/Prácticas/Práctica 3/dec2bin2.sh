#!/bin/bash

# Función para convertir un entero positivo a binario
# Recibe: Entero positivo
# Devuelve: Número convertido a binario
#
function dec2bin() {
  local numero=$1

cociente=$((numero/2))
resto=$((numero%2))

  if [[ $cociente -ge 2 ]]
  then 
    result=$(dec2bin $cociente)
    echo $result$resto
  elif [[ $cociente -eq 0 ]]
  then
    echo $resto
  else
    echo $cociente$resto
  fi

  echo $numero_binario
}

# El script recibe como parámetro el número entero y 
# positvo a convertir
for var in $@
do
  echo $(dec2bin $var)
done
