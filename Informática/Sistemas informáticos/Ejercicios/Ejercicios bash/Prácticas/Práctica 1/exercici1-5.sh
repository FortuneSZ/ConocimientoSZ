#!/bin/bash

ficheroValores=$1

if [[ ! -s $ficheroValores ]]
then
    echo "0"
    exit 1
fi

menor=0
mayor=0
par=0
impar=0

while read Linea
do
    sumaValores=$((sumaValores + Linea))
    numValores=$((numValores + 1))

if [[ $Linea -ge $mayor ]]
then
   	mayor=$Linea
fi
   	 
if [[ $Linea -le $menor ]]
then
   	menor=$Linea
   	echo $menor
fi

if [[ $(($Linea%2)) -eq 0 ]]
then
   	par=$(($par+1))
else
   	impar=$(($impar+1))
fi
done < $ficheroValores

echo "Numero de valores:" $numValores
echo "Suma de todos los valores:" $sumaValores
echo "El menor valor es:" $menor
echo "El mayor valor es:" $mayor
echo "El número de valores pares es:" $par
echo "El número de valores impares es:" $impar
