#!/bin/bash

read 1 2

valor1=$1
valor2=$2

if [[ "$#" != "2" ]]
then 
    echo "El número de parámetros debe ser dos"
else
    if [[ $valor1 > 0 ]]
    then
        echo "$valor1 es mayor que 0"
    else
        echo "valor1 es menor que 0"
    fi

    if [[ $valor1 < 100 || $valor1 = 100 ]]
    then 
        echo "$valor1 es menor o igual a 100"
    else
        echo "valor1 no es menor o igual a 100"
    fi

    if [[ $valor2 > 10 ]]
    then 
        echo "$valor2 es mayor a 10"
    else    
        echo "valor2 es menor a 10"
    fi

    if [[ $valor2 < 50 || $valor2 = 50 ]]
    then
        echo "$valor2 es menor o igual a 50"
    else
        echo "valor2 no es menor o igual a 50"
    fi

    if [[ $valor1 == $valor2 ]]
    then
        echo "$valor1 es igual a $valor2"
    else
        echo "valor1 no es igual a valor2"
    fi

    if [[ $valor1 > $valor2 ]]
    then
        echo "$valor1 es mayor a $valor2"
    else
        echo "valor1 no es mayor a valor2"
    fi

    if [[ $valor1 < $valor2 ]]
    then
        echo "$valor1 es menor a $valor2"
    else
        echo "valor1 no es menor a valor2"
    fi
fi
