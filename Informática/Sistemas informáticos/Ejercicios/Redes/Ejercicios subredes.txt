IP                192.10.10.0
Máscara de subred 255.255.255.240

Subred 1          192.10.10.0(192.10.10.0000xxxx)
broadcast         192.10.10.15
primer host       192.10.10.1
ultimo host       192.10.10.14

Subred 2          192.10.10.16(192.10.10.0001xxxx)
broadcast         192.10.10.31
primer host       192.10.10.17
ultimo host       192.10.10.30

Subred 3          192.10.10.32(192.10.10.0010xxxx)
broadcast         192.10.10.47
primer host       192.10.10.33
ultimo host       192.10.10.46

