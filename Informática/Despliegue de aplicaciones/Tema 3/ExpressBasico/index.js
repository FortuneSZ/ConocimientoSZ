const express = require('express');
const os = require('os');
let app = express();
app.listen(8080);

app.get('/fecha', (req, res) => {
    let fecha = new Date();
    let fechaString = 'Day: ' + fecha.getDate() + ", " + fecha.getHours()+":" + fecha.getMinutes() + " hours";
    res.send(fechaString);
});

app.get('/usuario', (req, res) => {
    res.send("Tu usuario es: " + os.userInfo().username);
});
