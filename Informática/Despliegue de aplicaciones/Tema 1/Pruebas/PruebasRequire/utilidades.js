console.log('Entrando en utilidades.js');

let sumar = (num1, num2) => num1 + num2;
let restar = (num1, num2) => num1 - num2;

module.exports = {
    pi: 3.1416,
    sumar: sumar,
    restar: restar
};