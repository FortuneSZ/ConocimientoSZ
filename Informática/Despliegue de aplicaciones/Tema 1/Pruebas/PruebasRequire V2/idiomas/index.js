const en = require('./en');
const es = require('./es');

module.exports = {
    es : es,
    en : en
};