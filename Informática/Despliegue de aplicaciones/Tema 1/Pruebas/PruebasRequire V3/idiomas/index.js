const en = require(__dirname + '/en');
const es = require(__dirname + '/es');

module.exports = {
    es : es,
    en : en
};