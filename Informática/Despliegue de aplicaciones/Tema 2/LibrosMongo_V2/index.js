const prompt = require('prompt-sync')();
const mongoose = require('mongoose');
const path = require('path');

const Libro = require(path.join(__dirname, "models/libro"));
const Autor = require(path.join(__dirname, "models/autor"));

mongoose.connect('mongodb://127.0.0.1:27017/libros', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(async () => {
    console.log("Conectado a MongoDB");

    async function agregarAutor() {
        const nombre = prompt('¿Cuál es el nombre del autor? ');
        const año = parseInt(prompt('¿Cuál es el año de nacimiento? '), 10);

        let autor = new Autor({ nombre, año });

        try {
            const resultado = await autor.save();
            console.log('Autor añadido:', resultado);
        } catch (error) {
            console.error("Error añadiendo el autor:", error.message);
        }
    }

    async function agregarLibro() {
        const titulo = prompt('¿Cuál es el título? ');
        const editorial = prompt('¿Cuál es la editorial? ');
        const precio = parseFloat(prompt('¿Cuál es el precio? '));
        const hayAutor = prompt('¿Se conoce el autor? (si/no) ').toLowerCase();

        let libro;

        if (hayAutor === "si") {
            const nombreAutor = prompt('¿Cuál es el nombre del autor? ');
            const autorEncontrado = await Autor.findOne({ nombre: nombreAutor });

            if (autorEncontrado) {
                libro = new Libro({
                    titulo,
                    editorial,
                    precio,
                    autor: autorEncontrado._id
                });
            } else {
                console.log("❌ Autor no encontrado. Creando libro sin autor...");
                libro = new Libro({ titulo, editorial, precio });
            }
        } else {
            libro = new Libro({ titulo, editorial, precio });
        }

        try {
            const resultado = await libro.save();
            console.log('✅ Libro añadido:', resultado);
        } catch (error) {
            console.error("Error añadiendo el libro:", error.message);
        }
    }

    // Ejecución de funciones
    await agregarLibro(); // Puedes descomentar para agregar autores también
    // await agregarAutor();

    mongoose.connection.close(); // Cerrar conexión cuando termine

}).catch(err => {
    console.error('❌ Error de conexión:', err);
});