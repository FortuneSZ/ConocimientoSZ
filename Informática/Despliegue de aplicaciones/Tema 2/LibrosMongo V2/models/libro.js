const mongoose = require('mongoose');

let libroSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true,
        trim: true
    },
    editorial: {
        type: String,
        required: false,
        trim: true
    },
    precio : {
        type: Number,
        required: true,
        min: 0
    }
});

let Libro = mongoose.model('libros', libroSchema);
module.exports = Libro;