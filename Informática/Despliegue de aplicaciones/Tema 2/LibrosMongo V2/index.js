const mongoose = require('mongoose');
const Libro = require('./models/libro');
const Contacto = require(__dirname + "/models/libro");

mongoose.connect('mongodb://127.0.0.1:27017/libros');

    let Libro1 = new Libro({
        titulo: "El capitán Alatriste",
        editorial: "Alfaguara", 
        precio: 15 
    });
    Libro1.save().then(resultado => {
        console.log("Contacto añadido:", resultado);
    }).catch(error => {
        console.log("ERROR añadiendo contacto:", error);
    });

    let Libro2 = new Libro({
        titulo: "El juego de Ender",
        editorial: "Ediciones B", 
        precio: 8.95 
    });
    Libro2.save().then(resultado => {
        console.log("Contacto añadido:", resultado);
    }).catch(error => {
        console.log("ERROR añadiendo contacto:", error);
    });