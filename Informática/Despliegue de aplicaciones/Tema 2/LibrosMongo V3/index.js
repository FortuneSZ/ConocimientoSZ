const mongoose = require('mongoose');
const Libro = require(__dirname + "/models/libro");

mongoose.connect('mongodb://127.0.0.1:27017/libros');

Libro.find({precio: {$gte: 10, $lte: 20}})
.then(resultado => {
    console.log('Resultado de la búsqueda:', resultado);
})
.catch(error => {
    console.log('ERROR:', error);
});

Libro.findById('6746a3ba1600463c9bf38fe7')
.then(resultado => {
    console.log('Resultado de la búsqueda por ID:', resultado);
})
.catch(error => {
    console.log('ERROR:', error);
});