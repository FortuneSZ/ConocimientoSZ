const mongoose = require('mongoose');
const Libro = require(__dirname + "/models/libro");

mongoose.connect('mongodb://127.0.0.1:27017/libros');

Libro.findByIdAndDelete('6746a395ffa7d9baf08f0eac')
.then(resultado => {
    console.log("libro eliminado:", resultado);
}).catch (error => {
    console.log("ERROR:", error);
});

Libro.findByIdAndUpdate('6746a3ba1600463c9bf38fe7', 
    {$set: {precio: 15 },
    $inc: {__v: 1}}, {new:true})
.then(resultado => {
    console.log("Modificado contacto:", resultado);
}).catch (error => {
    console.log("ERROR:", error);
});