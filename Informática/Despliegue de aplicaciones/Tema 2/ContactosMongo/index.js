const mongoose = require('mongoose');
const Contacto = require(__dirname + "/models/contacto");

mongoose.connect('mongodb://127.0.0.1:27017/contactos',
    { useNewUrlParser: true, useUnifiedTopology: true });